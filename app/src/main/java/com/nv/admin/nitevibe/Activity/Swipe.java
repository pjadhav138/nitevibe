package com.nv.admin.nitevibe.Activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nv.admin.nitevibe.Adapter.Mainprofile;
import com.nv.admin.nitevibe.Adapter.Mainprofilestack;
import com.nv.admin.nitevibe.Adapter.Subprofile;
import com.nv.admin.nitevibe.OverlayTutorial.TutoShowcase;
import com.nv.admin.nitevibe.R;
import com.nv.admin.nitevibe.custom.CardStackView;
import com.nv.admin.nitevibe.custom.SwipeDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Swipe extends AppCompatActivity {
    public static final String PREFS_NAME = "LoginPrefs";
    public static CardStackView cardStackView;
    public static List<Mainprofile> profiles;
    public static List<Subprofile> subimages;
    public static String loginUserId, loginUserMode, loginUserType, eventId, url, supervibescount, url11;
    public static Typeface bold_face, extra_bold_face, reg_face, semi_bold_face, calibri_bold, lato_bold, semi_bold_italic;
    public static RelativeLayout emptyRelative;
    public static TextView empty_txt;
    public static ImageView empty_img;
    public static String eventValue, fromValue;
    public static ImageView img_back;
    static String user_name;
    static String user_loc;
    static Mainprofilestack adapter;
    static JSONArray stack_jsonarray;
    public PopupWindow popupWindow;
    public RelativeLayout mRelativeLayout;
    SharedPreferences sharedpreferences;
    private ProgressBar progressBar;

    public static void paginate() {

        cardStackView.setPaginationReserved();
        adapter.addAll(createprofilestacks());
        adapter.notifyDataSetChanged();
    }

    public static void removeFirst() {
        LinkedList<Mainprofile> mainprofiles = extractRemainingProfiles();
        if (mainprofiles.isEmpty()) {
            return;
        }

        mainprofiles.removeFirst();
        adapter.clear();
        adapter.addAll(mainprofiles);
        adapter.notifyDataSetChanged();
    }

    public static List<Mainprofile> createprofilestacks() {

        subimages = new ArrayList<Subprofile>();
        profiles = new ArrayList<>();
        subimages.clear();
        profiles.clear();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("swipe_card_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if (res.equals("200")) {
                        // JSONArray jsonArray = object.getJSONArray("message");
                        try {
                            stack_jsonarray = object.getJSONArray("message");
                        } catch (Exception e) {
                            e.printStackTrace();
                            stack_jsonarray = new JSONArray();
                        }
                        Log.d("length_of_card", String.valueOf(stack_jsonarray.length()));

                        if (stack_jsonarray.length() == 0) {
                            emptyRelative.setVisibility(View.VISIBLE);
                        } else {


                            emptyRelative.setVisibility(View.GONE);

                            for (int i = 0; i < stack_jsonarray.length(); i++) {
                                Mainprofile mainprofile = new Mainprofile();
                                JSONObject jsonObject = stack_jsonarray.getJSONObject(i);
                                String userid = jsonObject.getString("user_id");
                                mainprofile.setM_id(jsonObject.getString("user_id"));
                                user_name = jsonObject.getString("user_name");
                                mainprofile.setName(jsonObject.getString("user_name"));
                                mainprofile.setFrom(fromValue);

                                String user_age = jsonObject.getString("user_age");
                                mainprofile.setAge(jsonObject.getString("user_age"));
                                user_loc = jsonObject.getString("user_loc");
                                mainprofile.setCity(jsonObject.getString("user_loc"));

                                String user_occupation = jsonObject.getString("user_occupation");
                                mainprofile.setJob(jsonObject.getString("user_occupation"));
                                String user_bio = jsonObject.getString("user_bio");
                                String superlikes = jsonObject.getString("superlikes");
                                mainprofile.setSupervibes(jsonObject.getString("superlikes"));
                                mainprofile.setProfile_url(jsonObject.getString("profile_url"));

                                if (superlikes.equals("3")) {
                                    cardStackView.setSwipeDirection(SwipeDirection.HORIZONTAL);
                                }

                                String user_drink = jsonObject.getString("user_drink");
                                JSONArray photoarray = jsonObject.getJSONArray("user_photos");
                                for (int j = 0; j < photoarray.length(); j++) {
                                    Subprofile subprofile = new Subprofile();
                                    subprofile.setImg(photoarray.getJSONObject(j).getString("img"));
                                    subprofile.setImg_id(photoarray.getJSONObject(j).getString("id"));
                                    subimages.add(subprofile);
                                    mainprofile.setSubimage(subimages);
                                }
                                JSONArray eventarray = jsonObject.getJSONArray("user_event");
                                for (int j = 0; j < eventarray.length(); j++) {
                                    JSONObject jsonObject1 = eventarray.getJSONObject(j);
                                    String event_id = jsonObject1.getString("event_id");
                                    String event_name = jsonObject1.getString("event_name");
                                    String event_date = jsonObject1.getString("event_date");
                                }
                                if (!subimages.get(0).equals("")) {
                                    mainprofile.setUrl(subimages.get(0));
                                    profiles.add(mainprofile);
                                }

                            }


                            adapter.clear();
                            adapter.addAll(profiles);
                            cardStackView.setAdapter(adapter);
                        }


                    } else {
                        //Toast.makeText(Swipe.this,object.getString("message"),Toast.LENGTH_LONG).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    // Toast.makeText(Swipe.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                // Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
        return profiles;

    }

    public static LinkedList<Mainprofile> extractRemainingProfiles() {
        LinkedList<Mainprofile> profiles = new LinkedList<Mainprofile>();
        try {
            for (int i = cardStackView.getTopIndex(); i < profiles.size(); i++) {
                profiles.add(adapter.getItem(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return profiles;
    }

    public static void swipeRight() {

        List<Mainprofile> spots = extractRemainingProfiles();
        if (spots.isEmpty()) {
            return;
        }

        View target = cardStackView.getTopView();
        View targetOverlay = cardStackView.getTopView().getOverlayContainer();

        ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("rotation", 10f));
        rotation.setDuration(200);
        ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationX", 0f, 2000f));
        ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
        translateX.setStartDelay(100);
        translateY.setStartDelay(100);
        translateX.setDuration(500);
        translateY.setDuration(500);
        AnimatorSet cardAnimationSet = new AnimatorSet();
        cardAnimationSet.playTogether(rotation, translateX, translateY);

        ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 0f, 1f);
        overlayAnimator.setDuration(200);
        AnimatorSet overlayAnimationSet = new AnimatorSet();
        overlayAnimationSet.playTogether(overlayAnimator);

        cardStackView.swipe(SwipeDirection.Right, cardAnimationSet, overlayAnimationSet);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe);
        sharedpreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");


        String swipeOverlay = sharedpreferences.getString("swipeoverlay", "");
        if (swipeOverlay.equals("")) {
            displayTuto();
        }

        //value comming from EventAdapter/clubEventDetails
        eventId = sharedpreferences.getString("event_id", "");
        // URL = "http://rednwhite.co.in/webservices/swipe_card.php?user_id=22&event_id=3";

        bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold.ttf");
        extra_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        reg_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        semi_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBold.ttf");
        calibri_bold = Typeface.createFromAsset(getAssets(), "fonts/Calibri Bold.ttf");
        lato_bold = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");
        semi_bold_italic = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBoldItalic.ttf");

        mRelativeLayout = findViewById(R.id.rel0);
        emptyRelative = findViewById(R.id.empty_cart);
        empty_txt = findViewById(R.id.find_txt);
        empty_img = findViewById(R.id.empty_img);
        img_back = findViewById(R.id.back);

        empty_txt.setTypeface(semi_bold_italic);
        //  url=AllUrl.SWIPE_CARD+loginUserId+"&event_id="+eventId;
        //Log.d("swipe_card_url",url);

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            //values coming from Clubsidehome
            eventValue = extra.getString("event_id");
            fromValue = extra.getString("val_from");


            if (fromValue.equals("going")) {
                url = AllUrl.IAM_GOING_SWIPE + loginUserId + "&event_id=" + eventValue;

            } else if (fromValue.equals("checkin")) {
                url = AllUrl.CHECKIN_SWIPE + loginUserId + "&event_id=" + eventValue;
            }


        }
        Log.d("swipe_card_url", url);

        empty_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // startActivity(new Intent(Swipe.this,Home.class));
                startActivity(new Intent(Swipe.this, Club.class));
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fromValue.equals("going")) {
                    Intent i = new Intent(Swipe.this, Events.class);
                    startActivity(i);

                } else if (fromValue.equals("checkin")) {
                    Intent i = new Intent(Swipe.this, ClubEventDetails.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(Swipe.this, Club.class);
                    startActivity(i);
                }

            }
        });


        setup();
    }

    protected void displayTuto() {
        TutoShowcase.from(this)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        displayTuto1();
                        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("swipeoverlay", "1");
                        editor.commit();

                    }
                })
                .setContentView(R.layout.swipe_overlay)
                .setFitsSystemWindows(true)
                .on(R.id.find_txt)
                .displaySwipableLeft()
                .delayed(399)
                .animated(true)
                .show();
    }

    protected void displayTuto1() {
        TutoShowcase.from(this)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {

                        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("swipeoverlay", "1");
                        editor.commit();
                    }
                })
                .setContentView(R.layout.swipe1_overlay)
                .setFitsSystemWindows(true)
                .on(R.id.find_txt)
                .displaySwipableLeft()
                .delayed(399)
                .animated(true)
                .show();
    }

    private Mainprofilestack createMainprofilestackAdapter() {
        final Mainprofilestack adapter = new Mainprofilestack(getApplicationContext());
        adapter.addAll(createprofilestacks());
        // adapter.addAll(createprofilestacks_new());

        return adapter;
    }

    public void setup() {
        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();
            }
        });

        Log.d("cardstack_setup", "setup() called");
        progressBar = findViewById(R.id.activity_main_progress_bar);
        cardStackView = findViewById(R.id.activity_main_card_stack_view);
        adapter = new Mainprofilestack(getApplicationContext());
        cardStackView.setAdapter(createMainprofilestackAdapter());
        //  cardStackView.setSwipeEnabled(false);
        cardStackView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        //  cardStackView.setSwipeDirection(SwipeDirection.HORIZONTAL);
        cardStackView.setCardEventListener(new CardStackView.CardEventListener() {

            @Override
            public void onCardDragging(float percentX, float percentY) {
                Log.d("CardStackView", "onCardDragging");
                //  Log.d("CardStackView", "topIndex: " + cardStackView.getTopIndex());
                Log.d("value_of_card_draging", percentX + " " + percentY);


            }

            @Override
            public void onCardSwiped(SwipeDirection direction) {
                Log.d("CardStackView", "onCardSwiped: " + direction.toString());
                MyApplication.getInstance().trackEvent("Profile Swipe", "Swipe", direction.toString());


                try {
                    Log.d("CardStackView", "topIndex: " + cardStackView.getTopIndex());

                    //     Log.d("adapter_top_index", String.valueOf(adapter.getItem(cardStackView.getTopIndex())));

                    //String abc=profiles.get(cardStackView.getTopIndex()-1).m_id;
                    String abc = adapter.getItem(cardStackView.getTopIndex() - 1).m_id;
                    Log.d("swipe_user_id", abc);
                    String m1 = String.valueOf(stack_jsonarray.length());
                    String m2 = String.valueOf(cardStackView.getTopIndex());

                    Log.d("swipe_length_value", m1 + " card:" + m2);
                    // if(cardStackView.getTopIndex()==stack_jsonarray.length()+1){
                    if (cardStackView.getTopIndex() == stack_jsonarray.length()) {
                        // emptyRelative.setVisibility(View.VISIBLE);
                        emptyRelative.setVisibility(View.VISIBLE);
                        if (direction.toString().equals("Top")) {
                            url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=3&event_id=" + eventId;
                            boolean flag = hasConnection();
                            if (flag) {
                                insertSwipeData(url11, "vibes"); //vibes

                            } else {
                                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                            }
                        } else if (direction.toString().equals("Left")) {
                            url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=2&event_id=" + eventId;
                            boolean flag = hasConnection();
                            if (flag) {
                                insertSwipeData(url11, "");


                            } else {
                                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                            }
                        } else if (direction.toString().equals("Right")) {
                            url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=1&event_id=" + eventId;
                            boolean flag = hasConnection();
                            if (flag) {
                                insertSwipeData(url11, "");

                            } else {
                                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                            }
                        }
                        Log.d("swipe_url", url11);
                    } else {
                        emptyRelative.setVisibility(View.GONE);
                        if (direction.toString().equals("Top")) {
                            url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=3&event_id=" + eventId;
                            boolean flag = hasConnection();
                            if (flag) {
                                insertSwipeData(url11, "vibes"); //vibes

                            } else {
                                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                            }
                        } else if (direction.toString().equals("Left")) {
                            url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=2&event_id=" + eventId;
                            boolean flag = hasConnection();
                            if (flag) {
                                insertSwipeData(url11, "");

                            } else {
                                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                            }
                        } else if (direction.toString().equals("Right")) {
                            url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=1&event_id=" + eventId;
                            boolean flag = hasConnection();
                            if (flag) {
                                insertSwipeData(url11, "");


                            } else {
                                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                            }
                        }
                        Log.d("swipe_url", url11);

                    }


                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onCardReversed() {
                Log.d("CardStackView", "onCardReversed");
            }

            @Override
            public void onCardMovedToOrigin() {
                Log.d("CardStackView", "onCardMovedToOrigin");
            }

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onCardClicked(int index) {
                Log.d("CardStackView", "onCardClicked: " + index + "name : " + profiles.get(index).name);
            }
        });

    }

    private void insertSwipeData(String url123, final String vibes) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url123, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("swipe_userdataresponse", response.toString());
                //  loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {
                        MyApplication.getInstance().trackEvent("Profile Swipe", "Swipe", "Profile Added");
                        JSONArray array = object.getJSONArray("message");
                        if (array.length() == 0) {

                        } else {
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                String user_id = object1.getString("user_id");
                                String user_photo = object1.getString("user_photo");
                                String match_id = object1.getString("match_id");
                                String match_photo = object1.getString("match_photo");

                                String url = AllUrl.MATCH_NOTIFICATION + loginUserId + "&match_id=" + match_id;
                                matchNotification(url);

                                initiatePopupWindow(user_id, user_photo, match_id, match_photo);
                            }
                        }
                        if (vibes.equals("vibes")) {
                            String url = AllUrl.SUPERVIBE_COUNT + loginUserId + "&event_id=" + eventId;
                            userSuperVibes(url);
                        }
                        //  createprofilestacks();


                    } else {
                        Toast.makeText(Swipe.this, object.getString("message"), Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                // loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void matchNotification(String url) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("matchnotify_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {
                        Log.d("notify_msg", "sucess");

                    } else {
                        // Toast.makeText(ServiceNoDelay.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    //  Toast.makeText(ServiceNoDelay.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                // Toast.makeText(ServiceNoDelay.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog


            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void initiatePopupWindow(String userId, String userPhoto, String matchId, String matchPhoto) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        // Inflate the custom layout/view
        View customView = inflater.inflate(R.layout.profile_match_row, null);


        popupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        );

        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            popupWindow.setElevation(5.0f);
        }

        // Get a reference for the custom view close button

        // RoundedImageView userImg=(RoundedImageView)customView.findViewById(R.id.girl_profile);
        //  RoundedImageView matchImg=(RoundedImageView)customView.findViewById(R.id.boy_profile);

        CircleImageView userImg = customView.findViewById(R.id.girl_profile);
        CircleImageView matchImg = customView.findViewById(R.id.boy_profile);

        TextView chat = customView.findViewById(R.id.chat);
        TextView swipe = customView.findViewById(R.id.swipe);
        TextView txt = customView.findViewById(R.id.txt);
        RoundedImageView profileimg = customView.findViewById(R.id.profile);

        txt.setTypeface(semi_bold_italic);
        swipe.setTypeface(bold_face);
        chat.setTypeface(bold_face);

        Log.d("userPhoto", userPhoto);
        Log.d("match_photo", matchPhoto);


        if (userPhoto.equals("")) {
            Glide.with(getApplicationContext()).load(R.drawable.default_icon).into(userImg);
        } else {


            Glide.with(this).load(userPhoto).into(userImg);
            //  userImg.setImageBitmap(img1);
            //  matchImg.setImageBitmap(img1);
          /*  URL url = null;
            try {
                url = new URL(userPhoto);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                String width= String.valueOf(bmp.getWidth());
                String height= String.valueOf(bmp.getHeight());
                Log.d("size_of_user_image",width+" "+height);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

        }

        if (matchPhoto.equals("")) {
            Glide.with(getApplicationContext()).load(R.drawable.default_icon).into(matchImg);
        } else {
            //   Bitmap img2 = getBitmapFromURL(matchPhoto);

            Glide.with(this).load(matchPhoto).into(matchImg);
           /* URL url = null;
            try {
                url = new URL(matchPhoto);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                String width= String.valueOf(bmp.getWidth());
                String height= String.valueOf(bmp.getHeight());
                Log.d("size_of_match_image",width+" "+height);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

        }

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(Swipe.this,"Chat",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Swipe.this, Chat.class));
            }
        });

        swipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);


    }

    public List<Mainprofile> createprofilestacks_new() {

        subimages = new ArrayList<Subprofile>();
        profiles = new ArrayList<>();
        subimages.clear();
        profiles.clear();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("swipe_card_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if (res.equals("200")) {
                        // JSONArray jsonArray = object.getJSONArray("message");
                        stack_jsonarray = object.getJSONArray("message");
                        Log.d("length_of_card", String.valueOf(stack_jsonarray.length()));

                        if (stack_jsonarray.length() == 0) {
                            emptyRelative.setVisibility(View.VISIBLE);
                        } else {
                            emptyRelative.setVisibility(View.GONE);

                            for (int i = 0; i < stack_jsonarray.length(); i++) {
                                Mainprofile mainprofile = new Mainprofile();
                                JSONObject jsonObject = stack_jsonarray.getJSONObject(i);
                                String userid = jsonObject.getString("user_id");
                                mainprofile.setM_id(jsonObject.getString("user_id"));
                                user_name = jsonObject.getString("user_name");
                                mainprofile.setName(jsonObject.getString("user_name"));
                                mainprofile.setFrom(fromValue);

                                String user_age = jsonObject.getString("user_age");
                                mainprofile.setAge(jsonObject.getString("user_age"));
                                user_loc = jsonObject.getString("user_loc");
                                mainprofile.setCity(jsonObject.getString("user_loc"));

                                String user_occupation = jsonObject.getString("user_occupation");
                                mainprofile.setJob(jsonObject.getString("user_occupation"));
                                String user_bio = jsonObject.getString("user_bio");
                                String superlikes = jsonObject.getString("superlikes");
                                mainprofile.setSupervibes(jsonObject.getString("superlikes"));
                                mainprofile.setProfile_url(jsonObject.getString("profile_url"));

                                if (superlikes.equals("3")) {
                                    cardStackView.setSwipeDirection(SwipeDirection.HORIZONTAL);
                                }

                                String user_drink = jsonObject.getString("user_drink");
                                JSONArray photoarray = jsonObject.getJSONArray("user_photos");
                                for (int j = 0; j < photoarray.length(); j++) {
                                    Subprofile subprofile = new Subprofile();
                                    subprofile.setImg(photoarray.getJSONObject(j).getString("img"));
                                    subprofile.setImg_id(photoarray.getJSONObject(j).getString("id"));
                                    subimages.add(subprofile);
                                    mainprofile.setSubimage(subimages);
                                }
                                JSONArray eventarray = jsonObject.getJSONArray("user_event");
                                for (int j = 0; j < eventarray.length(); j++) {
                                    JSONObject jsonObject1 = eventarray.getJSONObject(j);
                                    String event_id = jsonObject1.getString("event_id");
                                    String event_name = jsonObject1.getString("event_name");
                                    String event_date = jsonObject1.getString("event_date");
                                }
                                if (!subimages.get(0).equals("")) {
                                    mainprofile.setUrl(subimages.get(0));
                                    profiles.add(mainprofile);
                                }

                            }


                            adapter.clear();
                            adapter.addAll(profiles);
                            cardStackView.setAdapter(adapter);
                        }


                    } else {
                        //Toast.makeText(Swipe.this,object.getString("message"),Toast.LENGTH_LONG).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    // Toast.makeText(Swipe.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                // Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();
            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
        return profiles;

    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    public void swipeLeft() {
        List<Mainprofile> profiles = extractRemainingProfiles();
        if (profiles.isEmpty()) {
            return;
        }

        View target = cardStackView.getTopView();
        View targetOverlay = cardStackView.getTopView().getOverlayContainer();

        ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("rotation", -10f));
        rotation.setDuration(200);
        ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationX", 0f, -2000f));
        ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
        translateX.setStartDelay(100);
        translateY.setStartDelay(100);
        translateX.setDuration(500);
        translateY.setDuration(500);
        AnimatorSet cardAnimationSet = new AnimatorSet();
        cardAnimationSet.playTogether(rotation, translateX, translateY);

        ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 0f, 1f);
        overlayAnimator.setDuration(200);
        AnimatorSet overlayAnimationSet = new AnimatorSet();
        overlayAnimationSet.playTogether(overlayAnimator);

        cardStackView.swipe(SwipeDirection.Left, cardAnimationSet, overlayAnimationSet);
        removeFirst();
    }

    private void userSuperVibes(String url) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("check_in_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {
                        createprofilestacks();
                        supervibescount = msg;
                        Log.d("Remaining_count", supervibescount);
                        //  setup();
                    } else {
                        Toast.makeText(Swipe.this, object.getString("message"), Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }


}
