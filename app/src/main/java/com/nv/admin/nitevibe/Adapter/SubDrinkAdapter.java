package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Admin on 3/20/2018.
 */
public class SubDrinkAdapter extends RecyclerView.Adapter<SubDrinkAdapter.MyView> {
    private List<subDrinkModel> arrayList;
    private Context context;
    subDrinkModel current;
    List<Integer> integers = new ArrayList<>();
    HashSet<Integer> integerSet;
    public SubDrinkAdapter(List<subDrinkModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public SubDrinkAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sub_drink_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final SubDrinkAdapter.MyView holder, int position) {
        current=arrayList.get(position);
        holder.subName.setBackground(context.getResources().getDrawable(R.drawable.sub_drinks));
        holder.subName.setText(current.subDrinkName);

        integerSet = DrinkAdapter.integers;
        if(!integerSet.isEmpty()) {
            List<Integer> strings = new ArrayList<>(integers);

            for (int i = 0; i < integerSet.size(); i++) {
                subDrinkModel model = arrayList.get(position);
                if (current.subDrinkName.equals(strings.get(i))) {
                    int index = Integer.parseInt(model.subDrinkId);
                    integers.add(index);
                    holder.subName.setSelected(true);
                    holder.subName.setBackground(context.getResources().getDrawable(R.drawable.select_sub_drink));
                }

            }
        }else{
            integerSet = new HashSet<>(0);
        }

        holder.subName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // holder.subName.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.select_sub_drink) );
                holder.subName.setBackground(context.getResources().getDrawable(R.drawable.select_sub_drink));
            }
        });

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }


    public class MyView extends RecyclerView.ViewHolder {
        public TextView subName;
        public MyView(View itemView) {
            super(itemView);
            subName= itemView.findViewById(R.id.subdrink_txt);
        }
    }
}
