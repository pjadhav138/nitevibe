package com.nv.admin.nitevibe.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.BlockUser;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Admin on 4/5/2018.
 */
public class BlockAdapter extends RecyclerView.Adapter<BlockAdapter.MyView> {
    private List<BlockModel> arrayList;
    private Context context;
    BlockModel current;
    public String blockUserUrl;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    ProgressDialog loading;

    public BlockAdapter(List<BlockModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }


    @Override
    public BlockAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.block_user_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final BlockAdapter.MyView holder, int position) {
        sharedpreferences=context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);

        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");


        current=arrayList.get(position);
        String profileImg=current.blockImg;
        String blockName=current.blockName;
        final String blockId=current.blockId;
        holder.txt_id.setText(blockId);


        holder.txt_name.setText(blockName);


        if(profileImg.equals("")){

            holder.profileimg.setImageResource(R.drawable.default_icon);
        }
        else{
            Bitmap icon=getBitmapFromURL(profileImg);
            holder.profileimg.setImageBitmap(icon);
        }

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Toast.makeText(context,blockId,Toast.LENGTH_SHORT).show();

                showPopup(view,holder.txt_id.getText().toString());
            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public CircleImageView profileimg;
        public TextView txt_name,txt_details,txt_id;
        public ImageView more;
        public MyView(View itemView) {
            super(itemView);
            profileimg= itemView.findViewById(R.id.profile_img);
            txt_name= itemView.findViewById(R.id.name);
            txt_details= itemView.findViewById(R.id.user_detail);
            more= itemView.findViewById(R.id.more);
            txt_id= itemView.findViewById(R.id.user_id);
        }
    }

    //convert image url to bitmap
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    public void showPopup(View v, final String id1) {
        PopupMenu popup = new PopupMenu(context, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.block_setting, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.unblock:
                       // String block_user_id=current.blockId;
                        String block_user_id=id1;
                        boolean flag=hasConnection();
                        if(flag){
                            blockUserUrl= AllUrl.BLOCK_USER+loginUserId+"&block_id="+block_user_id+"&mode=1";
                            Log.d("blockUserUrl",blockUserUrl);
                            //Unblock(blockUserUrl);
                            UnblockVolley(blockUserUrl);
                        }
                        else{
                            Toast.makeText(context,"No internet connection",Toast.LENGTH_SHORT).show();
                        }


                        break;

                }
                return true;
            }
        });
    }

    private void UnblockVolley(String url){
        loading= new ProgressDialog(context,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("unblockuser_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        Intent i=new Intent(context, BlockUser.class);
                        context.startActivity(i);
                        MyApplication.getInstance().trackEvent("Unblock", "Unblock", "Success");

                    }else{
                        Toast.makeText(context,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                
                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }



}

