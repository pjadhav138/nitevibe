package com.nv.admin.nitevibe.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Adapter.ClubEventModel;
import com.nv.admin.nitevibe.Adapter.ClubSideEventAdapter;
import com.nv.admin.nitevibe.Adapter.ClubSliderAdapter;
import com.nv.admin.nitevibe.Adapter.FacilityModel;
import com.nv.admin.nitevibe.Adapter.FacilityRecyclerAdapter;
import com.nv.admin.nitevibe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class ClubHome extends AppCompatActivity implements View.OnClickListener {
    public static final String PREFS_NAME = "LoginPrefs";
    public static ImageView img_back, img_loc, contact_img;
    public static TextView txt_event_title, txt_clubName, txt_clubRate, txt_time, txt_star, time_label, cost_label, fac_label, happy_label, happy_time, time_close, txt_going;
    public static RatingBar txt_clubStar;
    public static RecyclerView facilityRecycler, eventRecycler;
    public static String clubUrl, club_mobileno;
    public static String clubId;
    public static Typeface bold_face, extra_bold_face, reg_face, semi_bold_face, calibri_bold, lato_bold, semi_bold_italic;
    public static String loginUserId, loginUserMode, loginUserType;
    public static RelativeLayout facilityRel, eventRel;
    public static String str_lat, str_long, str_current_event;
    ClubSliderAdapter sliderPagerAdapter;
    ArrayList<String> clubImageList;
    int page_position = 0;
    FacilityRecyclerAdapter facilityAdapter;
    ClubSideEventAdapter eventAdapter;
    List<FacilityModel> facilityList;
    List<ClubEventModel> eventList;
    AlertDialog alert;
    RecyclerView.LayoutManager mLayoutManager;
    //shared
    SharedPreferences sharedpreferences;
    ProgressDialog loading;
    CircleIndicator indicator;
    private ViewPager club_slider;
    private LinearLayout lin_dots;
    private TextView[] dots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_home);

        sharedpreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");

        img_back = findViewById(R.id.back);
        img_loc = findViewById(R.id.imgLoc);

        txt_clubName = findViewById(R.id.txt_name);
        txt_star = findViewById(R.id.txt_star_label);
        time_label = findViewById(R.id.txt_time_label);
        txt_clubRate = findViewById(R.id.txt_cost);
        txt_time = findViewById(R.id.txt_time);
        cost_label = findViewById(R.id.txt_cost_label);
        time_close = findViewById(R.id.txt_time_close);
        txt_going = findViewById(R.id.going_title);
        indicator = findViewById(R.id.indicator);
        contact_img = findViewById(R.id.imgcontact);
        txt_event_title = findViewById(R.id.txt_event_label);

        happy_label = findViewById(R.id.txt_happy_label);
        happy_time = findViewById(R.id.txt_happy);

        fac_label = findViewById(R.id.fac_title);

        txt_clubStar = findViewById(R.id.rating);

        facilityRel = findViewById(R.id.facilityrel);
        eventRel = findViewById(R.id.eventdetailrel);

        facilityRecycler = findViewById(R.id.facilityRecycler);
        facilityRecycler.setHasFixedSize(true);
        facilityRecycler.setLayoutManager(new LinearLayoutManager(this));//Linear Items

        eventRecycler = findViewById(R.id.eventRecycler);
        eventRecycler.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        eventRecycler.setLayoutManager(mLayoutManager);


        setFont();
        onClickEvent();

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            //values coming from Clubsidehome
            clubId = extra.getString("club_id");
            Log.d("club_id", clubId);
        }

        boolean flag = hasConnection();
        if (flag) {

            String url = AllUrl.CLUB + clubId + "&user_id=0";
            clubUrl = url.replace(" ", "%20");
            //clubDetail(clubUrl);
            clubSpecificDetail(clubUrl);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection!", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickEvent() {
        img_back.setOnClickListener(this);

        img_loc.setOnClickListener(this);
        contact_img.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.back) {
            // startActivity(new Intent(this,ClubSideHome.class));
            Intent i = new Intent(this, ClubSideHome.class);
            startActivity(i);

        }
        if (id == R.id.imgcontact) {
            MyApplication.getInstance().trackEvent("Club Details ", "Call Action", "call");
            if (isPermissionGranted()) {
                call_action();

            }
        }

        if (id == R.id.imgLoc) {
            if (!str_lat.equals("") && !str_long.equals("")) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + str_lat + "," + str_long + " (" + "Club" + ")";
                //String uri = String.format(Locale.ENGLISH, "geo:%f,%f", str_lat, str_long);
                String uri = String.format(geoUri);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);

            }

        }

    }

    private boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    private void call_action() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + club_mobileno));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    //volley
    public void clubSpecificDetail(String url) {

        loading = new ProgressDialog(ClubHome.this, R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("clubhome_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if (res.equals("200")) {
                        JSONArray array = object.getJSONArray("message");
                        if (array.length() == 0) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(ClubHome.this);
                            builder1.setMessage("No data found for your search!");
                            builder1.setCancelable(false);
                            builder1.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();

                                        }
                                    });
                            alert = builder1.create();
                            alert.show();
                            Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                            positive.setTextColor(getResources().getColor(R.color.female));
                        } else {
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);


                                String club_name = object1.getString("club_name");
                                String club_rate = object1.getString("club_rate");
                                String club_time = object1.getString("club_time");
                                String club_happy_time = object1.getString("happy_time");
                                String club_cost_two = object1.getString("cost_two");
                                String club_lat = object1.getString("lat");
                                String club_lon = object1.getString("lon");
                                String club_fav = object1.getString("fav_club");
                                club_mobileno = object1.getString("club_contact");

                                str_lat = club_lat;
                                str_long = club_lon;

                                if (club_fav.equals("1")) {


                                } else if (club_fav.equals("0")) {

                                }


                                txt_clubName.setText(club_name);
                                txt_clubRate.setText(club_cost_two);
                                txt_time.setText(club_time);
                                happy_time.setText(club_happy_time);
                                txt_clubStar.setRating(Float.parseFloat(club_rate));
                                //time_close

                                String[] time = club_time.split("-");

                                if (time.length == 0) {
                                    txt_time.setText("");
                                } else if (time.length == 1) {
                                    txt_time.setTextColor(getResources().getColor(R.color.light_green));
                                    txt_time.setText(club_time);
                                } else if (time.length == 2) {
                                    String start = time[0];
                                    String end = time[1];

                                    if (start.equals("Open")) {
                                        txt_time.setTextColor(getResources().getColor(R.color.light_green));
                                        txt_time.setText(start);
                                    } else {
                                        txt_time.setText(start);
                                    }

                                    if (end.equals("Closed")) {
                                        time_close.setTextColor(getResources().getColor(R.color.light_green));
                                        time_close.setText("-" + end);
                                    } else {
                                        time_close.setText("- CLOSES " + end);
                                    }


                                }


                                //pica array
                                JSONArray picarray = object1.getJSONArray("club_pic");
                                clubImageList = new ArrayList<>();
                                if (picarray.length() == 0) {

                                } else {
                                    for (int j = 0; j < picarray.length(); j++) {
                                        String abc = String.valueOf(picarray.get(j));

                                        Log.d("pic_array", abc);
                                        clubImageList.add(abc);

                                    }
                                    clubImageSlider();

                                }
                                //end of pic array

                                //facility array
                                JSONArray facarray = object1.getJSONArray("facility");
                                facilityList = new ArrayList<>();
                                if (facarray.length() == 0) {
                                    facilityRel.setVisibility(View.GONE);
                                } else {
                                    facilityRel.setVisibility(View.VISIBLE);

                                    for (int k = 0; k < facarray.length(); k++) {
                                        JSONObject object3 = facarray.getJSONObject(k);
                                        FacilityModel model = new FacilityModel();

                                        String fac_name = object3.getString("fac_name");
                                        model.facilityName = fac_name;
                                        String fac_icon = object3.getString("fac_icon");
                                        model.facilityImage = fac_icon;
                                        facilityList.add(model);

                                    }
                                    facilityAdapter = new FacilityRecyclerAdapter(facilityList, ClubHome.this);
                                    facilityRecycler.setAdapter(facilityAdapter);

                                }
                                //end of facility


                                //event array
                                JSONArray eventarr = object1.getJSONArray("event");
                                if (eventarr.length() == 0) {
                                    eventRel.setVisibility(View.GONE);

                                } else {
                                    eventRel.setVisibility(View.VISIBLE);
                                    eventList = new ArrayList<ClubEventModel>();
                                    for (int q = 0; q < eventarr.length(); q++) {

                                        JSONObject object5 = eventarr.getJSONObject(q);
                                        ClubEventModel model = new ClubEventModel();


                                        model.eventId = object5.getString("event_id");
                                        model.eventName = object5.getString("event_name");
                                        model.eventImg = object5.getString("event_img");
                                        model.eventDate = object5.getString("event_date");
                                        model.userGoing = object5.getString("user_going");
                                        model.male = object5.getString("male_count");
                                        model.female = object5.getString("female_count");
                                        model.fav = object5.getString("favourite");
                                        model.eventStartDate = object5.getString("event_start");
                                        model.eventEndDate = object5.getString("event_end");
                                        eventList.add(model);


                                    }
                                    eventAdapter = new ClubSideEventAdapter(eventList, ClubHome.this);
                                    eventRecycler.setAdapter(eventAdapter);

                                }
                                //end of event

                                //today event
                                JSONArray todayevent = object1.getJSONArray("today_event");
                                if (todayevent.length() == 0) {

                                } else {

                                    for (int t = 0; t < todayevent.length(); t++) {
                                        JSONObject object7 = todayevent.getJSONObject(t);

                                        String today_event_id = object7.getString("today_event_id");
                                        String today_event_start = object7.getString("today_event_start");
                                        String today_event_end = object7.getString("today_event_end");

                                        str_current_event = today_event_id;


                                    }
                                }


                                //end today event

                            }

                        }


                    } else {
                        Toast.makeText(ClubHome.this, object.getString("message"), Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    /*Toast.makeText(ClubHome.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    public void clubImageSlider() {

        // method for initialisation
        init();

        // method for adding indicators
        addBottomDots(0);

        final Handler handler = new Handler();

        final Runnable update = new Runnable() {
            public void run() {
                if (page_position == clubImageList.size()) {
                    page_position = 0;
                } else {
                    page_position = page_position + 1;
                }
                club_slider.setCurrentItem(page_position, true);
            }
        };

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 100, 5000);
    }

    private void init() {

        club_slider = findViewById(R.id.slider);
        lin_dots = findViewById(R.id.dots);


//Add few items to slider_image_list ,this should contain url of images which should be displayed in slider
// here i am adding few sample image links, you can add your own


        sliderPagerAdapter = new ClubSliderAdapter(ClubHome.this, clubImageList);
        club_slider.setAdapter(sliderPagerAdapter);
        indicator.setViewPager(club_slider);

        club_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void addBottomDots(int currentPage) {


        dots = new TextView[clubImageList.size()];


        lin_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);

            // dots[i].setGravity(Gravity.CENTER |Gravity.TOP);
            //dots[i].setGravity(Gravity.TOP);
            // dots[i].setPadding(5,0,5,5); //left,top,right,bottom


            dots[i].setText(Html.fromHtml(" &#8226&nbsp;"));
            dots[i].setTextSize(30);
            // dots[i].setTextColor(Color.parseColor("#000000"));
            dots[i].setTextColor(getResources().getColor(R.color.light_blue));
            // lin_dots.addView(dots[i]);
        }

        if (dots.length > 0)

            //dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
            dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    public void setFont() {
        bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold.ttf");
        extra_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        reg_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        semi_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBold.ttf");
        calibri_bold = Typeface.createFromAsset(getAssets(), "fonts/Calibri Bold.ttf");
        lato_bold = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");
        semi_bold_italic = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBoldItalic.ttf");

        txt_clubName.setTypeface(bold_face);
        txt_star.setTypeface(calibri_bold);
        time_label.setTypeface(calibri_bold);
        time_close.setTypeface(calibri_bold);
        txt_clubRate.setTypeface(calibri_bold);
        cost_label.setTypeface(calibri_bold);
        txt_time.setTypeface(calibri_bold);
        fac_label.setTypeface(calibri_bold);
        happy_label.setTypeface(calibri_bold);
        happy_time.setTypeface(calibri_bold);
        txt_event_title.setTypeface(calibri_bold);

    }


}
