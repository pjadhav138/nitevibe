package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.StrictMode;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nv.admin.nitevibe.Activity.ChatMenu;
import com.nv.admin.nitevibe.Activity.Chatting;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.Activity.RecyclerTest;
import com.nv.admin.nitevibe.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Admin on 4/11/2018.
 */
public class ChatListAdapter  extends RecyclerView.Adapter<ChatListAdapter.MyView> {
    private List<ChatListModel> arrayList;
    private Context context;
    ChatListModel current;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;

    public ChatListAdapter(List<ChatListModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public ChatListAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.block_user_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ChatListAdapter.MyView holder, int position) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        RelativeLayout relativeLayout= (RelativeLayout) holder.itemView;

        current=arrayList.get(position);
        semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        reg_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Regular.ttf");

        String profileImg=current.profile;
        final String userId=current.id;

        holder.user_id.setText(current.id);
        if(profileImg.equals("")){
           holder.pic_url.setText("");
          // holder.profile.setPadding(10,10,10,10); /////
            holder.profile.setImageResource(R.drawable.default_icon);
           // holder.profile.setImageDrawable(context.getResources().getDrawable(R.drawable.circle_profile));
        }
        else{
            holder.pic_url.setText(current.profile);
            Bitmap icon=getBitmapFromURL(profileImg);
            holder.profile.setImageBitmap(icon);
        }

        holder.user_name.setTypeface(semi_bold_face);
        holder.user_msg.setTypeface(reg_face);

        holder.user_name.setText(current.name);
        if(current.message.equals("") || current.message.equals("null")){
            holder.user_msg.setVisibility(View.GONE);
        }
        else{
            holder.user_msg.setVisibility(View.VISIBLE);
            holder.user_msg.setText(current.message);

        }
       /* Log.d("current_message",current.message);
        holder.user_msg.setVisibility(View.VISIBLE);
        holder.user_msg.setText(current.message);*/

        holder.occupation.setText(current.occupation);

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.getInstance().trackEvent("Chat", "More option", "Success");
                Intent intent=new Intent(context.getApplicationContext(), ChatMenu.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
             /*   intent.putExtra("user_id",current.id);
                intent.putExtra("user_name",current.name);
                intent.putExtra("user_profile",current.profile);*/
                intent.putExtra("user_id",holder.user_id.getText().toString());
                intent.putExtra("user_name",holder.user_name.getText().toString());
                intent.putExtra("user_profile",holder.pic_url.getText().toString());
                intent.putExtra("user_occupation",holder.occupation.getText().toString());
                context.getApplicationContext().startActivity(intent);
            }
        });

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(context,userId,Toast.LENGTH_SHORT).show();
                MyApplication.getInstance().trackEvent("Chat", "Start Chat", "Success");
                Intent intent=new Intent(context.getApplicationContext(), RecyclerTest.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
              /*  intent.putExtra("select_user_id",current.id);
                intent.putExtra("select_user_name",current.name);*/
                Log.d("passing_values",holder.user_id.getText().toString()+" "+holder.user_name.getText().toString());
                intent.putExtra("user_id",holder.user_id.getText().toString());
                intent.putExtra("user_name",holder.user_name.getText().toString());
                intent.putExtra("select_user_profile",holder.pic_url.getText().toString());
                context.getApplicationContext().startActivity(intent);



            }
        });




    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        TextView user_name,user_msg,user_id,pic_url,occupation;
        CircleImageView profile;
        ImageView more;
        public MyView(View itemView) {
            super(itemView);
            user_name= itemView.findViewById(R.id.name);
            user_msg= itemView.findViewById(R.id.user_detail);
            user_id= itemView.findViewById(R.id.user_id);
            profile= itemView.findViewById(R.id.profile_img);
            more= itemView.findViewById(R.id.more);
            pic_url= itemView.findViewById(R.id.pic_url);
            occupation= itemView.findViewById(R.id.occupation);
        }
    }

    //convert image url to bitmap
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
