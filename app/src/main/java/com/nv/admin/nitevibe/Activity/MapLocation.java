package com.nv.admin.nitevibe.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.nv.admin.nitevibe.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.nv.admin.nitevibe.custom.SessionManager;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapLocation extends FragmentActivity implements OnMapReadyCallback,  GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener  {


    private GoogleApiClient googleApiClient;
    AlertDialog alert11;
    ArrayList<LatLng> MarkerPoints;
    GPSTracker gps;
    private GoogleMap mMap;
    Marker marker;
    LatLng position;
    public static double gpslongitude,latitude,longitude;
    public static double gpslatitude;
    public static String lat1,lng1,club_name;
    public static double club_lat,club_lng;
    private String TAG=getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_location3);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        Bundle extra=getIntent().getExtras();
        if(extra!=null){
            //values coming from ClubEventDetails
            lat1=extra.getString("club_lat");
            lng1=extra.getString("club_lng");
            club_name=extra.getString("club_name");
            club_lat= Double.parseDouble(lat1);
            club_lng= Double.parseDouble(lng1);
            Log.d("club_location",club_name+" "+lat1+" "+lng1);
        }


        //   if(argument!=null){
        final LocationManager service = (LocationManager)this.getSystemService(LOCATION_SERVICE);
        final boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);


        // Check if enabled and if not send user to the GPS settings
        if (!enabled) {

            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("GPS is not enable. Please enable it.");
            builder1.setCancelable(false);
            builder1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int id) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                            // new DisplayLocation().execute();
                            //getCurrentLocation();
                        }
                    });
            builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alert11 = builder1.create();
            alert11.show();
        } else {
            gps = new GPSTracker(this);
         //   getCurrentLocation();
            //new DisplayLocation().execute();
        }
        MarkerPoints = new ArrayList<>();


        //   }


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        mMap.clear();
        // Check if GPS enabled
        if(gps.canGetLocation()) {

            gpslatitude = gps.getLatitude();
            gpslongitude = gps.getLongitude();
            Log.e("Location","Your Location is - \nLat: " + gpslatitude + "\nLong: " + gpslongitude);

            // Toast.makeText(getActivity(), "Your Location is - \nLat: " + gpslatitude + "\nLong: " + gpslongitude, Toast.LENGTH_LONG).show();
            moveMap();
        }
    }

    //Function to move the map
    private void moveMap() {
        SessionManager sessionManager = new SessionManager(MapLocation.this);

        final LatLng latLng = new LatLng(Double.parseDouble(sessionManager.getLocation().get(sessionManager.Lat)), Double.parseDouble(sessionManager.getLocation().get(sessionManager.Lng)));
        position = new LatLng(club_lat, club_lng);
        Log.e(TAG, "moveMap: "+club_lat+" - "+club_lng );
      //  position = new LatLng(19.0682366, 72.6980467);
       // position = new LatLng(19.2085718,72.9695301); //
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng).title("");  //new

        markerOptions.position(position).title(club_name);
//        mMap.addMarker(markerOptions).showInfoWindow();  //new
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

      //  marker = mMap.addMarker(markerOptions);
        marker = mMap.addMarker(markerOptions);

        if (MarkerPoints.size() > 1) {
            Log.d("map_log","entered");
            MarkerPoints.clear();
            mMap.clear();

//            mMap.addMarker(markerOptions.position(latLng).title("").snippet("")).showInfoWindow();
            mMap.addMarker(markerOptions.position(position).title("").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))).showInfoWindow();
        }
//        MarkerPoints.add(latLng);
        MarkerPoints.add(position);
        MarkerOptions options = new MarkerOptions();
        /*options.position(latLng).title("abcd");
        mMap.addMarker(options).showInfoWindow();*/

        options.position(latLng);
        mMap.addMarker(options);

        if (MarkerPoints.size() >= 2) {
            LatLng origin = MarkerPoints.get(0);
            LatLng dest = MarkerPoints.get(1);
            String url = getUrl(origin, dest);
            Log.d("onMapClick", url);
            FetchUrl FetchUrl = new FetchUrl();
            FetchUrl.execute(url);

//            mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        }
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data);
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0]);
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask","Executing routes");
                Log.d("ParserTask",routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask",e.toString());
                e.printStackTrace();
            }
            return routes;
        }


        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);


                    points.add(position);

                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.RED);

                Log.d("onPostExecute","onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                mMap.addPolyline(lineOptions);
            }
            else {
                Log.d("onPostExecute","without Polylines drawn");
            }
        }
    }


    private String getUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String url = "http://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        // String url="http://maps.google.com/maps/api/directions/json?origin="+origin.latitude+","+origin.longitude+"&destination=41.311725,-72.740211";

        return url;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        //Clearing all the markers
        mMap.clear();
        //Adding a new marker to the current pressed position
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true)).showInfoWindow();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
         //LatLng latLng = new LatLng(19.0682366,72.6980467);
        //LatLng latLng = new LatLng(72.6980467,19.0682366);
        LatLng latLng = new LatLng(gpslatitude,gpslongitude);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true).title("")).showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
       // mMap.animateCamera(CameraUpdateFactory.zoomTo(5));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);

        getCurrentLocation();
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        //Getting the coordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //Moving the map
        moveMap();
    }
}
