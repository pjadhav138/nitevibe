package com.nv.admin.nitevibe.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.Club;
import com.nv.admin.nitevibe.Activity.ClubEventDetails;
import com.nv.admin.nitevibe.Activity.EntryDetails;
import com.nv.admin.nitevibe.Activity.EventEnlargeImage;
import com.nv.admin.nitevibe.Activity.Help;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.Activity.Swipe;
import com.nv.admin.nitevibe.Activity.WhosGoing;
import com.nv.admin.nitevibe.R;
import com.bumptech.glide.Glide;
import com.github.lzyzsd.circleprogress.ArcProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class EventSliderAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private static int currentPage = 0;
    Activity activity;
    ArrayList<EventModel> arrayList;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    private List<WhosGoingModel> goingList;
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    RecyclerView.LayoutManager mLayoutManager;
    public WhosGoingAdapter goingAdapter;
    ProgressDialog loading;
    private List<WhosGoingModel> fewGoingList;
    EventModel current;
    private List<String> eventImages;
  /*  public TextView txt_date,txt_mnth,txt_name,txt_id,txt_going,txt_male_per,txt_female_per,txt_desc_label,txt_desc,txt_entry_label,txt_entry,txt_view_label,
            txt_who_going,txt_event_id,view_coupon;
    public Button btn_going;
    public RelativeLayout descRel,goingRel,progressRel;
    public ImageView img_main,back,more,bg_img;
    public ProgressBar progress;
    public RecyclerView goingRecycler;
    public  TextView txt_event_name,txt_add_title,txt_address,txt_date_title,txt_date11,txt_time_title,txt_time11,txt_entry_title,txt_entry_fee,txt_coupon,txt_coupon_count;
    public  Button btn_skip,btn_usecoupon;
    public  ImageView coupon_img;

    public  RelativeLayout couponRel,stagRel,coupleRel,mainRel;
    public  TextView coupon_title,txt_stag,txt_couple;*/
    public ArcProgress couponProgress;
   // EventModel current;
    public String currentEventId;


    public EventSliderAdapter(Activity activity, ArrayList<EventModel> arrayList, List<WhosGoingModel> goingList, List<WhosGoingModel> fewGoingList, List<String> eventImages) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.goingList = goingList;
        this.fewGoingList = fewGoingList;
        this.eventImages = eventImages;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.event_row, container, false);


      final ImageView img_main= view.findViewById(R.id.imageView1);
        final ImageView back= view.findViewById(R.id.back);
        final TextView txt_date= view.findViewById(R.id.date);
        final TextView  txt_mnth= view.findViewById(R.id.month);
        final TextView  txt_name= view.findViewById(R.id.eventname);
        final TextView   txt_id= view.findViewById(R.id.event_id);
        final TextView   txt_going= view.findViewById(R.id.peoplegoing);
        final TextView  txt_male_per= view.findViewById(R.id.maleper);
        final TextView txt_female_per= view.findViewById(R.id.femaleper);
        final TextView txt_desc_label= view.findViewById(R.id.desc_label);
        final TextView  txt_desc= view.findViewById(R.id.desc);
        final TextView txt_entry_label= view.findViewById(R.id.entry_label);
        final TextView txt_entry= view.findViewById(R.id.entry);
        final TextView txt_view_label= view.findViewById(R.id.viewprice);
        final TextView txt_who_going= view.findViewById(R.id.going_label);
        final TextView   txt_event_id= view.findViewById(R.id.eventid);

      final Button  btn_going= view.findViewById(R.id.btn_going);
       final RelativeLayout descRel= view.findViewById(R.id.descrel);
        final RelativeLayout goingRel= view.findViewById(R.id.goingrel);
        final RelativeLayout  progressRel= view.findViewById(R.id.progress_rel);
      final  ProgressBar  progress= view.findViewById(R.id.progressBar);
       final RecyclerView goingRecycler= view.findViewById(R.id.goingrecycler);
        final View more= view.findViewById(R.id.view_more);
     //   final TextView  txt_coupon= view.findViewById(R.id.total_title);
      //  final TextView  txt_coupon_count= view.findViewById(R.id.total_coupon);
        final RelativeLayout  couponRel= view.findViewById(R.id.couponrel);
        final TextView  coupon_title= view.findViewById(R.id.coupon_title);
        final RelativeLayout mainRel= view.findViewById(R.id.mainrel);
        final ImageView   bg_img= view.findViewById(R.id.imageView2);
        final TextView  view_coupon= view.findViewById(R.id.viewcoupon);
        final  TextView eventImageName= view.findViewById(R.id.eventimg);
        final  TextView stagRate= view.findViewById(R.id.stagrate);
        final  TextView coupleRate= view.findViewById(R.id.couplerate);
        final  TextView coupontime= view.findViewById(R.id.coupontym);


        sharedpreferences=activity.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");
        String eventOverlay=sharedpreferences.getString("eventoverlay","");
       /* if(!eventOverlay.equals("")){
            displayTuto();
        }*/

        bold_face=Typeface.createFromAsset(activity.getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(activity.getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(activity.getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(activity.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(activity.getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(activity.getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(activity.getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        txt_date.setTypeface(extra_bold_face);
        txt_mnth.setTypeface(semi_bold_face);
        txt_name.setTypeface(bold_face);
        txt_going.setTypeface(reg_face);
        txt_male_per.setTypeface(semi_bold_face);
        txt_female_per.setTypeface(semi_bold_face);
        txt_desc_label.setTypeface(calibri_bold);
        txt_desc.setTypeface(semi_bold_face);
        txt_entry_label.setTypeface(calibri_bold);
        //holder.txt_entry.setTypeface();
        txt_view_label.setTypeface(semi_bold_face);
        txt_who_going.setTypeface(calibri_bold);
        btn_going.setTypeface(bold_face);
        coupon_title.setTypeface(calibri_bold);
        view_coupon.setTypeface(semi_bold_face);

         current=arrayList.get(position);
         Log.d("eventslideadapter","entered");
         Log.d("positionslider", String.valueOf(current));
         Log.d("nameslider",current.eventName);
        coupontime.setText(current.coupontime);

        txt_name.setText(current.eventName);
        String event_date=current.eventDate;
        String[] abc=event_date.split(" ");
        String date=abc[0];
        String mnth=abc[1];
        txt_date.setText(date);
        txt_mnth.setText(mnth);

        txt_id.setText(current.eventId);
        txt_desc.setText(current.eventDesc);

        String abc1234=current.eventImages.get(currentPage);
        eventImageName.setText(abc1234);

       // stagRate.setText(current.st);

        currentEventId=current.eventId;
        txt_event_id.setText(current.eventId);

        if(current.eventTotal.equals("0")){
            txt_going.setText("");
        }
        else{
           // txt_going.setText(current.eventTotal+" People are going");
            if(current.eventTotal=="1"){
                txt_going.setText(current.eventTotal+" Person is going");
            }
            else { txt_going.setText(current.eventTotal+" People are going");}
        }

        String stag=current.eventStag;
        String couple=current.eventCouple;

        stagRate.setText(stag);
        coupleRate.setText(couple);


        if(current.eventStag.equals("NA")){
            //holder.txt_entry.setText("u20B9 "+model.eventCouple);
            if(current.eventCouple.equals("0") ){
                txt_entry.setText("Free");
            }

            else{
                txt_entry.setText("\u20B9 "+current.eventCouple);
            }

        }
        else if(current.eventCouple.equals("NA")){
            // holder.txt_entry.setText(R.string.Rs+model.eventStag);

            if(current.eventStag.equals("0")){
                txt_entry.setText("Free");
            }
            else{
                txt_entry.setText("\u20B9 "+current.eventStag);
            }
        }
        else if(current.eventStag.equals("NA") && current.eventCouple.equals("NA")){
            txt_entry.setText("");
            txt_entry.setVisibility(View.GONE);
            txt_entry_label.setVisibility(View.GONE);
        }
        else if(current.eventStag.equals("0") && current.eventCouple.equals("0")){
            txt_entry.setTextColor(activity.getResources().getColor(R.color.light_green));
            txt_entry.setText("Free");
        }
        else{
            txt_entry.setText("\u20B9 "+current.eventStag);
        }


        Log.d("user_count",current.eventMale+" "+current.eventFemale);


        if(current.eventMale.equals("0") && current.eventFemale.equals("0")){
            progressRel.setVisibility(View.GONE);

        }
        else{
            progressRel.setVisibility(View.VISIBLE);
            txt_male_per.setText(current.eventMale+"%");
            txt_female_per.setText(current.eventFemale+"%");

            String total_ratio=current.eventMale+current.eventFemale;

            // holder.progress.setMax(Integer.parseInt(total_ratio));
            progress.setSecondaryProgress(Integer.parseInt(current.eventFemale));

        }




        String userAge=current.userAge;
        String userPref=current.userGenderPref;

        if(current.eventImages!=null && !current.eventImages.isEmpty()){
            Glide.with(activity).load(current.eventImages.get(currentPage)).into(img_main);

        }

        img_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = activity.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                editor.remove("event_id");
                editor.commit();
                editor.putString("event_id", txt_event_id.getText().toString());
                editor.commit();

                Intent i=new Intent(activity, EventEnlargeImage.class);
               // i.putExtra("eventimage",current.eventImages.get(currentPage));
                i.putExtra("eventimage",eventImageName.getText().toString());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(i);

            }
        });


        txt_view_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = activity.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                editor.remove("event_id");
                editor.commit();
                editor.putString("event_id", txt_event_id.getText().toString());
                editor.commit();
                Intent i=new Intent(activity, EntryDetails.class);
               /* i.putExtra("stag",current.eventStag);
                i.putExtra("couple",current.eventCouple);*/
                i.putExtra("stag",stagRate.getText().toString());
                i.putExtra("couple",coupleRate.getText().toString());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                view.getContext().startActivity(i);
            }
        });

        btn_going.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean flag=hasConnection();

                if(flag){

                    //String url= AllUrl.USER_GOING+loginUserId+"&club_id="+current.clubId+"&event_id="+current.eventId;
                    String url= AllUrl.USER_GOING+loginUserId+"&club_id="+current.clubId+"&event_id="+txt_event_id.getText().toString();
                    Log.d("user_going_url",url);
                    //  userGoing(url);
                    userGoingVolley(url,txt_event_id.getText().toString());
                    //  Toast.makeText(context,current.eventId,Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(activity,"No internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(current.type.equals("event")){
                    activity.startActivity(new Intent(activity, ClubEventDetails.class));
                }
                else if(current.type.equals("banner")){
                    activity.startActivity(new Intent(activity,Club.class));
                }
                else if(current.type.equals("viewprofile")){
                    activity.finish();

                }

            }
        });

        //goingrecycler
        goingList=new ArrayList<>();

        if(current.fewGoingList.size()> 0){
            goingList=current.fewGoingList;
            Log.d("size_of_goingarray", String.valueOf(goingList.size()));
            goingRecycler.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
            goingRecycler.setLayoutManager(mLayoutManager);

         /*   if(current.goingList.size()>0){
                holder.goingRel.setVisibility(View.GONE);
            }
            else{
                holder.goingRel.setVisibility(View.VISIBLE);
                goingAdapter=new WhosGoingAdapter(current.goingList,context);
                holder.goingRecycler.setAdapter(goingAdapter);
            }*/
            goingRel.setVisibility(View.VISIBLE);
            goingAdapter=new WhosGoingAdapter(current.goingList,activity);
            goingRecycler.setAdapter(goingAdapter);

            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SharedPreferences.Editor editor = activity.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                    editor.remove("event_id");
                    editor.commit();
                    editor.putString("event_id",txt_event_id.getText().toString());
                    editor.commit();
                    Intent i=new Intent(activity, WhosGoing.class);
                    activity.startActivity(i);

                }
            });
        }
        else{
            more.setVisibility(View.GONE);
            goingRel.setVisibility(View.GONE);
        }

        if(current.coupon.equals("")){
            couponRel.setVisibility(View.GONE);
        }
        else{
            couponRel.setVisibility(View.VISIBLE);


        }

        view_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //String time1=current.coupontime;
                String time1=coupontime.getText().toString();
                final Dialog dialog = new Dialog(activity);
                dialog.setContentView(R.layout.coupon_alert);

                TextView label= dialog.findViewById(R.id.title);

                TextView txt1= dialog.findViewById(R.id.message1);
                TextView txt2= dialog.findViewById(R.id.message2);
                TextView txt3= dialog.findViewById(R.id.message3);
                TextView txt4= dialog.findViewById(R.id.message4);

                txt1.setTypeface(reg_face);
                txt2.setTypeface(reg_face);
                txt3.setTypeface(reg_face);
                txt4.setTypeface(reg_face);


                txt1.setText(R.string.coup_1);
                txt2.setText(R.string.coup_2);
                String abc= String.valueOf(R.string.coup_3);
                String abc1= String.valueOf(R.string.coup_4);


                String abc123=activity.getResources().getString(R.string.coup_3)+" "+time1+" "+activity.getResources().getString(R.string.coup_4);
                Log.d("concact_string",abc123);
                txt3.setText(abc123);
                txt4.setText(R.string.coup_5);

                txt4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i=new Intent(activity, Help.class);
                        activity.startActivity(i);
                    }
                });

                dialog.show();
                dialog.setCanceledOnTouchOutside(true);

            }
        });

        container.addView(view);

        //end of method
        return view;

    }

    //user going
    private void userGoingVolley(String url, final String event){

        loading= new ProgressDialog(activity,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(activity.getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("user_going_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){
                        //  Intent i=new Intent(context, Swipe.class);
                        //  context.startActivity(i);
                        MyApplication.getInstance().trackEvent("I am Going", "I am Going", "I am Going");
                        Intent i=new Intent(activity,Swipe.class);
                        i.putExtra("val_from","going");
                      //  i.putExtra("event_id",current.eventId);
                        i.putExtra("event_id",event);
                        activity.startActivity(i);


                    }
                    else if(res.equals("203")){
                        /*Intent i=new Intent(context, Swipe.class);
                        context.startActivity(i);*/
                        Intent i=new Intent(activity,Swipe.class);
                        i.putExtra("val_from","going");
                        i.putExtra("event_id",event);
                        activity.startActivity(i);
                    }
                    else{
                        Toast.makeText(activity,object.getString("message"),Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)activity.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }

}
