package com.nv.admin.nitevibe.custom;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SessionManager {
    private static final String PREF_NAME = "NiteVibe";
    private static final String TKN_PREF = "TokenPref";
    SharedPreferences pref;
    SharedPreferences token;
    SharedPreferences.Editor editor, editor2;
    Context _context;
    int PRIVATE_MODE = 0;
    SharedPreferences.Editor edrtoken;


    public String Lat = "lat";
    public String NOTSTATUS = "status";

    public String Lng = "long";
    public String FCM_TOKEN = "fcm_token";


    public SessionManager(Context context) {
        _context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        token = _context.getSharedPreferences(TKN_PREF, PRIVATE_MODE);
        editor = pref.edit();
        edrtoken = token.edit();
    }


    public void storeLocation(String lat, String lng) {
        editor.putString(Lat, lat);
        editor.putString(Lng, lng);
        editor.commit();
    }

    public void saveStatus(boolean flag){
        editor.putBoolean(NOTSTATUS, flag);
        editor.commit();
    }

    public boolean isStatusSaved(){
        return pref.getBoolean(NOTSTATUS, false);
    }

    public HashMap<String, String> getLocation() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(Lat, pref.getString(Lat, ""));
        user.put(Lng, pref.getString(Lng, ""));
        return user;
    }


    public void StoreFirebaseToken(String token) {
        edrtoken.putString(FCM_TOKEN, token);
        edrtoken.commit();
    }
    public String getFirebaseToken() {
        return token.getString(FCM_TOKEN, "");
    }


}
