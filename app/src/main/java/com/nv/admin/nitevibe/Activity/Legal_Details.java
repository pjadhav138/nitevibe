package com.nv.admin.nitevibe.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;

public class Legal_Details extends AppCompatActivity implements View.OnClickListener {
    public static ImageView back;
    public static TextView legal_data,title;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face;
    public static WebView legal_web;
    public ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal__details);

        back= findViewById(R.id.back);
        legal_data= findViewById(R.id.txt_data);
        title= findViewById(R.id.title);
        legal_web= findViewById(R.id.weblegal);

        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        WebSettings webSettings = legal_web.getSettings();
        webSettings.setJavaScriptEnabled(true);
        legal_web.setScrollContainer(false);
        //legal_web.loadUrl(AllUrl.LEGAL);


        progressBar= new ProgressDialog(Legal_Details.this,R.style.MyAlertDialogStyle);
        progressBar.setIndeterminate(true);
        progressBar.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        progressBar.setCancelable(false);
        progressBar.setMessage("Please wait...!");
        progressBar.show();
        legal_web.loadUrl(AllUrl.LEGAL);


        legal_web.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                Log.d("legal","shouldOverrideUrlLoading");
                view.loadUrl(request.toString());
                return true;

            }


            @Override
            public void onPageFinished(WebView view, String url) {
                Log.d("legal","page_finished");
                super.onPageFinished(view, url);
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }

            }


        });





        back.setOnClickListener(this);
        setFont();

    }

    @Override
    public void onClick(View view) {
        int id=view.getId();

        if(id==R.id.back){
            startActivity(new Intent(this,Setting.class));
        }

    }


    public void setFont(){
        title.setTypeface(bold_face);
        legal_data.setTypeface(semi_bold_face);
    }
}
