package com.nv.admin.nitevibe.Adapter;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.CropUserPic;
import com.nv.admin.nitevibe.Activity.EditProfile;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.R;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;



/**
 * Created by Admin on 3/3/2018.
 */
public class photoAdapter extends RecyclerView.Adapter<photoAdapter.MyView> {
   ArrayList<String> alImage;
//ArrayList<Integer> alImage;
    Context context;
    ArrayList<String> alCount;
    private int PICK_IMAGE_REQUEST = 1;
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    String profile_url,userProfileUrl;
    public static String loginUserId,loginUserMode,loginUserType;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold;
    AlertDialog alert;
    public CardView cardview;
    ProgressDialog loading;
    PopupWindow popupWindow;

    public photoAdapter(Context context, ArrayList<String> alImage, ArrayList<String> alCount) {
        this.context = context;
        this.alImage = alImage;
        this.alCount = alCount;
    }

 /*   public photoAdapter(Context context, ArrayList<Integer> alImage, ArrayList<String> alCount) {
        this.context = context;
        this.alImage = alImage;
        this.alCount = alCount;
    }*/

    @Override
    public photoAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_photo_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final photoAdapter.MyView holder, final int position) {



        Bitmap bitmapImage = null;
        sharedpreferences=context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        final String imageLink;
        //from editprofile
         userProfileUrl=sharedpreferences.getString("profile","");
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        Log.d("userProfileUrl",userProfileUrl);
        holder.count.setText(alCount.get(position));

        profile_url= String.valueOf(alImage.get(position));

        Log.d("profile_img_url",profile_url);

 /*       if(!profile_url.equals("")){
            try {
                URL url = new URL(profile_url);
                bitmapImage = BitmapFactory.decodeStream(url.openConnection().getInputStream());
               // RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(holder, bitmapImage);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }*/


        // Bitmap icon = BitmapFactory.decodeResource(context.getResources(),R.drawable.girl);
       // holder.profile_image.setImageBitmap(icon);



      //holder.profile_image.setImageDrawable(alImage.get(position));

        if(profile_url==""){
            holder.add_image.setVisibility(View.VISIBLE);
        }
        else{

          //  holder.profile_image.setImageBitmap(bitmapImage);

            Picasso.with(context.getApplicationContext())
                    .load(alImage.get(position))
                    .into(holder.profile_image);
            holder.add_image.setVisibility(View.GONE);
        }



        holder.add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value=holder.count.getText().toString();

                SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
                editor.remove("img_row");
                editor.commit();
                editor.putString("img_row",value);
                editor.commit();
                //Intent i=new Intent(context, CropUserPic.class);
                Intent i=new Intent(context, CropUserPic.class);
                context.startActivity(i);

            }
        });


        holder.profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //imageLink=profile_url;

                Log.d("check_image_set",alImage.get(position));
                String value=alImage.get(position);
                if(!value.isEmpty()){
                    initiatePopupWindow(position);
                }

                //initiatePopupWindow(position);
            }
        });




    }

    @Override
    public int getItemCount() {
        return alCount.size();
    }

    public class MyView extends RecyclerView.ViewHolder {
        public ImageView add_image;
        public TextView count;
        public RoundedImageView profile_image;
      //  public ImageView  profile_image;
        public MyView(View itemView) {
            super(itemView);
          //  profile_image=(ImageView)itemView.findViewById(R.id.main_photo);
            profile_image= itemView.findViewById(R.id.main_photo);
            add_image= itemView.findViewById(R.id.add);
            count= itemView.findViewById(R.id.count);
            cardview= itemView.findViewById(R.id.cardView);
        }
    }



    private void initiatePopupWindow(int pos){
        try{

            final String btn_action_value;
            // adapter.notifyDataSetChanged();
            bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Bold.ttf");
            extra_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-ExtraBold.ttf");
            reg_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Regular.ttf");
            semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
            calibri_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Calibri Bold.ttf");

            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // Inflate the custom layout/view
            View popupView = inflater.inflate(R.layout.remove_profile_photo,null);
            // View popupView = inflater.inflate(R.layout.demo2,null);


            popupWindow = new PopupWindow(
                    popupView,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            );

            // Set an elevation value for popup window
            // Call requires API level 21
            if(Build.VERSION.SDK_INT>=21){
                popupWindow.setElevation(5.0f);
            }


            ImageView cancel= popupView.findViewById(R.id.cancel);
            //ImageView img_profile=(ImageView)popupView.findViewById(R.id.main_photo);
            RoundedImageView img_profile= popupView.findViewById(R.id.main_photo);
            final Button btn_remove= popupView.findViewById(R.id.remove_photo);
            final Button btn_profile= popupView.findViewById(R.id.set_profile);
            TextView txt_img_value= popupView.findViewById(R.id.count);

           // popupWindow.showAtLocation(popupView, Gravity.CENTER,0,0);
            popupWindow.showAtLocation(cardview, Gravity.CENTER,0,0);

            btn_profile.setTypeface(bold_face);
            btn_remove.setTypeface(bold_face);
            txt_img_value.setTypeface(semi_bold_face);

            Log.d("user_profile_photo",userProfileUrl);

            if(userProfileUrl.equals("null")){
                btn_profile.setText("Set as profile");
                btn_remove.setText("Remove Photo");
            }
            else{
                if(alImage.get(pos).equals(userProfileUrl)){
                    //means this image is set as profile url
                    btn_profile.setText("Remove as profile");
                    btn_remove.setText("Remove Photo");

                }
                else{
                    btn_profile.setText("Set as profile");
                    btn_remove.setText("Remove Photo");
                }
            }




            Log.d("popup_profile_url",profile_url);
            txt_img_value.setText(alCount.get(pos));
            Log.d("userclicked_img",alImage.get(pos));

            //profile_url
            Picasso.with(context.getApplicationContext())
                    .load(alImage.get(pos))
                    .into(img_profile);

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow.dismiss();
                }
            });
            final String photo_name=alImage.get(pos);

            btn_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(context,"Set as profile",Toast.LENGTH_SHORT).show();
                    String btn_value=btn_profile.getText().toString();
                    Log.d("value_of_profile_btn",btn_value);
                    String url = null;
                    if(btn_value.equals("Set as profile")){
                        String action="1";
                        url= AllUrl.REMOVE_PHOTO+loginUserId+"&photo_name="+photo_name+"&status=1";


                    }
                    else if(btn_value.equals("Remove as profile")){
                        String action="2";
                        url= AllUrl.REMOVE_PHOTO+loginUserId+"&photo_name="+photo_name+"&status=2";
                    }
                    //btn_action_value
                    boolean flag=hasConnection();
                    if(flag){
                        Log.d("remove_photo_url",url);
                       // removePhoto(url);
                        removePhotoVolley(url);
                    }
                    else{
                        Toast.makeText(context,"No internet connection",Toast.LENGTH_SHORT).show();
                    }



                }
            });

            btn_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(context," profile removed",Toast.LENGTH_SHORT).show();
                    String remove_value=btn_remove.getText().toString();
                    Log.d("value_of_remove_btn",remove_value);
                    String action="3";
                    String url= AllUrl.REMOVE_PHOTO+loginUserId+"&photo_name="+photo_name+"&status=3";

                    boolean flag=hasConnection();
                    if(flag){
                      //  removePhoto(url);
                        removePhotoVolley(url);
                    }
                    else{
                        Toast.makeText(context,"No internet connection",Toast.LENGTH_SHORT).show();
                    }


                }
            });





        }
        catch (Exception e){
            e.printStackTrace();
        }
    }





    private void removePhotoVolley(String url){
        loading= new ProgressDialog(context,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("remove_photo_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                        builder1.setMessage("Your Profile Image is Updated Successfully!");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        context.startActivity(new Intent(context, EditProfile.class));


                                    }
                                });
                        alert = builder1.create();
                        alert.show();

                        Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                        positive.setTextColor(context.getResources().getColor(R.color.female));
                          // positive.setTextColor(Color.parseColor("#ff426f"));


                    }else{
                        Toast.makeText(context,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                Toast.makeText(context,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }



}

