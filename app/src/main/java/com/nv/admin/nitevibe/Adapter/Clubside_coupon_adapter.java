package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;

import java.util.List;

public class Clubside_coupon_adapter extends RecyclerView.Adapter<Clubside_coupon_adapter.MyView> {
    private List<ClubSideCouponModel> arrayList;
    private Context context;
    ClubSideCouponModel current;
    public static Typeface bold_face,extra_bold_face,reg_face;

    public Clubside_coupon_adapter(List<ClubSideCouponModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public Clubside_coupon_adapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.club_side_coupon_row, parent, false);
        Clubside_coupon_adapter.MyView viewHolder = new Clubside_coupon_adapter.MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Clubside_coupon_adapter.MyView holder, int position) {
        current=arrayList.get(position);
        reg_face= Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Regular.ttf");

        String user_name=current.name;
        String user_email=current.mobile;
        holder.txt_name.setTypeface(reg_face);
        if(!user_name.equals("")){
            holder.txt_name.setText(current.name);
        }
        else{
            holder.txt_name.setText(current.mobile);
        }



    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        TextView txt_name;
        public MyView(View itemView) {
            super(itemView);
            txt_name= itemView.findViewById(R.id.name);
        }
    }
}
