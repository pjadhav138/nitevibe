package com.nv.admin.nitevibe.Activity;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nv.admin.nitevibe.R;

public class Reset_Password extends AppCompatActivity implements View.OnClickListener {
    public static EditText password1,password2;
    public static Button update;
    public static TextInputLayout password1_layout,password2_layout;
    public static String userPassword1,userPassword2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset__password);

        password1= findViewById(R.id.password);
        password2= findViewById(R.id.repassword);
        password1_layout= findViewById(R.id.password_layout);
        password2_layout= findViewById(R.id.repassword_layout);
        update= findViewById(R.id.update);

        onClickEvent();
    }
    public void onClickEvent(){
        update.setOnClickListener(this);
        password1.addTextChangedListener(new MyTextWatcher(password1));
        password2.addTextChangedListener(new MyTextWatcher(password2));
        password1_layout.setHintTextAppearance(R.style.MyAppearance1);
        password1_layout.setHint("Password");
        password2_layout.setHintTextAppearance(R.style.MyAppearance1);
        //password2_layout.setHint("Password");

    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        if(id==R.id.update){

            userPassword1=password1.getText().toString();
            userPassword2=password2.getText().toString();

            if(!userPassword1.equals(userPassword2)){
                Toast.makeText(getApplicationContext(),"password does not match",Toast.LENGTH_SHORT).show();
            }
            else{
                MyApplication.getInstance().trackEvent("Login", "Password Update", "Success");
                Toast.makeText(getApplicationContext(),"password updated",Toast.LENGTH_SHORT).show();
            }

        }

    }

    private class MyTextWatcher implements TextWatcher {
        private View view;


        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Log.d("click_on_edit_text","beforeTextChanged");

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Log.d("click_on_edit_text","onTextChanged");

        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()){
                case R.id.password:
                    Log.d("click_on_edit_text","afterTextChanged");

                    password1_layout.setHintTextAppearance(R.style.MyAppearance);
                    password1_layout.setHint("Password");

                    break;
                case R.id.repassword:
                    Log.d("new_password",editable.toString());
                    password2_layout.setHintTextAppearance(R.style.MyAppearance);
                    password2_layout.setHint("Confirm Password");
                    userPassword1=password1.getText().toString();
                  /*  if(editable.length()>0 && userPassword1.length()>0){
                        if(!editable.equals(userPassword1)){
                            password2_layout.setError("Password doest not match");
                        }
                        else{
                            password2_layout.setError(null);
                        }
                    }*/

                    break;



            }

        }
    }

}
