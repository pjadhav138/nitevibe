package com.nv.admin.nitevibe.RangeSeekBar;

/**
 * Created by Admin on 3/8/2018.
 */
public interface OnRangeSeekbarFinalValueListener {
    void finalValue(Number minValue, Number maxValue);
}
