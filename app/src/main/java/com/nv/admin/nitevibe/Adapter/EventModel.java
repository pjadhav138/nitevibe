package com.nv.admin.nitevibe.Adapter;

import java.util.List;

/**
 * Created by Admin on 4/23/2018.
 */
public class EventModel {
    //model for event card
    public String clubId,eventId,eventName,eventDate,eventDesc,eventTotal,eventStag,eventCouple,eventMale,eventFemale,userAge,userGenderPref,type;
   public String clubAddress,coupon,date,time,img,leftcoupon,coupontime;
    public List<String> eventImages;
    public List<WhosGoingModel> goingList;
    public List<WhosGoingModel> fewGoingList;


    public String getClubId() {
        return clubId;
    }

    public void setClubId(String clubId) {
        this.clubId = clubId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public String getEventTotal() {
        return eventTotal;
    }

    public void setEventTotal(String eventTotal) {
        this.eventTotal = eventTotal;
    }

    public String getEventStag() {
        return eventStag;
    }

    public void setEventStag(String eventStag) {
        this.eventStag = eventStag;
    }

    public String getEventCouple() {
        return eventCouple;
    }

    public void setEventCouple(String eventCouple) {
        this.eventCouple = eventCouple;
    }

    public String getEventMale() {
        return eventMale;
    }

    public void setEventMale(String eventMale) {
        this.eventMale = eventMale;
    }

    public String getEventFemale() {
        return eventFemale;
    }

    public void setEventFemale(String eventFemale) {
        this.eventFemale = eventFemale;
    }

    public String getUserAge() {
        return userAge;
    }

    public void setUserAge(String userAge) {
        this.userAge = userAge;
    }

    public String getUserGenderPref() {
        return userGenderPref;
    }

    public void setUserGenderPref(String userGenderPref) {
        this.userGenderPref = userGenderPref;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClubAddress() {
        return clubAddress;
    }

    public void setClubAddress(String clubAddress) {
        this.clubAddress = clubAddress;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLeftcoupon() {
        return leftcoupon;
    }

    public void setLeftcoupon(String leftcoupon) {
        this.leftcoupon = leftcoupon;
    }

    public String getCoupontime() {
        return coupontime;
    }

    public void setCoupontime(String coupontime) {
        this.coupontime = coupontime;
    }

    public List<String> getEventImages() {
        return eventImages;
    }

    public void setEventImages(List<String> eventImages) {
        this.eventImages = eventImages;
    }

    public List<WhosGoingModel> getGoingList() {
        return goingList;
    }

    public void setGoingList(List<WhosGoingModel> goingList) {
        this.goingList = goingList;
    }

    public List<WhosGoingModel> getFewGoingList() {
        return fewGoingList;
    }

    public void setFewGoingList(List<WhosGoingModel> fewGoingList) {
        this.fewGoingList = fewGoingList;
    }

    @Override
    public String toString() {
        return "EventModel{" +
                "clubId='" + clubId + '\'' +
                ", eventId='" + eventId + '\'' +
                ", eventName='" + eventName + '\'' +
                ", eventDate='" + eventDate + '\'' +
                ", eventDesc='" + eventDesc + '\'' +
                ", eventTotal='" + eventTotal + '\'' +
                ", eventStag='" + eventStag + '\'' +
                ", eventCouple='" + eventCouple + '\'' +
                ", eventMale='" + eventMale + '\'' +
                ", eventFemale='" + eventFemale + '\'' +
                ", userAge='" + userAge + '\'' +
                ", userGenderPref='" + userGenderPref + '\'' +
                ", type='" + type + '\'' +
                ", clubAddress='" + clubAddress + '\'' +
                ", coupon='" + coupon + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", img='" + img + '\'' +
                ", leftcoupon='" + leftcoupon + '\'' +
                ", coupontime='" + coupontime + '\'' +
                ", eventImages=" + eventImages +
                ", goingList=" + goingList +
                ", fewGoingList=" + fewGoingList +
                '}';
    }
}
