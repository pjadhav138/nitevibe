package com.nv.admin.nitevibe.Adapter;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.Chat;
import com.nv.admin.nitevibe.Activity.Club;
import com.nv.admin.nitevibe.Activity.Events;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.Activity.Swipe;
import com.nv.admin.nitevibe.Activity.View_Profile;
import com.nv.admin.nitevibe.R;
import com.nv.admin.nitevibe.custom.AnimationType;
import com.nv.admin.nitevibe.custom.StartSmartAnimation;
import com.nv.admin.nitevibe.custom.SwipeDirection;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.nv.admin.nitevibe.Activity.Swipe.createprofilestacks;
import static com.nv.admin.nitevibe.Activity.Swipe.profiles;



/**
 * Created by Admin on 5/8/2018.
 */
public class Mainprofilestack extends ArrayAdapter<Mainprofile>  {
     public static ViewHolder holder;
    public  int pos;
    private int currentPage = 0;
    private static int NUM_PAGES = 0;
    int holderimages = 0;

    public static Mainprofile mainprofile;
    public static Subprofile subprofile;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    ProgressDialog loading;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType,eventId;
    public PopupWindow popupWindow;
    public static  String finalUserId;
    boolean flag;
    public static String supervibescount1;



    public Mainprofilestack(Context context) {
        super(context, 0);
    }

    @Override
    public View getView(final int position, View contentView, ViewGroup parent) {
        if (contentView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            contentView = inflater.inflate(R.layout.item_profile_card, parent, false);
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }
        AlphaAnimation alpha = new AlphaAnimation(0.5F, 52F);
        alpha.setDuration(0); // Make animation instant
        alpha.setFillAfter(true); // Tell it to persist after the animation ends

// And then on your layout
        holder.subrelative.startAnimation(alpha);
        mainprofile = getItem(position);

        bold_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getContext().getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getContext().getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        holder.name.setTypeface(bold_face);
        holder.occupation.setTypeface(bold_face);
        holder.city.setTypeface(reg_face);

        sharedpreferences=getContext().getSharedPreferences(PREFS_NAME,getContext().MODE_PRIVATE);

        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");
        //value comming from Swipe
        eventId=sharedpreferences.getString("event_id","");

         pos=position;
     //   finalUserId= Swipe.profiles.get(pos).m_id;

        final String super_vibes=mainprofile.getSupervibes();


        if(super_vibes.equals("0")){
            if(android.os.Build.VERSION.SDK_INT >= 21){
                holder.supervibe.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.supervibe3,getContext().getTheme()));
            } else {
                holder.supervibe.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.supervibe3));

            }

        }else if(super_vibes.equals("1")){

            if(android.os.Build.VERSION.SDK_INT >= 21){

                holder.supervibe.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.supervibe2,getContext().getTheme()));
            } else {
                holder.supervibe.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.supervibe2));

            }
        }else if(super_vibes.equals("2")){
            if(android.os.Build.VERSION.SDK_INT >= 21){
                holder.supervibe.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.supervibe1,getContext().getTheme()));
            } else {
                holder.supervibe.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.supervibe1));

            }
        }else if(super_vibes.equals("3")){

            if(android.os.Build.VERSION.SDK_INT >= 21){
                holder.supervibe.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.supervibe0,getContext().getTheme()));
            } else {
                holder.supervibe.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.supervibe0));

            }


        }else{
            // holder.supervibe.setBackgroundDrawable(getContext().getDrawable(R.drawable.supervibe3));
        }




        if(mainprofile.age.equals("")){
            holder.name.setText(mainprofile.name);
        }
        else{
            holder.name.setText(mainprofile.name+" ,"+mainprofile.age);
        }

        holder.occupation.setText(mainprofile.job);

        holder.city.setText(mainprofile.city);


      //  Glide.with(getContext()).load(mainprofile.profile_url).into(holder.image);


        if(mainprofile.subimage!=null && !mainprofile.subimage.isEmpty()){
            NUM_PAGES = mainprofile.subimage.size();
            for(int i=0;i<NUM_PAGES;i++) {
                if (mainprofile.m_id.equals(mainprofile.subimage.get(i).img_id)) {

                    holder.img_cnt.setText(""+holderimages);
                }else{
                    holderimages=0;
                }
            }

        }

        String userProfileUrl=mainprofile.profile_url;
        if(!userProfileUrl.equals("")){
            Glide.with(getContext()).load(mainprofile.profile_url).into(holder.image);
            //  Log.d("profilenameif",mainprofile.profile_url);
            //  Log.d("swipeuserif",mainprofile.profile_url);
        }
        else{
            String userId = "";
            try{

                Mainprofile mainprofile1 = profiles.get(position);
                 userId=mainprofile1.m_id;

                List<Subprofile> abc=new ArrayList<>();
                abc=mainprofile1.subimage;
                List<String> userAllImages=new ArrayList<>();
                List<String> userAllImagesId=new ArrayList<>();

                for(int q=0;q<abc.size();q++){
                    if(abc.get(q).img_id.equals(userId)){
                        userAllImages.add(abc.get(q).img);
                        userAllImagesId.add(abc.get(q).img_id);
                    }
                }
                Glide.with(getContext()).load(userAllImages.get(0)).into(holder.image);

          /*  if(mainprofile.m_id.equals(profiles.get(position).m_id)){
                Glide.with(getContext()).load(mainprofile.subimage.get(0).img).into(holder.image);
                Log.d("swipeuserelse",mainprofile.subimage.get(0).img);
            }*/
            }
            catch (Exception e){
                e.printStackTrace();
                Log.d("Exception_array",e.toString()+" "+userId);
            }



        }


        holder.userinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mainprofile mainprofile1 = profiles.get(position);
                String userId=mainprofile1.m_id;

                List<Subprofile> abc=new ArrayList<>();
                abc=mainprofile1.subimage;
                List<String> userAllImages=new ArrayList<>();
                List<String> userAllImagesId=new ArrayList<>();

                for(int q=0;q<abc.size();q++){
                    if(abc.get(q).img_id.equals(userId)){
                        userAllImages.add(abc.get(q).img);
                        userAllImagesId.add(abc.get(q).img_id);
                    }
                }

                String count= String.valueOf(userAllImagesId.size());
                // Toast.makeText(getContext(),Swipe.profiles.get(position).m_id,Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = getContext().getSharedPreferences(PREFS_NAME, getContext().MODE_PRIVATE).edit();
                editor.remove("userid");
                editor.remove("from");
               // editor.remove("imgLength");
                editor.commit();
                editor.putString("userid",Swipe.profiles.get(position).m_id);
                editor.putString("from","Swipe");
                //editor.putString("imgLength",count);
                editor.commit();
                Intent i=new Intent(getContext(), View_Profile.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                getContext().startActivity(i);

            }
        });


        holder.image.setOnTouchListener(new View.OnTouchListener() {

            @Override

            public boolean onTouch(View v, MotionEvent event) {



                try{
                    Mainprofile mainprofile1 = profiles.get(position);
                    String userId=mainprofile1.m_id;

                    List<Subprofile> abc=new ArrayList<>();
                    abc=mainprofile1.subimage;
                    List<String> userAllImages=new ArrayList<>();
                    List<String> userAllImagesId=new ArrayList<>();

                    for(int q=0;q<abc.size();q++){
                        if(abc.get(q).img_id.equals(userId)){
                            userAllImages.add(abc.get(q).img);
                            userAllImagesId.add(abc.get(q).img_id);
                        }
                    }




                    if(userAllImagesId.size()>1){
                        if(event.getX() > holder.image.getWidth() / 2) {

                            Log.d("current_page1", String.valueOf(currentPage));

                            if (currentPage == Integer.parseInt(holder.img_cnt.getText().toString())) {
                                currentPage = 0;
                            }

                            ImageView imageView = v.findViewById(R.id.card_image);
                            Log.d("current_page2", String.valueOf(currentPage));




                            if(mainprofile1.m_id.equals(userAllImagesId.get(currentPage))){

                                if(currentPage<userAllImagesId.size()-1){
                                    currentPage+=1;
                                    Log.d("current_page3", String.valueOf(currentPage));
                                }

                            }
                            Log.d("current_page4", String.valueOf(currentPage));


                            if(mainprofile1.m_id.equals(userAllImagesId.get(currentPage))){

                                Glide.with(getContext()).load(userAllImages.get(currentPage)).into(imageView);
                            }else{

                                if(currentPage>0){
                                    currentPage-=1;
                                    // Log.d("current_page5", String.valueOf(currentPage));
                                }

                            }

                        }

                        if(event.getX() < holder.image.getWidth() / 2) {
                            ImageView imageView = v.findViewById(R.id.card_image);
                            int val= Integer.parseInt(holder.img_cnt.getText().toString()) -1;
                            if (currentPage == val) {
                                currentPage = val;
                            }
                            else {
                                //   Log.d("current_page6", String.valueOf(currentPage));

                                if(mainprofile1.m_id.equals(userAllImagesId.get(currentPage))){

                                    currentPage -=1;
                                    // Log.d("current_page7", String.valueOf(currentPage));

                                    if(currentPage<0){
                                        currentPage=0;
                                    }

                                    // Log.d("current_page8", String.valueOf(currentPage));

                                }


                            }
                            // Log.d("current_page123", String.valueOf(currentPage));


                            if(mainprofile1.m_id.equals(userAllImagesId.get(currentPage))){

                                Glide.with(getContext()).load(userAllImages.get(currentPage)).into(imageView);
                            }

                        }

                    }



                }
                catch (Exception e){
                    e.printStackTrace();
                }






                return false;
            }
        });






        holder.rewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Swipe.cardStackView.reverse();

            }
        });
        flag=hasConnection();

        holder.forword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(flag){

                    String url= AllUrl.SWIPE+loginUserId+"&swipe_user_id="+Swipe.profiles.get(position).m_id+"&category=1&event_id="+eventId;
                    Log.d("swipe_url",url);
                    insertSwipeData(url,"");

                    View target = Swipe.cardStackView.getTopView();
                    View targetOverlay =Swipe. cardStackView.getTopView().getOverlayContainer();

                    ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                            target, PropertyValuesHolder.ofFloat("rotation", 80f));
                    rotation.setDuration(100);
                    ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                            target, PropertyValuesHolder.ofFloat("translationX", 20f, 100f));
                    ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                            target, PropertyValuesHolder.ofFloat("translationY", 20f, 200f));
                    translateX.setStartDelay(100);
                    translateY.setStartDelay(100);
                    translateX.setDuration(500);
                    translateY.setDuration(500);
                    AnimatorSet cardAnimationSet = new AnimatorSet();
                    cardAnimationSet.playTogether(rotation, translateX, translateY);

                    ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 5f, 8f);
                    overlayAnimator.setDuration(200);
                    AnimatorSet overlayAnimationSet = new AnimatorSet();
                    overlayAnimationSet.playTogether(overlayAnimator);
                    Swipe.cardStackView.swipe(SwipeDirection.Right, cardAnimationSet, overlayAnimationSet);
                }
                else{
                    Toast.makeText(getContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                }



            }
        });


        holder.superVibeRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(super_vibes.equals("3")){
                    Toast.makeText(getContext(),"No supervibes left",Toast.LENGTH_SHORT).show();

                }
                else{
                    if(flag){
                     /*   RotateAnimation rotate=new RotateAnimation(0,180, Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
                        rotate.setDuration(5000);
                        rotate.setInterpolator(new LinearInterpolator());
                         holder.superVibeRel.startAnimation(rotate);
                        */

                        StartSmartAnimation.startAnimation(view, AnimationType.BounceInDown, 1000,0,false,1500);





                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                String url=AllUrl.SWIPE+loginUserId+"&swipe_user_id="+Swipe.profiles.get(position).m_id+"&category=3&event_id="+eventId;
                                //insertSwipeData(url,"vibes");


                                //Do something after 100ms
                                View target = Swipe.cardStackView.getTopView();
                                View targetOverlay = Swipe.cardStackView.getTopView().getOverlayContainer();

                                ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                                        target, PropertyValuesHolder.ofFloat("rotation", 0f));
                                rotation.setDuration(100);
                                ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                                        target, PropertyValuesHolder.ofFloat("translationX", 0f,0));
                                ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                                        target, PropertyValuesHolder.ofFloat("translationY", 10f, -700));
                                translateX.setStartDelay(100);
                                translateY.setStartDelay(100);

                                translateX.setDuration(500);
                                translateY.setDuration(500);
                                AnimatorSet cardAnimationSet = new AnimatorSet();
                                cardAnimationSet.playTogether(rotation, translateX, translateY);

                                ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 5f, 8f);
                                overlayAnimator.setDuration(200);
                                AnimatorSet overlayAnimationSet = new AnimatorSet();
                                overlayAnimationSet.playTogether(overlayAnimator);

                                Swipe. cardStackView.swipe(SwipeDirection.Top, cardAnimationSet, overlayAnimationSet);


                            }
                        }, 500);

                    }
                    else{
                        Toast.makeText(getContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                    }
                }





            }
        });

        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(flag){
                    String url=AllUrl.SWIPE+loginUserId+"&swipe_user_id="+Swipe.profiles.get(position).m_id+"&category=1&event_id="+eventId;
                    insertSwipeData(url,"");

                    View target = Swipe.cardStackView.getTopView();
                    View targetOverlay =Swipe. cardStackView.getTopView().getOverlayContainer();

                    ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                            target, PropertyValuesHolder.ofFloat("rotation", 80f));

                    rotation.setDuration(100);
                    ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                            target, PropertyValuesHolder.ofFloat("translationX", 20f, 600f));
                    ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                            target, PropertyValuesHolder.ofFloat("translationY", 20f, 250f));
                    translateX.setStartDelay(100);
                    translateY.setStartDelay(100);
                    translateX.setDuration(500);
                    translateY.setDuration(500);
                    AnimatorSet cardAnimationSet = new AnimatorSet();
                    cardAnimationSet.playTogether(rotation, translateX, translateY);

                    ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 5f, 8f);
                    overlayAnimator.setDuration(200);
                    AnimatorSet overlayAnimationSet = new AnimatorSet();
                    overlayAnimationSet.playTogether(overlayAnimator);
                    Swipe.cardStackView.swipe(SwipeDirection.Right, cardAnimationSet, overlayAnimationSet);

                }
                else{
                    Toast.makeText(getContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                }



            }
        });

        holder.dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(flag){
                    String url=AllUrl.SWIPE+loginUserId+"&swipe_user_id="+Swipe.profiles.get(position).m_id+"&category=2&event_id="+eventId;
                    insertSwipeData(url,"");

                    View target = Swipe.cardStackView.getTopView();
                    View targetOverlay = Swipe.cardStackView.getTopView().getOverlayContainer();

                    ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                            target, PropertyValuesHolder.ofFloat("rotation", -10f));
                    rotation.setDuration(100);
                    ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                            target, PropertyValuesHolder.ofFloat("translationX", -10f, -10));
                    ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                            target, PropertyValuesHolder.ofFloat("translationY", -10f, -10));
                    translateX.setStartDelay(100);
                    translateY.setStartDelay(100);

                    translateX.setDuration(500);
                    translateY.setDuration(500);
                    AnimatorSet cardAnimationSet = new AnimatorSet();
                    cardAnimationSet.playTogether(rotation, translateX, translateY);

                    ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 5f, 8f);
                    overlayAnimator.setDuration(200);
                    AnimatorSet overlayAnimationSet = new AnimatorSet();
                    overlayAnimationSet.playTogether(overlayAnimator);

                    Swipe. cardStackView.swipe(SwipeDirection.Left, cardAnimationSet, overlayAnimationSet);
                }
                else{
                    Toast.makeText(getContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


        holder.back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comingfrom=mainprofile.from;
               // SharedPreferences.Editor editor = getContext().getSharedPreferences(PREFS_NAME, getContext().MODE_PRIVATE).edit();
              //  editor.remove   going,checkin
                Log.d("value_coming_from",comingfrom);
                if(comingfrom.equals("checkin")){
                     Intent i=new Intent(getContext(),Club.class);
                     i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                     getContext().startActivity(i);



                }
               // else if(comingfrom.equals("checkin")){
                    else{
                    Log.d("mainprofile","checkin enter");
                     Intent i1=new Intent(getContext(), Events.class);
                     i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                     getContext().startActivity(i1);
                }

            }
        });
        holder.home_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getContext(), Club.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(i);
            }
        });

        holder.chat_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getContext(), Chat.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(i);
            }
        });



        return contentView;
    }

    private void userSuperVibes(String url){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("supervibes_count", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if(res.equals("200")){
                        createprofilestacks();

                    }

                    else{
                        Toast.makeText(getContext(),object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());



                // hide the progress dialog

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }



    public static class ViewHolder {
        public TextView name,city,occupation,age,swipe_count,img_cnt,txt_user_id;
        public ImageView rewind,forword,like,supervibe,dislike,userinfo;
        public ViewPager pager;
        RelativeLayout layout;
        LinearLayout sublinear;
        public RelativeLayout superVibeRel,mRelativeLayout;
        public ImageView back_img,home_img,chat_img;
       // public ImageView image,subrelative;
        public RoundedImageView image,subrelative;



        public ViewHolder(View view) {
            this.name = view.findViewById(R.id.card_name);
            this.city = view.findViewById(R.id.card_city);
            this.occupation= view.findViewById(R.id.card_occupation);
            this.age= view.findViewById(R.id.card_age);
            this.swipe_count= view.findViewById(R.id.txt_swipe_count);
           // this.image = (ImageView) view.findViewById(R.id.card_image);
            this.image = view.findViewById(R.id.card_image);
            this.img_cnt= view.findViewById(R.id.card_img_count);
            this.layout = view.findViewById(R.id.cardLayout);
           // this.subrelative=(ImageView) view.findViewById(R.id.subrel);
            this.subrelative= view.findViewById(R.id.subrel);
            this.sublinear= view.findViewById(R.id.sublinear);
            this.rewind= view.findViewById(R.id.rewind);
            this.forword= view.findViewById(R.id.forword);
            superVibeRel= view.findViewById(R.id.supervibe_anim);
            like= view.findViewById(R.id.like);
            supervibe= view.findViewById(R.id.superlike);
            dislike= view.findViewById(R.id.dislike);
            txt_user_id= view.findViewById(R.id.user_id);
            mRelativeLayout= view.findViewById(R.id.cardLayout);
            userinfo= view.findViewById(R.id.info);
            back_img= view.findViewById(R.id.back);
            home_img= view.findViewById(R.id.home);
            chat_img= view.findViewById(R.id.chat);




        }
    }


    private void insertSwipeData(String url, final String vibes){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("swipe_userdataresponse", response.toString());
              //  loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){

                        }
                        else{
                            for (int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);
                                String user_id=object1.getString("user_id");
                                String user_photo=object1.getString("user_photo");
                                String match_id=object1.getString("match_id");
                                String match_photo=object1.getString("match_photo");

                                String url=AllUrl.MATCH_NOTIFICATION+loginUserId+"&match_id="+match_id;
                                matchNotification(url);


                                initiatePopupWindow( user_id, user_photo,match_id,match_photo);
                            }
                        }

                        if(vibes.equals("vibes")){
                            String url=AllUrl.SUPERVIBE_COUNT+loginUserId+"&event_id="+eventId;
                            userSuperVibes(url);
                        }

                    }else{
                        Toast.makeText(getContext(),object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
               // loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void matchNotification(String url){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("matchnotify_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        Log.d("notify_msg","sucess");

                    }else{
                        // Toast.makeText(ServiceNoDelay.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                    //  Toast.makeText(ServiceNoDelay.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                // Toast.makeText(ServiceNoDelay.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog


            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }



    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)getContext().getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    private void initiatePopupWindow(String userId,String userPhoto,String matchId,String matchPhoto){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
        // Inflate the custom layout/view
        View customView = inflater.inflate(R.layout.profile_match_row,null);


        popupWindow = new PopupWindow(customView,ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT
        );

        // Set an elevation value for popup window
        // Call requires API level 21
        if(Build.VERSION.SDK_INT>=21){
            popupWindow.setElevation(5.0f);
        }

        // Get a reference for the custom view close button

      /*  RoundedImageView userImg=(RoundedImageView)customView.findViewById(R.id.girl_profile);
        RoundedImageView matchImg=(RoundedImageView)customView.findViewById(R.id.boy_profile);*/

        CircleImageView userImg= customView.findViewById(R.id.girl_profile);
        CircleImageView matchImg= customView.findViewById(R.id.boy_profile);

        TextView chat= customView.findViewById(R.id.chat);
        TextView swipe= customView.findViewById(R.id.swipe);
        TextView txt= customView.findViewById(R.id.txt);
        RoundedImageView profileimg= customView.findViewById(R.id.profile);

        txt.setTypeface(semi_bold_italic);
        swipe.setTypeface(bold_face);
        chat.setTypeface(bold_face);

        Log.d("userPhoto",userPhoto);
        Log.d("match_photo",matchPhoto);



        if(userPhoto.equals("")){
            Glide.with(getContext()).load(R.drawable.default_icon).into(userImg);
        }
        else{

            Bitmap img1 = getBitmapFromURL(userPhoto);
            Glide.with(getContext()).load(userPhoto).into(userImg);
          //  userImg.setImageBitmap(img1);
          //  matchImg.setImageBitmap(img1);
            URL url = null;
            try {
                url = new URL(userPhoto);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                String width= String.valueOf(bmp.getWidth());
                String height= String.valueOf(bmp.getHeight());
                Log.d("size_of_user_image",width+" "+height);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        if(matchPhoto.equals("")){
            Glide.with(getContext()).load(R.drawable.default_icon).into(matchImg);
        }
        else{
            Bitmap img2 = getBitmapFromURL(matchPhoto);
           // matchImg.setImageBitmap(img2);
          //  profileimg.setImageBitmap(img2);
            Glide.with(getContext()).load(matchPhoto).into(matchImg);
            URL url = null;
            try {
                url = new URL(matchPhoto);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                String width= String.valueOf(bmp.getWidth());
                String height= String.valueOf(bmp.getHeight());
                Log.d("size_of_match_image",width+" "+height);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
          /*  matchImg.setMaxWidth(10);
            matchImg.setMaxHeight(90);
            matchImg.setCornerRadius((float)165);
            matchImg.mutateBackground(true);
           */


          /*  Picasso.with(getContext())
                    .load(matchPhoto)
                    .into(matchImg);*/

        }

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getContext(),"Chat",Toast.LENGTH_SHORT).show();
                Intent i=new Intent(getContext(), Chat.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(i);

            }
        });

        swipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(holder.mRelativeLayout, Gravity.CENTER,0,0);


    }

    //convert image url to bitmap
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }







}
