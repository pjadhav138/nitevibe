package com.nv.admin.nitevibe.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 3/24/2018.
 */
public class TutorialAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    Activity activity;
    ArrayList<TutorialModel> image_arraylist;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;

    public TutorialAdapter(ArrayList<TutorialModel> image_arraylist, Activity activity) {
        this.image_arraylist = image_arraylist;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return image_arraylist.size();
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.tutorial_row, container, false);

        ImageView tut_img= view.findViewById(R.id.tut_img);
        TextView tut_title= view.findViewById(R.id.label);
        TextView tut_text= view.findViewById(R.id.label2);
       // JustifiedTextView tut_text=(JustifiedTextView)view.findViewById(R.id.label2);


        bold_face=Typeface.createFromAsset(activity.getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(activity.getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(activity.getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(activity.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(activity.getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(activity.getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(activity.getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        tut_title.setTypeface(bold_face);
        tut_text.setTypeface(reg_face);

        TutorialModel model=image_arraylist.get(position);

        tut_title.setText(model.getTitle());
        tut_text.setText(model.getText());
        Picasso.with(activity.getApplicationContext())
                .load(model.getImage())
                .into(tut_img);


        container.addView(view);

        return view;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
