package com.nv.admin.nitevibe.Adapter;

import java.util.List;

/**
 * Created by Admin on 5/8/2018.
 */
public class Mainprofile extends Subprofile {
    public String id;
    public String name;
    public String city;
    public Subprofile url;
    public String job;
    public String age;
    public String m_id;
    public String img,img_id;
    public String supervibes;
    public String category;
    public String from,profile_url;

    public List<Subprofile> subimage;


    public Mainprofile(String name, String city, Subprofile url, List<Subprofile> subimage) {
        this.name = name;
        this.city = city;
        this.url = url;
        this.subimage = subimage;
    }

    public Mainprofile() {

    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    /*    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }*/

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSupervibes() {
        return supervibes;
    }

    public void setSupervibes(String supervibes) {
        this.supervibes = supervibes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Subprofile getUrl() {
        return url;
    }

    public void setUrl(Subprofile url) {
        this.url = url;
    }

    public List<Subprofile> getSubimage() {
        return subimage;
    }

    public void setSubimage(List<Subprofile> subimage) {
        this.subimage = subimage;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getM_id() {
        return m_id;
    }

    public void setM_id(String m_id) {
        this.m_id = m_id;
    }
    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg_id() {
        return img_id;
    }

    public void setImg_id(String img_id) {
        this.img_id = img_id;
    }
}
