package com.nv.admin.nitevibe.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Login extends AppCompatActivity implements View.OnClickListener {
    public static ImageView back,submit;
    public static TextView txt_username,txt_password,txt_signup,txt_forgotpass,main_label,first_time_label;
    public static RelativeLayout usernameRel,passwordRel,forgortpasswordRel,accountRel,loginRel;
    public static EditText ed_username,ed_password;
    public static Button btn_login;
    public static String str_username,str_password;
    public static String loginUrl;
    AlertDialog alert;
    //shared_pref
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";

    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    ProgressDialog loading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);

        back= findViewById(R.id.back);
        submit= findViewById(R.id.submit);
        txt_signup= findViewById(R.id.signup);
        txt_forgotpass= findViewById(R.id.forgot_password);

        main_label= findViewById(R.id.txt);
        first_time_label= findViewById(R.id.txt2);
        txt_username= findViewById(R.id.usertxt);
        txt_password= findViewById(R.id.passwordtxt);
        usernameRel= findViewById(R.id.username_rel);
        passwordRel= findViewById(R.id.password_rel);
        forgortpasswordRel= findViewById(R.id.forgot_rel);
        accountRel= findViewById(R.id.account_rel);
        loginRel= findViewById(R.id.login_rel);
        ed_username= findViewById(R.id.username);
        ed_password= findViewById(R.id.password);
        btn_login= findViewById(R.id.login);

        submit.setImageResource(R.drawable.login);



        //To add click event
        addClickEvent();
        setFont();

        String newUser=sharedpreferences.getString("newuser","");
        if(!newUser.equals("")){
            ed_username.setText(newUser);
        }


    }
    public void addClickEvent(){
        submit.setOnClickListener(this);
        back.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        txt_signup.setOnClickListener(this);
        txt_forgotpass.setOnClickListener(this);
        ed_username.addTextChangedListener(new MyTextWatcher(ed_username));
        ed_password.addTextChangedListener(new MyTextWatcher(ed_password));

    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        if(id==R.id.back){
            startActivity(new Intent(this, LandingPage.class));

        }
        if(id==R.id.submit){
            str_username=ed_username.getText().toString();
            if(!str_username.equals("")){
                if(str_username.matches("[0-9]+")){
                    validateMobile();
                }
                else{
                    validateEmail();
                }
            }
            else{
                Toast.makeText(getApplicationContext(),"Please enter the username",Toast.LENGTH_SHORT).show();
            }

        }
        if(id==R.id.signup){
            startActivity(new Intent(this,CreateAccount.class));

        }
        if(id==R.id.forgot_password){
            startActivity(new Intent(this,Forgot_Password.class));

        }
        if(id==R.id.login){
            getLoginDetails();

        }

    }


    public void getLoginDetails(){
        str_username=ed_username.getText().toString();
        str_password=ed_password.getText().toString();

        if(!str_password.equals("") && !str_username.equals("")){
            //Toast.makeText(getApplicationContext(),"Login successful",Toast.LENGTH_SHORT).show();
            //startActivity(new Intent(this,Home.class));
            boolean flag=hasConnection();
            if(flag){
                String url=AllUrl.LOGIN+str_username+"&password="+str_password;
                loginUrl=url.replaceAll(" ","%20");
                Log.d("loginurl",loginUrl);
               // userLogin(loginUrl);
                userLoginVolley(loginUrl);

            }
            else{
                Toast.makeText(Login.this, "Please Check Your Internet Connection!!", Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(getApplicationContext(),"Please  enter details",Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }

    public static boolean isValidPhone(String phone)
    {
        String expression = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
        CharSequence inputString = phone;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        return matcher.matches();
    }

    private boolean validateEmail(){
        String email_id=ed_username.getText().toString().trim();

        if(isValidEmail(email_id)==false){
            Toast.makeText(getApplicationContext(),"please enter a valid email id",Toast.LENGTH_SHORT).show();

            return false;
        }
        else{

            submit.setImageResource(R.drawable.login_ok);
            passwordRel.setVisibility(View.VISIBLE);
            accountRel.setVisibility(View.GONE);
            forgortpasswordRel.setVisibility(View.VISIBLE);
            loginRel.setVisibility(View.VISIBLE);
            Log.d("email_id_validation","email id validated");
        }
        return true;
    }

    private boolean validateMobile(){
        String phone_number=ed_username.getText().toString().trim();
        Log.d("length", String.valueOf(phone_number.length()));
        isValidPhone(phone_number);
        if(phone_number.length()>=10){
            if(isValidPhone(phone_number)==false){
               Toast.makeText(getApplicationContext(),"Please enter valid phone number",Toast.LENGTH_SHORT).show();
                return false;
            }
            else{

                submit.setImageResource(R.drawable.login_ok);
                passwordRel.setVisibility(View.VISIBLE);

                accountRel.setVisibility(View.GONE);
                forgortpasswordRel.setVisibility(View.VISIBLE);
                loginRel.setVisibility(View.VISIBLE);
            }
        }else{
            Toast.makeText(getApplicationContext(),"Please enter valid phone number",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private static boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email)&& Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    private class MyTextWatcher implements TextWatcher {
        private View view;


        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Log.d("click_on_edit_text","beforeTextChanged");

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Log.d("click_on_edit_text","onTextChanged");

        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()){
                case R.id.username:
                  txt_username.setVisibility(View.VISIBLE);
                    txt_username.setTextColor(getResources().getColor(R.color.light_green));

                    break;
                case R.id.password:
                    txt_password.setVisibility(View.VISIBLE);
                    txt_username.setTextColor(getResources().getColor(R.color.light_blue));
                    break;




            }

        }
    }

    private void userLoginVolley(String url){
        loading= new ProgressDialog(Login.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {



            @Override
            public void onResponse(JSONObject response) {
                Log.d("login_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        MyApplication.getInstance().trackEvent("Login", "Normal", "Success");
                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(Login.this);
                            builder1.setMessage("Failed to load data.Please try again later");
                            builder1.setCancelable(false);
                            builder1.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();

                                        }
                                    });
                            alert = builder1.create();
                            alert.show();
                            Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                            positive.setTextColor(getResources().getColor(R.color.female));

                        }
                        else{
                            for (int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);

                                String userId=object1.getString("user_id");
                                String userType=object1.getString("user_type");
                                Log.d("user_details",userId+" "+userType);

                                SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                                editor.remove("mode");
                                editor.remove("user_id");
                                editor.remove("user_type");
                                editor.commit();

                                editor.putString("mode","Normal");
                                editor.putString("user_id",userId);
                                editor.putString("user_type",userType);
                                editor.commit();

                                if(userType.equals("User")){
                                    //startActivity(new Intent(Login.this,Home.class));
                                    startActivity(new Intent(Login.this,Club.class));
                                }
                                else if(userType.equals("sub_admin")){
                                    startActivity(new Intent(Login.this,ClubSideHome.class));
                                }

                            }
                        }


                    }
                    else if(res.equals("203")){
                        //means user was inactive
                        boolean flag=hasConnection();
                        if(flag){
                            String url=AllUrl.ACTIVE_USER+str_username;
                            String activeUrl=url.replaceAll(" ","%20");
                            Log.d("active_user_url",activeUrl);
                           // ActiveUser(activeUrl);
                            ActiveUserVolley(activeUrl);
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                        }




                    }

                    else{
                        Toast.makeText(Login.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Errorlogin:" + error.getMessage());

                loading.dismiss();
                // hide the progress dialog

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }

    private void ActiveUserVolley(String url){
        loading= new ProgressDialog(Login.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("Active_user_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        MyApplication.getInstance().trackEvent("Login", "Active User", "Yes");
                        startActivity(new Intent(Login.this,Home.class));
                    }else{
                        Toast.makeText(Login.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }



    public void setFont(){
        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        main_label.setTypeface(bold_face);
        txt_username.setTypeface(lato_bold);
        txt_password.setTypeface(lato_bold);
        ed_username.setTypeface(semi_bold_face);
        ed_password.setTypeface(semi_bold_face);
        first_time_label.setTypeface(reg_face);
        txt_signup.setTypeface(semi_bold_italic);
        txt_forgotpass.setTypeface(semi_bold_italic);
        btn_login.setTypeface(bold_face);
//        setUser();
    }



    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Login.this,LandingPage.class));
    }

    private void setUser(){
        ed_username.setText("8898527975");
        ed_password.setText("Pankaj@123");
    }
}

