package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nv.admin.nitevibe.Activity.Events;
import com.nv.admin.nitevibe.R;

import java.util.List;

/**
 * Created by Admin on 4/4/2018.
 */
public class ProfileEventAdapter extends RecyclerView.Adapter<ProfileEventAdapter.MyView> {
    private List<ProfileEventModel> arrayList;
    private Context context;
    ProfileEventModel current;
    public static final String PREFS_NAME = "LoginPrefs";

    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;

    public ProfileEventAdapter(List<ProfileEventModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public ProfileEventAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_event_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ProfileEventAdapter.MyView holder, int position) {
        current=arrayList.get(position);

        bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        holder.event_name.setTypeface(semi_bold_face);
        holder.date.setTypeface(extra_bold_face);
        holder.month.setTypeface(semi_bold_face);


        String event_day=current.eventDate;
        String event_id=current.eventId;
        String event_name=current.eventName;

        String[] event_day1=event_day.split(" ");
        String event_date=event_day1[0];
        String event_month=event_day1[1];

        holder.date.setText(event_date);
        holder.month.setText(event_month);
        holder.event_name.setText(event_name);
        holder.eventid.setText(current.eventId);
        holder.clubid.setText(current.clubId);

        holder.event_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Toast.makeText(context,holder.eventid.getText().toString(),Toast.LENGTH_SHORT).show();
                Intent i=new Intent(context, Events.class);
                SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
                editor.remove("event_id");
                editor.remove("club_id");
                editor.commit();
                editor.putString("event_id", holder.eventid.getText().toString());
                editor.putString("club_id", holder.clubid.getText().toString());
                editor.commit();
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public TextView date,month,event_name,eventid,clubid;
        public MyView(View itemView) {
            super(itemView);
            date= itemView.findViewById(R.id.date);
            month= itemView.findViewById(R.id.month);
            event_name= itemView.findViewById(R.id.event_txt);
            eventid= itemView.findViewById(R.id.eventid);
            clubid= itemView.findViewById(R.id.clubid);
        }
    }
}
