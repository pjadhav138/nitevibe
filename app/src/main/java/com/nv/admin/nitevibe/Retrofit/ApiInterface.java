package com.nv.admin.nitevibe.Retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface ApiInterface {


    @GET("block_globle_notification.php")
    Call<StatusResponse> updateStatus(@QueryMap Map<String, String> params);

    @GET("get_chat_details.php")
    Call<ChatListResponse> getAllMessage(@QueryMap Map<String, String> params);


    @GET("msg_notification.php")
    Call<StatusResponse> msgNotification(@QueryMap Map<String, String> params);


    class StatusResponse {
        @SerializedName("response")
        int response;
        @SerializedName("message")
        String message;
        @SerializedName("status")
        String status;

        public int getResponse() {
            return response;
        }

        public void setResponse(int response) {
            this.response = response;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    class ChatListResponse {
        @SerializedName("response")
        int response;
        @SerializedName("chats")
        List<Chat> chats;
        @SerializedName("user_data")
        List<User> user_data;

        public int getResponse() {
            return response;
        }

        public void setResponse(int response) {
            this.response = response;
        }

        public List<Chat> getChats() {
            return chats;
        }

        public void setChats(List<Chat> chats) {
            this.chats = chats;
        }

        public List<User> getUser_data() {
            return user_data;
        }

        public void setUser_data(List<User> user_data) {
            this.user_data = user_data;
        }

        class User {
            @SerializedName("id")
            String id;
            @SerializedName("name")
            String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public class Chat {
            @SerializedName("id")
            String id;
            @SerializedName("me")
            String me;
            @SerializedName("you")
            String you;
            @SerializedName("message")
            String message;
            @SerializedName("sticker")
            String sticker;
            @SerializedName("sent")
            String sent;
            @SerializedName("entry")
            String entry;
            @SerializedName("img")
            String img;
            @SerializedName("chat_id")
            String chat_id;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getMe() {
                return me;
            }

            public void setMe(String me) {
                this.me = me;
            }

            public String getYou() {
                return you;
            }

            public void setYou(String you) {
                this.you = you;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getSticker() {
                return sticker;
            }

            public void setSticker(String sticker) {
                this.sticker = sticker;
            }

            public String getSent() {
                return sent;
            }

            public void setSent(String sent) {
                this.sent = sent;
            }

            public String getEntry() {
                return entry;
            }

            public void setEntry(String entry) {
                this.entry = entry;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getChat_id() {
                return chat_id;
            }

            public void setChat_id(String chat_id) {
                this.chat_id = chat_id;
            }
        }
    }

}
