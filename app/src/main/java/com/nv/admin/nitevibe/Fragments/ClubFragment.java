package com.nv.admin.nitevibe.Fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.Activity.RequestHandler;
import com.nv.admin.nitevibe.Adapter.BannerAdapter;
import com.nv.admin.nitevibe.Adapter.BannerModel;
import com.nv.admin.nitevibe.Adapter.ClubModel;
import com.nv.admin.nitevibe.Adapter.ClubRecyclerAdapter;
import com.nv.admin.nitevibe.R;
import com.nv.admin.nitevibe.notification.Config;
import com.nv.admin.nitevibe.notification.NotificationUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Admin on 3/9/2018.
 */
public class ClubFragment extends Fragment implements View.OnClickListener {
    //public static TextView  txt_location;
    public static RecyclerView clubRecycler;
    public ClubRecyclerAdapter clubAdapter;
    public static Spinner spin_location;
    public static CircleIndicator indicator;
    AlertDialog alert;
    List<ClubModel> data;

    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";

    //drop-down
    public static String locationUrl,clubListUrl;
    ArrayList locarrayList;
    public static String selectedLocationName;

    //for banner slider

    private TextView[] dots;
    int page_position = 0;
    public ViewPager banner_slider;
    private LinearLayout lin_dots;
    BannerAdapter bannerAdapter;
    ArrayList<BannerModel> bannerList;
//    ArrayList<BannerModel> bannerData;
     ArrayList<String> bannerData;
    ArrayList<String> bannerEventData;
    ArrayList<String> bannerClubData;
    public static RelativeLayout bannerRel;
    public static String loginUserId,loginUserMode,loginUserType;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    ProgressDialog loading;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_club_details, container, false);

        sharedpreferences=getActivity().getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        Log.d("login_user_details",loginUserId+" "+loginUserMode);



        //txt_location=(TextView)view.findViewById(R.id.location);
        bannerRel= view.findViewById(R.id.banner_rel);
        spin_location= view.findViewById(R.id.location);
        clubRecycler= view.findViewById(R.id.clubrecycler);
        indicator= view.findViewById(R.id.indicator);

        clubRecycler.setHasFixedSize(true);
        clubRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));//Linear Items

       // txt_location.setOnClickListener(this);
       // setData();
        locationUrl= AllUrl.LOCATION;
        Log.d("location_list_url",locationUrl);
        boolean flag=hasConnection();
        if(flag){
            getLocation(locationUrl);
            getLocationVolley(locationUrl);
        }
        else{
            Toast.makeText(getActivity(),"No Internet connection",Toast.LENGTH_SHORT).show();
        }


        return view;
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();

       /* if(id==R.id.location){
          startActivity(new Intent(getActivity(), SearchLocation.class));
        }*/

    }


    //getting location list
    //making user active

    private void getLocationVolley(String url){

        loading= new ProgressDialog(getActivity());
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("get_location_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){

                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){

                        }
                        else{
                            locarrayList = new ArrayList(array.length());
                            for(int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);


                                String location_name=object1.getString("loc");
                                Log.d("name_loca",location_name);
                                locarrayList.add(location_name);

                            }
                            Log.d("location_array", locarrayList.toString());

                            String[] arr1 = (String[])locarrayList.toArray(new String[locarrayList.size()]);


                            final ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(),R.layout.location_spin_item,R.id.loc_txt, arr1);

                            dataAdapter1.setDropDownViewResource(R.layout.location_spin_item);
                            spin_location.setAdapter(dataAdapter1);


                            spin_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                    selectedLocationName=spin_location.getSelectedItem().toString();

                                    //now calling the list of clubs
                                   /* String url=AllUrl.CLUB_LIST+loginUserId+"&loc="+selectedLocationName;
                                    clubListUrl=url.replaceAll(" ","%20");
                                    Log.d("list_of_club_url",clubListUrl);*/
                                    boolean flag=hasConnection();
                                    if(flag){
                                        // settingClubList(clubListUrl);
                                      //  postClubList(loginUserId,selectedLocationName);
                                        postClubListVolley(loginUserId,selectedLocationName);
                                    }
                                    else{
                                        Toast.makeText(getActivity(),"No Internet Connection!",Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });


                        }
                    }else{
                        Toast.makeText(getActivity(),object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }
    private void getLocation(String url) {
        class LocationList extends AsyncTask<String,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading= new ProgressDialog(getActivity());
                loading.setIndeterminate(true);
                //loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animatiion));
                loading.setCancelable(false);
                loading.setMessage("Please wait...!");
                loading.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String uri = params[0];

                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while((json = bufferedReader.readLine())!= null){
                        sb.append(json+"\n");
                    }

                    return sb.toString().trim();

                }catch(Exception e){
                    return null;
                }

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                try {
                    Log.d("location_list",s);

                    JSONObject object=new JSONObject(s);
                    String res=object.getString("response");
                    String msg=object.getString("message");

                    if(res.equals("200")) {

                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){

                        }
                        else{
                            locarrayList = new ArrayList(array.length());
                            for(int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);


                                String location_name=object1.getString("loc");
                                Log.d("name_loca",location_name);
                                locarrayList.add(location_name);

                            }
                            Log.d("location_array", locarrayList.toString());

                            String[] arr1 = (String[])locarrayList.toArray(new String[locarrayList.size()]);


                            final ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(),R.layout.location_spin_item,R.id.loc_txt, arr1);

                            dataAdapter1.setDropDownViewResource(R.layout.location_spin_item);
                            spin_location.setAdapter(dataAdapter1);


                            spin_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                    selectedLocationName=spin_location.getSelectedItem().toString();
                                    //Toast.makeText(getActivity(),selectedLocationName, Toast.LENGTH_LONG).show();
                                    //now calling the list of clubs
                                    String url=AllUrl.CLUB_LIST+loginUserId+"&loc="+selectedLocationName;
                                    clubListUrl=url.replaceAll(" ","%20");
                                    Log.d("list_of_club_url",clubListUrl);
                                    boolean flag=hasConnection();
                                    if(flag){
                                       // settingClubList(clubListUrl);
                                       // postClubList(loginUserId,selectedLocationName);
                                        postClubListVolley(loginUserId,selectedLocationName);
                                    }
                                    else{
                                        Toast.makeText(getActivity(),"No Internet Connection!",Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });


                        }
                    }

                    else{
                        Toast.makeText(getActivity(),msg,Toast.LENGTH_LONG).show();
                    }
                }catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }
        LocationList loc = new LocationList();
        loc.execute(url);
    }


    private void settingClubList(String search_url) {
        class ClubList extends AsyncTask<String, Void, String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading= new ProgressDialog(getActivity());
                loading.setIndeterminate(true);
                // loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animatiion));
                loading.setCancelable(false);
                loading.setMessage("Please wait...!");
                loading.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String uri = params[0];

                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while((json = bufferedReader.readLine())!= null){
                        sb.append(json+"\n");
                    }

                    return sb.toString().trim();

                }catch(Exception e){
                    return null;
                }

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Log.d("club_list_response",s);



                try {
                    JSONObject jsonObject = new JSONObject(s);
                    List<ClubModel> data = new ArrayList<>();
                    bannerData=new ArrayList<>();
                    bannerEventData=new ArrayList<>();
                    String response = jsonObject.getString("response");
                    JSONArray array = jsonObject.getJSONArray("message");
                    if (array.length() == 0) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setMessage("No data found for your search.");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();


                                        // finish();
                                    }
                                });
                        alert = builder1.create();
                        alert.show();
                    } else {
                        for(int i=0;i<array.length();i++){
                            JSONObject object = array.getJSONObject(i);

                            JSONArray club_array = object.getJSONArray("club");
                            JSONArray banner_array = object.getJSONArray("banner");
                            if(club_array.length()==0){
                                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
                                builder.setMessage("No club found !")
                                        .setCancelable(false)
                                        .setPositiveButton("OK",   new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //do things
                                                dialog.dismiss();
                                            }
                                        });
                                android.support.v7.app.AlertDialog alert = builder.create();
                                alert.show();

                            }
                            else{
                                for(int j=0;j<club_array.length();j++){
                                    JSONObject object1 = club_array.getJSONObject(j);

                                    ClubModel model = new ClubModel();

                                    model.club_id=object1.getString("club_id");
                                    model.club_name=object1.getString("club_name");
                                    Log.d("club_fragment_name",object1.getString("club_name"));
                                    model.club_date=object1.getString("date");
                                    model.club_img=object1.getString("picture");
                                    model.club_total=object1.getString("total");
                                    model.club_fav=object1.getString("favourite");
                                    model.club_male=object1.getString("male");
                                    model.club_total_ratio=object1.getString("total_ratio");
                                    model.club_female=object1.getString("female");


                                    data.add(model);
                                }

                                for(int h=0;h<data.size();h++){
                                    String abc= String.valueOf(data.get(h));
                                    Log.d("value_of_data",abc);
                                }

                                clubAdapter=new ClubRecyclerAdapter(data,getActivity());
                                clubRecycler.setAdapter(clubAdapter);


                            }

                            //banner
                            if(banner_array.length()==0){
                                bannerRel.setVisibility(View.GONE);
                            }
                            else{
                                bannerRel.setVisibility(View.VISIBLE);
                                for (int k=0;k<banner_array.length();k++){
                                    JSONObject object2 = banner_array.getJSONObject(k);
                                    BannerModel bannerModel=new BannerModel();

                                    BannerModel.banner_img =object2.getString("banner_img");
                                    String ban_name=object2.getString("banner_img");
                                    bannerData.add(ban_name);



                                }
                                Log.d("banner_images",bannerData.toString());
                                tutorialSlider();


                            }
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
        ClubList c_list = new ClubList();
        c_list.execute(search_url);


    }


    //post
    //post method
    private void postClubListVolley(final String user_id, final String loc){
        StringRequest postRequest = new StringRequest(Request.Method.POST,AllUrl.CLUB_LIST,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("club_list_response", response);
                       // loading.dismiss();
                        try {
                         //   loading.dismiss();
                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("value_of_club_response", response);
                            bannerData=new ArrayList<>();
                            bannerEventData=new ArrayList<>();
                            bannerClubData=new ArrayList<>();
                            String res = jsonObject.getString("response");
                            JSONArray array = jsonObject.getJSONArray("message");
                            if (array.length() == 0) {
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                                builder1.setMessage("No data found for your search.");
                                builder1.setCancelable(false);
                                builder1.setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();


                                                // finish();
                                            }
                                        });
                                alert = builder1.create();
                                alert.show();
                            } else {
                                data = new ArrayList<ClubModel>();
                                for(int i=0;i<array.length();i++){
                                    JSONObject object = array.getJSONObject(i);

                                    JSONArray club_array = object.getJSONArray("club");
                                    JSONArray banner_array = object.getJSONArray("banner");
                                    if(club_array.length()==0){

                                    }
                                    else{
                                        for(int j=0;j<club_array.length();j++){
                                            JSONObject object1 = club_array.getJSONObject(j);

                                            ClubModel model = new ClubModel();

                                            model.club_id=object1.getString("club_id");
                                            model.club_name=object1.getString("club_name");
                                            model.club_date=object1.getString("date");
                                            model.club_img=object1.getString("picture");
                                            model.club_total=object1.getString("total");
                                            model.club_fav=object1.getString("favourite");
                                            model.club_male=object1.getString("male");
                                            model.club_total_ratio=object1.getString("total_ratio");
                                            model.club_female=object1.getString("female");
                                            Log.d("club_data",object1.getString("club_name"));


                                            data.add(model);
                                        }

                                        for(int h=0;h<data.size();h++){
                                            String abc= String.valueOf(data.get(h));
                                            Log.d("value_of_data",abc);
                                        }

                                        clubAdapter=new ClubRecyclerAdapter(data,getActivity());
                                        clubRecycler.setAdapter(clubAdapter);


                                    }

                                    //banner
                                    if(banner_array.length()==0){

                                        bannerRel.setVisibility(View.GONE);
                                    }
                                    else{
                                        bannerRel.setVisibility(View.VISIBLE);
                                        for (int k=0;k<banner_array.length();k++){
                                            JSONObject object2 = banner_array.getJSONObject(k);
                                            BannerModel bannerModel=new BannerModel();

                                            BannerModel.banner_img =object2.getString("banner_img");
                                            String ban_name=object2.getString("banner_img");
                                            BannerModel.banner_event_id =object2.getString("banner_event");
                                            String event =object2.getString("banner_event");

                                            String club=object2.getString("banner_club");

                                            bannerData.add(ban_name);
                                            bannerEventData.add(event);
                                            bannerClubData.add(club);

                                        }
                                        Log.d("banner_images",bannerData.toString());
                                        tutorialSlider();


                                    }
                                }




                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        loading.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Log.d("post_volley","entered in getParams");
                Map<String, String>  data = new HashMap<>();
                // the POST parameters:

                data.put("user_id",user_id);
                data.put("loc",loc);


                return data;
            }

        };

        Volley.newRequestQueue(getActivity()).getCache().clear();

        Volley.newRequestQueue(getActivity()).add(postRequest);
      /*  loading= new ProgressDialog(getActivity());
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();*/

    }
    private void postClubList(String user_id,String loc){
        class fav_club extends AsyncTask<String, Void, String>{
            ProgressDialog loading;
            RequestHandler ruc = new RequestHandler();
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading= new ProgressDialog(getActivity());
                loading.setIndeterminate(true);
                // loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animatiion));
                loading.setCancelable(false);
                loading.setMessage("Please wait...!");
                loading.show();

            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(s);

                    Log.d("value_of_club_response",s);
                    bannerData=new ArrayList<>();
                    bannerEventData=new ArrayList<>();
                    bannerClubData=new ArrayList<>();
                    String response = jsonObject.getString("response");
                    JSONArray array = jsonObject.getJSONArray("message");
                    if (array.length() == 0) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setMessage("No data found for your search.");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();


                                        // finish();
                                    }
                                });
                        alert = builder1.create();
                        alert.show();
                    } else {
                        data = new ArrayList<ClubModel>();
                        for(int i=0;i<array.length();i++){
                            JSONObject object = array.getJSONObject(i);

                            JSONArray club_array = object.getJSONArray("club");
                            JSONArray banner_array = object.getJSONArray("banner");
                            if(club_array.length()==0){

                            }
                            else{
                                for(int j=0;j<club_array.length();j++){
                                    JSONObject object1 = club_array.getJSONObject(j);

                                    ClubModel model = new ClubModel();

                                    model.club_id=object1.getString("club_id");
                                    model.club_name=object1.getString("club_name");
                                    model.club_date=object1.getString("date");
                                    model.club_img=object1.getString("picture");
                                    model.club_total=object1.getString("total");
                                    model.club_fav=object1.getString("favourite");
                                    model.club_male=object1.getString("male");
                                    model.club_total_ratio=object1.getString("total_ratio");
                                    model.club_female=object1.getString("female");
                                    Log.d("club_data",object1.getString("club_name"));


                                    data.add(model);
                                }

                                for(int h=0;h<data.size();h++){
                                    String abc= String.valueOf(data.get(h));
                                    Log.d("value_of_data",abc);
                                }

                                clubAdapter=new ClubRecyclerAdapter(data,getActivity());
                                clubRecycler.setAdapter(clubAdapter);


                            }

                            //banner
                            if(banner_array.length()==0){

                                bannerRel.setVisibility(View.GONE);
                            }
                            else{
                                bannerRel.setVisibility(View.VISIBLE);
                                for (int k=0;k<banner_array.length();k++){
                                    JSONObject object2 = banner_array.getJSONObject(k);
                                    BannerModel bannerModel=new BannerModel();

                                    BannerModel.banner_img =object2.getString("banner_img");
                                    String ban_name=object2.getString("banner_img");
                                    BannerModel.banner_event_id =object2.getString("banner_event");
                                    String event =object2.getString("banner_event");

                                    String club=object2.getString("banner_club");

                                    bannerData.add(ban_name);
                                    bannerEventData.add(event);
                                    bannerClubData.add(club);

                                }
                                Log.d("banner_images",bannerData.toString());
                                tutorialSlider();


                            }
                        }




                    }
                   /* //notification
                    mRegistrationBroadcastReceiver=new BroadcastReceiver() {
                        @Override
                      //  public void onReceive(Context context, Intent intent) {
                        public void onReceive(Context context, Intent intent) {
                            // checking for type intent filter
                            if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                                // gcm successfully registered
                                // now subscribe to `global` topic to receive app wide notifications
                                FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                                displayFirebaseRegId();

                            } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                                // new push notification is received

                                String message = intent.getStringExtra("message");

                                Toast.makeText(getActivity(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                               // txtMessage.setText(message);
                            }
                        }
                    };

                    displayFirebaseRegId();*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... params) {
                HashMap<String, String> data = new HashMap<String,String>();


                data.put("user_id",params[0]);
                data.put("loc",params[1]);



                String result = ruc.sendPostRequest(AllUrl.CLUB_LIST,data);

                return  result;
            }
        }
        fav_club reg = new fav_club();
        reg.execute( user_id,loc);

    }


    //end of post

    public void tutorialSlider(){
        // method for initialisation

        init();

        // method for adding indicators
        addBottomDots(0);

        final Handler handler = new Handler();

        final Runnable update = new Runnable() {
            public void run() {
                if (page_position == bannerData.size()) {
                    page_position = 0;
                } else {
                    page_position = page_position + 1;
                }
                banner_slider.setCurrentItem(page_position, true);
            }
        };


    }/*  new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 100, 9000);*/

    private void init() {

        banner_slider = getActivity().findViewById(R.id.banner_slider);
        lin_dots = getActivity(). findViewById(R.id.dots);

        //bannerList = new ArrayList<BannerModel>();

//Add few items to slider_image_list ,this should contain url of images which should be displayed in slider
// here i am adding few sample image links, you can add your own

        //bannerList.add(new BannerModel("http://rednwhite.co.in/images/banner_ads/banner.jpg"));




//        bannerAdapter = new BannerAdapter(getActivity(),bannerList);
        bannerAdapter = new BannerAdapter(getActivity(),bannerData,bannerEventData,bannerClubData);
        banner_slider.setAdapter(bannerAdapter);
        indicator.setViewPager(banner_slider);

        banner_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[bannerData.size()];

        lin_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(getActivity());
            //dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setText(Html.fromHtml(" &#8226&nbsp;"));
            dots[i].setTextSize(30);
            // dots[i].setTextColor(Color.parseColor("#000000"));
            dots[i].setTextColor(getResources().getColor(R.color.transparent));
            lin_dots.addView(dots[i]);
        }

        if (dots.length > 0)

            dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
            //dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
           // dots[currentPage].setTextColor(getResources().getColor(R.color.light_green));
    }


    //notification display fcm id
    private void displayFirebaseRegId() {
        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e("firebase_id", "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId)) {
          //  txtRegId.setText("Firebase Reg Id: " + regId);
          //  txtRegId.setVisibility(View.GONE);
            String deviceurl=AllUrl.DEVICEID+loginUserId+"&device_id="+regId;
            Log.d("deviceurl",deviceurl);
            userDeviceId(deviceurl);
        }
        else{
            Toast.makeText(getActivity(),"Firebase Reg Id is not received yet!",Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getActivity());
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private void userDeviceId(String url) {
        class UserDeviceIdClass extends AsyncTask<String,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading= new ProgressDialog(getActivity());
                loading.setIndeterminate(true);
                // loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.select_home));
                loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
                loading.setCancelable(false);
                loading.setMessage("Please wait...!");
                loading.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String uri = params[0];

                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while((json = bufferedReader.readLine())!= null){
                        sb.append(json+"\n");
                    }

                    return sb.toString().trim();

                }catch(Exception e){
                    return null;
                }

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                try {
                    Log.d("user_device_id_response",s);

                    JSONObject object=new JSONObject(s);
                    String res=object.getString("response");
                    String msg=object.getString("message");

                    if(res.equals("200")) {

                      Log.d("device","sucessfully");
                    }

                    else{
                        Toast.makeText(getActivity(),msg,Toast.LENGTH_LONG).show();
                    }
                }catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }
        UserDeviceIdClass gj = new UserDeviceIdClass();
        gj.execute(url);
    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }


}
