package com.nv.admin.nitevibe.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;

public class Help extends AppCompatActivity implements View.OnClickListener {
    public static TextView  label,txt_data;
    public static ImageView back;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face;
    public static WebView help_web;
    public ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        label= findViewById(R.id.title);
        txt_data= findViewById(R.id.txt_data);
        back= findViewById(R.id.back);
       /// help_web=(WebView)findViewById(R.id.webhelp);
        help_web= findViewById(R.id.webhelp);

        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");

        //help_web.setVerticalScrollBarEnabled(false);
        help_web.setScrollContainer(false);
        WebSettings webSettings = help_web.getSettings();
        webSettings.setJavaScriptEnabled(true);
        // help_web.clearHistory();

        progressBar= new ProgressDialog(Help.this,R.style.MyAlertDialogStyle);
        progressBar.setIndeterminate(true);
        progressBar.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        progressBar.setCancelable(false);
        progressBar.setMessage("Please wait...!");
        progressBar.show();

        help_web.loadUrl(AllUrl.HELP);

        help_web.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String request) {
                Log.d("legal","shouldOverrideUrlLoading");
                //view.loadUrl(request);
                if (request.startsWith("https://play.google.com/store/apps/details")){
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                    //return false;
                }
                //else{

                    return true;
               // }


            }


            @Override
            public void onPageFinished(WebView view, String url) {
                Log.d("legal","page_finished");
                super.onPageFinished(view, url);
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }

            }


        });

        setClickEvent();
        setFont();

    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        if(id==R.id.back){
            startActivity(new Intent(this,Setting.class));
        }

    }
    public void setClickEvent(){
        back.setOnClickListener(this);
    }

    public void setFont(){
        label.setTypeface(bold_face);
        txt_data.setTypeface(semi_bold_face);
    }
}
