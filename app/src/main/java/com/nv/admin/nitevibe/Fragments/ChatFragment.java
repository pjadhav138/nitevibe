package com.nv.admin.nitevibe.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.Activity.SimpleDividerItemDecoration;
import com.nv.admin.nitevibe.Adapter.ChatListAdapter;
import com.nv.admin.nitevibe.Adapter.ChatListModel;
import com.nv.admin.nitevibe.Adapter.MatchListAdapter;
import com.nv.admin.nitevibe.Adapter.MatchProfileModel;
import com.nv.admin.nitevibe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 3/10/2018.
 */
public class ChatFragment  extends Fragment implements View.OnClickListener {
    private android.support.v4.app.Fragment fragment;
    private android.support.v4.app.FragmentManager fragmentManager;
    TextView swipe_label,event_near_label;
    AlertDialog alert;
    RelativeLayout emptyChat,list_main,matchRel,chatRel;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    TextView match_label,chat_label;
    RecyclerView matchRecycler,chatRecycler;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    List<MatchProfileModel> matchList;
    List<ChatListModel> chatList;
    MatchListAdapter matchAdapter;
    ChatListAdapter chatAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    ProgressDialog loading;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat, container, false);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        sharedpreferences=getActivity().getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);

        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        swipe_label= view.findViewById(R.id.chat_label);
        event_near_label= view.findViewById(R.id.txt_find_near);
        match_label= view.findViewById(R.id.match_txt);
        chat_label= view.findViewById(R.id.chat_txt);

        emptyChat= view.findViewById(R.id.emptychatrel); //empty chat
        list_main= view.findViewById(R.id.list_main_rel); //list of chat
        matchRel= view.findViewById(R.id.matchrel);
        chatRel= view.findViewById(R.id.chatrel);

        matchRecycler= view.findViewById(R.id.matchrecycler);
        matchRecycler.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        matchRecycler.setLayoutManager(mLayoutManager);


        chatRecycler= view.findViewById(R.id.chatlistrecycler);
        chatRecycler.setHasFixedSize(true);
        chatRecycler.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        chatRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));//Linear Items


        setFont();
        clickEvent();

        boolean flag=hasConnection();
        if(flag){
            String url= AllUrl.CHAT_LIST+loginUserId;
            String url1=url.replaceAll(" ","%20");
           // getList(url1);
            getListVolley(url1);

        }
        else{
            Toast.makeText(getActivity(),"No internet connection",Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        if(id==R.id.txt_find_near){
            fragmentManager =getActivity(). getSupportFragmentManager();
            fragment=new ClubFragment();
            final android.support.v4.app.FragmentTransaction transaction1 = fragmentManager.beginTransaction();
            transaction1.replace(R.id.rootLayout, fragment).commit();
            transaction1.addToBackStack(null);

        }


    }
    public void clickEvent(){
        event_near_label.setOnClickListener(this);
    }
    public void setFont(){
        bold_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getContext().getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getContext().getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        swipe_label.setTypeface(semi_bold_face);
        event_near_label.setTypeface(semi_bold_italic);
        match_label.setTypeface(bold_face);
        chat_label.setTypeface(bold_face);
    }


    private void getListVolley(String url){
        loading= new ProgressDialog(getActivity());
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("get_match_response", response.toString());

                try {
                    loading.dismiss();
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){

                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){
                            emptyChat.setVisibility(View.VISIBLE);
                            list_main.setVisibility(View.GONE);
                        }
                        else{
                            for(int j=0;j<array.length();j++){
                                JSONObject object11 = array.getJSONObject(j);
                                emptyChat.setVisibility(View.GONE);
                                list_main.setVisibility(View.VISIBLE);

                                chatList=new ArrayList<ChatListModel>();
                                matchList=new ArrayList<MatchProfileModel>();

                                ///match array
                                JSONArray matcharray=object11.getJSONArray("match");
                                if(matcharray.length()==0){
                                    matchRel.setVisibility(View.GONE);
                                }
                                else{
                                    matchRel.setVisibility(View.VISIBLE);
                                    String match_count= String.valueOf(matcharray.length());
                                    match_label.setText("NEW MATCHES("+match_count+")");
                                    for (int i=0;i<matcharray.length();i++){
                                        JSONObject object1 = matcharray.getJSONObject(i);
                                        MatchProfileModel matchModel=new MatchProfileModel();

                                        matchModel.match_id=object1.getString("match_id");
                                        matchModel.match_name=object1.getString("name");
                                        matchModel.match_profile=object1.getString("profile");
                                        matchModel.event=object1.getString("event");

                                        matchList.add(matchModel);

                                    }//end of for
                                    matchAdapter=new MatchListAdapter(matchList,getActivity());
                                    matchRecycler.setAdapter(matchAdapter);
                                }

                                //end of match array

                                //chat array
                                JSONArray chatarray=object11.getJSONArray("chat");

                                if(chatarray.length()==0){
                                    chatRel.setVisibility(View.GONE);
                                }
                                else{
                                    chatRel.setVisibility(View.VISIBLE);
                                    String  chat_count= String.valueOf(chatarray.length());
                                    chat_label.setText("CHATS("+chat_count+")");
                                    for(int i=0;i<chatarray.length();i++){
                                        JSONObject object2=chatarray.getJSONObject(i);
                                        ChatListModel chatModel=new ChatListModel();


                                        chatModel.id=object2.getString("match_id");
                                        chatModel.name=object2.getString("name");
                                        chatModel.profile=object2.getString("profile");
                                        chatModel.occupation=object2.getString("occupation");

                                        chatList.add(chatModel);

                                    }
                                    chatAdapter=new ChatListAdapter(chatList,getActivity());
                                    chatRecycler.setAdapter(chatAdapter);
                                }

                            }



                        }
                    }else{
                        Toast.makeText(getActivity(),object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }


    //getting the chat and matched list
    private void getList(String url) {
        class ChatListClass extends AsyncTask<String,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading= new ProgressDialog(getActivity());
                loading.setIndeterminate(true);
                //loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animatiion));
                loading.setCancelable(false);
                loading.setMessage("Please wait...!");
                loading.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String uri = params[0];

                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while((json = bufferedReader.readLine())!= null){
                        sb.append(json+"\n");
                    }

                    return sb.toString().trim();

                }catch(Exception e){
                    return null;
                }

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                try {
                    Log.d("match_chat_list",s);

                    JSONObject object=new JSONObject(s);
                    String res=object.getString("response");
                    String msg=object.getString("message");

/*
                    emptyChat=(RelativeLayout)view.findViewById(R.id.emptychatrel); //empty chat
                    list_main=(RelativeLayout)view.findViewById(R.id.list_main_rel); //list of chat
                    matchRel=(RelativeLayout)view.findViewById(R.id.matchrel);
                    chatRel=(RelativeLayout)view.findViewById(R.id.chatrel);
*/


                    if(res.equals("200")) {

                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){
                            emptyChat.setVisibility(View.VISIBLE);
                            list_main.setVisibility(View.GONE);
                        }
                        else{
                            for(int j=0;j<array.length();j++){
                                JSONObject object11 = array.getJSONObject(j);
                                emptyChat.setVisibility(View.GONE);
                                list_main.setVisibility(View.VISIBLE);

                                chatList=new ArrayList<ChatListModel>();
                                matchList=new ArrayList<MatchProfileModel>();

                                ///match array
                                JSONArray matcharray=object11.getJSONArray("match");
                                if(matcharray.length()==0){
                                    matchRel.setVisibility(View.GONE);
                                }
                                else{
                                    matchRel.setVisibility(View.VISIBLE);
                                    String match_count= String.valueOf(matcharray.length());
                                            match_label.setText("NEW MATCHES("+match_count+")");
                                    for (int i=0;i<matcharray.length();i++){
                                        JSONObject object1 = matcharray.getJSONObject(i);
                                        MatchProfileModel matchModel=new MatchProfileModel();

                                        matchModel.match_id=object1.getString("match_id");
                                        matchModel.match_name=object1.getString("name");
                                        matchModel.match_profile=object1.getString("profile");
                                        matchModel.event=object1.getString("event");

                                        matchList.add(matchModel);

                                    }//end of for
                                    matchAdapter=new MatchListAdapter(matchList,getActivity());
                                    matchRecycler.setAdapter(matchAdapter);
                                }

                                //end of match array

                                //chat array
                                JSONArray chatarray=object11.getJSONArray("chat");

                                if(chatarray.length()==0){
                                    chatRel.setVisibility(View.GONE);
                                }
                                else{
                                    chatRel.setVisibility(View.VISIBLE);
                                    String  chat_count= String.valueOf(chatarray.length());
                                    chat_label.setText("CHATS("+chat_count+")");
                                    for(int i=0;i<chatarray.length();i++){
                                        JSONObject object2=chatarray.getJSONObject(i);
                                        ChatListModel chatModel=new ChatListModel();


                                        chatModel.id=object2.getString("match_id");
                                        chatModel.name=object2.getString("name");
                                        chatModel.profile=object2.getString("profile");
                                        chatModel.occupation=object2.getString("occupation");

                                        chatList.add(chatModel);

                                    }
                                    chatAdapter=new ChatListAdapter(chatList,getActivity());
                                    chatRecycler.setAdapter(chatAdapter);
                                }

                            }



                        }
                    }

                    else{
                        Toast.makeText(getActivity(),msg,Toast.LENGTH_LONG).show();
                    }
                }catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }
        ChatListClass list1 = new ChatListClass();
        list1.execute(url);
    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)getActivity(). getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

}
