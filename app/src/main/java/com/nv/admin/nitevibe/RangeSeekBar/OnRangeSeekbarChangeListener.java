package com.nv.admin.nitevibe.RangeSeekBar;

/**
 * Created by Admin on 3/8/2018.
 */
public interface OnRangeSeekbarChangeListener {
    void valueChanged(Number minValue, Number maxValue);
}
