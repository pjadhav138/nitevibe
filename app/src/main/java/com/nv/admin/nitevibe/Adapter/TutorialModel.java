package com.nv.admin.nitevibe.Adapter;

/**
 * Created by Admin on 3/24/2018.
 */
public class TutorialModel {
    public String title,text;
    public int image;


    public TutorialModel(String title, String text, int image) {
        this.title = title;
        this.text = text;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "TutorialModel{" +
                "title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", image=" + image +
                '}';
    }
}
