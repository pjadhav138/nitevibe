package com.nv.admin.nitevibe.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nv.admin.nitevibe.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 3/24/2018.
 */
public class ProfileSliderAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    Activity activity;
    ArrayList<ProfileModel> image_arraylist;

    public ProfileSliderAdapter(ArrayList<ProfileModel> image_arraylist, Activity activity) {
        this.image_arraylist = image_arraylist;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return image_arraylist.size();
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.profile_slider_row, container, false);

        ImageView profile_img= view.findViewById(R.id.profile_img);


        ProfileModel model=image_arraylist.get(position);

        Picasso.with(activity.getApplicationContext())
                .load(model.profileImg)
                .into(profile_img);


        container.addView(view);

        return view;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }

}
