package com.nv.admin.nitevibe.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Adapter.GoingSwipeAdapter;
import com.nv.admin.nitevibe.Adapter.Mainprofile;
import com.nv.admin.nitevibe.Adapter.Subprofile;
import com.nv.admin.nitevibe.R;
import com.nv.admin.nitevibe.custom.CardStackView;
import com.nv.admin.nitevibe.custom.SwipeDirection;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import de.hdodenhof.circleimageview.CircleImageView;

public class WhosGoingSwipe extends AppCompatActivity {
    private ProgressBar progressBar;
    public static CardStackView cardStackView;
    public static List<Mainprofile> profiles;
    public static List<Subprofile> subimages;
    static String user_name;
    static String user_loc;
    static GoingSwipeAdapter adapter;

    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType,eventId,url,supervibescount,url11,clubId,userSelected;
    public PopupWindow popupWindow;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    public RelativeLayout mRelativeLayout;
    public static RelativeLayout emptyRelative;
    public static TextView empty_txt;
    public static ImageView empty_img;
    static JSONArray stack_jsonarray;
    public static Set<String> mids;
    public static ImageView img_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whos_going_swipe);

        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);

        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");
        //value comming from EventAdapter
        eventId=sharedpreferences.getString("event_id","");
        clubId=sharedpreferences.getString("club_id","");
        //value coming from whos adapter
        userSelected=sharedpreferences.getString("user_going_id","");



        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        mRelativeLayout= findViewById(R.id.rel0);
        emptyRelative= findViewById(R.id.empty_cart);
        empty_txt= findViewById(R.id.find_txt);
        empty_img= findViewById(R.id.empty_img);
        img_back= findViewById(R.id.back);

        empty_txt.setTypeface(semi_bold_italic);
        mids=new TreeSet<>();
        url=AllUrl.GOING_SWIPE+loginUserId+"&event_id="+eventId+"&club_id="+clubId+"&selected_id="+userSelected;
        Log.d("going_swipe_card_url",url);


        empty_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(WhosGoingSwipe.this,Home.class));
                startActivity(new Intent(WhosGoingSwipe.this,Club.class));
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(WhosGoingSwipe.this, WhosGoing.class);
                startActivity(i);
            }
        });


        setup();
    }

    private GoingSwipeAdapter createMainprofilestackAdapter() {
        //final Mainprofilestack adapter = new Mainprofilestack(getApplicationContext());
        final GoingSwipeAdapter adapter=new GoingSwipeAdapter(getApplicationContext());
        adapter.addAll(createprofilestacks());
        //adapter.addAll(createprofilestacks_new());

        return adapter;
    }
    public void setup() {
             Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();
            }
        });
        Log.d("cardstack_setup","setup() called");
        progressBar = findViewById(R.id.activity_main_progress_bar);
        cardStackView = findViewById(R.id.activity_main_card_stack_view);
        adapter = new GoingSwipeAdapter(getApplicationContext());
        cardStackView.setAdapter(createMainprofilestackAdapter());
        //  cardStackView.setSwipeEnabled(false);
        cardStackView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        //  cardStackView.setSwipeDirection(SwipeDirection.HORIZONTAL);
        cardStackView.setCardEventListener(new CardStackView.CardEventListener() {

            @Override
            public void onCardDragging(float percentX, float percentY) {
                Log.e("CardStackView", "onCardDragging");
                  Log.e("CardStackView", "topIndex: " + cardStackView.getTopIndex());
                Log.d("value_of_card_draging",percentX+" "+percentY);

            }

            @Override
            public void onCardSwiped(SwipeDirection direction) {
                MyApplication.getInstance().trackEvent("Whos Going Profile Swipe", "Swipe", direction.toString());

                try{
                    Log.d("CardStackView", "onCardSwiped: " + direction.toString());
                    Log.d("CardStackView", "topIndex: " + cardStackView.getTopIndex());

                    Log.e("CardStackView", "onCardSwiped: " + direction.toString());
                    Log.e("CardStackView", "topIndex: " + cardStackView.getTopIndex());
                    Log.d("top_index_card", String.valueOf(cardStackView.getTopIndex()));
                    Log.d("array_leng", String.valueOf(stack_jsonarray.length()+1));

                    // String abc = profiles.get(cardStackView.getTopIndex() - 1).m_id;
                    String abc = adapter.getItem(cardStackView.getTopIndex()-1).m_id;
                    Log.d("user_id",abc);

                    int card=cardStackView.getTopIndex();
                    int array_len=stack_jsonarray.length()+1;

                    // if(cardStackView.getTopIndex()==stack_jsonarray.length()+1){

                    //check usercategory

                    String usercategory=adapter.getItem(cardStackView.getTopIndex()-1).category;
                    Log.d("usercategory",usercategory);

                        if(cardStackView.getTopIndex()==stack_jsonarray.length()){
                            emptyRelative.setVisibility(View.VISIBLE);
                            if (direction.toString().equals("Top")) {
                                mids.add(abc);
                                url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=3&event_id=" + eventId;
                                boolean flag = hasConnection();
                                if (flag) {
                                    insertSwipeData(url11, "vibes");

                                } else {
                                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                                }
                            } else if (direction.toString().equals("Left")) {
                                mids.add(abc);
                                url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=2&event_id=" + eventId;
                                boolean flag = hasConnection();
                                if (flag) {
                                    insertSwipeData(url11, "");



                                } else {
                                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                                }
                            } else if (direction.toString().equals("Right")) {
                                mids.add(abc);
                                url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=1&event_id=" + eventId;
                                boolean flag = hasConnection();
                                if (flag) {
                                    insertSwipeData(url11, "");


                                } else {
                                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                                }
                            }
                            Log.d("goingswipe_url", url11);
                        }else {

                            emptyRelative.setVisibility(View.GONE);



                            if (direction.toString().equals("Top")) {

                                mids.add(abc);
                                url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=3&event_id=" + eventId;
                                boolean flag = hasConnection();
                                if (flag) {
                                    insertSwipeData(url11, "vibes");

                                } else {
                                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                                }
                            } else if (direction.toString().equals("Left")) {
                                mids.add(abc);
                                url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=2&event_id=" + eventId;
                                boolean flag = hasConnection();
                                if (flag) {
                                    insertSwipeData(url11, "");



                                } else {
                                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                                }
                            } else if (direction.toString().equals("Right")) {
                                mids.add(abc);
                                url11 = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + abc + "&category=1&event_id=" + eventId;
                                boolean flag = hasConnection();
                                if (flag) {
                                    insertSwipeData(url11, "");


                                } else {
                                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                                }
                            }
                            Log.d("goingswipe_url", url11);

                        }



                }
                catch (IndexOutOfBoundsException e){
                    e.printStackTrace();
                }


            }

            @Override
            public void onCardReversed() {
                Log.d("CardStackView", "onCardReversed");
            }

            @Override
            public void onCardMovedToOrigin() {
                Log.d("CardStackView", "onCardMovedToOrigin");
            }

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onCardClicked(int index) {
                Log.d("CardStackView", "onCardClicked: " + index +"name : "+profiles.get(index).name);
            }
        });

    }

    private void insertSwipeData(String url123, final String vibes){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url123, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("swipe_userdataresponse", response.toString());
                //  loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){

                        }
                        else{
                            for (int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);
                                String user_id=object1.getString("user_id");
                                String user_photo=object1.getString("user_photo");
                                String match_id=object1.getString("match_id");
                                String match_photo=object1.getString("match_photo");

                                String url=AllUrl.MATCH_NOTIFICATION+loginUserId+"&match_id="+match_id;

                                matchNotification(url);

                                initiatePopupWindow( user_id, user_photo,match_id,match_photo);
                            }
                        }
                        //  createprofilestacks();
                        if(vibes.equals("vibes")){
                            String url=AllUrl.SUPERVIBE_COUNT+loginUserId+"&event_id="+eventId;
                            userSuperVibes(url);
                        }


                    }else{
                        Toast.makeText(WhosGoingSwipe.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                // loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void userSuperVibes(String url){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("check_in_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if(res.equals("200")){
                        createprofilestacks();
                        supervibescount=msg;
                        Log.d("Remaining_count",supervibescount);
                        //  setup();
                    }

                    else{
                        Toast.makeText(WhosGoingSwipe.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }




    private void initiatePopupWindow(String userId,String userPhoto,String matchId,String matchPhoto){
        LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        // Inflate the custom layout/view
        View customView = inflater.inflate(R.layout.profile_match_row,null);


        popupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT
        );

        // Set an elevation value for popup window
        // Call requires API level 21
        if(Build.VERSION.SDK_INT>=21){
            popupWindow.setElevation(5.0f);
        }

        // Get a reference for the custom view close button

      /*  RoundedImageView userImg=(RoundedImageView)customView.findViewById(R.id.girl_profile);
        RoundedImageView matchImg=(RoundedImageView)customView.findViewById(R.id.boy_profile);*/

        CircleImageView userImg= customView.findViewById(R.id.girl_profile);
        CircleImageView matchImg= customView.findViewById(R.id.boy_profile);

        TextView chat= customView.findViewById(R.id.chat);
        TextView swipe= customView.findViewById(R.id.swipe);
        TextView txt= customView.findViewById(R.id.txt);
        RoundedImageView profileimg= customView.findViewById(R.id.profile);

        txt.setTypeface(semi_bold_italic);
        swipe.setTypeface(bold_face);
        chat.setTypeface(bold_face);

        Log.d("userPhoto",userPhoto);
        Log.d("match_photo",matchPhoto);



        if(userPhoto.equals("")){
            Glide.with(getApplicationContext()).load(R.drawable.default_icon).into(userImg);
        }
        else{


           // Glide.with(this).load(userPhoto).into(userImg);
            Glide.with(getApplicationContext()).load(userPhoto).into(userImg);
            //  userImg.setImageBitmap(img1);
            //  matchImg.setImageBitmap(img1);


        }

        if(matchPhoto.equals("")){
            Glide.with(getApplicationContext()).load(R.drawable.default_icon).into(matchImg);
        }
        else{
            //   Bitmap img2 = getBitmapFromURL(matchPhoto);

            Glide.with(this).load(matchPhoto).into(matchImg);


        }

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(WhosGoingSwipe.this,Chat.class));
            }
        });

        swipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);


    }


    public static void paginate() {

        cardStackView.setPaginationReserved();
        adapter.addAll(createprofilestacks());
        adapter.notifyDataSetChanged();
    }
    public static void removeFirst() {
        LinkedList<Mainprofile> mainprofiles = extractRemainingProfiles();
        if (mainprofiles.isEmpty()) {
            return;
        }

        mainprofiles.removeFirst();
        adapter.clear();
        adapter.addAll(mainprofiles);
        adapter.notifyDataSetChanged();
    }

    public static List<Mainprofile> createprofilestacks()  {

        subimages = new ArrayList<Subprofile>();
        profiles = new ArrayList<>();
      //  subimages.clear();
      //  profiles.clear();
        if(mids.size()!=0){

           	            String csv= mids.toString().replace("[","").replace("]","").replace(" ","");
                       Log.d("csv",csv);

          	            url=AllUrl.GOING_SWIPE+loginUserId+"&event_id="+eventId+"&club_id="+clubId+"&selected_id="+"" + "&visited="+csv;
            	            Log.d("csv_url",url);

                   }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                Log.d("goingswipecard_response", response.toString());

                Log.d("going_swipe_response", response.toString());


                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){

                        JSONArray array=object.getJSONArray("message");
                        Log.d("length_of_card", String.valueOf(array.length()));

                        if(array.length()==0){
                           // emptyRelative.setVisibility(View.VISIBLE);
                        }
                        else{
                            for(int i=0;i<array.length();i++){
                              //  Mainprofile mainprofile = new Mainprofile();
                                JSONObject jsonObject123 = array.getJSONObject(i);
                              //  JSONArray goingArray = jsonObject123.getJSONArray("user_going");
                                stack_jsonarray = jsonObject123.getJSONArray("user_going");
                                Log.d("length_of_goingcard", String.valueOf(stack_jsonarray.length()));
                                if(stack_jsonarray.length()==0){
                                    emptyRelative.setVisibility(View.VISIBLE);
                                }
                                else{
                                    emptyRelative.setVisibility(View.GONE);
                                    for(int a=0;a<stack_jsonarray.length();a++){
                                        Mainprofile mainprofile = new Mainprofile();
                                        JSONObject jsonObject = stack_jsonarray.getJSONObject(a);

                                        String userid = jsonObject.getString("going_user_id");
                                        mainprofile.setM_id(jsonObject.getString("going_user_id"));
                                        user_name = jsonObject.getString("name");
                                        mainprofile.setName(jsonObject.getString("name"));
                                        String gender=jsonObject.getString("gender");
                                        String user_age=jsonObject.getString("age");
                                        mainprofile.setAge(jsonObject.getString("age"));
                                        user_loc = jsonObject.getString("city");
                                        mainprofile.setCity(jsonObject.getString("city"));
                                        mainprofile.setCategory(jsonObject.getString("category"));
                                        String userCategory1=jsonObject.getString("category");
                                        String profile_pic123=jsonObject.getString("profile_url");
                                        mainprofile.setProfile_url(jsonObject.getString("profile_url"));
                                        mainprofile.setProfile_url(jsonObject.getString("profile_url"));


                                        String user_occupation = jsonObject.getString("occupation");
                                        mainprofile.setJob(jsonObject.getString("occupation"));
                                        String user_bio=jsonObject.getString("about");
                                        String superlikes = jsonObject.getString("superlike_count");
                                        mainprofile.setSupervibes(jsonObject.getString("superlike_count"));

                                        if(superlikes.equals("3")){
                                            cardStackView.setSwipeDirection(SwipeDirection.HORIZONTAL);
                                        }

                                        // String user_drink= jsonObject.getString("drinks");
                                        JSONArray photoarray = jsonObject.getJSONArray("photos");
                                        Log.d("kength_od_pic", String.valueOf(photoarray.length()));
                                        Log.d("user_id",userid);
                                        for(int j=0;j<photoarray.length();j++){
                                            Subprofile subprofile = new Subprofile();
                                            subprofile.setImg(photoarray.getJSONObject(j).getString("img"));
                                            subprofile.setImg_id(photoarray.getJSONObject(j).getString("id"));
                                            subimages.add(subprofile);
                                            mainprofile.setSubimage(subimages);
                                        }

                                   /* JSONArray eventarray = jsonObject.getJSONArray("event");
                                    for(int j=0;j<eventarray.length();j++){
                                        JSONObject jsonObject1 = eventarray.getJSONObject(j);
                                        String event_id = jsonObject1.getString("event_id");
                                        String event_name = jsonObject1.getString("event_name");
                                        // String event_date=jsonObject1.getString("event_date");
                                    }*/



                                        mainprofile.setUrl(subimages.get(0));
                                        profiles.add(mainprofile);
                                    }
                                    adapter.clear();
                                    adapter.addAll(profiles);
                                    cardStackView.setAdapter(adapter);

                                }

                            }



                        /*    adapter.clear();
                            adapter.addAll(profiles);
                            cardStackView.setAdapter(adapter);*/

                          /*  for(int p=0;p<profiles.size();p++){
                                Log.d("profile_list", String.valueOf(profiles.get(p)));
                                Log.d("sc", Arrays.toString(profiles);
                            }*/
                        }


                    }else{
                        //Toast.makeText(Swipe.this,object.getString("message"),Toast.LENGTH_LONG).show();

                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                    // Toast.makeText(Swipe.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
      /*  Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();
            }
        });*/
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
        return profiles;

    }

    public  List<Mainprofile> createprofilestacks_new()  {

        subimages = new ArrayList<Subprofile>();
        profiles = new ArrayList<>();
        //  subimages.clear();
        //  profiles.clear();
        if(mids.size()!=0){

            String csv= mids.toString().replace("[","").replace("]","").replace(" ","");
            Log.d("csv",csv);

            url=AllUrl.GOING_SWIPE+loginUserId+"&event_id="+eventId+"&club_id="+clubId+"&selected_id="+"" + "&visited="+csv;
            Log.d("csv_url",url);

        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                Log.d("goingswipecard_response", response.toString());

                Log.d("going_swipe_response", response.toString());


                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){

                        JSONArray array=object.getJSONArray("message");
                        Log.d("length_of_card", String.valueOf(array.length()));

                        if(array.length()==0){
                            // emptyRelative.setVisibility(View.VISIBLE);
                        }
                        else{
                            for(int i=0;i<array.length();i++){
                                //  Mainprofile mainprofile = new Mainprofile();
                                JSONObject jsonObject123 = array.getJSONObject(i);
                                //  JSONArray goingArray = jsonObject123.getJSONArray("user_going");
                                stack_jsonarray = jsonObject123.getJSONArray("user_going");
                                Log.d("length_of_goingcard", String.valueOf(stack_jsonarray.length()));
                                if(stack_jsonarray.length()==0){
                                    emptyRelative.setVisibility(View.VISIBLE);
                                }
                                else{
                                    emptyRelative.setVisibility(View.GONE);
                                    for(int a=0;a<stack_jsonarray.length();a++){
                                        Mainprofile mainprofile = new Mainprofile();
                                        JSONObject jsonObject = stack_jsonarray.getJSONObject(a);

                                        String userid = jsonObject.getString("going_user_id");
                                        mainprofile.setM_id(jsonObject.getString("going_user_id"));
                                        user_name = jsonObject.getString("name");
                                        mainprofile.setName(jsonObject.getString("name"));
                                        String gender=jsonObject.getString("gender");
                                        String user_age=jsonObject.getString("age");
                                        mainprofile.setAge(jsonObject.getString("age"));
                                        user_loc = jsonObject.getString("city");
                                        mainprofile.setCity(jsonObject.getString("city"));
                                        mainprofile.setCategory(jsonObject.getString("category"));
                                        String profile_pic123=jsonObject.getString("profile_url");
                                        mainprofile.setProfile_url(jsonObject.getString("profile_url"));
                                        mainprofile.setProfile_url(jsonObject.getString("profile_url"));




                                        String user_occupation = jsonObject.getString("occupation");
                                        mainprofile.setJob(jsonObject.getString("occupation"));
                                        String user_bio=jsonObject.getString("about");
                                        String superlikes = jsonObject.getString("superlike_count");
                                        mainprofile.setSupervibes(jsonObject.getString("superlike_count"));

                                        if(superlikes.equals("3")){
                                            cardStackView.setSwipeDirection(SwipeDirection.HORIZONTAL);
                                        }

                                        // String user_drink= jsonObject.getString("drinks");
                                        JSONArray photoarray = jsonObject.getJSONArray("photos");
                                        for(int j=0;j<photoarray.length();j++){
                                            Subprofile subprofile = new Subprofile();
                                            subprofile.setImg(photoarray.getJSONObject(j).getString("img"));
                                            subprofile.setImg_id(photoarray.getJSONObject(j).getString("id"));
                                            subimages.add(subprofile);
                                            mainprofile.setSubimage(subimages);
                                        }

                                   /* JSONArray eventarray = jsonObject.getJSONArray("event");
                                    for(int j=0;j<eventarray.length();j++){
                                        JSONObject jsonObject1 = eventarray.getJSONObject(j);
                                        String event_id = jsonObject1.getString("event_id");
                                        String event_name = jsonObject1.getString("event_name");
                                        // String event_date=jsonObject1.getString("event_date");
                                    }*/

                                        mainprofile.setUrl(subimages.get(0));
                                        profiles.add(mainprofile);
                                    }
                                    adapter.clear();
                                    adapter.addAll(profiles);
                                    cardStackView.setAdapter(adapter);

                                }

                            }



                        /*    adapter.clear();
                            adapter.addAll(profiles);
                            cardStackView.setAdapter(adapter);*/

                          /*  for(int p=0;p<profiles.size();p++){
                                Log.d("profile_list", String.valueOf(profiles.get(p)));
                                Log.d("sc", Arrays.toString(profiles);
                            }*/
                        }


                    }else{
                        //Toast.makeText(Swipe.this,object.getString("message"),Toast.LENGTH_LONG).show();

                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                    // Toast.makeText(Swipe.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();
            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
        return profiles;

    }


    public static LinkedList<Mainprofile> extractRemainingProfiles() {
        LinkedList<Mainprofile> profiles = new LinkedList<Mainprofile>();
        try {
            for (int i = cardStackView.getTopIndex(); i < profiles.size(); i++) {
                profiles.add(adapter.getItem(i));
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return profiles;
    }

    private void matchNotification(String url){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("matchnotify_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        Log.d("notify_msg","sucess");

                    }else{
                        // Toast.makeText(ServiceNoDelay.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                    //  Toast.makeText(ServiceNoDelay.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                // Toast.makeText(ServiceNoDelay.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog


            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }



    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }



}
