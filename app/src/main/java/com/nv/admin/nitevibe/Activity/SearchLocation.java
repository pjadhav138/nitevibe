package com.nv.admin.nitevibe.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;

public class SearchLocation extends AppCompatActivity implements View.OnClickListener {
    public static ImageView back;
    public static EditText ed_search_loc;
    public static TextView txt_current_loc,txt_auto_detect,txt_turn_loc;
    public static RecyclerView favRecycler;
    public static RelativeLayout mainRel,favRel,emptyRel;
    public static CheckBox chk_autoDetect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_location);


        back= findViewById(R.id.back);

        ed_search_loc= findViewById(R.id.search);

        txt_current_loc= findViewById(R.id.txt_current_loc);
        txt_auto_detect= findViewById(R.id.txt_auto_detect);
        chk_autoDetect= findViewById(R.id.chk_detect);
        txt_turn_loc= findViewById(R.id.txt_loc_turn);

        favRecycler= findViewById(R.id.favRecycler);
        mainRel= findViewById(R.id.mainview);
        favRel= findViewById(R.id.favrel);
        emptyRel= findViewById(R.id.empty_loc_rel);

        onClickEvent();

        chk_autoDetect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){

                }
                else{

                }
            }
        });

         mainRel.setVisibility(View.GONE);
         emptyRel.setVisibility(View.VISIBLE);


    }



    @Override
    public void onClick(View view) {
        int id=view.getId();

        if(id==R.id.back){
            super.onBackPressed();

        }
        if(id==R.id.txt_loc_turn){
            //Toast.makeText(getApplicationContext(),"Gps on",Toast.LENGTH_SHORT).show();
            mainRel.setVisibility(View.VISIBLE);
            emptyRel.setVisibility(View.GONE);
        }
    }

    public void onClickEvent(){
        ed_search_loc.setOnClickListener(this);
        back.setOnClickListener(this);
        txt_turn_loc.setOnClickListener(this);
    }
}
