package com.nv.admin.nitevibe.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.ClubEventDetails;
import com.nv.admin.nitevibe.R;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 3/9/2018.
 */
public class ClubRecyclerAdapter extends RecyclerView.Adapter<ClubRecyclerAdapter.MyView> {
    private List<ClubModel> arrayList;
    private Context context;
    ClubModel current;
    private boolean isChecked;
    public String favClubUrl;
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    ProgressDialog loading;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;

    public ClubRecyclerAdapter(List<ClubModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        setHasStableIds(true);

    }

    @Override
    public ClubRecyclerAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.club_detail_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(final ClubRecyclerAdapter.MyView holder, int position) {
        sharedpreferences=context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        //  public TextView txt_date,txt_mnth,txt_name,txt_count,txt_price,club_id,txt_male_ratio,txt_female_ratio;

        holder.txt_date.setTypeface(extra_bold_face);
        holder.txt_mnth.setTypeface(semi_bold_face);
        holder.txt_name.setTypeface(extra_bold_face);
        holder.txt_count.setTypeface(semi_bold_face);
        holder.txt_male_ratio.setTypeface(semi_bold_face);
        holder.txt_female_ratio.setTypeface(semi_bold_face);


        current=arrayList.get(position);
        String date=current.club_date;

        final String[] current_date = date.split(" ");
        String date1=current_date[0];
        String month=current_date[1];
        holder.txt_date.setText(date1);
        holder.txt_mnth.setText(month);

        Log.d("club_name",current.club_name);
        String abc=current.club_name;
        holder.txt_name.setText(current.club_name);
        holder.club_id.setText(current.club_id);

        Log.d("name_of_club",abc);



        String total_count=current.club_total;
        String male_count=current.club_male;
        String female_count=current.club_female;
        final String favourite=current.club_fav;
        final String club_id=current.club_id;
        final String total_ratio=current.club_total_ratio;
        Log.d("id_of_club",club_id);

         Log.d("club_fav_value",favourite);

        if(favourite.equals("1")){
            holder.img_fav.setImageResource(R.drawable.star_on_2);
            isChecked=true;

        }

        if(favourite.equals("0")){
            holder.img_fav.setImageResource(R.drawable.star_off);
            isChecked=false;

        }

        holder.img_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isChecked = !isChecked;
                holder.img_fav.setImageResource(isChecked ? R.drawable.star_on_2 : R.drawable.star_off);

                Log.d("value of isCheck", String.valueOf(isChecked));

                String status = null;
                if(isChecked==true){
                     status="Active";
                }
                else if(isChecked==false){
                     status="Inactive";
                }

                boolean flag=hasConnection();
                if(flag){

                   // postfavclub(loginUserId,club_id,"0",status);
                    postfavclubvolley(loginUserId,club_id,"0",status);
                }
                else{
                    Toast.makeText(context,"Please check your internet connection!",Toast.LENGTH_LONG).show();
                }




            }
        });

        int user_count= Integer.parseInt(total_count);

        if(user_count>=10){
            holder.txt_count.setVisibility(View.VISIBLE);
            holder.progresRel.setVisibility(View.VISIBLE);

            holder.progress_bar.setMax(Integer.parseInt(total_ratio));
            holder.progress_bar.setSecondaryProgress(Integer.parseInt(female_count));
            holder.txt_female_ratio.setText(current.club_female+"%");
            holder.txt_male_ratio.setText(current.club_male+"%");
            holder.txt_count.setText(user_count+" people are here");

            }
        else{
            holder.txt_count.setVisibility(View.GONE);
            holder.progresRel.setVisibility(View.GONE);
        }

        holder.img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String clubid=holder.club_id.getText().toString();
                byte[] data;

                data = clubid.getBytes(StandardCharsets.UTF_8);

                String base64 = Base64.encodeToString(data, Base64.DEFAULT);

                // Log.i("Base 64 ", base64);
                Log.d("base_64_conversion",base64);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                // Add data to the intent, the receiving app will decide
                // what to do with it.
                // String club_link="http://rednwhite.co.in/club_details.php?id="+base64;
                String club_link=AllUrl.CLUB_SHARE+base64;
                share.putExtra(Intent.EXTRA_TEXT,club_link);

                context.startActivity(Intent.createChooser(share,"Share link!"));

                // shareItem(current.club_img,current.club_name);
            }
        });

        String img_new_url=current.club_img;
        String img11=img_new_url.replaceAll(" ","%20");

        Picasso.with(context.getApplicationContext())
                .load(img11)
                .into(holder.img_club);
/*
        Picasso.with(context.getApplicationContext())
                .load(current.club_img)
                .into(holder.img_club);*/


        holder.img_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, ClubEventDetails.class);
                //i.putExtra("club_id",holder.club_id.getText().toString());
                SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
                editor.remove("club_id");
                editor.commit();
                editor.putString("club_id",holder.club_id.getText().toString());
                editor.commit();
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                context.startActivity(i);


            }
        });



    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public  ImageView img_share,img_fav;

        public TextView txt_date,txt_mnth,txt_name,txt_count,txt_price,club_id,txt_male_ratio,txt_female_ratio;
        public ProgressBar progress_bar;
     //   public RelativeLayout progresRel;
       // public  ImageView img_club;
       public RoundedImageView img_club;
       public LinearLayout progresRel;



        public MyView(View itemView) {
            super(itemView);

            //img_club=(ImageView)itemView.findViewById(R.id.club_img);
            img_club= itemView.findViewById(R.id.club_img);
            img_share= itemView.findViewById(R.id.share);
            img_fav= itemView.findViewById(R.id.fav);



            txt_date= itemView.findViewById(R.id.date);
            club_id= itemView.findViewById(R.id.club_id);
            txt_mnth= itemView.findViewById(R.id.month);
            txt_name= itemView.findViewById(R.id.club_name);
            txt_count= itemView.findViewById(R.id.club_total);
            txt_price= itemView.findViewById(R.id.club_rate);

            progress_bar= itemView.findViewById(R.id.progressBar);

           // progresRel=(RelativeLayout)itemView.findViewById(R.id.progress_rel);
            progresRel= itemView.findViewById(R.id.progress_rel);
            txt_male_ratio= itemView.findViewById(R.id.maleper);
            txt_female_ratio= itemView.findViewById(R.id.femaleper);



        }
    }



    //post method
    private void postfavclubvolley(final String user_id, final String fav_id, final String mode, final String status){
        StringRequest postRequest = new StringRequest(Request.Method.POST,AllUrl.FAV_CLUB,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("fav_club_response", response);
                        loading.dismiss();
                        try {
                            JSONObject object = new JSONObject(response);
                            String res = object.getString("response");
                            String message = object.getString("message");
                            if (res.equals("200")) {

                            }
                            else{
                                Toast.makeText(context,"Failed to make club favourite.Please try again!",Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        loading.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  data = new HashMap<>();
                // the POST parameters:

                data.put("user_id",user_id);
                data.put("fav_id",fav_id);
                data.put("mode",mode);
                data.put("status",status);

                return data;
            }

        };

        Volley.newRequestQueue(context).getCache().clear();

        Volley.newRequestQueue(context).add(postRequest);
        loading= new ProgressDialog(context,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();

    }



    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    public void shareItem(String url, final String club_name) {
        Picasso.with(context).load(url).into(new Target() {
            @Override public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/*");
                String txt="Imterested in the "+club_name;
                i.putExtra(Intent.EXTRA_TEXT,txt);
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                //i.setPackage("com.instagram.android");
                context.startActivity(Intent.createChooser(i, "Share Image"));
            }
            @Override public void onBitmapFailed(Drawable errorDrawable) { }
            @Override public void onPrepareLoad(Drawable placeHolderDrawable) { }
        });
    }

    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file =  new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

}
