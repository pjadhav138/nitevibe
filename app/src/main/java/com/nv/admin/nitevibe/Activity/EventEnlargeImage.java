package com.nv.admin.nitevibe.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nv.admin.nitevibe.R;
import com.squareup.picasso.Picasso;

public class EventEnlargeImage extends AppCompatActivity implements View.OnClickListener {
//    public static RoundedImageView imageView;
    public static ImageView back,imageView;
    public static String event_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_enlarge_image);
//        imageView=(RoundedImageView)findViewById(R.id.enlarge);
        imageView= findViewById(R.id.enlarge);
        back= findViewById(R.id.back);
        back.setOnClickListener(this);

        Bundle extra=getIntent().getExtras();
        if(extra!=null){
            event_img=extra.getString("eventimage");
            event_img=event_img.replaceAll(" ","%20");
            Log.d("view_event_image",event_img);
            Picasso.with(getApplicationContext()).load(event_img).into(imageView);
        }
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        if(id==R.id.back){
            startActivity(new Intent(this,Events.class));
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(this,Events.class);
        startActivity(i);
    }
}
