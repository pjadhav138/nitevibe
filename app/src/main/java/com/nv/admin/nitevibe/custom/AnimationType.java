package com.nv.admin.nitevibe.custom;

public enum AnimationType {

    BounceInDown(BounceInDown.class);


    private Class CreatingAnimation;

    AnimationType(Class clazz) {
        CreatingAnimation = clazz;
    }

    public BaseViewAnimator getAnimator() {
        try {
            return (BaseViewAnimator) CreatingAnimation.newInstance();
        } catch (Exception e) {
            throw new Error("Can not init animatorClazz instance");
        }
    }
}
