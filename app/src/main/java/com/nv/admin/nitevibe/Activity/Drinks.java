package com.nv.admin.nitevibe.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Adapter.DrinkAdapter;
import com.nv.admin.nitevibe.Adapter.DrinkModel;
import com.nv.admin.nitevibe.Adapter.subDrinkModel;
import com.nv.admin.nitevibe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class Drinks extends AppCompatActivity implements View.OnClickListener {

    public static RecyclerView drinkRecycler;
    public static DrinkAdapter drinkAdapter;
    List<DrinkModel> drinkList;
    android.app.AlertDialog alert;
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold;
    public static TextView main_label,drink_label;
    List<subDrinkModel> subDrinkList;
    public static ImageView img_back;
    ProgressDialog loading;
    public static List<String> sep_id;
	    public static List<String> sep_name;
	    public static String old_drink_name;
	    String old_drink_id;
	    public static List<String> prechecklist;
   public static List<String> predrinks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drinks);
        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        predrinks = new ArrayList<String>();


        	        //old_drink_id=getIntent().getStringExtra("drinkids");
        	        old_drink_name=getIntent().getStringExtra("drinknames");
               Log.d("old_drink_name",old_drink_name);
               if(old_drink_name.equals("")){

                    }else {
                     prechecklist = new ArrayList<String>(Arrays.asList(old_drink_name.split(",")));

                 }

               Log.d("drinksd", "old"+prechecklist);

        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        main_label= findViewById(R.id.txt);
        //drink_label=(TextView)findViewById(R.id.txt_drink);
        img_back= findViewById(R.id.back);

        drinkRecycler= findViewById(R.id.drinkRecycler);
        drinkRecycler.setHasFixedSize(true);
        drinkRecycler.addItemDecoration(new SimpleDividerItemDecoration(this));
        drinkRecycler.setLayoutManager(new LinearLayoutManager(this));//Linear Items

        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");

        main_label.setTypeface(bold_face);


        boolean flag=hasConnection();
        if(flag){
            String url=AllUrl.DRINKS;

          //  drinkList(url);
            drinkListVolley(url);
        }
        else{
            Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
        }


        img_back.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        int id=view.getId();

        if(id==R.id.back){
           /* Set<String> set123=sharedpreferences.getStringSet("key",new HashSet<String>());
            Log.d("drink_name_lsit", String.valueOf(set123));
            String drinks=set123.toString().replace("[","").replace("]","");
            Log.d("string_drink_list",drinks);

            Set<String> sub_drink_set=sharedpreferences.getStringSet("sub_drink_id",new HashSet<String>());
            String drink_id=sub_drink_set.toString().replace("[","").replace("]","");

            if(drinks.equals("")){
               // Toast.makeText(getApplicationContext(),"empty drinks",Toast.LENGTH_SHORT).show();
            }
            Intent i=new Intent(Drinks.this,EditProfile.class);
            i.putExtra("drink_name",drinks);
            i.putExtra("drink_id",drink_id);
            startActivity(i);*/
            List<String>subdrinkname=new ArrayList<>();
            Set<String>subdrink= DrinkAdapter.treesetList1;
            List<String>list_subdrink = new ArrayList<>(subdrink);
            String drink_id=subdrink.toString().replace("[","").replace("]","").replace(", ",",");
            for(int i=0;i<subdrink.size();i++){
                String val = list_subdrink.get(i);
                int index = sep_id.indexOf(val);
                String name = sep_name.get(index);
                subdrinkname.add(name);
            }
           // Toast.makeText(Drinks.this,subdrinkname.toString(),Toast.LENGTH_LONG).show();
            String drinks=subdrinkname.toString().replace("[","").replace("]","").replace(", ",",");
            Intent i=new Intent(Drinks.this,EditProfile.class);
            i.putExtra("drink_name",drinks);
            i.putExtra("drink_id",drink_id);
            startActivity(i);

            //////
        }  //end of back

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
      /* Set<String> set123=sharedpreferences.getStringSet("key",new HashSet<String>());
        Log.d("drink_name_lsit", String.valueOf(set123));

        Log.d("drink_name_lsit", String.valueOf(set123));
        String drinks=set123.toString().replace("["," ").replace("]"," ");
        Log.d("string_drink_list",drinks);

        Set<String> sub_drink_set=sharedpreferences.getStringSet("sub_drink_id",new HashSet<String>());
        String drink_id=sub_drink_set.toString().replace("["," ").replace("]"," ");

        Intent i=new Intent(Drinks.this,EditProfile.class);
        i.putExtra("drink_name",drinks);
        i.putExtra("drink_id",drink_id);
        startActivity(i);*/
        List<String>subdrinkname=new ArrayList<>();
        	        Set<String>subdrink= DrinkAdapter.treesetList1;
               List<String>list_subdrink = new ArrayList<>(subdrink);
       	        String drink_id=subdrink.toString().replace("[","").replace("]","").replace(", ",",");
      	        for(int i=0;i<subdrink.size();i++){
           	            String val = list_subdrink.get(i);
          	            int index = sep_id.indexOf(val);
            	            String name = sep_name.get(index);
           	            subdrinkname.add(name);
          	        }

        	     //   Toast.makeText(Drinks.this,subdrinkname.toString(),Toast.LENGTH_LONG).show();
                String drinks=subdrinkname.toString().replace("[","").replace("]","").replace(", ",",");
        Intent i=new Intent(Drinks.this,EditProfile.class);
        i.putExtra("drink_name",drinks);
        i.putExtra("drink_id",drink_id);
        startActivity(i);
        MyApplication.getInstance().trackEvent("Drink Preference", "Drink Preference", "Drink saved");
    }

    //////
    private void drinkListVolley(String url){

        loading= new ProgressDialog(Drinks.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("drink_list_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){
                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){
                            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(Drinks.this);
                            builder1.setMessage("No drinks!!");
                            builder1.setCancelable(false);
                            builder1.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            finish();

                                        }
                                    });
                            alert = builder1.create();
                            alert.show();
                            Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                            positive.setTextColor(getResources().getColor(R.color.female));

                        }
                        else{
                            drinkList=new ArrayList<DrinkModel>();
                            subDrinkList=new ArrayList<subDrinkModel>();
                            sep_id=new ArrayList<String>();
                            sep_name=new ArrayList<>();


                            for (int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);
                                DrinkModel model=new DrinkModel();
                                model.drinkId=object1.getString("drink_id");
                                model.drinkName=object1.getString("drink_name");
                                JSONArray subarray=object1.getJSONArray("sub_drink");

                                if(subarray.length()!=0){
                                    //  List<String> subDrinkId=new ArrayList<>();
                                    // List<String> subDrinkName=new ArrayList<>();

                                    for(int j=0;j<subarray.length();j++){
                                        JSONObject object2=subarray.getJSONObject(j);
                                        subDrinkModel model1=new subDrinkModel();
                                        String id=object2.getString("sub_drink_id");
                                        String name=object2.getString("sub_drink_name");
                                        model1.maindrinkid= model.drinkId;
                                        model1.subDrinkId=object2.getString("sub_drink_id");
                                        model1.subDrinkName=object2.getString("sub_drink_name");
                                        subDrinkList.add(model1);
                                        sep_id.add(model1.subDrinkId);
                                        sep_name.add(model1.subDrinkName);

                                    }

                                }
                                drinkList.add(model);
                            }
                            if (Drinks.old_drink_name.equals("")) {

                            } else {
                                for (int i = 0; i < prechecklist.size(); i++) {
                                    String val = prechecklist.get(i);
                                    int index = sep_name.indexOf(val);
                                    String name = sep_id.get(index);
                                    predrinks.add(name);
                                }
                                Log.d("old_drink_name", predrinks.toString());
                            }
                            drinkAdapter=new DrinkAdapter(drinkList,subDrinkList,Drinks.this);

                            drinkRecycler.setAdapter(drinkAdapter);

                            //blockRecycler.removeAllViews();


                        }

                    }else{
                        Toast.makeText(Drinks.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();
            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }



}
