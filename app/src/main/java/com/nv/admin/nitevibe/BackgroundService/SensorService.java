package com.nv.admin.nitevibe.BackgroundService;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.GPSTracker;
import com.nv.admin.nitevibe.Activity.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

///It is used
public class SensorService extends Service {
    public static final String PREFS_NAME = "LoginPrefs";
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = INITIAL_REQUEST + 3;
    // public static String url = "http://www.nitevibe.in/webservices/getLocation.php?";
    public static String TAG = "Background";
    public static String loginUserId, loginUserMode, loginUserType, str_inside = "", str_outside = "", str_club_id = "";
    public static String str_receiver = "servicetutorial.service.receiver";
    public int counter = 0;
    public double str_lat = 0.0, str_lng = 0.0;
    Context context;
    //shared
    SharedPreferences sharedpreferences;
    ////////new
    boolean isGPSEnable = false;
    boolean isNetworkEnable = false;
    double latitude, longitude;
    LocationManager locationManager;
    Location location;
    long notify_interval = 1000;
    Intent intent;
    GPSTracker gps;
    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };
    long oldTime = 0;
    private LocationManager mLocationManager = null;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    private Timer timer;
    private TimerTask timerTask;

    public SensorService(Context applicationContext) {
        super();
        Log.i("HERE", "here I am!");
    }

    public SensorService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        sharedpreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");


        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        Intent broadcastIntent = new Intent("uk.ac.shef.oak.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);
        stoptimertask();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.remove("chatuser");
        editor.commit();
        super.onTaskRemoved(rootIntent);
        Log.i("EXIT", "onTaskRemoved!");

        Intent broadcastIntent = new Intent("uk.ac.shef.oak.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);


        stoptimertask();
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 20 second
        timer.schedule(timerTask, 20000, 9000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  " + (counter++));
                String url = AllUrl.BACKGROUND_SERVICE + loginUserId;
                eventDetails(url);
            }
        };
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if
        //            timer.cancel(); it's not already null
        if (timer != null) {
            timer = null;
        }
    }

    private void getCurrentLocation() {

        // Check if GPS enabled
        gps = new GPSTracker(this);
        if (gps.canGetLocation()) {

            str_lat = gps.getLatitude();
            str_lng = gps.getLongitude();
            //  Log.e("Location","Your Location is - \nLat: " + str_lat + "\nLong: " + str_lng);

            // Toast.makeText(getActivity(), "Your Location is - \nLat: " + gpslatitude + "\nLong: " + gpslongitude, Toast.LENGTH_LONG).show();

        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void eventDetails(String url) {
        MyApplication.getInstance().getRequestQueue().getCache().remove(url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("event_details_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {

                        final LocationManager service = (LocationManager) getBaseContext().getSystemService(getBaseContext().LOCATION_SERVICE);
                        final boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
                        if (!enabled) {

                        } else {
                            gps = new GPSTracker(getBaseContext());
                            //  getCurrentLocation();  ///////////////

                        }
                        JSONArray array = object.getJSONArray("message");
                        if (array.length() == 0) {

                        } else {
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);

                                String club_id = object1.getString("club_id");
                                String club_name = object1.getString("club_name");
                                String lat1 = object1.getString("lat");
                                String lng1 = object1.getString("lng");
                                String radius1 = object1.getString("radius");
                                JSONArray eventArray = object1.getJSONArray("event");
                                Log.d("club_radius", radius1);

                                double lat = Double.parseDouble(lat1);
                                double lng = Double.parseDouble(lng1);

                                if (eventArray.length() == 0) {

                                } else {
                                    for (int j = 0; j < eventArray.length(); j++) {
                                        JSONObject object2 = eventArray.getJSONObject(j);

                                        String event_id = object2.getString("event_id");
                                        String event_name = object2.getString("event_name");
                                        String event_coupon = object2.getString("event_coupon");
                                        String checkin = object2.getString("checkin");
                                        String checkinReceived = object2.getString("received");

                                        Log.d("service_location", str_lat + " " + str_lng);

                                        //str_lat,str_lng
                                        if (!radius1.equals("")) {
                                            if (str_lat != 0.0 && str_lng != 0.0) {
                                                float[] results = new float[1];
                                                Location.distanceBetween(lat, lng, str_lat, str_lng, results);
                                                float distanceInMeters = results[0];
                                                long radius = Long.parseLong(radius1);
                                                boolean isWithin1km = distanceInMeters < radius;
                                                if (isWithin1km) {
                                                    // Toast.makeText(getBaseContext(), "Inside", Toast.LENGTH_LONG).show();
                                                    str_inside = "Inside";
                                                    str_club_id = club_id;
                                                } else {
                                                    // Toast.makeText(getBaseContext(), "Outside", Toast.LENGTH_LONG).show();
                                                    str_outside = "Outside";
                                                    str_club_id = club_id;
                                                }
                                                Log.d(TAG, "Getting lat and lng" + str_inside + "," + str_outside);
                                                //  if(str_inside.equals("Inside") && str_outside.equals("")){
                                                if (str_inside.equals("Inside")) {
                                                    if (checkinReceived.equals("0")) {
                                                        boolean flag = hasConnection();
                                                        if ((checkin.equals("0")) && (checkinReceived.equals("0")))
                                                        //if(checkin.equals("0") && checkinReceived.equals("0"))
                                                        {
                                                            if (flag) {
                                                                Log.d("condition_sta", "entered loop" + str_club_id);
                                                                String url = AllUrl.CHECKIN_NOTIFY + loginUserId + "&club_id=" + str_club_id;
                                                                Log.d("checkinnotiurl", url);
                                                                checkinNotification(url);
                                                            } else {
                                                                Log.d(TAG, "No internet connection");
                                                            }
                                                        } else if (checkin.equals("1") && str_outside.equals("Outside")) {
                                                            Log.d(TAG, "User has already checkin");

                                                        }
                                                    }


                                                }
                                                // else if(checkin.equals("1") && str_outside.equals("Outside") && str_inside.equals("Inside") ){
                                                else if (checkin.equals("1") && str_inside.equals("Inside")) {
                                                    boolean flag = hasConnection();
                                                    if (checkin.equals("1")) {
                                                        if (flag) {
                                                            String url2 = AllUrl.CHECK_OUT + loginUserId + "&event_id=" + event_id;
                                                            userCheckOutVolley(url2);
                                                        } else {
                                                            Log.d(TAG, "No internet connection");
                                                        }
                                                    } else {
                                                        Log.d(TAG, "User has not yet checkout");
                                                    }

                                                }


                                            }
                                        }


                                    }
                                }
                            }
                        }

                    } else {
                        //  Toast.makeText(ServiceNoDelay.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog


            }
        });

        // Adding request to request queue
        RequestQueue queue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
        queue.getCache().clear();
        jsonObjReq.setShouldCache(false);

        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();

            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void userCheckOutVolley(String url) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject object = new JSONObject(String.valueOf(response));
                    String res = object.getString("response");
                    String message = object.getString("message");

                    if (res.equals("200")) {
                        Log.d(TAG, "user has check_out");

                    } else {
                        // Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void checkinNotification(String url) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("checkin_notify_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {
                        Log.d("notify_msg", "sucess");

                    } else {
                        // Toast.makeText(ServiceNoDelay.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    //  Toast.makeText(ServiceNoDelay.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                // Toast.makeText(ServiceNoDelay.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog


            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);

            str_lat = location.getLatitude();
            str_lng = location.getLongitude();


        }


        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

}
