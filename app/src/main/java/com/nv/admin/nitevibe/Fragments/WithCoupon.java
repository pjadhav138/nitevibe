package com.nv.admin.nitevibe.Fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.Activity.SimpleDividerItemDecoration;
import com.nv.admin.nitevibe.Adapter.ClubSideCouponModel;
import com.nv.admin.nitevibe.Adapter.Clubside_coupon_adapter;
import com.nv.admin.nitevibe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 6/1/2018.
 */
public class WithCoupon extends Fragment {
    private View view;
    private static RecyclerView recyclerView;
    SharedPreferences sharedpreferences;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType,clubId;
    ProgressDialog loading;
    android.app.AlertDialog alert;
    List<ClubSideCouponModel> couponList;
    Clubside_coupon_adapter Adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.clubside_coupon, container, false);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        sharedpreferences=getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        recyclerView= view.findViewById(R.id.couponRecycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));//Linear Items

        Bundle extra=getActivity().getIntent().getExtras();
        if(extra!=null){
            //values coming from Clubsidehome
            clubId=extra.getString("club_id");
            Log.d("club_id",clubId);
            couponData();

           /* boolean flag=hasConnection();
            if(flag){
                String url= AllUrl.WITH_COUPON+clubId;
                String url1=url.replaceAll(" ","%20");
                getCoupon(url1);

            }
            else{
                Toast.makeText(getActivity(),"No internet connection",Toast.LENGTH_SHORT).show();
            }*/
        }



        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && isResumed()){
            couponData();
        }
    }

    private void couponData(){
         boolean flag=hasConnection();

         if(flag){
        String url= AllUrl.WITH_COUPON+clubId;
        String url1=url.replaceAll(" ","%20");
        getCoupon(url1);

            }
            else{
                Toast.makeText(getActivity(),"No internet connection",Toast.LENGTH_SHORT).show();
            }

    }

    private void getCoupon(String url){
        loading= new ProgressDialog(getActivity());
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("with_coupon", response.toString());

                try {
                    loading.dismiss();
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){

                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){
                            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getActivity());
                            builder1.setMessage("No data found for your search");
                            builder1.setCancelable(false);
                            builder1.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                           // getActivity().finish();


                                        }
                                    });
                            alert = builder1.create();
                            alert.show();
                            Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                            positive.setTextColor(getResources().getColor(R.color.female));
                        }
                        else{
                            couponList=new ArrayList<ClubSideCouponModel>();
                            for (int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);
                                ClubSideCouponModel model=new ClubSideCouponModel();


                                model.name=object1.getString("name");
                                model.mobile=object1.getString("mobile");
                                model.email=object1.getString("email");
                                model.gender=object1.getString("gender");
                                model.female=object1.getString("female");
                                model.male=object1.getString("male");
                                model.rate=object1.getString("rate");
                                model.coupon=object1.getString("coupon");
                                model.type=object1.getString("type");

                                couponList.add(model);




                            }

                            Adapter=new Clubside_coupon_adapter(couponList,getActivity());
                            recyclerView.setAdapter(Adapter);




                        }
                    }else{
                        Toast.makeText(getActivity(),object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)getActivity(). getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

}
