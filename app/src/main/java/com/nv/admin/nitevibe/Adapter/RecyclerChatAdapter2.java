package com.nv.admin.nitevibe.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.nv.admin.nitevibe.R;

import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

public class RecyclerChatAdapter2 extends RecyclerView.Adapter<RecyclerChatAdapter2.MyViewHolder> {
    Context context;
    List<ChatModel> chatMessages;
    String user_id;
    private static final int CHAT_LEFT = 0;
    private static final int CHAT_RIGHT = 1;
    String imageurl = "";

    public static Typeface bold_face, extra_bold_face, reg_face, semi_bold_face, calibri_bold, lato_bold, semi_bold_italic;
    private String TAG = getClass().getSimpleName();


    public RecyclerChatAdapter2(Context context, List<ChatModel> chatMessages) {
        this.context = context;
        this.chatMessages = chatMessages;
        setHasStableIds(true);
    }


    @NonNull
    @Override
    public RecyclerChatAdapter2.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        int layoutResource = 0; // determined by view type
//        itemView = inflater.inflate(R.layout.location_popup_row, viewGroup, false);
        String  receiverText = chatMessages.get(i).getReceiver();
//        user_id = Chatting.str_select_userid;
//        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (i){
            case CHAT_LEFT:
                layoutResource = R.layout.chat_left;
                break;

            case CHAT_RIGHT:
                layoutResource = R.layout.chat_right;
                break;

                default:
                    layoutResource = R.layout.chat_right;
        }

        /*if (itemView != null) {
            Log.e(TAG, "onCreateViewHolder: " );
        } else */
        {
            itemView =// inflater.inflate(layoutResource, viewGroup, false);
                    LayoutInflater.from(viewGroup.getContext()).inflate(layoutResource, viewGroup, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerChatAdapter2.MyViewHolder myViewHolder, int i) {
        ChatModel m = chatMessages.get(i);
        imageurl = m.getImage();
        bold_face = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Bold.ttf");
        extra_bold_face = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        reg_face = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
        semi_bold_face = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-SemiBold.ttf");
        calibri_bold = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri Bold.ttf");
        lato_bold = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Bold.ttf");
        semi_bold_italic = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-SemiBoldItalic.ttf");

        myViewHolder.msg.setTypeface(semi_bold_face);
        myViewHolder.sent.setTypeface(semi_bold_face);
        myViewHolder.sent.setText(m.sent);
        String img123 = m.getUserimage();
        if (m.getMessage().length()!=0) {
            myViewHolder.msg.setVisibility(View.VISIBLE);
            myViewHolder.img.setVisibility(View.GONE);

            myViewHolder.msg.setText(m.getMessage());

          /*  Picasso.with(activity.getApplicationContext())
                    .load(d)
                    .into(holder.img);*/


        } else {
            myViewHolder.msg.setVisibility(View.GONE);
            myViewHolder.img.setVisibility(View.VISIBLE);
            int d = 0;
            if (imageurl.equals("1")) {
                d = R.drawable.stickers_01;
                myViewHolder.img.setImageResource(d);

            } else if (imageurl.equals("2")) {
                d = R.drawable.stickers_02;
                myViewHolder.img.setImageResource(d);
            }
        }


    }

    @Override
    public int getItemCount() {
        return chatMessages.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView sent;
        ImageView img;
        CircularImageView img_user;
        private EmojiconTextView msg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            msg = itemView.findViewById(R.id.txt_msg);
            sent = itemView.findViewById(R.id.sent);
            img = itemView.findViewById(R.id.img);
            img_user = itemView.findViewById(R.id.userimg);
        }
    }

    @Override
    public int getItemViewType(int position) {
        String receiverText = chatMessages.get(position).receiver;
        if (!(receiverText.equals(user_id))) {
           return CHAT_LEFT;
        }else return  CHAT_RIGHT;

    }
    @Override
    public long getItemId(int position) {
        return position;
    }
}
