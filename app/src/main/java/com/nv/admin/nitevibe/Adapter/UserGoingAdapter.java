package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nv.admin.nitevibe.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Admin on 4/17/2018.
 */
public class UserGoingAdapter extends RecyclerView.Adapter<UserGoingAdapter.MyView> {
    private List<UserGoingModel> arrayList;
    private Context context;
    UserGoingModel current;
    private boolean isChecked;
    public String favClubUrl;
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;

    public UserGoingAdapter(List<UserGoingModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public UserGoingAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.usergoing_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UserGoingAdapter.MyView holder, int position) {
        sharedpreferences=context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        current=arrayList.get(position);
        String user_id=current.userId;
        String user_age=current.userAge;
        String user_gender=current.userGender;
        String user_img=current.userPhoto;

        if(user_img.equals("")){
            Picasso.with(context.getApplicationContext())
                    .load(R.drawable.default_icon)
                    .into(holder.profile_img);
           // holder.profile_img.setImageDrawable(context.getResources().getDrawable(R.drawable.profile));

        }
        else{
            Picasso.with(context.getApplicationContext())
                    .load(current.userPhoto)
                    .into(holder.profile_img);
        }
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        //public ImageView profile_img;
        //public CircularImageView profile_img;
        public CircleImageView profile_img;
        public MyView(View itemView) {
            super(itemView);

            profile_img= itemView.findViewById(R.id.user_profile);
        }
    }
}
