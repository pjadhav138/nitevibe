package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;

import java.util.List;

/**
 * Created by Admin on 4/13/2018.
 */
public class ViewProfileDrinkAdapter  extends RecyclerView.Adapter<ViewProfileDrinkAdapter.MyView> {
    private List<String> arrayList;
    private Context context;
    //ProfileEventModel current;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;


    public ViewProfileDrinkAdapter(List<String> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public ViewProfileDrinkAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_profile_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewProfileDrinkAdapter.MyView holder, int position) {

        semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        String drinkName=arrayList.get(position);
        holder.txt_name.setTypeface(semi_bold_face);
        holder.txt_name.setText(drinkName);


    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public  TextView txt_name;
        public MyView(View itemView) {
            super(itemView);
            txt_name= itemView.findViewById(R.id.txt);
        }
    }
}
