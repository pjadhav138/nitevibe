package com.nv.admin.nitevibe.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Adapter.WhosAdapter;
import com.nv.admin.nitevibe.Adapter.WhosModel;
import com.nv.admin.nitevibe.Adapter.WhosNonPrefAdapter;
import com.nv.admin.nitevibe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WhosGoing extends AppCompatActivity implements View.OnClickListener {
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    public static String club_id,event_id;
    public static ImageView back;
    public static RecyclerView whosRecycler,otherRecycler;

    List<WhosModel> goingList;
    List<WhosModel> otherList;
    public WhosAdapter adapter;
    public WhosNonPrefAdapter otherAdapter;
    ProgressDialog loading;
    public static ImageView userTab,homeTab,chatTab;
    public static RelativeLayout preferenceRel,otherRel;
    public static TextView txt_label1,txt_label2;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whos_going);
        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);


        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");
        club_id=sharedpreferences.getString("club_id","");
        //value comming from clubEventAdapter
        event_id=sharedpreferences.getString("event_id","");
        back= findViewById(R.id.back);
        userTab= findViewById(R.id.usertab);
        homeTab= findViewById(R.id.hometab);
        chatTab= findViewById(R.id.chattab);

        preferenceRel= findViewById(R.id.prefRel);
        otherRel= findViewById(R.id.otherRel);
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");
        txt_label1= findViewById(R.id.label1);
        txt_label2= findViewById(R.id.label2);

        txt_label1.setTypeface(calibri_bold);
        txt_label2.setTypeface(calibri_bold);


        back.setOnClickListener(this);
        userTab.setOnClickListener(this);
        homeTab.setOnClickListener(this);
        chatTab.setOnClickListener(this);

        whosRecycler= findViewById(R.id.whosrecycler);
        whosRecycler.setHasFixedSize(true);
        whosRecycler.setLayoutManager(new GridLayoutManager(this,4));//Grid item

        otherRecycler= findViewById(R.id.otherrecycler);
        otherRecycler.setHasFixedSize(true);
        otherRecycler.setLayoutManager(new GridLayoutManager(this,4)); //Grid item




        boolean flag=hasConnection();
        if(flag){
           // String url=AllUrl.EVENTS+loginUserId+"&club_id="+club_id+"&selected_id="+event_id;
            String url=AllUrl.WHOS_GOING_LIST+loginUserId+"&club_id="+club_id+"&event_id="+event_id;
            Log.d("whosgoing_url",url);
           // eventList(url);
            eventListVolley(url);

           /* String otherurl=AllUrl.GOING_NONPREF_LIST+loginUserId+"&club_id="+club_id+"&event_id="+event_id;
            otherUserList(otherurl);*/
        }
        else{
            Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onClick(View view) {
        int id=view.getId();

        if(id==R.id.back){
           // super.finish();
            startActivity(new Intent(WhosGoing.this,Events.class));
        }
        if(id==R.id.usertab){
            MyApplication.getInstance().trackEvent("Whos Going", "Profile", "Success");
            startActivity(new Intent(this,UserProfile.class));
        }
        if(id==R.id.hometab){
            MyApplication.getInstance().trackEvent("Whos Going", "Club", "Success");
            startActivity(new Intent(this,Club.class));

        }
        if(id==R.id.chattab){
            MyApplication.getInstance().trackEvent("Whos Going", "Chat", "Success");
            startActivity(new Intent(this,Chat.class));
        }
    }

    ///displaying user information

    private void eventListVolley(String url){

        loading= new ProgressDialog(WhosGoing.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        MyApplication.getInstance().getRequestQueue().getCache().remove(url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("whos_goingeventresponse", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){
                        JSONArray array = object.getJSONArray("message");


                        if(array.length()==0){
                          preferenceRel.setVisibility(View.GONE);

                        }
                        else{
                            preferenceRel.setVisibility(View.VISIBLE);
                            goingList=new ArrayList<>();
                            for (int i=0;i<array.length();i++){

                                JSONObject object3=array.getJSONObject(i);
                                WhosModel model=new WhosModel();


                                // model.eventId=object3.getString("eventid");
                                model.whosId=object3.getString("user_going_id");
                                model.whosName=object3.getString("name");
                                model.whosProfile=object3.getString("profile");
                                model.whosAge=object3.getString("age");
                                model.whosGender=object3.getString("gender");
                                model.whosCategory=object3.getString("category");
                                model.whosEntry=object3.getString("entry");

                                      /*  String userEvent=object3.getString("eventid");

                                        if(userEvent.equals(event_id)){
                                            goingList.add(model);
                                        }*/

                                goingList.add(model);





                            } //end of main for
                            adapter=new WhosAdapter(goingList,WhosGoing.this);
                            whosRecycler.setAdapter(adapter);




                        }

                        boolean flag=hasConnection();
                        if(flag){
                            String otherurl=AllUrl.GOING_NONPREF_LIST+loginUserId+"&club_id="+club_id+"&event_id="+event_id;
                            otherUserList(otherurl);
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Toast.makeText(WhosGoing.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        RequestQueue queue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
        queue.getCache().clear();
        jsonObjReq.setShouldCache(false);

        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();

            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    //other pref going user
    private void otherUserList(String url){
        loading= new ProgressDialog(WhosGoing.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("otherlist_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){
                        JSONArray array = object.getJSONArray("message");


                        if(array.length()==0){
                            otherRel.setVisibility(View.GONE);

                        }
                        else{
                            otherRel.setVisibility(View.VISIBLE);
                            otherList=new ArrayList<>();
                            for (int i=0;i<array.length();i++){

                                JSONObject object3=array.getJSONObject(i);
                                WhosModel model=new WhosModel();


                                // model.eventId=object3.getString("eventid");
                                model.whosId=object3.getString("user_going_id");
                                model.whosName=object3.getString("name");
                                model.whosProfile=object3.getString("profile");
                                model.whosAge=object3.getString("age");
                                model.whosGender=object3.getString("gender");
                                model.whosCategory=object3.getString("category");
                                model.whosEntry=object3.getString("entry");

                                      /*  String userEvent=object3.getString("eventid");

                                        if(userEvent.equals(event_id)){
                                            goingList.add(model);
                                        }*/

                                otherList.add(model);





                            } //end of main for
                            otherAdapter=new WhosNonPrefAdapter(otherList,WhosGoing.this);
                            otherRecycler.setAdapter(otherAdapter);


                        }

                    }else{
                        Toast.makeText(WhosGoing.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(WhosGoing.this,Events.class));
        super.onBackPressed();
    }
}
