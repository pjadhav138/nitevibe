package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nv.admin.nitevibe.Activity.View_Profile;
import com.nv.admin.nitevibe.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class WhosNonPrefAdapter extends RecyclerView.Adapter<WhosNonPrefAdapter.MyView> {
    private List<WhosModel> arrayList;
    private Context context;
    WhosModel current;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    public static final String PREFS_NAME = "LoginPrefs";
    SharedPreferences sharedpreferences;

    public WhosNonPrefAdapter(List<WhosModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public WhosNonPrefAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.whos_row, parent, false);
        WhosNonPrefAdapter.MyView viewHolder = new WhosNonPrefAdapter.MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final WhosNonPrefAdapter.MyView holder, int position) {
        bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        current=arrayList.get(position);

        String event_id=current.eventId;
        String url=current.whosProfile;

        if(current.whosProfile.equals("")){
            Picasso.with(context.getApplicationContext())
                    .load(R.drawable.default_icon)
                    .into(holder.profile);
        }
        else{
            Picasso.with(context.getApplicationContext())
                    .load(current.whosProfile)
                    .into(holder.profile);

        }


        if(current.whosCategory.equals("1")){ //like
            holder.profile.setBorderWidth(3);
            holder.profile.setBorderColor(context.getResources().getColor(R.color.pink));
        }
        else if(current.whosCategory.equals("2")){ //dislike
            holder.profile.setBorderWidth(3);
            holder.profile.setBorderColor(context.getResources().getColor(R.color.gray));
        }
        else if(current.whosCategory.equals("3")){ //superlike
            holder.profile.setBorderWidth(3);
            holder.profile.setBorderColor(context.getResources().getColor(R.color.light_blue));
        }
        else{
            holder.profile.setBorderWidth(3);
            holder.profile.setBorderColor(context.getResources().getColor(R.color.light_green));

        }

        holder.user_event.setText(current.eventId);
        holder.user_category.setText(current.whosCategory);
        holder.user_id.setText(current.whosId);

        holder.txt_username.setTypeface(reg_face);
        holder.txt_username.setText(current.whosName);



        holder.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               //  Toast.makeText(context,holder.user_id.getText().toString(),Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
                editor.remove("userid");
                editor.remove("from");
                editor.commit();
                editor.putString("userid",holder.user_id.getText().toString());
                editor.putString("from","whosgoing");
                editor.commit();
                Intent myIntent = new Intent(context,View_Profile.class);
                context.startActivity(myIntent);


            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public CircleImageView profile;
       // public CircularImageView profile;
        public TextView user_id,user_category,user_event,txt_username;
        public MyView(View itemView) {
            super(itemView);
            profile= itemView.findViewById(R.id.img);
            user_id= itemView.findViewById(R.id.userid);
            user_category= itemView.findViewById(R.id.category);
            user_event= itemView.findViewById(R.id.event_id);
            txt_username= itemView.findViewById(R.id.user_name);
        }
    }
}
