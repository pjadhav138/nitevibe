package com.nv.admin.nitevibe.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//setContentView(R.layout.activity_club_side_home);

public class ClubSideHome extends AppCompatActivity {
   // float values[] = { 700, 400, 100, 500,600 };

    float[] values = {50, 51}; //girl/boy
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType,clubId;

    public static ImageView img_add,img_menu;
    AlertDialog alert;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    public static TextView txt_club,txt_date,txt_male,txt_female,txt_male_count,txt_female_count,txt_total,txt_total_count;
    public static String str_club_id,str_total_coupon,str_coupon_left;
    ImageView add_entry;
    public static final int[] COLORS = {
            Color.rgb(53,73,238), Color.rgb(255,66,111) };
    Context context;
    ProgressDialog loading;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_side_home);
        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        context=this;


        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        txt_club= findViewById(R.id.clubName);
        txt_date= findViewById(R.id.date);
        txt_male= findViewById(R.id.male_txt);
        txt_female= findViewById(R.id.female_txt);
        txt_male_count= findViewById(R.id.male_count);
        txt_female_count= findViewById(R.id.female_count);
        add_entry= findViewById(R.id.add_guest);
        txt_total= findViewById(R.id.total_txt);
        txt_total_count= findViewById(R.id.total_count);





        setfont();

        boolean flag=hasConnection();
        if(flag){
            String url=AllUrl.CLUB_HOME+loginUserId;
            String url1=url.replaceAll(" ","%20");
           // userClubHome(url1);
            userClubHomeVolley(url1);

        }
        else{
            Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
        }


      /*  LinearLayout lv1 = (LinearLayout) findViewById(R.id.linear);

        values = calculateData(values);
        MyGraphview graphview = new MyGraphview(this, values);
        lv1.addView(graphview);*/

        img_menu= findViewById(R.id.more);
        img_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //openOptionsMenu();
                //invalidateOptionsMenu();
                showPopup(view);
            }
        });

        add_entry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.getInstance().trackEvent("Club Side Details", "Add guest", "Add Guest");
                //startActivity(new Intent(ClubSideHome.this,AddGuest.class));
                Intent i=new Intent(ClubSideHome.this,AddGuest.class);
                i.putExtra("club_id",str_club_id);
                startActivity(i);
            }
        });





    }

    public void setfont(){
        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");




        txt_club.setTypeface(bold_face);
        txt_date.setTypeface(calibri_bold);
        txt_male.setTypeface(semi_bold_face);
        txt_female.setTypeface(semi_bold_face);
        txt_male_count.setTypeface(semi_bold_face);
        txt_female_count.setTypeface(semi_bold_face);
        txt_total.setTypeface(semi_bold_face);
        txt_total_count.setTypeface(semi_bold_face);
    }


    public void setPieChart(int male ,int female){
        //pie chart
        PieChart pieChart = findViewById(R.id.pie);
        pieChart.getLegend().setEnabled(false);
        pieChart.setUsePercentValues(true);
        ArrayList<Entry> yvalues = new ArrayList<Entry>();

        yvalues.add(new Entry(male, 0,"")); //male
        yvalues.add(new Entry(female, 1,"")); //female




        PieDataSet dataSet = new PieDataSet(yvalues, "");

       dataSet.setColors(new int[] { R.color.male, R.color.female, R.color.light_blue, R.color.light_blue },context);



        ArrayList<String> xVals = new ArrayList<String>();

        xVals.add(String.valueOf(male));
        xVals.add(String.valueOf(female));

        PieData data = new PieData(xVals, dataSet);
        data.setDrawValues(false);

       /* // In Percentage term
        data.setValueFormatter(new PercentFormatter());*/
        // Default value
        //data.setValueFormatter(new DefaultValueFormatter(0));
        pieChart.setData(data);

        pieChart.setDescription("");
        pieChart.setDrawHoleEnabled(false);
        pieChart.setTransparentCircleRadius(25f);
        pieChart.setHoleRadius(25f);
        pieChart.animateXY(1400, 1400);
         data.setValueTextColor(Color.WHITE);
         data.setValueTextSize(15);


        //end pie

    }

    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
        return s;
    }


    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.club_side_menu, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_home:
                        MyApplication.getInstance().trackEvent("Club Side Details", "Club Home", "Success");
                        Intent i=new Intent(ClubSideHome.this,ClubHome.class);
                        i.putExtra("club_id",str_club_id);
                        startActivity(i);
                        break;
                    case R.id.action_list:

                        Intent i2=new Intent(ClubSideHome.this,Clubsidecoupon.class);
                        i2.putExtra("club_id",str_club_id);
                        startActivity(i2);
                        break;

                    case R.id.action_logout:
                        MyApplication.getInstance().trackEvent("Club Side Details", "Logout", "Logout");
                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        editor.apply();
                        Intent i1 = new Intent(getApplicationContext(), LandingPage.class);
                        startActivity(i1);
                        finish();

                        break;
                }
                return true;
            }
        });
    }

    private void userClubHomeVolley(String url){

        loading= new ProgressDialog(ClubSideHome.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("club_side_home_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){
                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(ClubSideHome.this);
                            builder1.setMessage("No details found");
                            builder1.setCancelable(false);
                            builder1.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();

                                        }
                                    });
                            alert = builder1.create();
                            alert.show();
                            Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                            positive.setTextColor(getResources().getColor(R.color.female));

                        }
                        else{
                            for (int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);

                                String club_id=object1.getString("club_id");
                                clubId=club_id;
                                String club_name=object1.getString("club_name");
                                String current_date=object1.getString("date");
                                String male_count=object1.getString("male_count");
                                String female_count=object1.getString("female_count");

                                //public static TextView txt_club,txt_date,txt_male,txt_female,txt_male_count,txt_female_count;

                                txt_club.setText(club_name);
                                txt_date.setText(current_date);
                                txt_male_count.setText(male_count);
                                txt_female_count.setText(female_count);
                                int male_val= Integer.parseInt(male_count);
                                int female_val= Integer.parseInt(female_count);
                                String total_user= String.valueOf(male_val+female_val);

                                txt_total_count.setText(total_user);

                                str_club_id=club_id;
                                int male_value= Integer.parseInt(male_count);
                                int female_value= Integer.parseInt(female_count);

                                setPieChart(male_value,female_value);

                                JSONArray eventarray = object1.getJSONArray("today_event_arr");
                                if(eventarray.length()==0){
                                    SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.remove("total_coupon");
                                    editor.remove("left_coupon");
                                    editor.commit();
                                    editor.putString("total_coupon","");
                                    editor.putString("left_coupon", "");
                                    editor.commit();
                                }
                                else{
                                    for (int k=0;k<eventarray.length();k++){
                                        JSONObject object2 = eventarray.getJSONObject(k);

                                        String coupon_available=object2.getString("coupon");
                                        String coupon_left=object2.getString("left_coupon");
                                        String event_id=object2.getString("today_event_id");
                                        String event_name=object2.getString("today_event_name");
                                        String event_time=object2.getString("today_event_start");
                                        String event_date=object2.getString("event_date");

                                        str_total_coupon=coupon_available;
                                        str_coupon_left=coupon_left;
                                        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                                        editor.remove("total_coupon");
                                        editor.remove("left_coupon");
                                        editor.commit();
                                        editor.putString("total_coupon", str_total_coupon);
                                        editor.putString("left_coupon", str_coupon_left);
                                        editor.commit();
                                    }
                                }


                            }
                        }
                    }else{
                        Toast.makeText(ClubSideHome.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });
        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

}
