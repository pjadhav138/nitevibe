package com.nv.admin.nitevibe.Fragments;


import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.EditProfile;
import com.nv.admin.nitevibe.Activity.Home;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.Activity.Setting;
import com.nv.admin.nitevibe.Adapter.ProfileEventAdapter;
import com.nv.admin.nitevibe.Adapter.ProfileEventModel;
import com.nv.admin.nitevibe.R;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 3/9/2018.
 */
public class UserProfileFragment extends Fragment implements View.OnClickListener {
    public static ImageView profile1,setting,home,chat,userprofile;
    public static TextView edit,txt_name,txt_occupation,txt_city;
    public static RelativeLayout eventRelative;
    public static RecyclerView eventRecycler;
    public static String profileUrl;
    AlertDialog alert;
    public static ProfileEventAdapter  profileEventAdapter;

    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType,blockUserCount;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    ProgressDialog loading;
    // public static RoundedImage profile;
    public static RoundedImageView profile;
    public static RelativeLayout emptyRel;
    public static TextView emptyTxt,label;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_user_profile, container, false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        sharedpreferences=getActivity().getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);

        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

      //  profile=(ImageView)view.findViewById(R.id.profile);
       // profile=(RoundedImage) view.findViewById(R.id.profile);
        profile= view.findViewById(R.id.profile);
        setting= view.findViewById(R.id.setting);


       emptyRel= view.findViewById(R.id.emptyrel);
        emptyTxt= view.findViewById(R.id.find);
        label= view.findViewById(R.id.find123);

        edit= view.findViewById(R.id.edit);
        txt_name= view.findViewById(R.id.name);
        txt_occupation= view.findViewById(R.id.occupation);
        txt_city= view.findViewById(R.id.city);

        eventRecycler= view.findViewById(R.id.eventRecycler);

        eventRecycler.setHasFixedSize(true);
        eventRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));//Linear Items

        eventRelative= view.findViewById(R.id.eventRel);

        setFont();


        boolean flag=hasConnection();
        if(flag){
            profileUrl= AllUrl.PROFILE+loginUserId;
           // settingProfileDetails(profileUrl);
            settingProfileDetailVolley(profileUrl);
        }
        else{
            Toast.makeText(getActivity(),"No Internet Connection!",Toast.LENGTH_SHORT).show();
        }


        onClickEvent();




       return view;
    }

    public UserProfileFragment() {
    }

    public void setFont(){
        bold_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getContext().getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getContext().getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        txt_name.setTypeface(bold_face);
        txt_occupation.setTypeface(reg_face);
        txt_city.setTypeface(reg_face);
        edit.setTypeface(bold_face);
        emptyTxt.setTypeface(semi_bold_italic);
        label.setTypeface(reg_face);

    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        if(id==R.id.setting){
            //startActivity(new Intent(getActivity(),Setting.class));
            Intent i=new Intent(getActivity(), Setting.class);
            i.putExtra("block_count",blockUserCount);
            startActivity(i);
        }
        if(id==R.id.edit){
            //Toast.makeText(getApplicationContext(),"Edit",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getActivity(),EditProfile.class));
        }
        if(id==R.id.find){
            startActivity(new Intent(getActivity(), Home.class));
        }


    }

    public void onClickEvent(){
        edit.setOnClickListener(this);
        setting.setOnClickListener(this);
        emptyTxt.setOnClickListener(this);
    }

      public void settingProfileDetailVolley(String search_url){
          loading= new ProgressDialog(getActivity());
          loading.setIndeterminate(true);
          loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
          loading.setCancelable(false);
          loading.setMessage("Please wait...!");
          loading.show();
          JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                  search_url, null, new Response.Listener<JSONObject>() {

              @Override
              public void onResponse(JSONObject response) {
                  Log.d("userprofile_response", response.toString());
                  loading.dismiss();
                  try {
                      // Parsing json object response
                      // response will be a json object
                      JSONObject jsonObject = new JSONObject(response.toString());
                      String res = jsonObject.getString("response");
                      List<ProfileEventModel> data = new ArrayList<>();
                      if(res.equals("200")){
                          JSONArray array = jsonObject.getJSONArray("message");
                          if(array.length()==0){
                              AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                              builder1.setMessage("No data found for your search.");
                              builder1.setCancelable(false);
                              builder1.setPositiveButton("Ok",
                                      new DialogInterface.OnClickListener() {
                                          @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                          public void onClick(DialogInterface dialog, int id) {
                                              dialog.cancel();


                                              // finish();
                                          }
                                      });
                              alert = builder1.create();
                              alert.show();
                          }
                          else{
                              for (int i = 0; i < array.length(); i++) {

                                  JSONObject object = array.getJSONObject(i);

                                  JSONArray user_array = object.getJSONArray("user");
                                  JSONArray event_array = object.getJSONArray("event");

                                  if (user_array.length() == 0) {

                                      //  initiatePopupWindow();
                                      startActivity(new Intent(getActivity(),EditProfile.class));

                                  } else {
                                      for (int k = 0; k < user_array.length(); k++) {
                                          JSONObject user_object = user_array.getJSONObject(k);

                                          String user_name = user_object.getString("name");
                                          String user_age = user_object.getString("age");
                                          String user_location = user_object.getString("location");
                                          String user_occupation = user_object.getString("occupation");
                                          String block_count = user_object.getString("block_count");
                                          blockUserCount=block_count;
                                          String user_profile = user_object.getString("profile");

                                          if (user_age.equals("")) {
                                              txt_name.setText(user_name);
                                          } else {
                                              txt_name.setText(user_name + "," + user_age);
                                          }
                                          //occup
                                          if (user_occupation.equals("")) {
                                              txt_occupation.setVisibility(View.GONE);
                                          } else {
                                              txt_occupation.setVisibility(View.VISIBLE);
                                              txt_occupation.setText(user_occupation);
                                          }
                                          //loc
                                          if (user_location.equals("")) {

                                          } else {
                                              txt_city.setText(user_location);
                                          }

                                          //profile
                                          if (!user_profile.equals("")) {
                                              Bitmap img = getBitmapFromURL(user_profile);
                                              profile.setImageBitmap(img);

                                          }
                                          else{
                                              //Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.default_icon);
                                              //  profile.setImageBitmap(icon);

                                          }

                                      }
                                  }


                                  //event
                                  if (event_array.length() == 0) {
                                      //no event
                                     // eventRelative.setVisibility(View.GONE);

                                      emptyRel.setVisibility(View.VISIBLE);
                                       eventRecycler.setVisibility(View.GONE);
                                  } else {
                                      emptyRel.setVisibility(View.GONE);
                                      eventRecycler.setVisibility(View.VISIBLE);
                                      for (int j = 0; j < event_array.length(); j++) {
                                          JSONObject event_object = event_array.getJSONObject(j);

                                          ProfileEventModel model=new ProfileEventModel();

                                          model.eventId=event_object.getString("event_id");
                                          model.eventDate=event_object.getString("event_date");
                                          model.eventName=event_object.getString("event_name");

                                          data.add(model);


                                      }

                                      profileEventAdapter=new ProfileEventAdapter(data,getActivity());
                                      eventRecycler.setAdapter(profileEventAdapter);
                                  }


                              }


                          }  //end of else



                      }else{
                          Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                      }



                  } catch (JSONException e) {
                      e.printStackTrace();
                      Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                  }

              }
          }, new Response.ErrorListener() {

              @Override
              public void onErrorResponse(VolleyError error) {
                  VolleyLog.d("main", "Error: " + error.getMessage());
                  Toast.makeText(getActivity(),
                          error.getMessage(), Toast.LENGTH_SHORT).show();
                  // hide the progress dialog
                  loading.dismiss();

              }
          });

          // Adding request to request queue
          jsonObjReq.setShouldCache(false);
          MyApplication.getInstance().addToRequestQueue(jsonObjReq);
      }

    private void settingProfileDetails(String search_url) {
        class UserProfileDetails extends AsyncTask<String, Void, String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading= new ProgressDialog(getActivity());
                loading.setIndeterminate(true);
                // loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animatiion));
                loading.setCancelable(false);
                loading.setMessage("Please wait...!");
                loading.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String uri = params[0];

                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while((json = bufferedReader.readLine())!= null){
                        sb.append(json+"\n");
                    }

                    return sb.toString().trim();

                }catch(Exception e){
                    return null;
                }

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Log.d("user_profile_list",s);



                try {
                    JSONObject jsonObject = new JSONObject(s);


                    String response = jsonObject.getString("response");
                    String msg = jsonObject.getString("message");
                    JSONArray array = jsonObject.getJSONArray("message");
                    List<ProfileEventModel> data = new ArrayList<>();
                    Log.d("array_length", String.valueOf(array.length()));
                    if(response.equals("200")) {
                        if (array.length() == 0) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                            builder1.setMessage("No data found for your search.");
                            builder1.setCancelable(false);
                            builder1.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();


                                            // finish();
                                        }
                                    });
                            alert = builder1.create();
                            alert.show();
                        } else {
                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.getJSONObject(i);

                                JSONArray user_array = object.getJSONArray("user");
                                JSONArray event_array = object.getJSONArray("event");

                                if (user_array.length() == 0) {

                                  //  initiatePopupWindow();
                                    startActivity(new Intent(getActivity(),EditProfile.class));

                                } else {
                                    for (int k = 0; k < user_array.length(); k++) {
                                        JSONObject user_object = user_array.getJSONObject(k);

                                        String user_name = user_object.getString("name");
                                        String user_age = user_object.getString("age");
                                        String user_location = user_object.getString("location");
                                        String user_occupation = user_object.getString("occupation");
                                        String block_count = user_object.getString("block_count");
                                        blockUserCount=block_count;
                                        String user_profile = user_object.getString("profile");

                                        if (user_age.equals("")) {
                                            txt_name.setText(user_name);
                                        } else {
                                            txt_name.setText(user_name + "," + user_age);
                                        }
                                        //occup
                                        if (user_occupation.equals("")) {
                                            txt_occupation.setVisibility(View.GONE);
                                        } else {
                                            txt_occupation.setVisibility(View.VISIBLE);
                                            txt_occupation.setText(user_occupation);
                                        }
                                        //loc
                                        if (user_location.equals("")) {

                                        } else {
                                            txt_city.setText(user_location);
                                        }

                                        //profile
                                        if (!user_profile.equals("")) {
                                            Bitmap img = getBitmapFromURL(user_profile);
                                            profile.setImageBitmap(img);

                                        }
                                        else{
                                                //Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.default_icon);
                                              //  profile.setImageBitmap(icon);

                                        }

                                    }
                                }


                                //event
                                if (event_array.length() == 0) {
                                    //no event
                                    eventRelative.setVisibility(View.GONE);
                                } else {
                                    eventRelative.setVisibility(View.VISIBLE);
                                    for (int j = 0; j < event_array.length(); j++) {
                                        JSONObject event_object = event_array.getJSONObject(j);

                                        ProfileEventModel model=new ProfileEventModel();

                                        model.eventId=event_object.getString("event_id");
                                        model.eventDate=event_object.getString("event_date");
                                        model.eventName=event_object.getString("event_name");

                                        data.add(model);


                                    }

                                    profileEventAdapter=new ProfileEventAdapter(data,getActivity());
                                    eventRecycler.setAdapter(profileEventAdapter);
                                }


                            }


                        }
                    }
                    else{
                        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
                    }

                    ///
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
        UserProfileDetails profile = new UserProfileDetails();
        profile.execute(search_url);


    }



    //convert image url to bitmap
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }


}
