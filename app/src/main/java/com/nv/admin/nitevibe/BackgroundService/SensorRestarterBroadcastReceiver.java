package com.nv.admin.nitevibe.BackgroundService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Admin on 5/17/2018.
 */
public class SensorRestarterBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        /*Log.d("Service_class","SensorRestarterBroadcastReceiver is called");
        Log.i(SensorRestarterBroadcastReceiver.class.getSimpleName(), "Service Stops! Oops!!!!");

        context.startService(new Intent(context.getApplicationContext(), ServiceNoDelay.class));*/
        Log.i(SensorRestarterBroadcastReceiver.class.getSimpleName(), "Service Stops! Oooooooooooooppppssssss!!!!");
        context.startService(new Intent(context.getApplicationContext(), ServiceNoDelay.class));

    }
}
