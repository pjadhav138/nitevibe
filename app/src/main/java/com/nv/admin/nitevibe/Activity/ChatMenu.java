package com.nv.admin.nitevibe.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Database.DatabaseHandler;
import com.nv.admin.nitevibe.Database.SettingModel;
import com.nv.admin.nitevibe.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatMenu extends AppCompatActivity implements View.OnClickListener {
    private android.support.v4.app.Fragment fragment;
    private android.support.v4.app.FragmentManager fragmentManager;
    public static CircleImageView profile_img;
    //public static CircularImageView profile_img;
    public static TextView txt_unmatch,txt_block,txt_spam,txt_name,txt_occupation;
    public static TextView main_label,mute_label;
    public static CustomSwitchButton mute;
    public static String str_userId,str_userName,str_userProfile,str_occupation;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    List<SettingModel> arrayList;
    public static DatabaseHandler handler;
    public static ImageView back;
    AlertDialog alert;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    ProgressDialog loading;
    PopupWindow popupWindow;
    public static RelativeLayout mRelativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_menu);
        profile_img= findViewById(R.id.user_profile);
        txt_unmatch= findViewById(R.id.unmatch_txt);
        txt_block= findViewById(R.id.block_txt);
        txt_spam= findViewById(R.id.spam_txt);
        mute= findViewById(R.id.toggleButton1);
        txt_name= findViewById(R.id.user_name); //name
        txt_occupation= findViewById(R.id.user_occupation); //occ
        back= findViewById(R.id.back);
        mRelativeLayout= findViewById(R.id.rel111);


        main_label= findViewById(R.id.txt);
        mute_label= findViewById(R.id.mute_txt);
        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);


        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");


        Bundle extra=getIntent().getExtras();
        //values are coming from ChatListAdapter
        if(extra!=null){
            str_userId=extra.getString("user_id");
            str_userName=extra.getString("user_name");
            str_userProfile=extra.getString("user_profile");
            Log.d("user_info",str_userId+" "+str_userName+" "+str_userProfile);
            str_occupation=extra.getString("user_occupation");

            txt_occupation.setText(str_occupation);

            txt_name.setText(str_userName);

            if(str_userProfile.equals("")){
                profile_img.setImageResource(R.drawable.default_icon);
            }
            else{
                Bitmap icon=getBitmapFromURL(str_userProfile);
                profile_img.setImageBitmap(icon);
            }

        }
        onClickEvent();
        setFont();

        //initializing db
        handler=new DatabaseHandler(this);
        //inserting values
        arrayList=new ArrayList<>();
        arrayList=handler.getAllData();
        for(int i=0;i<arrayList.size();i++){
            SettingModel model;
            model=arrayList.get(i);
            String name=model.getLabel();
            String data=model.getData();

            if(name.equals("mute")){
                mute.setChecked(Boolean.parseBoolean(data));
            }
        }

        mute.setOnCheckedChangeListener(new CustomSwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomSwitchButton view, boolean isChecked) {
                Log.d("mute_noti_value", String.valueOf(isChecked));
                String value= String.valueOf(isChecked);
                handler.updateUser(new SettingModel("mute",value));
            }

        });


    }

    public void onClickEvent(){
        profile_img.setOnClickListener(this);
        txt_unmatch.setOnClickListener(this);
        txt_block.setOnClickListener(this);
        txt_spam.setOnClickListener(this);
        back.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        if(id==R.id.block_txt){
            initiatePopupWindow();


        }
        if(id==R.id.spam_txt){
          String spam_url=AllUrl.SPAM_USER+loginUserId+"&spam_user_id="+str_userId;
            spamuservolley(spam_url);
        }
        if(id==R.id.unmatch_txt){
            boolean flag=hasConnection();
            if(flag){
                String str=AllUrl.UNMATCH_USER+loginUserId+"&unmatch_id="+str_userId;
                String url=str.replaceAll(" ","%20");
                //unmatch(url);
                unMatchVolley(url);

            }
            else{
                Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
            }

        }
        if(id==R.id.user_profile){
            Intent i=new Intent(this,View_Profile.class);
            i.putExtra("view_user_id",str_userId);
            startActivity(i);

        }
        if(id==R.id.back){

            Intent i=new Intent(ChatMenu.this, Chat.class);
            startActivity(i);

        }

    }

    public void spamuservolley(String search_url){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,search_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("blockcountresponse", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String res = jsonObject.getString("response");
                    String message=jsonObject.getString("message");

                    if(res.equals("200")){
                        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(ChatMenu.this);
                        builder1.setMessage("The user is registered as spam user");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        startActivity(new Intent(ChatMenu.this,Chat.class));
                                        MyApplication.getInstance().trackEvent("Chat", "Spam User", "Success");

                                    }
                                });
                        alert = builder1.create();
                        alert.show();
                        Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                        positive.setTextColor(getResources().getColor(R.color.female));

                    }else if(res.equals("201")){
                        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(ChatMenu.this);
                        builder1.setMessage("Already registered as spam user");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        //finish();
                                        startActivity(new Intent(ChatMenu.this,Setting.class));

                                    }
                                });
                        alert = builder1.create();
                        alert.show();
                        Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                        positive.setTextColor(getResources().getColor(R.color.female));
                    }
                    else{

                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog


            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    private void blockVolley(String url){
        loading= new ProgressDialog(ChatMenu.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("block_user_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(ChatMenu.this);
                        builder1.setMessage("The user is blocked!");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        startActivity(new Intent(ChatMenu.this,Chat.class));
                                        MyApplication.getInstance().trackEvent("Chat", "Block User", "Success");

                                    }
                                });
                        alert = builder1.create();
                        alert.show();
                        Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                        positive.setTextColor(getResources().getColor(R.color.female));
                    }else{
                        Toast.makeText(ChatMenu.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                   /* Toast.makeText(ChatMenu.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
              /*  Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();*/

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }



    //unmatch user
    private void unMatchVolley(String url){

        loading= new ProgressDialog(ChatMenu.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("unmatch_user_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(ChatMenu.this);
                        builder1.setMessage("The user is unmatched!");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int id) {
                                       // dialog.cancel();
                                        startActivity(new Intent(ChatMenu.this,Chat.class));
                                        MyApplication.getInstance().trackEvent("Chat", "Unmatch User", "Success");

                                    }
                                });
                        alert = builder1.create();
                        alert.show();
                        Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                        positive.setTextColor(getResources().getColor(R.color.female));

                    }else{
                        Toast.makeText(ChatMenu.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                   /* Toast.makeText(ChatMenu.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
              /*  Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();*/
                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void initiatePopupWindow(){
        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");

        LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        // Inflate the custom layout/view
        View customView = inflater.inflate(R.layout.block_user_popup,null);


        popupWindow = new PopupWindow(
                customView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );

        // Set an elevation value for popup window
        // Call requires API level 21
        if(Build.VERSION.SDK_INT>=21){
            popupWindow.setElevation(5.0f);
        }

        // Get a reference for the custom view close button

        TextView txt_yes= customView.findViewById(R.id.block_txt);
        TextView txt_no= customView.findViewById(R.id.noblock_txt);
        TextView txt_label= customView.findViewById(R.id.txt);
        TextView txt_data= customView.findViewById(R.id.label);
        popupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean flag=hasConnection();
                if(flag){
                    String str=AllUrl.BLOCK_USER+loginUserId+"&block_id="+str_userId+"&mode=0";
                    String url=str.replaceAll(" ","%20");
                    // block(url);
                    blockVolley(url);
                    popupWindow.dismiss();
                }
                else{
                    Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                }



            }
        });



    }



    public void setFont(){
        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        main_label.setTypeface(bold_face);
        txt_unmatch.setTypeface(semi_bold_face);
        txt_block.setTypeface(semi_bold_face);
        txt_spam.setTypeface(semi_bold_face);
        txt_name.setTypeface(semi_bold_face);
        txt_occupation.setTypeface(reg_face);
        mute_label.setTypeface(semi_bold_face);



    }

    //convert image url to bitmap
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(ChatMenu.this,Chat.class);
        startActivity(i);
    }
}
