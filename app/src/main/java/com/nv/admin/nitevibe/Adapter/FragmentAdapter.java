package com.nv.admin.nitevibe.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nv.admin.nitevibe.Fragments.WithCoupon;
import com.nv.admin.nitevibe.Fragments.WithoutCoupon;

/**
 * Created by Admin on 6/1/2018.
 */
public class FragmentAdapter extends FragmentStatePagerAdapter {
    int TabCount;


    public FragmentAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        TabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                WithCoupon tab1 = new WithCoupon();
                return tab1;
            case 1:
                WithoutCoupon tab2 = new WithoutCoupon();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return TabCount;
    }


}
