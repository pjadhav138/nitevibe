package com.nv.admin.nitevibe.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Adapter.ProfileEventAdapter;
import com.nv.admin.nitevibe.Adapter.ProfileEventModel;
import com.nv.admin.nitevibe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class
UserProfile extends AppCompatActivity implements View.OnClickListener {
    public static ImageView profile1, setting, home, chat, userprofile;
    public static TextView edit, txt_name, txt_occupation, txt_city;
    public static RelativeLayout eventRelative;
    public static RecyclerView eventRecycler;
    public static String profileUrl;
    AlertDialog alert;
    public static ProfileEventAdapter profileEventAdapter;

    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId, loginUserMode, loginUserType, blockUserCount;
    public static Typeface bold_face, extra_bold_face, reg_face, semi_bold_face, calibri_bold, lato_bold, semi_bold_italic;
    ProgressDialog loading;
    // public static RoundedImage profile;
    // public static RoundedImageView profile;
    //  public static CircularImageView profile;
    public static ImageView profile;
    // public static CircleImageView profile;
    public static RelativeLayout emptyRel;
    public static TextView emptyTxt, label;
    public static ImageView userTab, homeTab, chatTab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        sharedpreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");


        profile = findViewById(R.id.profile);

        setting = findViewById(R.id.setting);


        emptyRel = findViewById(R.id.emptyrel);
        emptyTxt = findViewById(R.id.find);
        label = findViewById(R.id.find123);

        edit = findViewById(R.id.edit);
        txt_name = findViewById(R.id.name);
        txt_occupation = findViewById(R.id.occupation);
        txt_city = findViewById(R.id.city);

        userTab = findViewById(R.id.usertab);
        homeTab = findViewById(R.id.hometab);
        chatTab = findViewById(R.id.chattab);

        userTab.setImageDrawable(getResources().getDrawable(R.drawable.select_profile));

        eventRecycler = findViewById(R.id.eventRecycler);

        eventRecycler.setHasFixedSize(true);
        eventRecycler.setLayoutManager(new LinearLayoutManager(this));//Linear Items

        eventRelative = findViewById(R.id.eventRel);

        setFont();


        boolean flag = hasConnection();
        if (flag) {
            profileUrl = AllUrl.PROFILE + loginUserId;
            // settingProfileDetails(profileUrl);
            settingProfileDetailVolley(profileUrl);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection!", Toast.LENGTH_SHORT).show();
        }


        onClickEvent();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.setting) {
            //startActivity(new Intent(getActivity(),Setting.class));
            MyApplication.getInstance().trackEvent("Profile", "Setting", "Clicked");
            Intent i = new Intent(this, Setting.class);
            i.putExtra("block_count", blockUserCount);
            startActivity(i);
        }
        if (id == R.id.edit) {
            //Toast.makeText(getApplicationContext(),"Edit",Toast.LENGTH_SHORT).show();
            MyApplication.getInstance().trackEvent("Profile", "Edit Profile", "Success");
            startActivity(new Intent(this, EditProfile.class));
        }
        if (id == R.id.find) {
            // startActivity(new Intent(this, Home.class));
            startActivity(new Intent(this, Club.class));
            finish();
        }

        if (id == R.id.usertab) {
            MyApplication.getInstance().trackEvent("Profile", "User Profile", "Success");
            startActivity(new Intent(this, UserProfile.class));
            finish();
        }
        if (id == R.id.hometab) {
            MyApplication.getInstance().trackEvent("Club", "Club", "Success");
            startActivity(new Intent(this, Club.class));
            finish();
        }
        if (id == R.id.chattab) {
            MyApplication.getInstance().trackEvent("Chat", "Chat", "Success");
            startActivity(new Intent(this, Chat.class));
            finish();
        }


    }

    public void onClickEvent() {
        edit.setOnClickListener(this);
        setting.setOnClickListener(this);
        emptyTxt.setOnClickListener(this);

        userTab.setOnClickListener(this);
        homeTab.setOnClickListener(this);
        chatTab.setOnClickListener(this);
    }

    public void setFont() {
        bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold.ttf");
        extra_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        reg_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        semi_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBold.ttf");
        calibri_bold = Typeface.createFromAsset(getAssets(), "fonts/Calibri Bold.ttf");
        lato_bold = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");
        semi_bold_italic = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBoldItalic.ttf");

        txt_name.setTypeface(bold_face);
        txt_occupation.setTypeface(reg_face);
        txt_city.setTypeface(reg_face);
        edit.setTypeface(bold_face);
        emptyTxt.setTypeface(semi_bold_italic);
        label.setTypeface(reg_face);

    }

    public void settingProfileDetailVolley(String search_url) {
        loading = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        MyApplication.getInstance().getRequestQueue().getCache().remove(search_url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                search_url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("userprofile_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String res = jsonObject.getString("response");
                    List<ProfileEventModel> data = new ArrayList<>();
                    if (res.equals("200")) {
                        JSONArray array = jsonObject.getJSONArray("message");
                        if (array.length() == 0) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(UserProfile.this);
                            builder1.setMessage("No data found for your search.");
                            builder1.setCancelable(false);
                            builder1.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();


                                            // finish();
                                        }
                                    });
                            alert = builder1.create();
                            alert.show();
                            Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                            positive.setTextColor(getResources().getColor(R.color.female));
                        } else {
                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.getJSONObject(i);

                                JSONArray user_array = object.getJSONArray("user");
                                JSONArray event_array = object.getJSONArray("event");

                                if (user_array.length() == 0) {

                                    //  initiatePopupWindow();
                                    startActivity(new Intent(UserProfile.this, EditProfile.class));

                                } else {
                                    for (int k = 0; k < user_array.length(); k++) {
                                        JSONObject user_object = user_array.getJSONObject(k);

                                        String user_name = user_object.getString("name");
                                        String user_age = user_object.getString("age");
                                        String user_location = user_object.getString("location");
                                        String user_occupation = user_object.getString("occupation");
                                        String block_count = user_object.getString("block_count");
                                        blockUserCount = block_count;
                                        String user_profile = user_object.getString("profile");

                                        if (user_age.equals("")) {
                                            txt_name.setText(user_name);
                                        } else {
                                            txt_name.setText(user_name + ", " + user_age);
                                        }
                                        //occup
                                        if (user_occupation.equals("")) {
                                            txt_occupation.setVisibility(View.GONE);
                                        } else {
                                            txt_occupation.setVisibility(View.VISIBLE);
                                            txt_occupation.setText(user_occupation);
                                        }
                                        //loc
                                        if (user_location.equals("")) {

                                        } else {
                                            txt_city.setText(user_location);
                                        }

                                        //profile
                                        if (!user_profile.equals("")) {

                                            Bitmap img = getBitmapFromURL(user_profile);
                                            // CircleShape circleShape=new CircleShape(img);
                                            RoundedBitmapDrawable roundedImageDrawable = createRoundedBitmapImageDrawableWithBorder(img, 5);

                                            profile.setImageDrawable(roundedImageDrawable);


                                        } else {
                                            /*Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.default_icon);
                                              RoundedBitmapDrawable roundedImageDrawable = createRoundedBitmapImageDrawableWithBorder(icon,1);
                                              profile.setImageDrawable(roundedImageDrawable);
                                            */

                                            profile.setImageDrawable(getResources().getDrawable(R.drawable.circle_profile));


                                        }

                                    }
                                }


                                //event
                                if (event_array.length() == 0) {
                                    //no event
                                    // eventRelative.setVisibility(View.GONE);

                                    emptyRel.setVisibility(View.VISIBLE);
                                    eventRecycler.setVisibility(View.GONE);
                                } else {
                                    emptyRel.setVisibility(View.GONE);
                                    eventRecycler.setVisibility(View.VISIBLE);
                                    for (int j = 0; j < event_array.length(); j++) {
                                        JSONObject event_object = event_array.getJSONObject(j);

                                        ProfileEventModel model = new ProfileEventModel();

                                        model.eventId = event_object.getString("event_id");
                                        model.eventDate = event_object.getString("event_date");
                                        model.eventName = event_object.getString("event_name");
                                        model.clubId = event_object.getString("club_id");

                                        data.add(model);


                                    }

                                    profileEventAdapter = new ProfileEventAdapter(data, UserProfile.this);
                                    eventRecycler.setAdapter(profileEventAdapter);
                                }


                            }


                        }  //end of else


                    } else {
                        Toast.makeText(UserProfile.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        RequestQueue queue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
        queue.getCache().clear();
        jsonObjReq.setShouldCache(false);

        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();

            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    //convert image url to bitmap
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }


    private RoundedBitmapDrawable createRoundedBitmapImageDrawableWithBorder(Bitmap bitmap, int widthValue) {
        int bitmapWidthImage = bitmap.getWidth();
        int bitmapHeightImage = bitmap.getHeight();
        int borderWidthHalfImage = 4;

        int bitmapRadiusImage = Math.min(bitmapWidthImage, bitmapHeightImage) / 2;
        int bitmapSquareWidthImage = Math.min(bitmapWidthImage, bitmapHeightImage);
        int newBitmapSquareWidthImage = bitmapSquareWidthImage + borderWidthHalfImage;

        Bitmap roundedImageBitmap = Bitmap.createBitmap(newBitmapSquareWidthImage, newBitmapSquareWidthImage, Bitmap.Config.ARGB_8888);
        Canvas mcanvas = new Canvas(roundedImageBitmap);
        // mcanvas.drawColor(Color.RED);
        // mcanvas.drawColor(getResources().getColor(R.color.transparent));
        int i = borderWidthHalfImage + bitmapSquareWidthImage - bitmapWidthImage;
        int j = borderWidthHalfImage + bitmapSquareWidthImage - bitmapHeightImage;

        mcanvas.drawBitmap(bitmap, i, j, null);

        Paint borderImagePaint = new Paint();
        borderImagePaint.setStyle(Paint.Style.STROKE);
        borderImagePaint.setAntiAlias(true);

        // borderImagePaint.setStrokeWidth(borderWidthHalfImage*2);
        borderImagePaint.setStrokeWidth(widthValue);
        //  borderImagePaint.setColor(Color.GRAY);
        borderImagePaint.setColor(getResources().getColor(R.color.male));
        mcanvas.drawCircle(mcanvas.getWidth() / 2, mcanvas.getWidth() / 2, newBitmapSquareWidthImage / 2, borderImagePaint);

        RoundedBitmapDrawable roundedImageBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), roundedImageBitmap);
        roundedImageBitmapDrawable.setCornerRadius(bitmapRadiusImage);
        roundedImageBitmapDrawable.setAntiAlias(true);
        return roundedImageBitmapDrawable;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, Club.class);
        startActivity(i);
    }

}
