package com.nv.admin.nitevibe.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.ClubEventDetails;
import com.nv.admin.nitevibe.Activity.Events;
import com.nv.admin.nitevibe.R;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 4/16/2018.
 */
public class ClubEventAdapter extends RecyclerView.Adapter<ClubEventAdapter.MyView> {
    //specificclub details event adapter
    private List<ClubEventModel> arrayList;
    private Context context;
    ClubEventModel current;
    private boolean isChecked;
    public String favClubUrl;
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    ProgressDialog loading;

    public ClubEventAdapter(List<ClubEventModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public ClubEventAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
       /* View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.club_detail_row, parent, false);*/
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_detail_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ClubEventAdapter.MyView holder, int position) {
        sharedpreferences=context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");
        current=arrayList.get(position);
        bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        holder.date.setTypeface(extra_bold_face);
        holder.month.setTypeface(semi_bold_face);
        holder.name.setTypeface(extra_bold_face);
        holder.rate.setTypeface(semi_bold_face);
        holder.total.setTypeface(semi_bold_face);

        String event_date=current.eventDate;
        String[] abc=event_date.split(" ");
        String date=abc[0];
        String mnth=abc[1];

        holder.date.setText(date);
        holder.month.setText(mnth);
        String event_id=current.eventId;
        holder.name.setText(current.eventName);
        String user_going=current.userGoing;
        int going= Integer.parseInt(user_going);

        holder.event_id.setText(current.eventId);

        if(going>=10){
            holder.total.setVisibility(View.VISIBLE);
           holder.total.setText(user_going+ " People are going");
            holder.progressRel.setVisibility(View.VISIBLE);
          //  holder.progressBar.setMax(Integer.parseInt(current.male));
            holder.progressBar.setSecondaryProgress(Integer.parseInt(current.female));


        }
        else{
           holder.total.setVisibility(View.GONE);
            holder.progressRel.setVisibility(View.GONE);
        }


        if(!current.eventImg.equals("")){
            Picasso.with(context.getApplicationContext())
                    .load(current.eventImg)
                    .into(holder.main_img);
        }


         String fav=current.fav;
        if(current.fav.equals("1")){

            Picasso.with(context.getApplicationContext())
                    .load(R.drawable.star_on_2)
                    .into(holder.fav_club);
        }
        else{

            Picasso.with(context.getApplicationContext())
                    .load(R.drawable.star_off)
                    .into(holder.fav_club);
        }

        holder.main_img.setOnClickListener(  new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(context,holder.event_id.getText().toString(),Toast.LENGTH_SHORT).show();
                ClubEventDetails.handlerStop=true;
                Intent i=new Intent(context, Events.class);
                SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
                editor.remove("event_id");
                editor.commit();
                editor.putString("event_id", holder.event_id.getText().toString());
                editor.commit();

                context.startActivity(i);
            }
        });

        holder.img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                byte[] data;
                String eventid=holder.event_id.getText().toString();
                data = eventid.getBytes(StandardCharsets.UTF_8);

                String base64 = Base64.encodeToString(data, Base64.DEFAULT);

                // Log.i("Base 64 ", base64);
                Log.d("base_64eventconversion",base64);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                // Add data to the intent, the receiving app will decide
                // what to do with it.
                // String club_link="http://rednwhite.co.in/event_details.php?id="+base64;
                String event_link=AllUrl.EVENT_SHARE+base64;
                share.putExtra(Intent.EXTRA_TEXT,event_link);

                context.startActivity(Intent.createChooser(share,"Share link!"));

            }
        });

        holder.fav_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isChecked = !isChecked;
                holder.fav_club.setImageResource(isChecked ? R.drawable.star_on_2 : R.drawable.star_off);

                Log.d("value of isCheck", String.valueOf(isChecked));

                String status = null;
                if(isChecked==true){
                    status="Active";
                }
                else if(isChecked==false){
                    status="Inactive";
                }

                boolean flag=hasConnection();
                if(flag){

                   // postfavclub(loginUserId,holder.event_id.getText().toString(),"1",status);
                    postfavclubvolley(loginUserId,holder.event_id.getText().toString(),"1",status);
                }
                else{
                    Toast.makeText(context,"Please check your internet connection!",Toast.LENGTH_LONG).show();
                }



            }
        });




    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public ImageView fav_club,img_share;
        public TextView date,month,name,rate,total,event_id;
        public RelativeLayout progressRel;
        public ProgressBar progressBar;
        public RoundedImageView main_img;
       // public ImageView main_img;
        public MyView(View itemView) {
            super(itemView);
           // main_img=(ImageView)itemView.findViewById(R.id.club_img);
            main_img= itemView.findViewById(R.id.club_img);

            fav_club= itemView.findViewById(R.id.fav);
            img_share= itemView.findViewById(R.id.share);
            date= itemView.findViewById(R.id.date);
            month= itemView.findViewById(R.id.month);
            name= itemView.findViewById(R.id.club_name);
            rate= itemView.findViewById(R.id.club_rate);
            total= itemView.findViewById(R.id.club_total);
            progressRel= itemView.findViewById(R.id.progress_rel);
            progressBar= itemView.findViewById(R.id.progressBar);
            event_id= itemView.findViewById(R.id.event_id);


        }
    }


    //post method

    //post method
    private void postfavclubvolley(final String user_id, final String fav_id, final String mode, final String status){
        StringRequest postRequest = new StringRequest(Request.Method.POST,AllUrl.FAV_CLUB,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("fav_club_response", response);
                       // loading.dismiss();
                        try {
                            JSONObject object = new JSONObject(response);
                            String res = object.getString("response");
                            String message = object.getString("message");
                            if (res.equals("200")) {

                            }
                            else{
                                Toast.makeText(context,"Failed to make club favourite.Please try again!",Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                       // loading.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  data = new HashMap<>();
                // the POST parameters:

                data.put("user_id",user_id);
                data.put("fav_id",fav_id);
                data.put("mode",mode);
                data.put("status",status);

                return data;
            }

        };

        Volley.newRequestQueue(context).getCache().clear();

        Volley.newRequestQueue(context).add(postRequest);
      /*  loading= new ProgressDialog(context,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();*/


    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }


}
