package com.nv.admin.nitevibe.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Forgot_Password extends AppCompatActivity implements View.OnClickListener {

    public static PinEntryEditText ed_otp;
    public static ImageView submit,back;
    public static TextView  txt_title,txt_detail,txt_timer,txt_user,txt_password,txt_repassword,txt_resend;
    public static EditText ed_username,ed_password,ed_repassword;
    public static Button btn_update;
    public static RelativeLayout userRel,otpRel,passwordRel;
    //long millisInFuture = 25000;
    long millisInFuture = 60000;
    long countDownInterval = 1000;
    public static String str_userName,str_password,str_rePassword,str_otp;
    AlertDialog alert;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold;
    CountDownTimer waitTimer;
    ProgressDialog loading;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot__password);
        ed_otp= findViewById(R.id.otp); //ed otp
        submit= findViewById(R.id.submit);
        back= findViewById(R.id.back);
        txt_title= findViewById(R.id.txt); //forgot_password_label
        txt_detail= findViewById(R.id.txt1); //below data
        txt_timer= findViewById(R.id.time);
        txt_user= findViewById(R.id.usertxt); //mobile/name
        txt_password= findViewById(R.id.passwordtxt); //passtxt
        txt_repassword= findViewById(R.id.repasswordtxt); //retxt
        txt_resend= findViewById(R.id.resend);
        ed_username= findViewById(R.id.username);
        ed_password= findViewById(R.id.password); //pass
        ed_repassword= findViewById(R.id.repassword); //repass
        btn_update= findViewById(R.id.update);
        userRel= findViewById(R.id.user_rel);
        otpRel= findViewById(R.id.otp_rel);
        passwordRel= findViewById(R.id.password_rel);


        setFont();

        addClickEvent();
        //ed_username.setText("shweta@appmonks.net");




        }

    public void addClickEvent(){
        submit.setOnClickListener(this);
        txt_resend.setOnClickListener(this);
        back.setOnClickListener(this);
        ed_username.addTextChangedListener(new MyTextWatcher(ed_username));
        ed_password.addTextChangedListener(new MyTextWatcher(ed_password));
        ed_repassword.addTextChangedListener(new MyTextWatcher(ed_repassword));

        btn_update.setOnClickListener(this);
        otp_edittext_validation();
    }

    public void otp_edittext_validation(){
        if(ed_otp != null){
            ed_otp.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {


                    Log.d("timer",txt_timer.getText().toString());
                    String time_left=txt_timer.getText().toString();
                    if(time_left.equals("00:00 seconds")){
                        Toast.makeText(getApplicationContext(),"Time ups! Please resend the otp ",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Log.d("value_of_otp",str.toString());
                        //start of if
                        //str_otp="123456";
                        if(str.length()==6 && str.toString().equals(str_otp) ){
                            Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();


                        }
                        else{
                            ed_otp.setError(true);
                            Toast.makeText(getApplicationContext(),"Invalid Otp",Toast.LENGTH_SHORT).show();

                            ed_otp.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ed_otp.setText(null);
                                }
                            },1000);
                        }

                        //end of else
                    }

                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        if(id==R.id.submit){
            String userName=ed_username.getText().toString();
            if(!userName.equals("")){
                if(userName.matches("[0-9]+")){
                    validateMobile();
                }
                else{
                    validateEmail();
                }
            }
            else{
                Toast.makeText(getApplicationContext(),"Please enter user name",Toast.LENGTH_SHORT).show();
            }

        }
        if(id==R.id.update){
            String btn_value=btn_update.getText().toString();

            if(btn_value.equals("Verify OTP")){
                String otp123=ed_otp.getText().toString();
                if(otp123.equals("")){
                    Toast.makeText(getApplicationContext(),"Please enter otp",Toast.LENGTH_SHORT).show();
                }
                else{
                    resetScreen();
                }

            }
            else if(btn_value.equals("Update Password")){
                PasswordDetails();


            }

        }
        if(id==R.id.back){
            String buttonValue=btn_update.getText().toString();
            Log.d("buttonValue",buttonValue);

            if(buttonValue.equals("")){
                startActivity(new Intent(this,Login.class));
            }
            else if(buttonValue.equals("Verify OTP")){


                otpRel.setVisibility(View.GONE);
                userRel.setVisibility(View.VISIBLE);
                btn_update.setVisibility(View.GONE);
                txt_title.setText(R.string.forgot_password);
                txt_detail.setText(R.string.forgot_password_txt);
                ed_otp.setText("");
                waitTimer.cancel();

            }
            else if(buttonValue.equals("Update Password")){
               // PasswordDetails();
            }
            else{
                startActivity(new Intent(this,Login.class));
            }


        }
        if(id==R.id.resend){

            ed_otp.setText("");
            waitTimer.cancel();
            String url2=AllUrl.OTP+ed_username.getText().toString();
            String url3=url2.replaceAll(" ","%20");
            Log.d("otp_url",url3);
            boolean flag=hasConnection();
            if(flag){
              //  userOtp(url3);
                userOtpVolley(url3);
                startCountDown();
            }
            else{
                Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
            }


        }

    }

    public void resetScreen(){
        txt_title.setText("Reset Password");
        txt_detail.setText("Enter a new password for your account");
        passwordRel.setVisibility(View.VISIBLE);
        otpRel.setVisibility(View.GONE);
        back.setVisibility(View.GONE);
        btn_update.setText("Update Password");
    }

    public void PasswordDetails(){
        str_userName=ed_username.getText().toString();
        str_password=ed_password.getText().toString();
        str_rePassword=ed_repassword.getText().toString();

        if(str_password.equals(str_rePassword)){

            String url=AllUrl.FORGOT_PASSWORD+str_userName+"&password="+str_password;
            String url1=url.replaceAll(" ","%20");
            boolean flag=hasConnection();
            if(flag){
               // userForgotPassword(url1);
                userForgotPasswordVolley(url1);
            }
            else{
                Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT);
            }
        }
        else{
            Toast.makeText(getApplicationContext(),"Password does not match",Toast.LENGTH_SHORT).show();
        }

    }

    private class MyTextWatcher implements TextWatcher {
        private View view;


        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Log.d("click_on_edit_text","beforeTextChanged");

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Log.d("click_on_edit_text","onTextChanged");

        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()){
                case R.id.username:
                    Log.d("click_on_edit_text","afterTextChanged");
                         txt_user.setVisibility(View.VISIBLE);

                    break;
                case R.id.password:
                    txt_password.setVisibility(View.VISIBLE);
                    txt_password.setTextColor(getResources().getColor(R.color.light_green));
                    break;
                case R.id.repassword:
                    txt_repassword.setVisibility(View.VISIBLE);
                    txt_repassword.setTextColor(getResources().getColor(R.color.light_green));
                    txt_password.setTextColor(getResources().getColor(R.color.light_blue));
                    break;




            }

        }
    }

    private boolean validateMobile(){
        String phone_number=ed_username.getText().toString().trim();
        Log.d("length", String.valueOf(phone_number.length()));
        isValidPhone(phone_number);

        if(phone_number.length()>=10){
            if(isValidPhone(phone_number)==false){
                Toast.makeText(getApplicationContext(),"please enter a valid mobile number",Toast.LENGTH_SHORT).show();
                return false;
            }
            else{
                String url2=AllUrl.OTP+ed_username.getText().toString();
                String url3=url2.replaceAll(" ","%20");
                Log.d("otp_url",url3);
                boolean flag=hasConnection();
                if(flag){
                   // userOtp(url3);
                    userOtpVolley(url3);
                    txt_title.setText(phone_number);
                    txt_detail.setText(R.string.forgot_mobile);
                    userRel.setVisibility(View.GONE);
                    otpRel.setVisibility(View.VISIBLE);
                    btn_update.setVisibility(View.VISIBLE);
                    btn_update.setText("Verify OTP");
                    startCountDown();

                }
                else{
                    Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        }else{
            Toast.makeText(getApplicationContext(),"please enter a valid mobile number",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public static boolean isValidPhone(String phone)
    {
        String expression = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
        CharSequence inputString = phone;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        return matcher.matches();
    }

    private boolean validateEmail(){
        String email_id=ed_username.getText().toString().trim();

        if(isValidEmail(email_id)==false){
            Toast.makeText(getApplicationContext(),"please enter a valid email id",Toast.LENGTH_SHORT).show();


            return false;
        }
        else{
            String url2=AllUrl.OTP+ed_username.getText().toString();
            String url3=url2.replaceAll(" ","%20");
            Log.d("otp_url",url3);
            boolean flag=hasConnection();
            if(flag){
               // userOtp(url3);
                userOtpVolley(url3);
                txt_title.setText(email_id);
                txt_detail.setText(R.string.forgot_email);
                userRel.setVisibility(View.GONE);
                otpRel.setVisibility(View.VISIBLE);
                btn_update.setVisibility(View.VISIBLE);
                btn_update.setText("Verify OTP");
                startCountDown();

            }
            else{
                Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
            }


        }
        return true;
    }

    private static boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email)&& Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public void startCountDown(){
        waitTimer= new CountDownTimer(millisInFuture,countDownInterval){
            public void onTick(long millisUntilFinished){
                //do something in every tick
                //Display the remaining seconds to app interface
                //1 second = 1000 milliseconds
                txt_timer.setText("Seconds remaining : " + millisUntilFinished / 1000);
            }
            public void onFinish(){
                //Do something when count down finished
                txt_timer.setText("00:00 seconds");
            }
        }.start();
    }

    //change the password
    private void userForgotPasswordVolley(String url){

        loading= new ProgressDialog(Forgot_Password.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("forgotpasswordresponse", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(Forgot_Password.this);
                        builder1.setMessage("You have successfully changed your password");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        startActivity(new Intent(Forgot_Password.this,Login.class));
                                        MyApplication.getInstance().trackEvent("Forgot Password", "Change Password", "Success");

                                    }
                                });
                        alert = builder1.create();
                        alert.show();
                        Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                        positive.setTextColor(getResources().getColor(R.color.female));

                    }else{
                        Toast.makeText(Forgot_Password.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    //otp
    //change the password
    private void userOtpVolley(String url){

        loading= new ProgressDialog(Forgot_Password.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("otp_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        str_otp=msg;
                        MyApplication.getInstance().trackEvent("Forgot Password", "Send OTP", "Success");
                    }else{
                        Toast.makeText(Forgot_Password.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });
        jsonObjReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
       // jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void setFont(){
        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");

        txt_title.setTypeface(bold_face);
        txt_detail.setTypeface(reg_face);
        txt_user.setTypeface(lato_bold);
        ed_username.setTypeface(semi_bold_face);
        ed_otp.setTypeface(semi_bold_face);

        txt_password.setTypeface(lato_bold);
        txt_repassword.setTypeface(lato_bold);
        ed_password.setTypeface(semi_bold_face);
        ed_repassword.setTypeface(semi_bold_face);

    }




    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

}
