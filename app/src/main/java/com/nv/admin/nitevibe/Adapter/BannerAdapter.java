package com.nv.admin.nitevibe.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nv.admin.nitevibe.Activity.Events;
import com.nv.admin.nitevibe.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 4/2/2018.
 */
public class BannerAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    Activity activity;
    //ArrayList<BannerModel> image_arraylist;
    ArrayList<String> image_arraylist;
    ArrayList<String> eventlist;
    ArrayList<String> clubList;

    public BannerAdapter(Activity activity, ArrayList<String> image_arraylist, ArrayList<String> eventlist, ArrayList<String> clubList) {
        this.activity = activity;
        this.image_arraylist = image_arraylist;
        this.eventlist = eventlist;
        this.clubList = clubList;
    }





    @Override
    public int getCount() {
        return image_arraylist.size();
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.banner_row, container, false);

        ImageView ban_img= view.findViewById(R.id.banner_img);
         final TextView ban_event_id= view.findViewById(R.id.eventid);
        final  TextView ban_club_id= view.findViewById(R.id.clubid);



        //BannerModel model=image_arraylist.get(position);
        String abc=image_arraylist.get(position);
        String img_name= String.valueOf(image_arraylist.get(position));

        ban_event_id.setText(eventlist.get(position));
        ban_club_id.setText(clubList.get(position));



        Log.d("banner_img_urls",img_name);

        Picasso.with(activity.getApplicationContext())
                .load(image_arraylist.get(position))
                .into(ban_img);

        ban_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(activity,ban_event_id.getText().toString(),Toast.LENGTH_SHORT).show();
                String eventId1=ban_event_id.getText().toString();
                if(eventId1.equals("0")){
                    Log.d("Bannerevent","banner with no event");
                }
                else {
                    Intent i=new Intent(activity, Events.class);
                    i.putExtra("banner_club_id",ban_club_id.getText().toString());
                    i.putExtra("banner_event_id",ban_event_id.getText().toString());
                    i.putExtra("banner_from","banner");
                    activity.startActivity(i);
                }

            }
        });


        container.addView(view);

        return view;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
