package com.nv.admin.nitevibe.notification;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.nv.admin.nitevibe.Database.DatabaseHandler;
import com.nv.admin.nitevibe.Database.SettingModel;
import com.nv.admin.nitevibe.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.Context.POWER_SERVICE;
import static android.support.v4.app.NotificationCompat.BADGE_ICON_SMALL;

/**
 * Created by Admin on 4/30/2018.
 */
public class NotificationUtils {

    private static String TAG = NotificationUtils.class.getSimpleName();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    SharedPreferences sharedpreferences;
    public static String  PREFS_NAME="mypref";
    public static String email,role;

    private Context mContext;
    public static int count;
    public int dummyuniqueInt;
    PendingIntent resultPendingIntent;
    String CHANNEL_ID = "my_channel_01";// The id of the channel.
    CharSequence name = "Nitevibe";// The user-visible name of the channel.
    int importance = NotificationManager.IMPORTANCE_HIGH;
    public static DatabaseHandler handler;
    List<SettingModel> arrayList;
    public boolean mute_flag;
    AudioManager amanager;


    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }



    public void showNotificationMessage(String title, String message, String timeStamp, Intent intent) {
        showNotificationMessage(title, message, timeStamp, intent, null);
        sharedpreferences = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        }
        public void showNotificationMessage(final String title, final String message, final String timeStamp, Intent intent, String imageUrl) {
        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;

        count++;

        //int dummyuniqueInt = new Random().nextInt(543254);
        dummyuniqueInt=(int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        Log.i("Count", "Notification Count Value =====>" + dummyuniqueInt);
        // notification icon
        //final int icon = R.drawable.loader;
            final int icon = R.drawable.notify_logo;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Log.e(TAG,"userid : -"+intent.hasExtra("userid"));
        PendingIntent resultPendingIntent =PendingIntent.getActivity(mContext, dummyuniqueInt, intent, PendingIntent.FLAG_CANCEL_CURRENT);
       // PendingIntent resultPendingIntent =PendingIntent.getActivity(mContext, dummyuniqueInt, intent, PendingIntent.FLAG_UPDATE_CURRENT);
       // PendingIntent resultPendingIntent =PendingIntent.getActivity(mContext, dummyuniqueInt, intent, PendingIntent.FLAG_ACTIVITY_NEW_TASK);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);


        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + mContext.getPackageName() + "/raw/notification");
            //final Uri alarmSound = Uri.parse("");

        if (!TextUtils.isEmpty(imageUrl)) {

            if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {

                Bitmap bitmap = getBitmapFromURL(imageUrl);

                if (bitmap != null) {
                    showBigNotification(bitmap, mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
                } else {
                    showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
                }
            }
        } else {
            showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
            playNotificationSound();
        }

        //
        Log.d("Notification_page","This is notification");
        //


    }

    private void showSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound) {
        // Logic to turn on the screen
        Log.d("Type_of_noti","SmallNoti");
        PowerManager powerManager = (PowerManager) mContext.getSystemService(POWER_SERVICE);

        if (!powerManager.isInteractive()){ // if screen is not already on, turn it on (get wake_lock for 10 seconds)
            PowerManager.WakeLock wl = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MH24_SCREENLOCK");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MH24_SCREENLOCK");
            wl_cpu.acquire(10000);
        }


        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(message);

        Notification notification;

        handler=new DatabaseHandler(mContext);
        //inserting values
        arrayList=new ArrayList<>();
        arrayList=handler.getAllData();
        for(int i=0;i<arrayList.size();i++){
            SettingModel model;
            model=arrayList.get(i);
            String name=model.getLabel();
            String dbdata=model.getData();

            //mute,show,message,match,event,location,checkin
            //public boolean mute_flag,message_flag,match_flag,event_flag,location_flag,check_in_flag
            if(name.equals("mute")){
                mute_flag= Boolean.parseBoolean(dbdata);

            }



        }


        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)){
            Log.d("notify_loop","enytered");

            if(mute_flag){
                Log.d("mute_notify","true");
                notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                        .setAutoCancel(true)
                        .setContentTitle(title)
                        .setContentIntent(resultPendingIntent)
                        .setSound(null)
                        //.setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setChannelId(CHANNEL_ID)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setWhen(getTimeMilliSec(timeStamp))
                        .setSmallIcon(R.drawable.loader)
                        .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                        .setContentText(message)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) //to show content on lock screen
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setBadgeIconType(BADGE_ICON_SMALL)
                        .setNumber(count)
                        .build();
            }
            else{
                Log.d("mute_notify","false");
                notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                        .setAutoCancel(true)
                        .setContentTitle(title)
                        .setContentIntent(resultPendingIntent)
                        .setSound(alarmSound)
                        .setDefaults(0)
                        //.setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setChannelId(CHANNEL_ID)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setWhen(getTimeMilliSec(timeStamp))
                        .setSmallIcon(R.drawable.loader)
                        .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                        .setContentText(message)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) //to show content on lock screen
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setBadgeIconType(BADGE_ICON_SMALL)
                        .setNumber(count)
                        .build();
            }




        }
        else{
            Log.d("notify_loop"," not enytered");

            if(mute_flag){
                Log.d("mute_notify","true");
                notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                        .setAutoCancel(true)
                        .setContentTitle(title)
                        .setContentIntent(resultPendingIntent)
                        .setSound(null)
                        //.setStyle(new NotificationCompat.BigTextStyle().bigText(message))

                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))

                        .setWhen(getTimeMilliSec(timeStamp))
                        .setSmallIcon(R.drawable.loader)
                        .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                        .setContentText(message)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) //to show content on lock screen
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setBadgeIconType(BADGE_ICON_SMALL)
                        .setNumber(count)

                        .build();

            }
            else{
                Log.d("mute_notify","false");
                notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                        .setAutoCancel(true)
                        .setContentTitle(title)
                        .setContentIntent(resultPendingIntent)
                        .setSound(alarmSound)
                        .setDefaults(0)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setWhen(getTimeMilliSec(timeStamp))
                        .setSmallIcon(R.drawable.loader)
                        .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                        .setContentText(message)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) //to show content on lock screen
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setBadgeIconType(BADGE_ICON_SMALL)
                        .setNumber(count)
                        .build();

            }



        }


        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) ){
            Log.d("notification_enterted","oreo device");



            NotificationChannel notificationChannel = notificationManager.getNotificationChannel("my_channel_01");
            if (notificationChannel == null) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                notificationChannel = new NotificationChannel("my_channel_01","channelDescription", importance);
                notificationChannel.setLightColor(Color.GREEN);
                notificationChannel.enableVibration(true);
                notificationChannel.setShowBadge(true);

                notificationManager.createNotificationChannel(notificationChannel);
            }



        }
        notificationManager.notify(count, notification);

      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(mChannel);
        }*/



    }

    private void showBigNotification(Bitmap bitmap, NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound) {
        // Logic to turn on the screen
        Log.d("Type_of_noti","BigNoti");

        handler=new DatabaseHandler(mContext);
        //inserting values
        arrayList=new ArrayList<>();
        arrayList=handler.getAllData();
        for(int i=0;i<arrayList.size();i++){
            SettingModel model;
            model=arrayList.get(i);
            String name=model.getLabel();
            String dbdata=model.getData();

            //mute,show,message,match,event,location,checkin
            //public boolean mute_flag,message_flag,match_flag,event_flag,location_flag,check_in_flag
            if(name.equals("mute")){
                mute_flag= Boolean.parseBoolean(dbdata);

            }



        }
        PowerManager powerManager = (PowerManager) mContext.getSystemService(POWER_SERVICE);

        if (!powerManager.isInteractive()){ // if screen is not already on, turn it on (get wake_lock for 10 seconds)
            PowerManager.WakeLock wl = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MH24_SCREENLOCK");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MH24_SCREENLOCK");
            wl_cpu.acquire(10000);
        }

        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);
        Notification notification;


        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)){
            Log.d("notify_loop","enytered");
            if(mute_flag){
                Log.d("mute_notify","true");
                notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                        .setAutoCancel(true)
                        .setSmallIcon(R.drawable.loader)
                        .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                        .setContentTitle(title)
                        .setContentIntent(resultPendingIntent)
                        .setSound(null)
                        .setContentText(message)
                        .setChannelId(CHANNEL_ID)
                        //.setStyle(bigPictureStyle)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                        .setWhen(getTimeMilliSec(timeStamp))
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) //to show content on lock screen
                        ///.setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setBadgeIconType(BADGE_ICON_SMALL)
                        .setNumber(count)
                        .build();


            }
            else{
                Log.d("mute_notify","false");
                notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                        .setAutoCancel(true)
                        .setSmallIcon(R.drawable.loader)
                        .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                        .setContentTitle(title)
                        .setContentIntent(resultPendingIntent)
                        .setSound(alarmSound)
                        .setDefaults(0)
                        .setContentText(message)
                        .setChannelId(CHANNEL_ID)
                        //.setStyle(bigPictureStyle)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                        .setWhen(getTimeMilliSec(timeStamp))
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) //to show content on lock screen
                        ///.setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setBadgeIconType(BADGE_ICON_SMALL)
                        .setNumber(count)
                        .build();


            }



        }
        else{
            Log.d("notify_loop"," not enytered");
            if(mute_flag){
                notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                        .setAutoCancel(true)
                        .setSmallIcon(R.drawable.loader)
                        .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                        .setContentTitle(title)
                        .setContentIntent(resultPendingIntent)
                        .setSound(null)
                        .setContentText(message)
                        .setWhen(getTimeMilliSec(timeStamp))
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) //to show content on lock screen
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                        .setBadgeIconType(BADGE_ICON_SMALL)
                        .setNumber(count)
                        .build();
            }
            else{
                notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                        .setAutoCancel(true)
                        .setSmallIcon(R.drawable.loader)
                        .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                        .setContentTitle(title)
                        .setContentIntent(resultPendingIntent)
                        .setSound(alarmSound)
                        .setDefaults(0)
                        .setContentText(message)
                        .setWhen(getTimeMilliSec(timeStamp))
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) //to show content on lock screen
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                        .setBadgeIconType(BADGE_ICON_SMALL)
                        .setNumber(count)
                        .build();
            }




        }
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(Config.NOTIFICATION_ID_BIG_IMAGE, notification);

    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Playing notification sound
    public void playNotificationSound() {
        try {
            handler=new DatabaseHandler(mContext);
            //inserting values
            arrayList=new ArrayList<>();
            arrayList=handler.getAllData();
            for(int i=0;i<arrayList.size();i++){
                SettingModel model;
                model=arrayList.get(i);
                String name=model.getLabel();
                String dbdata=model.getData();

                //mute,show,message,match,event,location,checkin
                //public boolean mute_flag,message_flag,match_flag,event_flag,location_flag,check_in_flag
                if(name.equals("mute")){
                    mute_flag= Boolean.parseBoolean(dbdata);

                }



            }
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + mContext.getPackageName() + "/raw/notification");
            Ringtone r = RingtoneManager.getRingtone(mContext, alarmSound);

           // r.play();
            if(mute_flag){
                Log.d("sound_value","true");

            }
            else{
                Log.d("sound_value","false");
                r.play();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static long getTimeMilliSec(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }


}
