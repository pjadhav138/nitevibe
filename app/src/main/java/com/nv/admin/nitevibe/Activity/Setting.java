package com.nv.admin.nitevibe.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nv.admin.nitevibe.Adapter.Mainprofile;
import com.nv.admin.nitevibe.Adapter.Subprofile;
import com.nv.admin.nitevibe.Database.DatabaseHandler;
import com.nv.admin.nitevibe.Database.SettingModel;
import com.nv.admin.nitevibe.R;
import com.facebook.login.LoginManager;
import com.nv.admin.nitevibe.Retrofit.ApiClient;
import com.nv.admin.nitevibe.Retrofit.ApiInterface;
import com.nv.admin.nitevibe.custom.SwipeDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class Setting extends AppCompatActivity implements View.OnClickListener {
    public static CustomSwitchButton muteNoti, showNoti, message, match, event, autoLocation, checkInNoti;
    public static ImageView img_block, img_help, img_support, img_legal, back, img_share;
    public static TextView txtBlockUser, deleteAcc;
    public static Button logout;
    public static String Str_muteNotification, str_showNotification, str_message, str_match, str_event, str_autoLocation, str_checkInNotification;
    AlertDialog logoutAlert, deleteAlert;
    public static TextView txt_setting, txt_noti, txt_mute, txt_show_noti, txt_msg, txt_match, txt_event, txt_loc,
            txt_autodetect, txt_checkin, txt_tellfrnd, txt_shareapp, txt_blocklabel, txt_block, txt_help_label, txt_help, txt_support, txt_legal;
    public static Typeface bold_face, extra_bold_face, reg_face, semi_bold_face;
    public static DatabaseHandler handler;
    List<SettingModel> arrayList;
    public static String deleteReason, deleteUrl;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId, loginUserMode, loginUserType, userBlockCount;
    public PopupWindow popupWindow;
    RelativeLayout mRelativeLayout;
    ProgressDialog loading;
    private String TAG=getClass().getSimpleName();
    ApiInterface apiService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        // setContentView(R.layout.demo2);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        sharedpreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");

        bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold.ttf");
        extra_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        reg_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        semi_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBold.ttf");

        mRelativeLayout = findViewById(R.id.settingREl);


        //initializing db
        handler = new DatabaseHandler(this);
        //inserting values
        arrayList = new ArrayList<>();
        arrayList = handler.getAllData();

        Log.d("setting_database_value", arrayList.toString());


        txt_setting = findViewById(R.id.txt);

        txt_noti = findViewById(R.id.notif_label);
        txt_mute = findViewById(R.id.notif_txt1);
        txt_show_noti = findViewById(R.id.notif_txt2);
        txt_msg = findViewById(R.id.notif_txt3);
        txt_match = findViewById(R.id.notif_txt4);
        txt_event = findViewById(R.id.notif_txt5);
        txt_loc = findViewById(R.id.location_label);
        txt_autodetect = findViewById(R.id.loc_txt1);
        txt_checkin = findViewById(R.id.loc_txt2);
        txt_tellfrnd = findViewById(R.id.share_txt);
        txt_shareapp = findViewById(R.id.share_label);
        txt_blocklabel = findViewById(R.id.block_label);
        txt_block = findViewById(R.id.block_txt);
        txt_help_label = findViewById(R.id.help_label);
        txt_help = findViewById(R.id.help_txt);
        txt_support = findViewById(R.id.support_txt);
        txt_legal = findViewById(R.id.legal_txt);


        boolean flag = hasConnection();
        if (flag) {
            String url = AllUrl.BLOCK_COUNT + loginUserId;
            blockcount(url);
        } else {
            Toast.makeText(getApplicationContext(), "No internet connection ", Toast.LENGTH_SHORT).show();
        }


        Bundle extras = getIntent().getExtras();
        Intent intent = getIntent();

        if (extras != null) {

            userBlockCount = intent.getExtras().getString("block_count");
            // txt_block.setText(userBlockCount+" Blocked Users");
        } else {
            // txt_block.setText("Blocked User");
        }


        muteNoti = findViewById(R.id.notif_tog1);
        showNoti = findViewById(R.id.notif_tog2);
        message = findViewById(R.id.notif_tog3);
        match = findViewById(R.id.notif_tog4);
        event = findViewById(R.id.notif_tog5);
        autoLocation = findViewById(R.id.loc_tog1);
        checkInNoti = findViewById(R.id.loc_tog2);

        img_block = findViewById(R.id.block_data);
        img_help = findViewById(R.id.help_data);
        img_support = findViewById(R.id.support_data);
        img_legal = findViewById(R.id.legal_data);
        back = findViewById(R.id.back);
        img_share = findViewById(R.id.share_data);

        txtBlockUser = findViewById(R.id.block_txt);
        deleteAcc = findViewById(R.id.delete);

        logout = findViewById(R.id.logout);
        onClickEvent();
        setfont();

        for (int i = 0; i < arrayList.size(); i++) {

            SettingModel model;
            model = arrayList.get(i);
            String name = model.getLabel();
            String data = model.getData();


            Log.d("iterate_value", name + " " + data);

            if (name.equals("mute")) {
                muteNoti.setChecked(Boolean.parseBoolean(data));
            } else if (name.equals("show")) {
                showNoti.setChecked(Boolean.parseBoolean(data));
            } else if (name.equals("message")) {
                message.setChecked(Boolean.parseBoolean(data));
            } else if (name.equals("match")) {
                match.setChecked(Boolean.parseBoolean(data));
            } else if (name.equals("event")) {
                event.setChecked(Boolean.parseBoolean(data));
            } else if (name.equals("location")) {
                autoLocation.setChecked(Boolean.parseBoolean(data));
            } else if (name.equals("checkin")) {
                checkInNoti.setChecked(Boolean.parseBoolean(data));
            }

        }

        muteNoti.setOnCheckedChangeListener(new CustomSwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomSwitchButton view, boolean isChecked) {
                Log.d("mute_noti_value", String.valueOf(isChecked));
                String value = String.valueOf(isChecked);
                if (isChecked) {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic("global").addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    FirebaseMessaging.getInstance().subscribeToTopic("global").addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
                        }
                    });

                }
                handler.updateUser(new SettingModel("mute", value));
                pushStatus(getSharedPreferences(PREFS_NAME, MODE_PRIVATE).getString("user_id", ""));
            }

        });

        showNoti.setOnCheckedChangeListener(new CustomSwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomSwitchButton view, boolean isChecked) {
                Log.d("showNoti_value", String.valueOf(isChecked));
                String value = String.valueOf(isChecked);
                handler.updateUser(new SettingModel("show", value));

                 /*   //new code
                if(isChecked){
                Intent intent = new Intent();
                    intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");

                    //for Android 5-7
                    intent.putExtra("app_package", getPackageName());
                    intent.putExtra("app_uid", getApplicationInfo().uid);
                    // for Android O
                    intent.putExtra("android.provider.extra.APP_PACKAGE", getPackageName());

                    startActivity(intent);
                    // PRIMARY_CHANNEL:


                }
                //end of code*/
            }

        });

        message.setOnCheckedChangeListener(new CustomSwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomSwitchButton view, boolean isChecked) {
                Log.d("message_value", String.valueOf(isChecked));
                String value = String.valueOf(isChecked);
                handler.updateUser(new SettingModel("message", value));
            }

        });
        match.setOnCheckedChangeListener(new CustomSwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomSwitchButton view, boolean isChecked) {
                Log.d("match_value", String.valueOf(isChecked));
                String value = String.valueOf(isChecked);
                handler.updateUser(new SettingModel("match", value));
            }

        });
        event.setOnCheckedChangeListener(new CustomSwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomSwitchButton view, boolean isChecked) {
                Log.d("event_value", String.valueOf(isChecked));
                String value = String.valueOf(isChecked);
                handler.updateUser(new SettingModel("event", value));
            }

        });
        autoLocation.setOnCheckedChangeListener(new CustomSwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomSwitchButton view, boolean isChecked) {
                Log.d("autoLocation_value", String.valueOf(isChecked));
                String value = String.valueOf(isChecked);
                handler.updateUser(new SettingModel("location", value));
            }

        });
        checkInNoti.setOnCheckedChangeListener(new CustomSwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomSwitchButton view, boolean isChecked) {
                Log.d("checkInNoti_value", String.valueOf(isChecked));
                String value = String.valueOf(isChecked);
                handler.updateUser(new SettingModel("checkin", value));
            }

        });

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.block_data) {
            startActivity(new Intent(this, BlockUser.class));

        }
        if (id == R.id.help_data) {

            startActivity(new Intent(this, Help.class));

        }
        if (id == R.id.support_data) {

            startActivity(new Intent(this, Support.class));

        }
        if (id == R.id.legal_data) {
            startActivity(new Intent(this, Legal_Details.class));


        }
        //txt_tellfrnd,img_share
        if (id == R.id.share_data) {
            startActivity(new Intent(Setting.this, TellAboutApp.class));
        }
        if (id == R.id.share_txt) {
           /* String appname="com.appmonks.admin.nitevibe";
            Uri uri = Uri.parse("market://details?id="+appname);
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);*/
            startActivity(new Intent(Setting.this, TellAboutApp.class));
        }

        if (id == R.id.block_txt) {
            startActivity(new Intent(this, BlockUser.class));
        }
        if (id == R.id.help_txt) {
            startActivity(new Intent(this, Help.class));
        }
        if (id == R.id.support_txt) {
            startActivity(new Intent(this, Support.class));
        }
        if (id == R.id.legal_txt) {
            startActivity(new Intent(this, Legal_Details.class));
        }
        if (id == R.id.back) {

            startActivity(new Intent(Setting.this, UserProfile.class));

        }
        if (id == R.id.delete) {
            initiatePopupWindow();

        }
        if (id == R.id.logout) {

            if (loginUserMode.equals("Normal")) {
                Log.e("logout normal", "logout");
                SharedPreferences preferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                editor.apply();
                Intent i = new Intent(getApplicationContext(), LandingPage.class);
                startActivity(i);
                finish();
            } else if (loginUserMode.equals("Facebook")) {
                SharedPreferences preferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                editor.apply();
                PrefUtils.clearCurrentUser(getApplicationContext());
                LoginManager.getInstance().logOut();
                Intent i = new Intent(getApplicationContext(), LandingPage.class);
                startActivity(i);
                finish();
            }


        }

    }

    public void onClickEvent() {
        img_block.setOnClickListener(this);
        img_help.setOnClickListener(this);
        img_support.setOnClickListener(this);
        img_legal.setOnClickListener(this);
        img_share.setOnClickListener(this);
        back.setOnClickListener(this);
        deleteAcc.setOnClickListener(this);
        logout.setOnClickListener(this);

        txt_block.setOnClickListener(this);
        txt_tellfrnd.setOnClickListener(this);
        txt_help.setOnClickListener(this);
        txt_support.setOnClickListener(this);
        txt_legal.setOnClickListener(this);


    }

    private void initiatePopupWindow() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        // Inflate the custom layout/view
        View customView = inflater.inflate(R.layout.delete_account_row, null);


        popupWindow = new PopupWindow(
                customView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );

        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            popupWindow.setElevation(5.0f);
        }

        // Get a reference for the custom view close button
        ImageView cancel = customView.findViewById(R.id.close);


        RelativeLayout vibeRel = customView.findViewById(R.id.viberel);
        RelativeLayout breakRel = customView.findViewById(R.id.breakrel);
        RelativeLayout reasonRel = customView.findViewById(R.id.reasonrel);

        // Set a click listener for the popup window close button
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                popupWindow.dismiss();
            }
        });
        popupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
        final boolean flag = hasConnection();
        vibeRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(),"Bad vibes",Toast.LENGTH_SHORT).show();
                deleteReason = "1";
                if (flag) {
                    String url = AllUrl.DELETE + loginUserId + "&reason=" + deleteReason;
                    Log.d("delect_account_url", url);
                    deleteUrl = url.replaceAll(" ", "%20");
                    //InactiveUser(deleteUrl);
                    InactiveUserVolley(deleteUrl);
                    popupWindow.dismiss();
                    if (loginUserMode.equals("Normal")) {
                        MyApplication.getInstance().trackEvent("Logout", "Normal", "Success");
                        Log.e("logout normal", "logout");
                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        editor.apply();
                        Intent i = new Intent(getApplicationContext(), LandingPage.class);
                        startActivity(i);
                        finish();
                    } else if (loginUserMode.equals("Facebook")) {
                        MyApplication.getInstance().trackEvent("Logout", "Facebook", "Success");
                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        editor.apply();
                        PrefUtils.clearCurrentUser(getApplicationContext());
                        LoginManager.getInstance().logOut();
                        Intent i = new Intent(getApplicationContext(), LandingPage.class);
                        startActivity(i);
                        finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                }


            }
        });

        breakRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(),"need break",Toast.LENGTH_SHORT).show();
                deleteReason = "2";
                if (flag) {
                    String url = AllUrl.DELETE + loginUserId + "&reason=" + deleteReason;
                    deleteUrl = url.replaceAll(" ", "%20");
                    //InactiveUser(deleteUrl);
                    InactiveUserVolley(deleteUrl);
                    popupWindow.dismiss();
                    if (loginUserMode.equals("Normal")) {
                        Log.e("logout normal", "logout");
                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        editor.apply();
                        Intent i = new Intent(getApplicationContext(), LandingPage.class);
                        startActivity(i);
                        finish();
                    } else if (loginUserMode.equals("Facebook")) {
                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        editor.apply();
                        PrefUtils.clearCurrentUser(getApplicationContext());
                        LoginManager.getInstance().logOut();
                        Intent i = new Intent(getApplicationContext(), LandingPage.class);
                        startActivity(i);
                        finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        reasonRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(),"some reason",Toast.LENGTH_SHORT).show();
                deleteReason = "3";
                if (flag) {
                    String url = AllUrl.DELETE + loginUserId + "&reason=" + deleteReason;
                    deleteUrl = url.replaceAll(" ", "%20");
                    // InactiveUser(deleteUrl);
                    InactiveUserVolley(deleteUrl);
                    popupWindow.dismiss();
                    if (loginUserMode.equals("Normal")) {
                        Log.e("logout normal", "logout");
                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        editor.apply();
                        Intent i = new Intent(getApplicationContext(), LandingPage.class);
                        startActivity(i);
                        finish();
                    } else if (loginUserMode.equals("Facebook")) {
                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        editor.apply();
                        PrefUtils.clearCurrentUser(getApplicationContext());
                        LoginManager.getInstance().logOut();
                        Intent i = new Intent(getApplicationContext(), LandingPage.class);
                        startActivity(i);
                        finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void setfont() {

        txt_setting.setTypeface(bold_face);
        txt_noti.setTypeface(bold_face);
        txt_mute.setTypeface(semi_bold_face);
        txt_show_noti.setTypeface(semi_bold_face);
        txt_msg.setTypeface(semi_bold_face);
        txt_match.setTypeface(semi_bold_face);
        txt_event.setTypeface(semi_bold_face);
        txt_loc.setTypeface(bold_face);
        txt_autodetect.setTypeface(semi_bold_face);
        txt_checkin.setTypeface(semi_bold_face);
        txt_tellfrnd.setTypeface(semi_bold_face);
        txt_shareapp.setTypeface(bold_face);
        txt_blocklabel.setTypeface(bold_face);
        txt_block.setTypeface(semi_bold_face);
        txt_help_label.setTypeface(bold_face);
        txt_help.setTypeface(semi_bold_face);
        txt_support.setTypeface(semi_bold_face);
        txt_legal.setTypeface(semi_bold_face);
        deleteAcc.setTypeface(reg_face);
        logout.setTypeface(bold_face);

    }

    private void InactiveUserVolley(String url) {
        loading = new ProgressDialog(Setting.this, R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("inactive_user_response", response.toString());

                try {
                    loading.dismiss();
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {
                        finish();
                        // moveTaskToBack(true);
                        MyApplication.getInstance().trackEvent("Login", "Inactive User", "Yes");
                        startActivity(new Intent(Setting.this, Login.class));
                    } else {
                        Toast.makeText(Setting.this, object.getString("message"), Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void blockcount(String search_url) {
        MyApplication.getInstance().getRequestQueue().getCache().remove(search_url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, search_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("blockcountresponse", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String res = jsonObject.getString("response");
                    String message = jsonObject.getString("message");

                    if (res.equals("200")) {
                        if (message.equals("0")) {
                            txt_block.setText("Blocked User");
                        } else {
                            txt_block.setText(message + " Blocked Users");
                        }
                    } else {
                        txt_block.setText("Blocked User");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog


            }
        });

        // Adding request to request queue
        RequestQueue queue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
        queue.getCache().clear();
        jsonObjReq.setShouldCache(false);

        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();

            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    public static boolean isConnectingToInternet(Context _context) {
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, UserProfile.class);
        startActivity(i);
    }

    private void pushStatus(String id) {
        Map map =new HashMap<String, String>();
        map.put("id",id);
        Call<ApiInterface.StatusResponse> call = apiService.updateStatus(map);
        call.enqueue(new Callback<ApiInterface.StatusResponse>() {
            @Override
            public void onResponse(Call<ApiInterface.StatusResponse> call, retrofit2.Response<ApiInterface.StatusResponse> response) {
                Log.e(TAG, "onResponse: " + response.body());
                if (response.body().getResponse()==200){
                    Toast.makeText(Setting.this, "Status Updated.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiInterface.StatusResponse> call, Throwable t) {
                Log.e(TAG, "onFailure" + t.getMessage());
            }
        });

/*

        final Map<String, String> map = new HashMap<>();
        map.put("id", id);
        Log.e(TAG, "pushStatus: "+map );
        Log.e(TAG, "pushStatus: "+AllUrl.NOTIFICATION_STATUS );


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AllUrl.NOTIFICATION_STATUS, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, response.toString());
                try {
                    switch (response.getString("status")) {
                        case "inactive":
//                            muteNoti.setChecked(false);
                            break;
                        case "active":
//                            muteNoti.setChecked(true);
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                // Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String,String> map1= new HashMap<>();
                map1.put("content-type","form-data;");
                return map1;
            }
        };

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();
            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
*/

    }
}
