package com.nv.admin.nitevibe.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.nv.admin.nitevibe.Adapter.subDrinkModel;
import com.nv.admin.nitevibe.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class TagAdapter<T> {
    private List<subDrinkModel> mTagDatas;
    private TagAdapter context;
    private OnDataChangedListener mOnDataChangedListener;
    public static   HashSet<Integer> mCheckedPosList = new HashSet<Integer>();
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;

    public boolean val;

    @Deprecated
    private static HashSet<Integer> mCheckedPosList1 = new HashSet<Integer>();

    public TagAdapter(Context context, List<subDrinkModel> datas) {
        context = context;
        mTagDatas = datas;
    }




    @Deprecated
    public TagAdapter(T[] datas) {
        mTagDatas = new ArrayList<subDrinkModel>((Collection<? extends subDrinkModel>) Arrays.asList(datas));
    }

    public abstract void setSelected(int position, String s);

      public void setSelectedList(List<Integer> integers) {
          mCheckedPosList.clear();
          if (integers != null) {
              mCheckedPosList.addAll(integers);
          }
      }
    interface OnDataChangedListener {
        void onChanged();
    }

    void setOnDataChangedListener(OnDataChangedListener listener) {
        mOnDataChangedListener = listener;
    }


    public void setSelectedList(int... poses) {
        Set<Integer> set = new HashSet<>();
        for (int pos : poses) {
            set.add(pos);
        }
        setSelectedList(set);
    }


    public void setSelectedList(Set<Integer> set) {
        mCheckedPosList.clear();
        if (set != null) {
            mCheckedPosList.addAll(set);
        }
        notifyDataChanged();
    }

    static HashSet<Integer> getPreCheckedList() {
        return mCheckedPosList;
    }


    public int getCount() {
        return mTagDatas == null ? 0 : mTagDatas.size();
    }

    public void notifyDataChanged() {
        if (mOnDataChangedListener != null)
            mOnDataChangedListener.onChanged();
    }

    public subDrinkModel getItem(int position) {
        return mTagDatas.get(position);
    }

    public abstract View getView(FlowLayout parent, int position, T t);


    public void onSelected(int position, View view){

        Log.d("sub_drink","onSelected " + position);
        TextView txt= view.findViewById(R.id.subdrink_txt);
    }

    public void unSelected(int position, View view){

        Log.d("sub_drink","unSelected " + position);
        TextView txt= view.findViewById(R.id.subdrink_txt);
    }

    public boolean setSelected(int position, T t) {
        return false;
    }


}
