package com.nv.admin.nitevibe.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.ClubEventDetails;
import com.nv.admin.nitevibe.Activity.EntryDetails;
import com.nv.admin.nitevibe.Activity.EventEnlargeImage;
import com.nv.admin.nitevibe.Activity.Help;
import com.nv.admin.nitevibe.Activity.Home;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.Activity.Swipe;
import com.nv.admin.nitevibe.Activity.WhosGoing;
import com.nv.admin.nitevibe.OverlayTutorial.TutoShowcase;
import com.nv.admin.nitevibe.R;
import com.bumptech.glide.Glide;
import com.github.lzyzsd.circleprogress.ArcProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Admin on 4/23/2018.
 */
public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyView>  {
    private List<EventModel> arrayList;
    private List<String> eventImages;
    private List<WhosGoingModel> goingList;
    private List<WhosGoingModel> fewGoingList;

    private Context context;
    EventModel current;
    String eventCurrent;
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    RecyclerView.LayoutManager mLayoutManager;
    public WhosGoingAdapter goingAdapter;
    AlertDialog alert;
    ProgressDialog loading;
    public static String ceventName,cdate,ctime,cstag,ccouple,cimg,caddress;
    public static int height11;

    public EventAdapter(List<EventModel> arrayList, List<String> eventImages, List<WhosGoingModel> goingList, List<WhosGoingModel> fewGoingList, Context context) {
        this.arrayList = arrayList;
        this.eventImages = eventImages;
        this.goingList = goingList;
        this.fewGoingList = fewGoingList;
        this.context = context;
    }

    @Override
    public EventAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_row, parent, false);

        MyView viewHolder = new MyView(v);

    /*    v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    v.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }

                // get width and height of the view
                 height11=v.getHeight();

                Log.d("hieght_of_view", String.valueOf(height11));
            }
        });*/

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final EventAdapter.MyView holder, int position) {
        sharedpreferences=context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");
        String eventOverlay=sharedpreferences.getString("eventoverlay","");




        bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");


        holder.txt_date.setTypeface(extra_bold_face);
        holder.txt_mnth.setTypeface(semi_bold_face);
        holder.txt_name.setTypeface(bold_face);
        holder.txt_going.setTypeface(reg_face);
        holder.txt_male_per.setTypeface(semi_bold_face);
        holder.txt_female_per.setTypeface(semi_bold_face);
        holder.txt_desc_label.setTypeface(calibri_bold);
        holder.txt_desc.setTypeface(semi_bold_face);
        holder.txt_entry_label.setTypeface(calibri_bold);
        //holder.txt_entry.setTypeface();
        holder.txt_view_label.setTypeface(semi_bold_face);
        holder.txt_who_going.setTypeface(calibri_bold);
        holder.btn_going.setTypeface(bold_face);
        holder.coupon_title.setTypeface(calibri_bold);
        holder.view_coupon.setTypeface(semi_bold_face);

        current=arrayList.get(position);

        holder.txt_name.setText(current.eventName);
        String event_date=current.eventDate;
        String[] abc=event_date.split(" ");
        String date=abc[0];
        String mnth=abc[1];
        holder.txt_date.setText(date);
        holder.txt_mnth.setText(mnth);

        holder.txt_id.setText(current.eventId);
        holder.txt_desc.setText(current.eventDesc);

        holder.txt_event_id.setText(current.eventId);


        if(current.eventTotal.equals("0")){
           // holder.txt_going.setText("");
            holder.txt_going.setVisibility(View.GONE);
        }
        else{
            holder.txt_going.setVisibility(View.VISIBLE);
            if(current.eventTotal=="1"){
                holder.txt_going.setText(current.eventTotal+" Person is going");
            }
            else { holder.txt_going.setText(current.eventTotal+" People are going");}

        }

        String stag=current.eventStag;
        String couple=current.eventCouple;

        if(current.eventStag.equals("NA")){
            //holder.txt_entry.setText("u20B9 "+model.eventCouple);
            holder.txt_entry.setText("\u20B9 "+current.eventCouple);
        }
        else if(current.eventCouple.equals("NA")){
            // holder.txt_entry.setText(R.string.Rs+model.eventStag);
            holder.txt_entry.setText("\u20B9 "+current.eventStag);
        }
        else if(current.eventStag.equals("NA") && current.eventCouple.equals("NA")){
            holder.txt_entry.setText("");
        }
        else{
            holder.txt_entry.setText("\u20B9 "+current.eventStag);
        }


        Log.d("user_count",current.eventMale+" "+current.eventFemale);


        if(current.eventMale.equals("0") && current.eventFemale.equals("0")){
            holder.progressRel.setVisibility(View.GONE);

        }
        else{
            holder.progressRel.setVisibility(View.VISIBLE);
            holder.txt_male_per.setText(current.eventMale+"%");
            holder.txt_female_per.setText(current.eventFemale+"%");

            String total_ratio=current.eventMale+current.eventFemale;

           // holder.progress.setMax(Integer.parseInt(total_ratio));
            holder.progress.setSecondaryProgress(Integer.parseInt(current.eventFemale));

        }




        final String userAge=current.userAge;
        final String userPref=current.userGenderPref;

        if(current.eventImages!=null && !current.eventImages.isEmpty()){
           // NUM_PAGES=current.eventImages.size();
          //  Log.d("num_page_size", String.valueOf(NUM_PAGES));


            Glide.with(context).load(current.eventImages.get(currentPage)).into(holder.img_main);
/*
            holder.img_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(currentPage==NUM_PAGES){
                        currentPage=0;
                    }
                    ImageView imageView = (ImageView) view;

                    Glide.with(context).load(current.eventImages.get(currentPage++)).into(imageView);

                }
            });*/
        }

        holder.img_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                editor.remove("event_id");
                editor.commit();
                editor.putString("event_id", holder.txt_event_id.getText().toString());
                editor.commit();

                Intent i=new Intent(context, EventEnlargeImage.class);
                i.putExtra("eventimage",current.eventImages.get(currentPage));
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(i);
                
            }
        });


        holder.txt_view_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                editor.remove("event_id");
                editor.commit();
                editor.putString("event_id", holder.txt_event_id.getText().toString());
                editor.commit();
                Intent i=new Intent(context, EntryDetails.class);
                i.putExtra("stag",current.eventStag);
                i.putExtra("couple",current.eventCouple);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                view.getContext().startActivity(i);
            }
        });
       // String userAge=current.userAge;
       // String userPref=current.userGenderPref;

        holder.btn_going.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean flag=hasConnection();

                if(flag){


                    if(!userAge.equals("") && !userPref.equals("")){
                        // String url= AllUrl.USER_GOING+loginUserId+"&club_id="+model.clubId+"&event_id="+holder.txt_id.getText().toString();
                        String url= AllUrl.USER_GOING+loginUserId+"&club_id="+current.clubId+"&event_id="+current.eventId;
                        Log.d("user_going_url",url);

                        userGoingVolley(url);
                    }
                    else{
                        Toast.makeText(context,"Please fill your profile",Toast.LENGTH_SHORT).show();
                    }


                }
                else{
                    Toast.makeText(context,"No internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(current.type.equals("event")){
                    context.startActivity(new Intent(context, ClubEventDetails.class));
                }
                else if(current.type.equals("banner")){
                    context.startActivity(new Intent(context,Home.class));
                }

            }
        });

        //goingrecycler
        goingList=new ArrayList<>();

        if(current.fewGoingList.size()> 0){
            goingList=current.fewGoingList;
            Log.d("size_of_goingarray", String.valueOf(goingList.size()));
            holder.goingRecycler.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.goingRecycler.setLayoutManager(mLayoutManager);

         /*   if(current.goingList.size()>0){
                holder.goingRel.setVisibility(View.GONE);
            }
            else{
                holder.goingRel.setVisibility(View.VISIBLE);
                goingAdapter=new WhosGoingAdapter(current.goingList,context);
                holder.goingRecycler.setAdapter(goingAdapter);
            }*/
            holder.goingRel.setVisibility(View.VISIBLE);
            goingAdapter=new WhosGoingAdapter(current.goingList,context);
            holder.goingRecycler.setAdapter(goingAdapter);

            holder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                    editor.remove("event_id");
                    editor.commit();
                    editor.putString("event_id", holder.txt_event_id.getText().toString());
                    editor.commit();

                    Intent i=new Intent(context, WhosGoing.class);
                    context.startActivity(i);

                }
            });
        }
        else{
            holder.more.setVisibility(View.GONE);
            holder.goingRel.setVisibility(View.GONE);
        }

        if(current.coupon.equals("")){
            holder.couponRel.setVisibility(View.GONE);
        }
        else{
            holder.couponRel.setVisibility(View.VISIBLE);


        }





    /*    holder.view_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String time1=current.coupontime;

                new MaterialStyledDialog.Builder(context)
                        .setDescription("Jump  the Queue!! Get a chance to grab a “Preference Pass” to get into the venue before anyone else!! Check  - in to the Venue between "+time1+" to enjoy this feature!! Check the FAQ for more information! \n ")
                        .setIcon(R.drawable.coupon)
                        .setHeaderColor(R.color.female)
                        .withDialogAnimation(true)
                        .show();
            }
        });*/

        holder.view_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String time1=current.coupontime;
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.coupon_alert);

                TextView label= dialog.findViewById(R.id.title);

                TextView txt1= dialog.findViewById(R.id.message1);
                TextView txt2= dialog.findViewById(R.id.message2);
                TextView txt3= dialog.findViewById(R.id.message3);
                TextView txt4= dialog.findViewById(R.id.message4);

                txt1.setTypeface(reg_face);
                txt2.setTypeface(reg_face);
                txt3.setTypeface(reg_face);
                txt4.setTypeface(reg_face);


                txt1.setText(R.string.coup_1);
                txt2.setText(R.string.coup_2);
                String abc= String.valueOf(R.string.coup_3);
                String abc1= String.valueOf(R.string.coup_4);


                String abc123=context.getResources().getString(R.string.coup_3)+" "+time1+" "+context.getResources().getString(R.string.coup_4);
                Log.d("concact_string",abc123);
                txt3.setText(abc123);
                txt4.setText(R.string.coup_5);

                txt4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       /* String url = "http://rednwhite.co.in/faq.php";
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        context.startActivity(i);*/
                        Intent i=new Intent(context, Help.class);
                        context.startActivity(i);
                    }
                });

                dialog.show();
                dialog.setCanceledOnTouchOutside(true);

            }
        });



    }

    protected void displayTuto() {
        TutoShowcase.from((Activity)context)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        // Toast.makeText(Club.this, "Tutorial dismissed", Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("eventoverlay","1");
                        editor.commit();
                        // displayTutogoing();
                    }
                })
                .setContentView(R.layout.eventoverlay)
                .setFitsSystemWindows(true)


                .show();
    }



    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public TextView txt_date,txt_mnth,txt_name,txt_id,txt_going,txt_male_per,txt_female_per,txt_desc_label,txt_desc,txt_entry_label,txt_entry,txt_view_label,
                txt_who_going,txt_event_id,view_coupon;
        public Button btn_going;
        public RelativeLayout descRel,goingRel,progressRel;
        public ImageView img_main,back,more,bg_img;
        public ProgressBar progress;
        public RecyclerView goingRecycler;
        public  TextView txt_event_name,txt_add_title,txt_address,txt_date_title,txt_date11,txt_time_title,txt_time11,txt_entry_title,txt_entry_fee,txt_coupon,txt_coupon_count;
        public  Button btn_skip,btn_usecoupon;
        public  ImageView coupon_img;

        public  RelativeLayout couponRel,stagRel,coupleRel,mainRel;
        public  TextView coupon_title,txt_stag,txt_couple;
        public ArcProgress couponProgress;

        public MyView(View itemView) {
            super(itemView);
            img_main= itemView.findViewById(R.id.imageView1);
            back= itemView.findViewById(R.id.back);
            txt_date= itemView.findViewById(R.id.date);
            txt_mnth= itemView.findViewById(R.id.month);
            txt_name= itemView.findViewById(R.id.eventname);
            txt_id= itemView.findViewById(R.id.event_id);
            txt_going= itemView.findViewById(R.id.peoplegoing);
            txt_male_per= itemView.findViewById(R.id.maleper);
            txt_female_per= itemView.findViewById(R.id.femaleper);
            txt_desc_label= itemView.findViewById(R.id.desc_label);
            txt_desc= itemView.findViewById(R.id.desc);
            txt_entry_label= itemView.findViewById(R.id.entry_label);
            txt_entry= itemView.findViewById(R.id.entry);
            txt_view_label= itemView.findViewById(R.id.viewprice);
            txt_who_going= itemView.findViewById(R.id.going_label);
            txt_event_id= itemView.findViewById(R.id.eventid);

            btn_going= itemView.findViewById(R.id.btn_going);
            descRel= itemView.findViewById(R.id.descrel);
            goingRel= itemView.findViewById(R.id.goingrel);
            progressRel= itemView.findViewById(R.id.progress_rel);

            progress= itemView.findViewById(R.id.progressBar);
            goingRecycler= itemView.findViewById(R.id.goingrecycler);
            more= itemView.findViewById(R.id.view_more);



            /*txt_coupon= itemView.findViewById(R.id.total_title);
            txt_coupon_count= itemView.findViewById(R.id.total_coupon);*/



            couponRel= itemView.findViewById(R.id.couponrel);


            coupon_title= itemView.findViewById(R.id.coupon_title);

            mainRel= itemView.findViewById(R.id.mainrel);

            bg_img= itemView.findViewById(R.id.imageView2);
            view_coupon= itemView.findViewById(R.id.viewcoupon);



        }
    }

    //user going
    private void userGoingVolley(String url){

        loading= new ProgressDialog(context,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("user_going_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){
                      //  Intent i=new Intent(context, Swipe.class);
                      //  context.startActivity(i);
                        MyApplication.getInstance().trackEvent("I am Going", "I am Going", "I am Going");
                        Intent i=new Intent(context,Swipe.class);
                        i.putExtra("val_from","going");
                        i.putExtra("event_id",current.eventId);
                        context.startActivity(i);


                    }
                    else if(res.equals("203")){
                        /*Intent i=new Intent(context, Swipe.class);
                        context.startActivity(i);*/
                        Intent i=new Intent(context,Swipe.class);
                        i.putExtra("val_from","going");
                        i.putExtra("event_id",current.eventId);
                        context.startActivity(i);
                    }
                    else{
                        Toast.makeText(context,object.getString("message"),Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }




    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }



}
