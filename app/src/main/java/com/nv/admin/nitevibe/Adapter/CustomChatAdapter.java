package com.nv.admin.nitevibe.Adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nv.admin.nitevibe.Activity.Chatting;
import com.nv.admin.nitevibe.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by Admin on 4/16/2018.
 */
public class CustomChatAdapter extends BaseAdapter {
    private Activity activity;
    private List<ChatModel> chatMessages;
    private RelativeLayout singleMessageContainer, fullcontainer;
    String receiverText;//, friend_mob,friendimg;
    ChatModel m;
    String imageurl="", file_name;
    EmojiconTextView chatText;
    TextView txtInfo;
    ImageView imageViewMessage;
    Integer[] imageIDs = {
            R.drawable.stickers_01, R.drawable.stickers_02

    };
    String user_id;
    public static Typeface bold_face, extra_bold_face, reg_face, semi_bold_face, calibri_bold, lato_bold, semi_bold_italic;

    public CustomChatAdapter(Activity activity, List<ChatModel> chatMessages) {
        this.activity = activity;
        this.chatMessages = chatMessages;

    }

    @Override
    public int getCount() {
        Log.d("count", String.valueOf(chatMessages.size()));
        return chatMessages.size();

    }

    @Override
    public Object getItem(int position) {
        return chatMessages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
        // return  0;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {


        Log.d("getview", "inserted");


//        user_id = Chatting.str_select_userid;
        //        else if (!(receiverText.equals(user_id))&& ((imageurl.equals("null")))) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        int layoutResource = 0; // determined by view type
        int viewType = getItemViewType(position);

        m = chatMessages.get(position);
        imageurl = m.getImage();
        receiverText = m.getReceiver();

        Log.d("receiver", receiverText);
        Log.d("userid", user_id);

        if (!(receiverText.equals(user_id))) {
            //left
            layoutResource = R.layout.chat_left;

        } else if (receiverText.equals(user_id)) {
            //right
            layoutResource = R.layout.chat_right;
        }

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(layoutResource, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        bold_face = Typeface.createFromAsset(activity.getAssets(), "fonts/OpenSans-Bold.ttf");
        extra_bold_face = Typeface.createFromAsset(activity.getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        reg_face = Typeface.createFromAsset(activity.getAssets(), "fonts/OpenSans-Regular.ttf");
        semi_bold_face = Typeface.createFromAsset(activity.getAssets(), "fonts/OpenSans-SemiBold.ttf");
        calibri_bold = Typeface.createFromAsset(activity.getAssets(), "fonts/Calibri Bold.ttf");
        lato_bold = Typeface.createFromAsset(activity.getAssets(), "fonts/Lato-Bold.ttf");
        semi_bold_italic = Typeface.createFromAsset(activity.getAssets(), "fonts/OpenSans-SemiBoldItalic.ttf");

        holder.msg.setTypeface(semi_bold_face);
        holder.sent.setTypeface(semi_bold_face);
        holder.sent.setText(m.sent);


        Log.d("get_message", m.getMessage());


        String img123 = m.getUserimage();
        if (!m.getMessage().equals("")) {
            holder.msg.setVisibility(View.VISIBLE);
            holder.img.setVisibility(View.GONE);

            holder.msg.setText(m.getMessage());

          /*  Picasso.with(activity.getApplicationContext())
                    .load(d)
                    .into(holder.img);*/


        } else {
            holder.msg.setVisibility(View.GONE);
            holder.img.setVisibility(View.VISIBLE);
            int d = 0;
            if (imageurl.equals("1")) {
                d = R.drawable.stickers_01;
                holder.img.setImageResource(d);

            } else if (imageurl.equals("2")) {
                d = R.drawable.stickers_02;
                holder.img.setImageResource(d);
            }
        }

        //Log.d("userimage",img123);
        /*      if(img123!=""){
         *//* Picasso.with(activity.getApplicationContext())
                    .load(img123)
                    .into(holder.img_user);*//*
            Glide.with(activity.getApplicationContext())
                    .load(img123)
                    .into(holder.img_user);

        }*/


        return convertView;
    }


    private class ViewHolder {
        private TextView sent;
        private ImageView img;
        private CircularImageView img_user;
        // private TextView msg
        private EmojiconTextView msg;

        public ViewHolder(View v) {
            //msg = (TextView) v.findViewById(R.id.txt_msg);
            msg = v.findViewById(R.id.txt_msg);
            sent = v.findViewById(R.id.sent);
            img = v.findViewById(R.id.img);
            img_user = v.findViewById(R.id.userimg);
        }
    }
}
