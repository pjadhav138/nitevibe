package com.nv.admin.nitevibe.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import info.hoang8f.android.segmented.SegmentedGroup;

public class AddGuest extends AppCompatActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener  {
    ///helllo
    public static SeekBar male_seekbar,female_seekbar;
    public static TextView txt_male_count,txt_female_count,main_label,num_label,guest_label,username_label,name_label,email_label;
    public static Button btn_logout;
    public static EditText ed_userName,ed_name,ed_email,ed_amount;
    public static RelativeLayout maleRel,femaleRel,couponRel,couponPrefRel;
    public static TextView txt_male,txt_female,entry_label,rupee_label,txt_coupon,coupon_txt,without_txt,txt_left;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    AlertDialog alert;
    public static String club_id;
    String str_mobile,str_name,str_email,str_gender,str_male,str_female,str_price,str_entry_type,str_coupon_value,str_coupon,str_left_coupon;
    public static ImageView img_male,img_female,back;
    ProgressDialog loading;
    public static SegmentedGroup gendersegment,prefSegment;
    public static CheckBox coupon_chk,without_chk;
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    public static String coupon_value="0",entry_value="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_guest);
        //kjdbfsjdb
        male_seekbar= findViewById(R.id.male_seek);
        female_seekbar= findViewById(R.id.female_seek);
        txt_male_count= findViewById(R.id.male_count);
        txt_female_count= findViewById(R.id.female_count);
        btn_logout= findViewById(R.id.logout);
        ed_userName= findViewById(R.id.username);
        ed_name= findViewById(R.id.name);
        ed_email= findViewById(R.id.email);
        ed_amount= findViewById(R.id.amount);
        maleRel= findViewById(R.id.malerel);
        femaleRel= findViewById(R.id.femalerel);
        main_label= findViewById(R.id.txt);
        num_label= findViewById(R.id.label);
        guest_label= findViewById(R.id.txt2);
        username_label= findViewById(R.id.txt_username);
        name_label= findViewById(R.id.txt_name);
        email_label= findViewById(R.id.txt_email);
        // txt_male=(TextView)findViewById(R.id.male_txt);
        //txt_female=(TextView)findViewById(R.id.female_txt);
        entry_label= findViewById(R.id.txt3);
        txt_left= findViewById(R.id.txt_coupon_left);

        couponRel= findViewById(R.id.couponRel);
        txt_coupon= findViewById(R.id.txt_coupon);
        coupon_chk= findViewById(R.id.chk);
        without_chk= findViewById(R.id.chk1);
        coupon_txt= findViewById(R.id.txt_withcoupon);
        without_txt= findViewById(R.id.txt_withoutcoupon);
        couponPrefRel= findViewById(R.id.couponpref);
        prefSegment= findViewById(R.id.segmented3);


        // img_male=(ImageView)findViewById(R.id.img_male);
        // img_female=(ImageView)findViewById(R.id.img_female);
        rupee_label= findViewById(R.id.rupee);
        back= findViewById(R.id.back);
        str_gender="";

        ed_name.setFilters(new InputFilter[] {
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        // TODO Auto-generated method stub
                        if(cs.equals("")){ // for backspace
                            return cs;
                        }
                        if(cs.toString().matches("[a-zA-Z ]+")){
                            return cs;
                        }
                        return "";
                    }
                }
        });

        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        str_coupon=sharedpreferences.getString("total_coupon","");
        str_left_coupon=sharedpreferences.getString("left_coupon","");

        txt_left.setText("Coupons remaining: "+str_left_coupon);

        if(str_coupon.equals("")){
            couponRel.setVisibility(View.GONE);
        }
        else{
            couponRel.setVisibility(View.VISIBLE);
        }



        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        gendersegment= findViewById(R.id.segmented2);

        setFont();
        clickEvent();

        Bundle extra=getIntent().getExtras();
        if(extra!=null){
            //values coming from Clubsidehome
            club_id=extra.getString("club_id");
            Log.d("club_id",club_id);
        }

        male_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                int val = (progress * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                txt_male_count.setText("" + progress);
                txt_male_count.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                txt_male_count.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });


        female_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                int val = (progress * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                txt_female_count.setText("" + progress);
                txt_female_count.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                txt_female_count.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });

        coupon_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                str_coupon_value="1";
                if(coupon_chk.isChecked()==true){
                    couponPrefRel.setVisibility(View.VISIBLE);
                }
                else if(coupon_chk.isChecked()==false){
                    couponPrefRel.setVisibility(View.GONE);


                }
            }
        });

        without_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                str_coupon_value="0";

                coupon_value="0";
                entry_value="";

            }
        });
    }
    public void clickEvent(){
        btn_logout.setOnClickListener(this);
        // maleRel.setOnClickListener(this);
        //  femaleRel.setOnClickListener(this);
        gendersegment.setOnCheckedChangeListener(this);
        prefSegment.setOnCheckedChangeListener(this);
        back.setOnClickListener(this);

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.button21: //male gender
                //  Toast.makeText(AddGuest.this, "Male", Toast.LENGTH_SHORT).show();
                str_gender="Male";
                break;
            case R.id.button22:  //female gender
                // Toast.makeText(AddGuest.this, "Female", Toast.LENGTH_SHORT).show();
                str_gender="Female";
                break;
            case R.id.button23:
                //  Toast.makeText(AddGuest.this, "Stag", Toast.LENGTH_SHORT).show();
                str_entry_type="Stag";
                entry_value=str_entry_type;
                coupon_value="1";
                break;
            case R.id.button24:
                // Toast.makeText(AddGuest.this, "Couple", Toast.LENGTH_SHORT).show();
                str_entry_type="Couple";
                entry_value=str_entry_type;
                coupon_value="2";
                break;

            default:
                // Nothing to do
        }
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        if(id==R.id.logout){
            getAllValues();
            MyApplication.getInstance().trackEvent("Logout", "Logout", "Logout from app");

        }
        if(id==R.id.back){
            startActivity(new Intent(AddGuest.this,ClubSideHome.class));
        }

    }

    public void getAllValues(){

        str_mobile=ed_userName.getText().toString();
        if(!str_mobile.equals("")){
            if(str_mobile.matches("[0-9]+")){
                validateMobile();
                if(validateMobile()){

                    if(!txt_male_count.getText().toString().equals("")){
                        if(!txt_female_count.getText().toString().equals("")){
                            if(!ed_userName.getText().toString().equals("")){
                                if(!str_gender.equals("")){
                                    boolean flag=hasConnection();
                                    if(flag){
                                        str_mobile=ed_userName.getText().toString();
                                        str_name=ed_name.getText().toString();
                                        str_email=ed_email.getText().toString();
                                        //str_gender=str_gender;
                                        str_male=txt_male_count.getText().toString();
                                        str_female=txt_female_count.getText().toString();
                                        str_price=ed_amount.getText().toString();
//                                       str_coupon
                                        Log.d("all_value",str_mobile+" "+str_name+" "+str_email+" "+str_gender+" "+str_male+" "+str_female+" "+str_price+" "+club_id+" "+coupon_value+" "+entry_value);
                                        // insertClubEntry(str_mobile,str_name,str_email,str_gender,str_male,str_female,str_price,club_id);
                                        insertClubEntryVolley(str_mobile,str_name,str_email,str_gender,str_male,str_female,str_price,club_id,coupon_value,entry_value);

                                    }
                                    else{
                                        Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                                    }

                                }
                                else{
                                    Toast.makeText(getApplicationContext(),"Please select the gender",Toast.LENGTH_SHORT).show();
                                }


                            }
                            else{
                                Toast.makeText(getApplicationContext(),"Please enter the username",Toast.LENGTH_SHORT).show();
                            }

                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Please select no of female",Toast.LENGTH_SHORT).show();
                        }

                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Please select no of male",Toast.LENGTH_SHORT).show();
                    }


                }
            }
            else{
                validateEmail();
                if(validateEmail()){

                    if(!txt_male_count.getText().toString().equals("")){
                        if(!txt_female_count.getText().toString().equals("")){
                            if(!ed_userName.getText().toString().equals("")){
                                if(!str_gender.equals("")){
                                    boolean flag=hasConnection();
                                    if(flag){
                                        str_mobile=ed_userName.getText().toString();
                                        str_name=ed_name.getText().toString();
                                        str_email=ed_email.getText().toString();
                                        //str_gender=str_gender;
                                        str_male=txt_male_count.getText().toString();
                                        str_female=txt_female_count.getText().toString();
                                        str_price=ed_amount.getText().toString();
//                                       str_coupon
                                        Log.d("all_value",str_mobile+" "+str_name+" "+str_email+" "+str_gender+" "+str_male+" "+str_female+" "+str_price+" "+club_id+" "+coupon_value+" "+entry_value);
                                        // insertClubEntry(str_mobile,str_name,str_email,str_gender,str_male,str_female,str_price,club_id);
                                        insertClubEntryVolley(str_mobile,str_name,str_email,str_gender,str_male,str_female,str_price,club_id,coupon_value,entry_value);

                                    }
                                    else{
                                        Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                                    }

                                }
                                else{
                                    Toast.makeText(getApplicationContext(),"Please select the gender",Toast.LENGTH_SHORT).show();
                                }


                            }
                            else{
                                Toast.makeText(getApplicationContext(),"Please enter the username",Toast.LENGTH_SHORT).show();
                            }

                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Please select no of female",Toast.LENGTH_SHORT).show();
                        }

                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Please select no of male",Toast.LENGTH_SHORT).show();
                    }


                }
            }




        }
        else{
            Toast.makeText(getApplicationContext(),"Please enter the username",Toast.LENGTH_SHORT).show();
        }




    }


    //post method
    private void insertClubEntryVolley(final String mobile,final String name,final String email,final String gender,final String male,final String female,final String price,final String club_id,final String coupon,final String type){
        StringRequest postRequest = new StringRequest(Request.Method.POST,AllUrl.CLUB_ENTRY,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("club_sideentryresponse", response);
                        loading.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String res = jsonObject.getString("response");
                            String message = jsonObject.getString("message");
                            if (res.equals("200")) {
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(AddGuest.this);
                                builder1.setMessage("Entry added sucessfully");
                                builder1.setCancelable(false);
                                builder1.setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                startActivity(new Intent(AddGuest.this,ClubSideHome.class));
                                                MyApplication.getInstance().trackEvent("Club side entry", "Club side entry", "Success");
                                            }
                                        });
                                alert = builder1.create();
                                alert.show();
                                Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                                positive.setTextColor(getResources().getColor(R.color.female));


                            }
                            else{
                                Toast.makeText(getApplicationContext(),"Failed to add new entry!",Toast.LENGTH_LONG).show();
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        loading.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Log.d("post_volley","entered in getParams");
                Map<String, String>  data = new HashMap<>();
                // the POST parameters:

                data.put("mobile",mobile);
                data.put("name",name);
                data.put("email",email);
                data.put("gender",gender);
                data.put("male",male);
                data.put("female",female);
                data.put("price",price);
                data.put("club_id",club_id);
                data.put("coupon",coupon);
                data.put("type",type);

                return data;
            }

        };

        Volley.newRequestQueue(this).getCache().clear();

        Volley.newRequestQueue(this).add(postRequest);
        loading= new ProgressDialog(AddGuest.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();

    }
    private void insertClubEntry(String mobile,String name,String email,String gender,String male,String female,String price,String club_id){
        class ClunEntryClass extends AsyncTask<String, Void, String> {
            ProgressDialog loading;
            RequestHandler ruc = new RequestHandler();
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading= new ProgressDialog(AddGuest.this,R.style.MyAlertDialogStyle);
                loading.setIndeterminate(true);
                loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
                loading.setCancelable(false);
                loading.setMessage("Please wait...!");
                loading.show();
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                try {
                    Log.d("club_add_entry_response",s);
                    JSONObject jsonObject = new JSONObject(s);
                    String response = jsonObject.getString("response");
                    String message = jsonObject.getString("message");
                    if (response.equals("200")) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(AddGuest.this);
                        builder1.setMessage("Entry added sucessfully");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        startActivity(new Intent(AddGuest.this,ClubSideHome.class));

                                    }
                                });
                        alert = builder1.create();
                        alert.show();
                        Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                        positive.setTextColor(getResources().getColor(R.color.female));

                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Failed to add new entry!",Toast.LENGTH_LONG).show();
                    }




                }catch (JSONException e){
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... params) {
                HashMap<String, String> data = new HashMap<String,String>();


                data.put("mobile",params[0]);
                data.put("name",params[1]);
                data.put("email",params[2]);
                data.put("gender",params[3]);
                data.put("male",params[4]);
                data.put("female",params[5]);
                data.put("price",params[6]);
                data.put("club_id",params[7]);




                String result = ruc.sendPostRequest(AllUrl.CLUB_ENTRY,data);
                return  result;
            }
        }
        ClunEntryClass entry = new ClunEntryClass();
        entry.execute( mobile, name, email, gender, male, female, price, club_id);

    }

    private boolean validateMobile(){
        String phone_number=ed_userName.getText().toString().trim();
        Log.d("length", String.valueOf(phone_number.length()));
        isValidPhone(phone_number);
        if(phone_number.length()>=10){
            if(isValidPhone(phone_number)==false){
                Toast.makeText(getApplicationContext(),"Please enter valid phone number",Toast.LENGTH_SHORT).show();
                return false;
            }
            else{

            }
        }else{
            Toast.makeText(getApplicationContext(),"Please enter valid phone number",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    private boolean validateEmail(){
        String email_id=ed_userName.getText().toString().trim();

        if(isValidEmail(email_id)==false){
            Toast.makeText(getApplicationContext(),"please enter a valid email id",Toast.LENGTH_SHORT).show();

            return false;
        }
        else{

        }
        return true;
    }
    private static boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email)&& Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPhone(String phone)
    {
        String expression = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
        CharSequence inputString = phone;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        return matcher.matches();
    }



    public void setFont(){
        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");



        txt_male_count.setTypeface(calibri_bold);
        txt_female_count.setTypeface(calibri_bold);
        btn_logout.setTypeface(bold_face);
        ed_userName.setTypeface(semi_bold_face);
        ed_name.setTypeface(semi_bold_face);
        ed_email.setTypeface(semi_bold_face);
        ed_amount.setTypeface(semi_bold_face);
        main_label.setTypeface(bold_face);
        num_label.setTypeface(bold_face);
        guest_label.setTypeface(bold_face);
        username_label.setTypeface(lato_bold);
        name_label.setTypeface(lato_bold);
        email_label.setTypeface(lato_bold);
        //txt_male.setTypeface(semi_bold_face);
        //txt_female.setTypeface(semi_bold_face);
        entry_label.setTypeface(bold_face);



    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }
}