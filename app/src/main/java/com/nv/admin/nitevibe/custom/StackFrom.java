package com.nv.admin.nitevibe.custom;

public enum StackFrom {
    Bottom, Top;
    public static final StackFrom DEFAULT = Top;
}
