package com.nv.admin.nitevibe;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.Chat;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.Activity.View_Profile;
import com.nv.admin.nitevibe.Adapter.ChatAdapter;
import com.nv.admin.nitevibe.Adapter.ChatModel;
import com.nv.admin.nitevibe.Adapter.ChattingModel;
import com.nv.admin.nitevibe.Adapter.RecyclerChatAdapter;
import com.nv.admin.nitevibe.Database.AppDatabse;
import com.nv.admin.nitevibe.Database.ChatTable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class Chatting2 extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    public ImageView img_more, img_back, img_emoji, img_nv, img_send;
    public TextView txt_name;
    //public static CircularImageView img_user;
    public static CircleImageView img_user;
//    public static ListView messageList;
    public RecyclerView messageList;
    //extra sticker
    public static boolean sticker_key = true;
    //sticker grid
    public GridView sticker_gridview;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId, loginUserMode, loginUserType;
    AlertDialog alert;
    public static Typeface bold_face, extra_bold_face, reg_face, semi_bold_face, calibri_bold, lato_bold, semi_bold_italic;
    public static String str_select_userid, str_select_username, str_select_profile;
    //emoji
    EmojIconActions emojIcon;
    EmojiconEditText text_keyboard;
    View rootView;
    Integer[] imageIDs = {
            R.drawable.stickers_01, R.drawable.stickers_02

    };
    //   public static SwipeRefreshLayout swipeRefreshLayout;
    public static String get_msg_str, send_msg_str;
    private List<ChatModel> chatMessages;
    private List<ChattingModel> chatMessages1;
    private RecyclerChatAdapter adapter;
    ProgressDialog loading;
    public ChatAdapter chatAdapter;
    RelativeLayout msg_rel;
    public static boolean handlerStop = false;
    private Socket mSocket;
    private Boolean isConnected = true;
    private String TAG = getClass().getSimpleName();
    AppDatabse db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        handlerStop = false;
        MyApplication app = (MyApplication) getApplication();
        mSocket = app.getSocket();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        sharedpreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");

        db = Room.databaseBuilder(Chatting2.this, AppDatabse.class, "UserChats").allowMainThreadQueries().build();

        img_back = findViewById(R.id.back);
        // img_user=(CircularImageView)findViewById(R.id.user_img);
        img_user = findViewById(R.id.user_img);
        img_more = findViewById(R.id.more);
        img_emoji = findViewById(R.id.emoji_icon);
        img_nv = findViewById(R.id.nv_icon);
        img_send = findViewById(R.id.send);
        // swipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);

        txt_name = findViewById(R.id.txt_name);
        text_keyboard = findViewById(R.id.typetext);

        messageList = findViewById(R.id.messageRecycler);
        LinearLayoutManager manager = new LinearLayoutManager(Chatting2.this);
//        manager.setReverseLayout(true);
        messageList.setLayoutManager(manager);
        ///////
        // messageList.setStackFromBottom(true);


        sticker_gridview = findViewById(R.id.stickers_gridview);
        sticker_gridview.setAdapter(new StickersGridView(Chatting2.this));
        sticker_gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String pos = String.valueOf(position + 1);
                // Toast.makeText(getBaseContext(), "Grid Item " + (position + 1) + " Selected", Toast.LENGTH_LONG).show();
                ChatModel model = new ChatModel();
                model.setMe(false);
                model.setMessage("");
                model.setSender(loginUserId);
                model.setReceiver(str_select_userid);
                model.setSent("Sent");
                if (pos.equals("1")) {
                    model.setImage("1");

                } else if (pos.equals("2")) {
                    model.setImage("2");
                }

//                setadapterview();
                //   sendmessage(loginUserId,str_select_userid,"",pos);
                sendmessageVolley(model, loginUserId, str_select_userid, "", pos);
            }
        });

        setFont();

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            //values from ChatListAdapter
            str_select_userid = extra.getString("user_id");
            str_select_username = extra.getString("user_name");
            str_select_profile = extra.getString("select_user_profile");
            Log.d("chatting_val", str_select_userid + " " + str_select_username);

            txt_name.setText(str_select_username);

            if (str_select_profile.equals("")) {
                img_user.setImageResource(R.drawable.default_icon);
            } else {
                Bitmap icon = getBitmapFromURL(str_select_profile);
                img_user.setImageBitmap(icon);
            }


            SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
            editor.remove("chatuser");
            editor.commit();
            editor.putString("chatuser", str_select_userid);
            editor.commit();


        }
        clickEvent();
        rootView = findViewById(R.id.keyboard_rel);
        emojIcon = new EmojIconActions(this, rootView, text_keyboard, img_emoji);
        emojIcon.ShowEmojIcon();

      /*  emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e("Keyboard", "open");
            }

            @Override
            public void onKeyboardClose() {
                Log.e("Keyboard", "close");
            }
        });*/


        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                sticker_gridview.setVisibility(View.GONE);
                Log.e("Keyboard", "open");
//                messageList.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
                //  messageList.setStackFromBottom(true);
            }

            @Override
            public void onKeyboardClose() {
                Log.e("Keyboard", "close");
            }
        });


        boolean flag = hasConnection();
        if (flag) {

            get_msg_str = AllUrl.CHATTING_DATA + loginUserId + "&receiver_id=" + str_select_userid;
            setMessageVolleyStart(loginUserId, str_select_userid);


        } else {
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
        }
        adapter = new RecyclerChatAdapter(Chatting2.this, chatMessages, str_select_userid);
        messageList.setAdapter(adapter);
        messageList.scrollToPosition(chatMessages.size()-1);
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;


        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Log.d("click_on_edit_text", "beforeTextChanged");

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Log.d("click_on_edit_text", "onTextChanged");
//            messageList.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
            // messageList.setStackFromBottom(true);

        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {


            }

        }
    }

    private void setMessageVolleyStart(String loginUserId, String str_select_userid) {
        List<ChatTable> array = db.taksInterface().getChatForUser(loginUserId, str_select_userid);

        {
            chatMessages = new ArrayList<>();

            //else part
            for (ChatTable chatTable : array) {


                ChatModel model = new ChatModel();

                model.setId(chatTable.getId());
                model.setSender(chatTable.getFrom());
                model.setMessage(chatTable.getText());

                model.setReceiver(chatTable.getTo());
                model.setSent(chatTable.getDelivery_status());
                model.setImage(chatTable.getImage());
//                model.setUserimage(chatTable.getId());
                model.setChat_id(chatTable.getId());
                model.setDate(chatTable.getDate());
                chatMessages.add(model);

            }


            // messageList.setStackFromBottom(true);
            // messageList.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);

            adapter = new RecyclerChatAdapter(Chatting2.this, chatMessages, str_select_userid);
            if (chatMessages.size() > 0) messageList.setAdapter(adapter);
            for (ChatModel message : chatMessages) {
                if (message.getReceiver().equals(loginUserId) && !message.getSent().equals("read") && !message.getId().equals("")) {
                    JSONObject object1 = new JSONObject();
                    try {
                        object1.put("id", message.chat_id);
                        object1.put("to", loginUserId);
                        object1.put("from", message.getSender());
                        object1.put("date", message.getDate());
                        mSocket.emit("user_msg_read", object1);
                        if (db.taksInterface().isExist(message.chat_id).size() > 0) {
                            db.taksInterface().updateStatusById(message.chat_id, "read");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            messageList.post(new Runnable() {
                public void run() {
//                    messageList.setSelection(messageList.getCount()-1);
                }
            });

            // Parcelable state = messageList.onSaveInstanceState();
            //  messageList.onRestoreInstanceState(state);


/*
                            if (!handlerStop) {
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        get_msg_str = AllUrl.CHATTING_DATA + loginUserId + "&receiver_id=" + str_select_userid;
                                        Log.d("chat_list_url", get_msg_str);
                                        //setMessages(get_msg_str);
                                        setMessageVolley(get_msg_str);
                                        handler.postDelayed(this, 5000);
                                    }
                                }, 1000);
                            }
*/


        }


    }





    public void clickEvent() {
        img_emoji.setOnClickListener(this);
        img_nv.setOnClickListener(this);
        img_more.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_send.setOnClickListener(this);
        img_user.setOnClickListener(this);
        text_keyboard.addTextChangedListener(new MyTextWatcher(text_keyboard));

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.user_img) {
            handlerStop = true;
            SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
            editor.remove("userid");
            editor.remove("from");
            editor.remove("chatuser");

            editor.commit();
            editor.putString("userid", str_select_userid);
            editor.putString("from", "Chatting");
            editor.commit();
            startActivity(new Intent(Chatting2.this, View_Profile.class));
        }
        if (id == R.id.emoji_icon) {
        }

        if (id == R.id.nv_icon) {
//            messageList.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
            // messageList.setStackFromBottom(true);

            messageList.post(new Runnable() {
                public void run() {
//                    messageList.setSelection(messageList.getCount()-1);
                }
            });
            if (sticker_key) {
                sticker_gridview.setVisibility(View.GONE);
                emojIcon.closeEmojIcon();
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(
                        img_send.getWindowToken(), 0);
                img_nv.setImageDrawable(getResources().getDrawable(R.drawable.select_home));
                sticker_key = false;
            } else if (!sticker_key) {
                sticker_gridview.setVisibility(View.VISIBLE);
                emojIcon.closeEmojIcon();
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(
                        img_send.getWindowToken(), 0);
                img_nv.setImageDrawable(getResources().getDrawable(R.drawable.select_home));
                sticker_key = true;

            }
        }

        //
        if (id == R.id.more) {
            showPopup(view);
        }
        if (id == R.id.back) {
            // super.finish();
            handlerStop = true;
            SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
            editor.remove("chatuser");
            editor.commit();
            Intent i = new Intent(Chatting2.this, Chat.class);
            startActivity(i);
        }
        if (id == R.id.send) {
            img_send.setEnabled(false);
          /*  messageList.setSelection(messageList.getCount()+1);///////////////////////
            Parcelable state = messageList.onSaveInstanceState();
            messageList.onRestoreInstanceState(state);
            //sdvkjsdvnsoifvnsdfv*/
            //send msg
            String sticker = "null";
            String newText = text_keyboard.getText().toString();
            Log.d("text_send", newText);

            ChatModel model = new ChatModel();
            model.setMe(false);
            model.setMessage(text_keyboard.getText().toString());
            model.setSender(loginUserId);
            model.setReceiver(str_select_userid);
            model.setSent("Sent");

            if (sticker.equals("null")) {
                model.setImage("null");
            }

            if (!newText.equals("")) {
                boolean flag = hasConnection();
                if (flag) {
                    //  sendmessage(loginUserId,str_select_userid,newText,sticker);
                    sendmessageVolley(model, loginUserId, str_select_userid, newText, sticker);
//                    setadapterview();
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    private void sendmessageVolley(ChatModel model, final String sender_id, final String receiver_id, final String message, final String sticker) {
        JSONObject object = new JSONObject();
        try {

            Log.e(TAG, "sendmessageVolley: " );
            object.put("to", receiver_id);
            object.put("from", sender_id);
            object.put("text", message);
            object.put("id", Calendar.getInstance().getTimeInMillis() + "_" + sender_id);
            object.put("date", Calendar.getInstance().getTimeInMillis());
            object.put("sent", "sent");
            object.put("sticker", sticker);
            mSocket.emit("new message", object);
            model.setId(Calendar.getInstance().getTimeInMillis() + "_" + sender_id);
            /*chatMessages.add(model);
            adapter = new CustomChatAdapter(Chatting.this, chatMessages);
            messageList.setAdapter(adapter);
            adapter.notifyDataSetChanged();*/
            messageList.post(new Runnable() {
                public void run() {
                    messageList.scrollToPosition(chatMessages.size()-1);
//                    messageList.setSelection(messageList.getCount());
                }
            });

            text_keyboard.getText().clear();
            ////msg notification
            String url = AllUrl.MSG_NOTIFY + loginUserId + "&receiver_id=" + receiver_id;
            msgNotification(url);

        } catch (JSONException e) {
            e.printStackTrace();
        }


//        Volley.newRequestQueue(this).add(postRequest);
       /* loading= new ProgressDialog(Chatting.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();*/

    }

    private void msgNotification(String url) {
      /*  loading= new ProgressDialog(Chatting.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(this.getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();*/
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("msgnotificationresponse", response.toString());
                //loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {
                        Log.d("notify_msg", "sucess");

                    } else {
                        // Toast.makeText(Chatting.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    /*Toast.makeText(Chatting.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
              /*  Toast.makeText(Chatting.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();*/
                // hide the progress dialog
                //  loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    public void setFont() {
        bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold.ttf");
        extra_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        reg_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        semi_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBold.ttf");
        calibri_bold = Typeface.createFromAsset(getAssets(), "fonts/Calibri Bold.ttf");
        lato_bold = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");
        semi_bold_italic = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBoldItalic.ttf");

        txt_name.setTypeface(bold_face);

    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    //convert image url to bitmap
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onRefresh() {

    }


    private class StickersGridView extends BaseAdapter {
        private Context mContext;

        public StickersGridView(Context c) {
            mContext = c;
        }

        public int getCount() {
            return imageIDs.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView mImageView;

            if (convertView == null) {
                mImageView = new ImageView(mContext);
                mImageView.setLayoutParams(new GridView.LayoutParams(250, 250));
                mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                mImageView.setPadding(20, 20, 20, 20);
            } else {
                mImageView = (ImageView) convertView;
            }
            Glide.with(mContext)
                    .load(imageIDs[position]).
                    /* asBitmap().*/
                            override(230, 230).into(mImageView);
            return mImageView;
        }

    }


    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.chat_menu, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_clear:
                        //Toast.makeText(getApplicationContext(),"clear",Toast.LENGTH_SHORT).show();
                        boolean flag = hasConnection();
                        if (flag) {
                            String url = AllUrl.CLEAR_CHAT + loginUserId + "&receiver_id=" + str_select_userid;
                            db.taksInterface().removeAll(loginUserId, str_select_userid);
                            // clearMsg(url);
                            clearMsgVolley(url);
                        } else {
                            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                        }
                        break;

                }
                return true;
            }
        });
    }

    private void clearMsgVolley(final String url) {

        loading = new ProgressDialog(Chatting2.this, R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("clear_chat_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if (res.equals("200")) {
                        chatMessages.clear();
                     /*   get_msg_str=AllUrl.CHATTING_DATA+loginUserId+"&receiver_id="+str_select_userid;
                        Log.d("chat_list_url",get_msg_str.toString());
                        //setMessages(get_msg_str);
                        setMessageVolleyStart(get_msg_str);*/
                       /* str_select_userid=extra.getString("user_id");
                        str_select_username=extra.getString("user_name");
                        str_select_profile=extra.getString("select_user_profile");*/
                        setMessageVolleyStart(loginUserId, str_select_userid);
                    } else if (res.equals("202")) {

                    } else {
                        Toast.makeText(Chatting2.this, object.getString("message"), Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    /*Toast.makeText(Chatting.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                /*Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();*/

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handlerStop = true;
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.remove("chatuser");
        editor.commit();
        /*Intent i=new Intent(this,Chat.class);
        startActivity(i);*/
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("new message", onNewMessage);
        mSocket.on("user joined", onUserJoined);
        mSocket.on("user left", onUserLeft);
        mSocket.on("delivery", onDelivery);
        mSocket.on("msg_read", onReadMessage);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("new message", onNewMessage);
        mSocket.off("user joined", onUserJoined);
        mSocket.off("user left", onUserLeft);
        mSocket.off("delivery", onDelivery);
        mSocket.off("msg_read", onReadMessage);
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isConnected) {
                        if (null != loginUserId) {
                            mSocket.emit("add user", loginUserId);
//                                mSocket.emit("adduser", new JSONObject().put("username", sessionManager.getUserDetails().get(sessionManager.KEY_EmployeeCode)));
                        }
//                        Toast.makeText(getContext(), R.string.connect, Toast.LENGTH_LONG).show();
                        isConnected = true;
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "diconnected");
                    isConnected = false;
                    mSocket.disconnect();
//                    Toast.makeText(getActivity().getApplicationContext(), R.string.disconnect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");
//                    Toast.makeText(getActivity().getApplicationContext(), R.string.error_connect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];

                    try {

                        Log.e(TAG, "run: onNewMessage" + data);
                        ChatModel model = new ChatModel();
                        model.setId(data.getString("id"));
                        model.setSender(data.getString("from"));
                        model.setMessage(data.getString("text"));
                        model.setReceiver(loginUserId);
                        model.setSent("sent");
                        model.setImage(data.getString("sticker"));
                        model.setUserimage("");
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String strdate = format.format(Long.parseLong(data.getString("date")));
                        model.setDate(strdate);
                        mSocket.emit("user_msg_read", data);
                        Date d = new Date(data.getLong("date"));
                        Calendar c = Calendar.getInstance();
                        c.setTime(d);
                        if (db.taksInterface().isExist(data.getString("id")).size() < 1)
                            db.taksInterface().insertAll(new ChatTable(data.getString("id"), data.getString("from"), loginUserId, data.getString("text"), String.valueOf(c.getTimeInMillis()), "person", "received", data.getString("sticker")));

                        getArrayModel();


                        /*adapter = new CustomChatAdapter(Chatting.this, chatMessages);
                        if (chatMessages.size()>0)messageList.setAdapter(adapter);
                        messageList.setSelection(adapter.getCount());*/
//                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "run: onUserJoined" + data);
//                    addLog(getResources().getString(R.string.message_user_joined));
//                    addParticipantsLog(numUsers);
                }
            });
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "run: onUserLeft" + data);

//                    addLog(getResources().getString(R.string.message_user_left, username));
//                    addParticipantsLog(numUsers);
//                    removeTyping(sessionManager.getUserDetails().get(sessionManager.KEY_EmployeeCode));
                }
            });
        }
    };

    private Emitter.Listener onDelivery = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "onDelivery" + data);
                    try {
                        img_send.setClickable(true);
                        ChatModel model = new ChatModel();
                        model.setId(data.getString("id"));
                        model.setReceiver(data.getString("to"));
                        model.setMe(true);
                        model.setDate(data.getString("date"));
                        model.setMessage(data.getString("text"));
                        model.setImage(data.optString("sticker"));
                        if (data.getBoolean("status")) {
                            model.setSent("delivered");
                        } else {
                            model.setSent("sent");
                        }
                        if (db.taksInterface().isExist(model.id).size() > 0) {

                            if (data.getBoolean("status")) {
                                db.taksInterface().updateStatusById(model.id, "delivered");
                            } else {
                                db.taksInterface().updateStatusById(model.id, "sent");
                            }

                            for (ChatModel message : chatMessages) {
                                if (message.getId().equals(model.id)) {
                                    message.setSent(model.sent);
                                    break;
                                }
                            }
//                            if (model.receiver.equals(loginUserId))
//                                adapter.notifyDataSetChanged();
                        } else {
                            ChatTable table = new ChatTable(data.getString("id"), data.getString("from"), data.getString("to"), data.getString("text"), data.getString("date"), "person", "", data.getString("sticker"));
                            if (data.getBoolean("status")) {
                                table.setDelivery_status("delivered");
                            } else {
                                table.setDelivery_status("sent");
                            }
                            db.taksInterface().insertAll(table);
//                        setMessageVolleyStart(loginUserId, str_select_userid);
                            chatMessages.add(model);
//                            adapter.notifyDataSetChanged();
                            messageList.scrollToPosition(chatMessages.size()-1);
                        }

                       /* chatMessages.add(model);
                        adapter = new CustomChatAdapter(Chatting.this, chatMessages);
                        messageList.setAdapter(adapter);
                        messageList.setSelection(messageList.getCount());*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });
        }
    };

    private Emitter.Listener onReadMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "run: onReadMessage" + args[0]);
                    JSONObject data = (JSONObject) args[0];
                    try {
                        for (ChatModel model : chatMessages) {

                            if (model.getId().equals(data.getString("id")) && data.getBoolean("status")) {
                                model.setSent("read");

                                continue;
                            }

                        }

                        if (db.taksInterface().isExist(data.getString("id")).size() > 0) {
                            db.taksInterface().updateStatusById(data.getString("id"), "read");
                        } else {
                            ChatTable table = new ChatTable(data.getString("id"), data.getString("from"), data.getString("to"), data.getString("text"), data.getString("date"), "person", "read", data.getString("sticker"));
                            db.taksInterface().insertAll(table);
                        }
                        setMessageVolleyStart(loginUserId, str_select_userid);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    /*adapter = new CustomChatAdapter(Chatting.this, chatMessages);
                    messageList.setAdapter(adapter);
                    messageList.setSelection(messageList.getCount());*/
                }
            });
        }
    };


    private Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            Log.e(TAG, "call: onLogin" + data);
        }
    };

    private List<ChatModel> getArrayModel() {
        List<ChatTable> array = db.taksInterface().getChatForUser(loginUserId, str_select_userid);


        chatMessages = new ArrayList<>();

        //else part
        for (ChatTable chatTable : array) {


            ChatModel model = new ChatModel();

            model.setId(chatTable.getId());
            model.setSender(chatTable.getFrom());
            model.setMessage(chatTable.getText());

            model.setReceiver(chatTable.getTo());
            model.setSent(chatTable.getDelivery_status());
            model.setImage(chatTable.getImage());
//                model.setUserimage(chatTable.getId());
            model.setChat_id(chatTable.getId());
            model.setDate(chatTable.getDate());
            chatMessages.add(model);

        }
        adapter = new RecyclerChatAdapter(Chatting2.this, chatMessages, str_select_userid);
        messageList.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
        messageList.scrollToPosition(chatMessages.size()-1);
        return chatMessages;
    }
}
