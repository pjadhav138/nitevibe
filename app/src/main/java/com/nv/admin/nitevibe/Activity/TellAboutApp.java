package com.nv.admin.nitevibe.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.TypefaceSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;
import com.nv.admin.nitevibe.custom.CustomTypefaceSpan;

public class TellAboutApp extends AppCompatActivity implements View.OnClickListener {
    public static ImageView back;
    public static TextView data,title,data1;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face;
    public static Button btn_share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tell_about_app);

        back= findViewById(R.id.back);
        data= findViewById(R.id.txt_data);
        data1= findViewById(R.id.txt_data1);
        title= findViewById(R.id.title);
        btn_share= findViewById(R.id.share);

        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");

        back.setOnClickListener(this);
        btn_share.setOnClickListener(this);
        setFont();

        TypefaceSpan boldspan=new CustomTypefaceSpan("",bold_face);
        TypefaceSpan regularspan=new CustomTypefaceSpan("",reg_face);


        String txt1=this.getResources().getString(R.string.aboutus1);
        String txt2=this.getResources().getString(R.string.aboutus2);

        SpannableStringBuilder sb1 = new SpannableStringBuilder(txt1);
        SpannableStringBuilder sb2 = new SpannableStringBuilder(txt2);

        sb1.setSpan(regularspan, 0,2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        sb1.setSpan(regularspan, 12,txt1.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        sb1.setSpan(boldspan, 3,11, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        sb1.setSpan(new ForegroundColorSpan(this.getResources().getColor(R.color.female)), 3,11, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        data.setText(sb1);

        sb2.setSpan(regularspan, 0,9, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        sb2.setSpan(regularspan, 28,txt2.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        sb2.setSpan(boldspan, 10,27, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        sb2.setSpan(new ForegroundColorSpan(this.getResources().getColor(R.color.female)), 10,27, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        data1.setText(sb2);

    }

    @Override
    public void onClick(View view) {
        int id=view.getId();

        if(id==R.id.back){
            startActivity(new Intent(this,Setting.class));
        }
        if(id==R.id.share){


            String appname="com.nv.admin.nitevibe";
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);

            String link="https://play.google.com/store/apps/details?id="+appname;
            String message="Connect your vibes with the person you meet at the venue and groove your night, the right way and enjoy your nightlife experience." +
                    "\n" +
                    "Your favorite clubs and events happening, all at one stop app. Download Nitevibe to know more from "+link;
            //String message="Install Nitevibe app from "+link;
            sendIntent.putExtra(Intent.EXTRA_TEXT,message);
            sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            sendIntent.setType("text/plain");

            startActivity(sendIntent);
        }

    }


    public void setFont(){
        title.setTypeface(bold_face);
        // support_data.setTypeface(semi_bold_face);
    }
}
