package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Admin on 4/25/2018.
 */
public class WhosGoingAdapter extends RecyclerView.Adapter<WhosGoingAdapter.MyView> {
    private List<WhosGoingModel> arrayList;
    private Context context;
    WhosGoingModel current;

    public WhosGoingAdapter(List<WhosGoingModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.whos_going_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyView holder, int position) {
        current=arrayList.get(position);

        Log.d("size_of_arrayList", String.valueOf(arrayList.size()));

        String img=current.userGoingProfile;
        String entry=current.userGoingEntry;

     /*   if(current.userGoingEntry.equals("Recent")){
            holder.profile.setBorderWidth(3);
            holder.profile.setBorderColor(context.getResources().getColor(R.color.light_blue));
           // holder.profile.setBorderColor(context.getColor(R.color.light_blue));

        }
        else if(current.userGoingEntry.equals("Old")){
            holder.profile.setBorderWidth(3);
            holder.profile.setBorderColor(context.getResources().getColor(R.color.light_green));
        }*/

        if(current.userGoingCategory.equals("1")){ //like
            holder.profile.setBorderWidth(3);
            holder.profile.setBorderColor(context.getResources().getColor(R.color.pink));
        }
        else if(current.userGoingCategory.equals("2")){ //dislike
            holder.profile.setBorderWidth(3);
            holder.profile.setBorderColor(context.getResources().getColor(R.color.gray));
        }
        else if(current.userGoingCategory.equals("3")){ //superlike
            holder.profile.setBorderWidth(3);
            holder.profile.setBorderColor(context.getResources().getColor(R.color.light_blue));
        }
        else{
            holder.profile.setBorderWidth(3);
            holder.profile.setBorderColor(context.getResources().getColor(R.color.light_green));

        }


        holder.userId.setText(current.userGoingId);

        if(img.equals("")){

            Picasso.with(context.getApplicationContext())
                    .load(R.drawable.default_icon)
                    .into(holder.profile);
        }
        else{
            Picasso.with(context.getApplicationContext())
                    .load(current.userGoingProfile)
                    .into(holder.profile);

        }

        holder.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(context,holder.userId.getText().toString(),Toast.LENGTH_SHORT).show();

            }
        });





    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public CircleImageView profile;
        public TextView userId;
        public MyView(View itemView) {
            super(itemView);
            profile= itemView.findViewById(R.id.profile_img);
            userId= itemView.findViewById(R.id.user_id);
        }
    }
}
