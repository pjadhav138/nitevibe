package com.nv.admin.nitevibe.notification;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.Chat;
import com.nv.admin.nitevibe.Activity.ClubEventDetails;
import com.nv.admin.nitevibe.Activity.Events;
import com.nv.admin.nitevibe.Activity.MyApplication;
import com.nv.admin.nitevibe.Activity.SuperVibeSwipe;
import com.nv.admin.nitevibe.Database.DatabaseHandler;
import com.nv.admin.nitevibe.Database.SettingModel;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 4/30/2018.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
//    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
private static final String TAG = "Notification_res";

    public static String message,timestamp,title,value,imageUrl,eventId,userId,clubId,loginUserId;
    private NotificationUtils notificationUtils;
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static DatabaseHandler handler;
    List<SettingModel> arrayList;
    public boolean mute_flag,message_flag,match_flag,event_flag,location_flag,check_in_flag;
    ProgressDialog loading;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        sharedpreferences=getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        loginUserId=sharedpreferences.getString("user_id","");

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }


        //sharedpreferences =getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);


    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext()))     {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();

        }else{

            // If the app is in background, firebase itself handles the notification

        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());
        Intent pushNotification=null;
        try {
            JSONObject data = json.getJSONObject("data");
            //JSONObject data = json.getJSONObject("notification");


             title = data.getString("title");
            message = data.getString("message");
            boolean isBackground = data.getBoolean("is_background");
             imageUrl = data.getString("image");
            timestamp = data.getString("timestamp");
            JSONObject payload = data.getJSONObject("payload");



            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "isBackground: " + isBackground);
            Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);
            sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);

            handler=new DatabaseHandler(this);
            //inserting values
            arrayList=new ArrayList<>();
            arrayList=handler.getAllData();
            for(int i=0;i<arrayList.size();i++){
                SettingModel model;
                model=arrayList.get(i);
                String name=model.getLabel();
                String dbdata=model.getData();

                //mute,show,message,match,event,location,checkin
                //public boolean mute_flag,message_flag,match_flag,event_flag,location_flag,check_in_flag
                if(name.equals("mute")){
                    mute_flag= Boolean.parseBoolean(dbdata);

                }
                /*else if(name.equals("show")){ mute_flag= Boolean.parseBoolean(dbdata); }*/
                else if(name.equals("message")){ message_flag= Boolean.parseBoolean(dbdata); }
                else if(name.equals("match")){ match_flag= Boolean.parseBoolean(dbdata); }
                else if(name.equals("event")){ event_flag= Boolean.parseBoolean(dbdata); }
                else if(name.equals("location")){ location_flag= Boolean.parseBoolean(dbdata); }
                else if(name.equals("checkin")){ check_in_flag= Boolean.parseBoolean(dbdata); }


            }


            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                 pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
             // LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();

                if(title.equals("Check-In")){
                    value=data.getString("value");

                    String eventValue=data.getString("event");
                    Log.d("value_of_club",value+","+eventValue);
                    SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                    editor.remove("club_id");
                    editor.commit();
                    editor.putString("club_id",value);
                    editor.commit();
                    Intent resultIntent = new Intent(getApplicationContext(), ClubEventDetails.class);
                    resultIntent.putExtra("message", message);



                    if(check_in_flag){

                           boolean flag=hasConnection();
                    if(flag){
                        String checkinreceive= AllUrl.CHECKIN_NOTIFY_RECEIVED+loginUserId+"&event_id="+eventValue;
                        checkinreceive=checkinreceive.replaceAll(" ","%20");
                        insertCheckinValue(checkinreceive);
                    }
                    else{
                        //no internet connection
                    }
                        if (TextUtils.isEmpty(imageUrl)) {
                            MyApplication.getInstance().trackEvent("Notification", "Check-in", "Success");
                            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                        } else {
                            MyApplication.getInstance().trackEvent("Notification", "Check-in", "Success");
                            // image is present, show notification with image
                          //  showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                            showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent,"");
                        }
                    }


                }
                else if(title.equals("New Message")){
                    userId=data.getString("userid");
                    ///coming form chatting user is on chat screen
                    String userViewId=sharedpreferences.getString("chatuser","");
                    Log.d("userViewId",userViewId);
                    Intent resultIntent = new Intent(getApplicationContext(), Chat.class);
                    resultIntent.putExtra("message", message);
                    resultIntent.putExtra("userid", userId);
                    resultIntent.putExtra("user_name", data.getString("user_name"));
                    resultIntent.putExtra("profile_pic", data.getString("profile_pic"));
                    if(message_flag){
                        if(!userViewId.equals(userId)){
                            if (TextUtils.isEmpty(imageUrl)) {
                                MyApplication.getInstance().trackEvent("Notification", "New Message", "Success");
                                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                            } else {
                                MyApplication.getInstance().trackEvent("Notification", "New Message", "Success");
                                // image is present, show notification with image
                                //showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                                showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent,"");
                            }
                        }

                    }

                }
                //match profile
                else if(title.equals("Match Profile")){
                    Intent resultIntent = new Intent(getApplicationContext(), Chat.class);
                    resultIntent.putExtra("message", message);
                    if(match_flag){
                        Intent resultIntent1 = new Intent(getApplicationContext(), Chat.class);
                        resultIntent1.putExtra("message", message);
                        if (TextUtils.isEmpty(imageUrl)) {
                            MyApplication.getInstance().trackEvent("Notification", "Profile Match", "Success");
                            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                        } else {
                            MyApplication.getInstance().trackEvent("Notification", "Profile Match", "Success");
                            // image is present, show notification with image
                            //showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                            showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, "");
                        }
                    }

                }

                //supervibe
                else if(title.equals("Supervibe")){
                    eventId=data.getString("event");
                    userId=data.getString("userid");
                    SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                    editor.remove("notify_event");
                    editor.remove("notify_user");
                    editor.commit();
                    editor.putString("notify_event",eventId);
                    editor.putString("notify_user",userId);
                    editor.commit();
                    Intent resultIntent = new Intent(getApplicationContext(), SuperVibeSwipe.class);
                    resultIntent.putExtra("message", message);
                    //  if(match_flag){

                        if (TextUtils.isEmpty(imageUrl)) {
                            MyApplication.getInstance().trackEvent("Notification", "Supervibe", "Success");
                            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                        } else {
                            MyApplication.getInstance().trackEvent("Notification", "Supervibe", "Success");
                            // image is present, show notification with image
                            //showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                            showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent,"");
                        }
                  //  }

                }
                else{

                    eventId=data.getString("event");
                    clubId=data.getString("idclub");
                    Log.d("club_event1",eventId+" "+clubId);
                    SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                    editor.remove("event_id");
                    editor.remove("club_id");
                    editor.commit();

                    editor.putString("event_id",eventId);
                    editor.putString("club_id",clubId);
                    editor.commit();

                    Intent resultIntent1 = new Intent(getApplicationContext(),Events.class);
                  //  resultIntent1.putExtra("message", message);
                    if (TextUtils.isEmpty(imageUrl)) {
                       // MyApplication.getInstance().trackEvent("Notification", "Supervibe", "Success");
                        showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent1);
                    } else {
                          //Log.d("notification_img",imageUrl);
                        imageUrl=imageUrl.replaceAll(" ","%20");
                       // Log.d("notification_img_next",imageUrl);
                        // image is present, show notification with image
                        //showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent1,imageUrl);
                    }

                }




            }
            else{
                pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                //   LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();

                if(title.equals("Check-In")){
                    value=data.getString("value");
                    //eventId=data.getString("event");
                    String eventValue=data.getString("event");
                    SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                    editor.remove("club_id");
                    editor.commit();
                    editor.putString("club_id",value);
                    editor.commit();
                    Intent resultIntent = new Intent(getApplicationContext(), ClubEventDetails.class);
                    resultIntent.putExtra("message", message);




                    if(check_in_flag){

                          boolean flag=hasConnection();
                    if(flag){
                        String checkinreceive= AllUrl.CHECKIN_NOTIFY_RECEIVED+loginUserId+"&event_id="+eventValue;
                        checkinreceive=checkinreceive.replaceAll(" ","%20");
                        insertCheckinValue(checkinreceive);
                    }
                    else{
                        //no internet connection
                    }
                        Log.d("notify_flagcheck","entered");
                        if (TextUtils.isEmpty(imageUrl)) {
                            MyApplication.getInstance().trackEvent("Notification", "Check-in", "Success");
                            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                        } else {
                            MyApplication.getInstance().trackEvent("Notification", "Check-in", "Success");
                            // image is present, show notification with image
                            //showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                            showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent,"");
                        }
                    }

                }
                else if(title.equals("New Message")){
                    userId=data.getString("userid");
                    ///coming form chatting user is on chat screen
                    String userViewId=sharedpreferences.getString("chatuser","");
                    Log.d("userViewId",userViewId);

                    Intent resultIntent = new Intent(getApplicationContext(), Chat.class);
                    resultIntent.putExtra("message", message);
                    resultIntent.putExtra("userid", userId);
                    resultIntent.putExtra("user_name", data.getString("user_name"));
                    resultIntent.putExtra("profile_pic", data.getString("profile_pic"));
                    if(message_flag){
                        if(!userViewId.equals(userId)){
                            if (TextUtils.isEmpty(imageUrl)) {
                                MyApplication.getInstance().trackEvent("Notification", "New Message", "Success");
                                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                            } else {
                                MyApplication.getInstance().trackEvent("Notification", "New Message", "Success");
                                // image is present, show notification with image
                                //showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                                showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent,"");
                            }
                        }

                    }

                }
                //match profile
                else if(title.equals("Match Profile")){
                    Intent resultIntent = new Intent(getApplicationContext(), Chat.class);
                    resultIntent.putExtra("message", message);
                    if(match_flag){
                        Intent resultIntent1 = new Intent(getApplicationContext(), Chat.class);
                        resultIntent1.putExtra("message", message);
                        if (TextUtils.isEmpty(imageUrl)) {
                            MyApplication.getInstance().trackEvent("Notification", "Profile Match", "Success");
                            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                        } else {
                            // image is present, show notification with image
                            MyApplication.getInstance().trackEvent("Notification", "Profile Match", "Success");
                            //showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                            showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent,"");
                        }
                    }

                }
                //supervibe
                else if(title.equals("Supervibe")){
                    eventId=data.getString("event");
                    userId=data.getString("userid");
                    SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                    editor.remove("notify_event");
                    editor.remove("notify_user");
                    editor.commit();
                    editor.putString("notify_event",eventId);
                    editor.putString("notify_user",userId);
                    editor.commit();
                    Intent resultIntent = new Intent(getApplicationContext(), SuperVibeSwipe.class);
                    resultIntent.putExtra("message", message);
                    //  if(match_flag){

                    if (TextUtils.isEmpty(imageUrl)) {
                        MyApplication.getInstance().trackEvent("Notification", "Supervibe", "Success");
                        showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                    } else {
                        // image is present, show notification with image
                        MyApplication.getInstance().trackEvent("Notification", "Supervibe", "Success");
                        //showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, "");
                    }
                    //  }

                }
                else{
                    eventId=data.getString("event");
                    clubId=data.getString("idclub");
                    SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                    editor.remove("event_id");
                    editor.remove("club_id");
                    editor.commit();
                    editor.putString("event_id",eventId);
                    editor.putString("club_id",clubId);
                    editor.commit();
                    Intent resultIntent1 = new Intent(getApplicationContext(), Events.class);
                    //resultIntent1.putExtra("message", message);
                    if (TextUtils.isEmpty(imageUrl)) {
                        // MyApplication.getInstance().trackEvent("Notification", "Supervibe", "Success");
                        showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent1);
                    } else {
                       // Log.d("notification_img",imageUrl);
                        imageUrl=imageUrl.replaceAll(" ","%20");
                       // Log.d("notification_img_next",imageUrl);
                        // image is present, show notification with image
                        //showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent1,imageUrl);
                    }

                }

            }


        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
     //   LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
       // LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(pushNotification);

    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }


    private void insertCheckinValue(String url){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("checkinresponse", response.toString());

                try {

                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        Log.d("checkinresponse","successfully");


                    }else{
                       // Toast.makeText(MyFirebaseMessagingService.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                    /*Toast.makeText(Chat.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

            }
        });

        // Adding request to request queue
        RequestQueue queue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
        queue.getCache().clear();
        jsonObjReq.setShouldCache(false);

        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();

            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }



    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN",s);

        // Saving reg id to shared preferences
        storeRegIdInPref(s);

        // sending reg id to your server
        sendRegistrationToServer(s);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", s);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        Log.d("firebaseinstance","entered in method");
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();
    }
}
