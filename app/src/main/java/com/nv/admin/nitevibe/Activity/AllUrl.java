package com.nv.admin.nitevibe.Activity;

/**
 * Created by Admin on 4/2/2018.
 */
public class AllUrl {

    //http://www.nitevibe.in/images/user/user.png

    ////webservices
    public static String ip = "http://www.nitevibe.in/webservices/";
    public static String share_ip = "http://www.nitevibe.in/";

   /* public static String ip="http://rednwhite.co.in/webservices/";
    public static String share_ip="http://rednwhite.co.in/";*/
    //public static String otp_ip="https://bookingsmaker.com/webservices";

    public static final String OTP = ip + "otp.php?username=";  //done


    //registration
    public static final String REGISTRATION = ip + "register_post.php";  //done

    //login
    public static final String LOGIN = ip + "login.php?username=";  //done

    //forgot password
    public static final String FORGOT_PASSWORD = ip + "forgot_password.php?username=";  //done

    //otp


    //active user
    public static final String ACTIVE_USER = ip + "active_user.php?user_name="; //done

    //location drop-down
    public static final String LOCATION = ip + "location_list.php?";  //done

    //club list
    //   public static final String CLUB_LIST=ip+"club_details.php?user_id=";
    //post
    public static final String CLUB_LIST = ip + "club_details.php";  //done

    //making club g=favourite
    public static final String FAV_CLUB = ip + "favourite.php";  //done

    //getting specific club details
    public static final String CLUB = ip + "club.php?club_id=";  //done

    //profile
    public static final String PROFILE = ip + "profile.php?user_id=";  //done

    //delete user
    public static final String DELETE = ip + "delete_account.php?user_id=";  //done

    //block user list
    public static final String BLOCK_LIST = ip + "block_user_list.php?user_id="; //done

    //block User
    public static final String BLOCK_USER = ip + "block.php?user_id=";  //done

    //display profile
    public static final String DISPLAY_PROFILE = ip + "display_profile.php?user_id=";  //done

    //add photo
    public static final String ADD_PHOTO = ip + "add_photo.php";   //done

    //remove photo
    public static final String REMOVE_PHOTO = ip + "remove_photo.php?user_id=";  //done

    //getting all drinks list
    public static final String DRINKS = ip + "drinks.php?";  //done

    //getting all match and chat name list(chat fragment)
    public static final String CHAT_LIST = ip + "match_profile_new.php?user_id=";  //done
//    public static final String CHAT_LIST = ip + "recent_chat.php?sender_id=";  //done

    //club side home screen
    public static final String CLUB_HOME = ip + "club_side_home.php?user_id="; //done

    //add guest from club side
    public static final String CLUB_ENTRY = ip + "club_entry.php";  //done

    //unmatch the user
    public static final String UNMATCH_USER = ip + "unmatch_user.php?user_id=";  //done

    //single user profile
    public static final String VIEW_PROFILE = ip + "view_profile.php?user_id=";  //done

    //getting list of chat
    public static final String CHATTING_DATA = ip + "chat_list.php?sender_id=";  //done

    //send chat message
    public static final String SEND_MSG = ip + "send_chat.php";  //done

    //clear chat msg
    public static final String CLEAR_CHAT = ip + "clear_chat.php?sender_id=";  //done

    //checkin
    public static final String CHECK_IN = ip + "check_in.php?user_id="; //done

    //update user profile data(post)
    public static final String UPDATE_PROFILE = ip + "update_profile.php"; //done

    //events
    public static final String EVENTS = ip + "event_details.php?user_id=";  //done

    //user going
    public static final String USER_GOING = ip + "going.php?user_id=";  //done

    //device id
    public static final String DEVICEID = ip + "device_id.php?user_id=";  //done

    //banner event details slide
    public static final String BANNER_EVENTS = ip + "banner_slide.php?user_id=";  //done

    //swipe card
    public static final String SWIPE_CARD = ip + "swipe_card.php?user_id=";

    //swipe detail
    public static final String SWIPE = ip + "swipe.php?user_id=";

    //supervibe count
    public static final String SUPERVIBE_COUNT = ip + "supervibe_count.php?user_id=";

    //usergoing swipe
    public static final String GOING_SWIPE = ip + "user_going_swipe.php?user_id=";

    //send msgNotification
    public static final String MSG_NOTIFY = ip + "msg_notification.php?sender_id=";

    //check in notification
    public static final String CHECKIN_NOTIFY = ip + "checkin_notification.php?user_id=";

    //getting current event details for checkin notification
    public static final String BACKGROUND_SERVICE = ip + "user_checkin_radius.php?user_id=";

    //checkout from club
    public static final String CHECK_OUT = ip + "check_out.php?user_id=";

    //notification when profile matches during swipe
    public static final String MATCH_NOTIFICATION = ip + "match_notification.php?user_id=";

    //blockuser count on settings
    public static final String BLOCK_COUNT = ip + "block_user_count.php?user_id=";

    //whos going list
    public static final String WHOS_GOING_LIST = ip + "user_going_list.php?user_id=";

    //spam user
    public static final String SPAM_USER = ip + "spam_user.php?user_id=";

    //club coupon
    public static final String CLUB_COUPON = ip + "club_coupon.php?club_id=";

    //clubside coupon list
    public static final String WITH_COUPON = ip + "club_side_withcoupon.php?club_id=";

    public static final String WITH_OUTCOUPON = ip + "club_side_withoutcoupon.php?club_id=";

    //drinkid of user
    public static final String DRINKID = ip + "user_drink_id.php?user_id=";

    //share for club and events

    //public static final String CLUB_SHARE="http://rednwhite.co.in/club_details.php?id=";

    public static final String CLUB_SHARE = share_ip + "club_details.php?id=";
    public static final String EVENT_SHARE = share_ip + "event_details.php?id=";

    //setting page url
    public static final String HELP = ip + "helpnow.php?";
    public static final String LEGAL = ip + "terms_conditions.php";
    public static final String WHOS_CHECKIN = ip + "whos_checkin.php?user_id=";
    public static final String GOING_NONPREF_LIST = ip + "going_nonpref_list.php?user_id=";

    public static final String IAM_GOING_SWIPE = ip + "going_swipe_card.php?user_id=";
    public static final String CHECKIN_SWIPE = ip + "checkin_swipe_card.php?user_id=";
    public static final String NOTIFICATION_STATUS = ip + "block_globle_notification.php";
    public static final String GET_NOTIFICATION_STATUS = ip + "globle_notification.php?";
//globle_notification.php
    //check who has supervibed
    public static final String WHO_SEND_SUPERVIBE = ip + "who_supervibe.php?login_userid=";

    //to know checkin_notification has been received
    public static final String CHECKIN_NOTIFY_RECEIVED = ip + "checkin_received.php?user_id=";
    public static final String MSG_DUMP_URL = ip + "get_chat_details.php?id=";


}
