package com.nv.admin.nitevibe.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.TypefaceSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;
import com.nv.admin.nitevibe.custom.CustomTypefaceSpan;

public class Support extends AppCompatActivity implements View.OnClickListener {
    public static ImageView back;
    public static TextView support_data,title,txt_email1,txt_email2;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);

        back= findViewById(R.id.back);
        support_data= findViewById(R.id.txt_data);
        txt_email1= findViewById(R.id.email1);
        txt_email2= findViewById(R.id.email2);

        title= findViewById(R.id.title);

        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");

        txt_email1.setTypeface(bold_face);
        txt_email2.setTypeface(bold_face);

        TypefaceSpan boldspan=new CustomTypefaceSpan("",bold_face);
        TypefaceSpan regularspan=new CustomTypefaceSpan("",reg_face);

        String txt1=this.getResources().getString(R.string.support);
        SpannableStringBuilder sb1 = new SpannableStringBuilder(txt1);

        sb1.setSpan(regularspan, 0,75, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        sb1.setSpan(regularspan, 87,txt1.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        sb1.setSpan(boldspan, 77,86, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        sb1.setSpan(new ForegroundColorSpan(this.getResources().getColor(R.color.female)), 77,86, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        support_data.setText(sb1);

        back.setOnClickListener(this);
        txt_email1.setOnClickListener(this);
        txt_email2.setOnClickListener(this);
        setFont();

        }


    @Override
    public void onClick(View view) {
        int id=view.getId();

        if(id==R.id.back){
            startActivity(new Intent(this,Setting.class));
        }
        if(id==R.id.email1){

            String emailid=txt_email1.getText().toString();
            /*Intent intent = new Intent(Intent.ACTION_SEND);
            //intent.setType("plain/text");
            intent.setType("text/email");
            intent.putExtra(Intent.EXTRA_EMAIL, new String[] {emailid});
            intent.putExtra(Intent.EXTRA_SUBJECT, "Support");
            intent.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(intent, ""));*/

            Intent testIntent = new Intent(Intent.ACTION_VIEW);
            Uri data = Uri.parse("mailto:?subject=" + "Support" + "&body=" + "" + "&to=" + emailid);
            testIntent.setData(data);
            startActivity(testIntent);

        }
        if(id==R.id.email2){
            String emailid=txt_email1.getText().toString();

            Intent testIntent = new Intent(Intent.ACTION_VIEW);
            Uri data = Uri.parse("mailto:?subject=" + "Support" + "&body=" + "" + "&to=" + emailid);
            testIntent.setData(data);
            startActivity(testIntent);

        }

    }


    public void setFont(){
        title.setTypeface(bold_face);
        support_data.setTypeface(semi_bold_face);
    }
}
