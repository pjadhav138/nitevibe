package com.nv.admin.nitevibe.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nv.admin.nitevibe.Adapter.ChatListAdapter;
import com.nv.admin.nitevibe.Adapter.ChatListModel;
import com.nv.admin.nitevibe.Adapter.MatchListAdapter;
import com.nv.admin.nitevibe.Adapter.MatchProfileModel;
import com.nv.admin.nitevibe.Database.AppDatabse;
import com.nv.admin.nitevibe.Database.ChatTable;
import com.nv.admin.nitevibe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class Chat extends AppCompatActivity implements View.OnClickListener {
    private android.support.v4.app.Fragment fragment;
    private android.support.v4.app.FragmentManager fragmentManager;
    TextView swipe_label, event_near_label;
    AlertDialog alert;
    RelativeLayout emptyChat, list_main, matchRel, chatRel;
    public static Typeface bold_face, extra_bold_face, reg_face, semi_bold_face, calibri_bold, lato_bold, semi_bold_italic;
    TextView match_label, chat_label;
    RecyclerView matchRecycler, chatRecycler;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId, loginUserMode, loginUserType;
    List<MatchProfileModel> matchList;
    List<ChatListModel> chatList;
    MatchListAdapter matchAdapter;
    ChatListAdapter chatAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    ProgressDialog loading, loading1;
    public static ImageView userTab, homeTab, chatTab;
    private Boolean isConnected = true;
    private Socket mSocket;
    private String TAG = getClass().getSimpleName();
    AppDatabse db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);
        db = Room.databaseBuilder(Chat.this, AppDatabse.class, "UserChats").allowMainThreadQueries().build();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        sharedpreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        MyApplication app = (MyApplication) getApplication();
        mSocket = app.getSocket();


        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");

        Log.e(TAG, "onCreate:LocalChat " + db.taksInterface().getRecentChatUser(loginUserId).size());

        swipe_label = findViewById(R.id.chat_label);
        event_near_label = findViewById(R.id.txt_find_near);
        match_label = findViewById(R.id.match_txt);
        chat_label = findViewById(R.id.chat_txt);

        emptyChat = findViewById(R.id.emptychatrel); //empty chat
        list_main = findViewById(R.id.list_main_rel); //list of chat
        matchRel = findViewById(R.id.matchrel);
        chatRel = findViewById(R.id.chatrel);


        userTab = findViewById(R.id.usertab);
        homeTab = findViewById(R.id.hometab);
        chatTab = findViewById(R.id.chattab);
        chatTab.setImageDrawable(getResources().getDrawable(R.drawable.select_chat));

        matchRecycler = findViewById(R.id.matchrecycler);
        matchRecycler.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(Chat.this, LinearLayoutManager.HORIZONTAL, false);
        matchRecycler.setLayoutManager(mLayoutManager);


        chatRecycler = findViewById(R.id.chatlistrecycler);
        chatRecycler.setHasFixedSize(true);
        chatRecycler.addItemDecoration(new SimpleDividerItemDecoration(Chat.this));
        chatRecycler.setLayoutManager(new LinearLayoutManager(Chat.this));//Linear Items


        setFont();
        clickEvent();

        boolean flag = hasConnection();
        if (flag) {
           /* loading1 = new ProgressDialog(this,R.style.full_screen_dialog){
                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.feed_loader);
                    getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT,
                            LinearLayout.LayoutParams.FILL_PARENT);
                }
            };

            loading1.setCancelable(false);
            loading1.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    String url= AllUrl.CHAT_LIST+loginUserId;
                    String url1=url.replaceAll(" ","%20");
                    Log.d("chatdataurl",url1);

                    getListVolley(url1);
                    loading1.dismiss();
                }
            }, 2000); // 3000 milliseconds delay
*/

            String url = AllUrl.CHAT_LIST + loginUserId;
            String url1 = url.replaceAll(" ", "%20");
            Log.d("chatdataurl", url1);

            getListVolley(url1);


        } else {
            Toast.makeText(Chat.this, "No internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("login", onLogin);
        mSocket.on("new message", onNewMessage);
        mSocket.on("user joined", onUserJoined);
        mSocket.on("user left", onUserLeft);
        mSocket.on("delivery", onDelivery);
        mSocket.on("msg_read", onReadMessage);

        mSocket.connect();
        mSocket.emit("add user", loginUserId);
        if (getIntent().hasExtra("userid")) {
            Log.e(TAG, "onCreate: " + getIntent().getStringExtra("userid"));
            Log.e(TAG, "onCreate: " + getIntent().getStringExtra("user_name"));
            Log.e(TAG, "onCreate: " + getIntent().getStringExtra("profile_pic"));
            Intent i = new Intent(Chat.this, Chatting.class);
            i.putExtra("user_id", getIntent().getStringExtra("userid"));
            i.putExtra("user_name", getIntent().getStringExtra("user_name"));
            i.putExtra("select_user_profile", getIntent().getStringExtra("profile_pic"));
            try {
                Thread.sleep(1000);
                startActivity(i);
                getIntent().getExtras().clear();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("login", onLogin);
        mSocket.off("new message", onNewMessage);
        mSocket.off("user joined", onUserJoined);
        mSocket.off("user left", onUserLeft);
        mSocket.off("delivery", onDelivery);
        mSocket.off("msg_read", onReadMessage);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.txt_find_near) {
            startActivity(new Intent(Chat.this, Club.class));
            finish();

        }
        if (id == R.id.usertab) {
            startActivity(new Intent(this, UserProfile.class));
            finish();
        }
        if (id == R.id.hometab) {
            startActivity(new Intent(this, Club.class));
            finish();
        }
        if (id == R.id.chattab) {
            startActivity(new Intent(this, Chat.class));
            finish();
        }

    }

    public void clickEvent() {
        event_near_label.setOnClickListener(this);

        userTab.setOnClickListener(this);
        homeTab.setOnClickListener(this);
        chatTab.setOnClickListener(this);
    }

    public void setFont() {
        bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold.ttf");
        extra_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        reg_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        semi_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBold.ttf");
        calibri_bold = Typeface.createFromAsset(getAssets(), "fonts/Calibri Bold.ttf");
        lato_bold = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");
        semi_bold_italic = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBoldItalic.ttf");

        swipe_label.setTypeface(semi_bold_face);
        event_near_label.setTypeface(semi_bold_italic);
        match_label.setTypeface(bold_face);
        chat_label.setTypeface(bold_face);
    }


    private void getListVolley(String url) {
        loading = new ProgressDialog(this, R.style.full_screen_dialog) {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.feed_loader);
                getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT,
                        LinearLayout.LayoutParams.FILL_PARENT);
            }
        };

        loading.setCancelable(false);
        loading.show();
        MyApplication.getInstance().getRequestQueue().getCache().remove(url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("get_match_response", response.toString());

                try {

                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {

                        JSONArray array = object.getJSONArray("message");
                        if (array.length() == 0) {
                            Log.d("chatdata", "log1");
                            emptyChat.setVisibility(View.VISIBLE);
                            list_main.setVisibility(View.GONE);
                            loading.dismiss();
                        } else {
                            Log.d("chatdata", "log2");
                            for (int j = 0; j < array.length(); j++) {
                                JSONObject object11 = array.getJSONObject(j);
                                emptyChat.setVisibility(View.GONE);
                                list_main.setVisibility(View.VISIBLE);

                                chatList = new ArrayList<ChatListModel>();
                                matchList = new ArrayList<MatchProfileModel>();

                                ///match array
                                JSONArray matcharray = object11.getJSONArray("match");
                                if (matcharray.length() == 0) {
                                    Log.d("chatdata", "log3");
                                    matchRel.setVisibility(View.GONE);
                                    // loading.dismiss();
                                } else {
                                    Log.d("chatdata", "log4");
                                    matchRel.setVisibility(View.VISIBLE);
                                    String match_count = String.valueOf(matcharray.length());
                                    match_label.setText("NEW MATCHES (" + match_count + ")");
                                    for (int i = 0; i < matcharray.length(); i++) {
                                        JSONObject object1 = matcharray.getJSONObject(i);
                                        MatchProfileModel matchModel = new MatchProfileModel();

                                        matchModel.match_id = object1.getString("match_id");
                                        matchModel.match_name = object1.getString("name");
                                        matchModel.match_profile = object1.getString("profile");
                                        matchModel.event = object1.getString("event");
                                        matchModel.matchCategory = object1.getString("category");


                                        matchList.add(matchModel);

                                    }//end of for
                                    matchAdapter = new MatchListAdapter(matchList, Chat.this);
                                    matchRecycler.setAdapter(matchAdapter);
                                }

                                //end of match array

                                //chat array
                                JSONArray chatarray = object11.getJSONArray("chat");

                                if (chatarray.length() == 0) {
                                    Log.d("chatdata", "log5");
                                    chatRel.setVisibility(View.GONE);
                                    //loading.dismiss();
                                } else {
                                    Log.d("chatdata", "log6");
                                    chatRel.setVisibility(View.VISIBLE);
                                    String chat_count = String.valueOf(chatarray.length());
                                    chat_label.setText("CHATS (" + chat_count + ")");
                                    for (int i = 0; i < chatarray.length(); i++) {
                                        JSONObject object2 = chatarray.getJSONObject(i);
                                        ChatListModel chatModel = new ChatListModel();


                                        chatModel.id = object2.getString("match_id");
                                        chatModel.name = object2.getString("name");
                                        chatModel.profile = object2.getString("profile");
                                        chatModel.occupation = object2.getString("occupation");
                                        chatModel.message = object2.getString("message");


                                        chatList.add(chatModel);

                                    }


                                    chatAdapter = new ChatListAdapter(chatList, Chat.this);
                                    chatRecycler.setAdapter(chatAdapter);
                                    // loading.dismiss();
                                    Log.d("chatdata", "log7");
                                }
                                Log.d("chatdata", "log8");
                                // loading.dismiss();
                            }
                            //  loading.dismiss();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    loading.dismiss();
                                }
                            }, 2000);

                        }
                    } else {
                        Toast.makeText(Chat.this, object.getString("message"), Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    /*Toast.makeText(Chat.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                loading.dismiss();

            }
        });

        // Adding request to request queue
        RequestQueue queue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
        queue.getCache().clear();
        jsonObjReq.setShouldCache(false);

        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();
                //loading.dismiss();

            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Chat.this, Club.class);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isConnected) {
                        if (null != loginUserId) {
                            mSocket.emit("add user", loginUserId);
//                                mSocket.emit("adduser", new JSONObject().put("username", sessionManager.getUserDetails().get(sessionManager.KEY_EmployeeCode)));
                        }
//                        Toast.makeText(getContext(), R.string.connect, Toast.LENGTH_LONG).show();
                        isConnected = true;
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "diconnected");
                    isConnected = false;
                    mSocket.disconnect();
//                    Toast.makeText(getActivity().getApplicationContext(), R.string.disconnect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");
//                    Toast.makeText(getActivity().getApplicationContext(), R.string.error_connect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "run: onNewMessage" + data);
                    try {

                        Date d = new Date(data.getLong("date"));
                        Calendar c = Calendar.getInstance();
                        c.setTime(d);
                        if (db.taksInterface().isExist(data.getString("id")).size() < 1)
                            db.taksInterface().insertAll(new ChatTable(data.getString("id"), data.getString("from"), loginUserId, data.getString("text"), String.valueOf(c.getTimeInMillis()), "person", "received", data.getString("sticker")));
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        return;
                    }
//                    RefreshList();
                }
            });
        }
    };

    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "run: onUserJoined" + data);
//                    addLog(getResources().getString(R.string.message_user_joined));
//                    addParticipantsLog(numUsers);
                }
            });
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "run: onUserLeft" + data);

//                    addLog(getResources().getString(R.string.message_user_left, username));
//                    addParticipantsLog(numUsers);
//                    removeTyping(sessionManager.getUserDetails().get(sessionManager.KEY_EmployeeCode));
                }
            });
        }
    };

    private Emitter.Listener onDelivery = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "onDelivery" + data);
                    try {
                        if (db.taksInterface().isExist(data.getString("id")).size() > 0) {

                            if (data.getBoolean("status")) {
                                db.taksInterface().updateStatusById(data.getString("id"), "delivered");
                            } else {
                                db.taksInterface().updateStatusById(data.getString("id"), "sent");
                            }
                        } else {
                            ChatTable table = new ChatTable(data.getString("id"), data.getString("from"), data.getString("to"), data.getString("text"), data.getString("date"), "person", "", data.getString("sticker"));
                            if (data.getBoolean("status")) {
                                table.setDelivery_status("delivered");
                            } else {
                                table.setDelivery_status("sent");
                            }
                            db.taksInterface().insertAll(table);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };

    private Emitter.Listener onReadMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "run: onReadMessage" + args[0]);
                    JSONObject data = (JSONObject) args[0];

                    try {
                        if (db.taksInterface().isExist(data.getString("id")).size() > 0) {
                            db.taksInterface().updateStatusById(data.getString("id"), "read");
                        } else {
                            ChatTable table = new ChatTable(data.getString("id"), data.getString("from"), data.getString("to"), data.getString("text"), data.getString("date"), "person", "read", data.getString("sticker"));
                            db.taksInterface().insertAll(table);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };


    private Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            Log.e(TAG, "call: onLogin" + data);
        }
    };

    private void RefreshList() {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AllUrl.CHAT_LIST + loginUserId, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("get_match_response", response.toString());

                try {

                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {

                        JSONArray array = object.getJSONArray("message");
                        if (array.length() == 0) {
                            Log.d("chatdata", "log1");
                            emptyChat.setVisibility(View.VISIBLE);
                            list_main.setVisibility(View.GONE);
                            loading.dismiss();
                        } else {
                            Log.d("chatdata", "log2");
                            for (int j = 0; j < array.length(); j++) {
                                JSONObject object11 = array.getJSONObject(j);
                                emptyChat.setVisibility(View.GONE);
                                list_main.setVisibility(View.VISIBLE);

                                chatList = new ArrayList<ChatListModel>();
                                matchList = new ArrayList<MatchProfileModel>();

                                ///match array
                                JSONArray matcharray = object11.getJSONArray("match");
                                if (matcharray.length() == 0) {
                                    Log.d("chatdata", "log3");
                                    matchRel.setVisibility(View.GONE);
                                    // loading.dismiss();
                                } else {
                                    Log.d("chatdata", "log4");
                                    matchRel.setVisibility(View.VISIBLE);
                                    String match_count = String.valueOf(matcharray.length());
                                    match_label.setText("NEW MATCHES (" + match_count + ")");
                                    for (int i = 0; i < matcharray.length(); i++) {
                                        JSONObject object1 = matcharray.getJSONObject(i);
                                        MatchProfileModel matchModel = new MatchProfileModel();

                                        matchModel.match_id = object1.getString("match_id");
                                        matchModel.match_name = object1.getString("name");
                                        matchModel.match_profile = object1.getString("profile");
                                        matchModel.event = object1.getString("event");
                                        matchModel.matchCategory = object1.getString("category");


                                        matchList.add(matchModel);

                                    }//end of for
                                    matchAdapter = new MatchListAdapter(matchList, Chat.this);
                                    matchRecycler.setAdapter(matchAdapter);
                                }

                                //end of match array

                                //chat array
                                JSONArray chatarray = object11.getJSONArray("chat");

                                if (chatarray.length() == 0) {
                                    Log.d("chatdata", "log5");
                                    chatRel.setVisibility(View.GONE);
                                    //loading.dismiss();
                                } else {
                                    Log.d("chatdata", "log6");
                                    chatRel.setVisibility(View.VISIBLE);
                                    String chat_count = String.valueOf(chatarray.length());
                                    chat_label.setText("CHATS (" + chat_count + ")");
                                    for (int i = 0; i < chatarray.length(); i++) {
                                        JSONObject object2 = chatarray.getJSONObject(i);
                                        ChatListModel chatModel = new ChatListModel();


                                        chatModel.id = object2.getString("match_id");
                                        chatModel.name = object2.getString("name");
                                        chatModel.profile = object2.getString("profile");
                                        chatModel.occupation = object2.getString("occupation");
                                        chatModel.message = object2.getString("message");


                                        chatList.add(chatModel);

                                    }


                                    chatAdapter = new ChatListAdapter(chatList, Chat.this);
                                    chatRecycler.setAdapter(chatAdapter);
                                    // loading.dismiss();
                                    Log.d("chatdata", "log7");
                                }
                                Log.d("chatdata", "log8");
                                // loading.dismiss();
                            }

                        }
                    } else {
                        Toast.makeText(Chat.this, object.getString("message"), Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    /*Toast.makeText(Chat.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                loading.dismiss();

            }
        });

        // Adding request to request queue

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }
}
