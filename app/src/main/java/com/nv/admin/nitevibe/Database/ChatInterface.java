package com.nv.admin.nitevibe.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.nv.admin.nitevibe.Database.ChatTable;

import java.util.List;

@Dao
public interface ChatInterface {
    @Query("SELECT * FROM UserChats")
    List<ChatTable> getTasksList();

    @Query("SELECT * FROM UserChats where (`from`In (:user)and `to`in(:emmcode)) or(`from`in(:emmcode) and `to`in(:user))")
    List<ChatTable> getChatForUser(String user, String emmcode);

    @Query("SELECT * FROM UserChats where ((`from`In (:user)and `to`in(:emmcode)) or(`from`in(:emmcode)) and `to`in(:user))and delivery_status='received'")
    List<ChatTable> getUnReadChatForUser(String user, String emmcode);

    @Query("SELECT * FROM UserChats where  `to` = :emmcode and `delivery_status` ='received' and msg_type = 'group'")
    List<ChatTable> getUnReadChatForGroup(String emmcode);

    @Query("SELECT * FROM UserChats where id in(:id)")
    List<ChatTable> getChatForId(String id);

    @Query("select u.*,groupdata.type from (SELECT max(date ) as maxid,CASE WHEN `to` = :emmcode THEN `to`||`from` ELSE  `from`||`to` END as  type FROM\n" +
            " UserChats where (`to`=:emmcode or `from`=:emmcode ) and msg_type ='person'  group by `to`,`from`) groupdata inner join UserChats u on groupdata.maxid=u.date group by groupdata.type order by  date desc")
    List<ChatTable> getRecentChatUser(String emmcode);

    @Query("select u.*,groupdata.type from (SELECT max(date ) as maxid,CASE WHEN `to` = :emmcode THEN `to`||`from` ELSE  `from`||`to` END as  type FROM\n" +
            " UserChats where (`to`=:emmcode or `from`=:emmcode ) and msg_type ='group'  group by `to`,`from`) groupdata inner join UserChats u on groupdata.maxid=u.date group by groupdata.type order by  id desc")
    List<ChatTable> getRecentChatGroup(String emmcode);

    @Query("SELECT * FROM UserChats where (`to`In (:group))and msg_type ='group'")
    List<ChatTable> getChatForGroup(String group);

/*    @Query("select  empCode.*, UserChats.uid,UserChats.id,UserChats .`to`,UserChats.text,UserChats .date ,UserChats .delivery_status ,UserChats .msg_type , empCode.name as `from`   from UserChats INNER JOIN  empCode on UserChats.`from` = empCode.code and  (`to`In (:group))and msg_type ='group'")
    List<ChatTable> getChatForGroupWithName(String group);*/

    @Insert
    void insertAll(ChatTable... tasks);
    @Insert
    void insertAll(List<ChatTable>list);
    @Delete
    void removeAll(List<ChatTable>list);

    @Query("DELETE FROM UserChats where (`from`In (:user)and `to`in(:emmcode)) or(`from`in(:emmcode) and `to`in(:user))")
    void removeAll(String user, String emmcode);

    @Query("SELECT delivery_status FROM UserChats where id in (:id)")
    String getStatusById(String id);

    @Query("update UserChats set delivery_status = :status where id in(:id)")
    long updateStatusById(String id, String status);

    @Query("update UserChats set delivery_status = 'read' where id in(:ids)")
    long updateReadStatusById(String ids);

    @Query("SELECT * FROM UserChats where (`id`In (:id))")
    List<ChatTable> isExist(String id);
}
