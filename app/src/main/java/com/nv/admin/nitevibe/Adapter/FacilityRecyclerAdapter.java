package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Admin on 3/10/2018.
 */
public class FacilityRecyclerAdapter extends RecyclerView.Adapter<FacilityRecyclerAdapter.MyView> {
    private List<FacilityModel> arrayList;
    private Context context;
    FacilityModel current;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;

    public FacilityRecyclerAdapter(List<FacilityModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public FacilityRecyclerAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.facility_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FacilityRecyclerAdapter.MyView holder, int position) {
        current=arrayList.get(position);
        semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
          holder.name.setTypeface(semi_bold_face);
        holder.name.setText(current.facilityName);

        if(!current.facilityImage.equals("")){
            Picasso.with(context.getApplicationContext())
                    .load(current.facilityImage)
                    .into(holder.img);
        }

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public ImageView img;
        public TextView name;
        public MyView(View itemView) {
            super(itemView);
            img= itemView.findViewById(R.id.facility_img);
            name= itemView.findViewById(R.id.txt_fac_name);
        }
    }
}
