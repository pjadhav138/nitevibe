package com.nv.admin.nitevibe.BackgroundService;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Activity.AllUrl;
import com.nv.admin.nitevibe.Activity.GPSTracker;
import com.nv.admin.nitevibe.Activity.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Admin on 5/17/2018.
 * not been used
 */
public class ServiceNoDelay extends Service {
    public int counter = 0;
    Context context;
    public static String url = "http://www.nitevibe.in/webservices/getLocation.php?";
    public static String TAG = "Background";

    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    public double str_lat = 0.0, str_lng = 0.0;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId, loginUserMode, loginUserType, str_inside = "", str_outside = "", str_club_id = "";
    ////////new
    boolean isGPSEnable = false;
    boolean isNetworkEnable = false;
    double latitude, longitude;
    LocationManager locationManager;
    Location location;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    long notify_interval = 1000;
    public static String str_receiver = "servicetutorial.service.receiver";
    Intent intent;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    GPSTracker gps;




    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);

            str_lat = location.getLatitude();
            str_lng = location.getLongitude();


        }




        @Override
        public void onProviderDisabled(String provider)
        {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider)
        {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }
    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };



    public ServiceNoDelay(Context applicationContext) {
        super();
        context = applicationContext;
        Log.i("HERE", "here service created!");
    }

    public ServiceNoDelay() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service_class","onStartCommand is called");
        super.onStartCommand(intent, flags, startId);
        startTimer();

        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }


        return START_STICKY;
       // return  START_NOT_STICKY;
    }

  /*  @Override
    public void onCreate()
    {
        Log.d("Service_class","onCreate is called");
        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");



        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }


    }
*/

    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    //////


    /////////


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Service_class","onDestroy is called");
        Intent broadcastIntent = new Intent("ac.in.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);
        stoptimertask();

        }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.d("Service_class","onTaskRemoved is called");
        sendBroadcast(new Intent("ac.in.ActivityRecognition.RestartSensor"));
       // Intent intent = new Intent("ac.in.ActivityRecognition.RestartSensor");
        //sendBroadcast(intent);

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

    private Timer timer;
    private TimerTask timerTask;

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        //timer.schedule(timerTask, 1000, 1000);
        //schedule the timer, to wake up every 2 second
        timer.schedule(timerTask, 1000, 5000);
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  " + (counter++));
                boolean flag=hasConnection();
                if(flag){
                    String url= AllUrl.BACKGROUND_SERVICE+loginUserId;
                    //eventDetails(url);
                }
                else{

                    //Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                }

                //drinkList(url);
            }
        };
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void checkinNotification(String url){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("checkin_notify_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        Log.d("notify_msg","sucess");

                    }else{
                       // Toast.makeText(ServiceNoDelay.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                  //  Toast.makeText(ServiceNoDelay.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
               // Toast.makeText(ServiceNoDelay.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog


            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

  /*  private void getCurrentLocation() {

        // Check if GPS enabled
        gps = new GPSTracker(this);
        if(gps.canGetLocation()) {

         //   str_lat = gps.getLatitude();
           // str_lng = gps.getLongitude();
          //  Log.e("Location","Your Location is - \nLat: " + str_lat + "\nLong: " + str_lng);

            // Toast.makeText(getActivity(), "Your Location is - \nLat: " + gpslatitude + "\nLong: " + gpslongitude, Toast.LENGTH_LONG).show();

        }
    }*/


    private void eventDetails(String url){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("event_details_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        final LocationManager service = (LocationManager)getBaseContext().getSystemService(getBaseContext().LOCATION_SERVICE);
                        final boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
                        if (!enabled) {

                        } else {
                           // gps = new GPSTracker(getBaseContext());
                           // getCurrentLocation();
                            //new DisplayLocation().execute();
                        }
                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){

                        }
                        else{
                            for (int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);

                                String club_id=object1.getString("club_id");
                                String club_name=object1.getString("club_name");
                                String lat1=object1.getString("lat");
                                String lng1=object1.getString("lng");
                                String radius1=object1.getString("radius");
                                JSONArray eventArray=object1.getJSONArray("event");
                                Log.d("club_radius",radius1);

                                double lat= Double.parseDouble(lat1);
                                double lng= Double.parseDouble(lng1);

                                if(eventArray.length()==0){

                                }
                                else{
                                    for (int j=0;j<eventArray.length();j++){
                                        JSONObject object2 = eventArray.getJSONObject(j);

                                        String event_id=object2.getString("event_id");
                                        String event_name=object2.getString("event_name");
                                        String event_coupon=object2.getString("event_coupon");
                                        String checkin=object2.getString("checkin");

                                        Log.d("service_location",str_lat+" "+str_lng);

                                        //str_lat,str_lng
                                        if(!radius1.equals("")){
                                            if(str_lat!=0.0 && str_lng!=0.0){
                                                float[] results = new float[1];
                                                Location.distanceBetween(lat,lng,str_lat,str_lng, results);
                                                float distanceInMeters = results[0];
                                                int radius= Integer.parseInt(radius1);
                                                boolean isWithin1km = distanceInMeters < radius;
                                                if(isWithin1km){
                                                    // Toast.makeText(getBaseContext(), "Inside", Toast.LENGTH_LONG).show();
                                                    str_inside="Inside";
                                                    str_club_id=club_id;
                                                }
                                                else{
                                                    // Toast.makeText(getBaseContext(), "Outside", Toast.LENGTH_LONG).show();
                                                    str_outside="Outside";
                                                    str_club_id=club_id;
                                                }
                                                Log.d(TAG,"Getting lat and lng"+str_inside);
                                                if(str_inside.equals("Inside") && str_outside.equals("")){
                                                    boolean flag=hasConnection();
                                                    if(checkin.equals("0")){
                                                        if(flag){

                                                            String url=AllUrl.CHECKIN_NOTIFY+loginUserId+"&club_id="+str_club_id;
                                                            checkinNotification(url);
                                                        }
                                                        else{
                                                            Log.d(TAG,"No internet connection");
                                                        }
                                                    }
                                                    else if(checkin.equals("1") && str_outside.equals("Outside")){
                                                        Log.d(TAG,"User has already checkin");

                                                    }


                                                }
                                                else if(checkin.equals("1") && str_outside.equals("Outside") && str_inside.equals("Inside") ){
                                                    boolean flag=hasConnection();
                                                    if(checkin.equals("1")){
                                                        if(flag){
                                                            String url2=AllUrl.CHECK_OUT+loginUserId+"&event_id="+event_id;
                                                            userCheckOutVolley(url2);
                                                        }
                                                        else{
                                                            Log.d(TAG,"No internet connection");
                                                        }
                                                    }
                                                    else{
                                                        Log.d(TAG,"User has not yet checkout");
                                                    }

                                                }



                                            }
                                        }


                                    }
                                }
                            }
                        }

                    }else{
                      //  Toast.makeText(ServiceNoDelay.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog


            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


   private void userCheckOutVolley(String url){
       JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
               url, null, new Response.Listener<JSONObject>() {

           @Override
           public void onResponse(JSONObject response) {
               try {
                   JSONObject object=new JSONObject(String.valueOf(response));
                   String res=object.getString("response");
                   String message=object.getString("message");

                   if(res.equals("200")){
                       Log.d(TAG,"user has check_out");

                   }
                   else{
                      // Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                   }
               } catch (JSONException e) {
                   e.printStackTrace();
               }

           }
       }, new Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error) {


           }
       });
       jsonObjReq.setShouldCache(false);
       MyApplication.getInstance().addToRequestQueue(jsonObjReq);
   }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }


}
