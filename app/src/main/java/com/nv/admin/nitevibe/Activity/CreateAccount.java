package com.nv.admin.nitevibe.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateAccount extends AppCompatActivity implements View.OnClickListener {
    public static ImageView back;
    public static EditText ed_username,ed_password,ed_repassword;
    public static Button create;
    public static TextView txt_title,txt_detail,txt_user,txt_password,txt_repassword,txt_time,txt_resend,txt_sendotp,txt_timer;
    public static PinEntryEditText ed_otp;
    public static String str_username,str_password,str_rePassword,str_otp,otp_btn_click="0";
    public static RelativeLayout otpRel;
    long millisInFuture = 60000;
    long countDownInterval = 1000;
    public static final String PREFS_NAME = "LoginPrefs";
    SharedPreferences sharedpreferences;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    AlertDialog alert;
    CountDownTimer waitTimer;
    ProgressDialog loading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        sharedpreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        back= findViewById(R.id.back);
        txt_title= findViewById(R.id.txt); //lable
        txt_detail= findViewById(R.id.txt1); //label2
        txt_user= findViewById(R.id.usertxt);
        ed_username= findViewById(R.id.username);
        txt_sendotp= findViewById(R.id.send_otp);
        ed_otp= findViewById(R.id.otp);
        txt_resend= findViewById(R.id.resend);
        txt_timer= findViewById(R.id.timer_title);
        txt_time= findViewById(R.id.time);
        txt_password= findViewById(R.id.passwordtxt);
        ed_password= findViewById(R.id.password);
        txt_repassword= findViewById(R.id.repasswordtxt);
        ed_repassword= findViewById(R.id.repassword);
        create= findViewById(R.id.account);
        otpRel= findViewById(R.id.opt_rel);



        onClickEvent();
        setFont();
    }



    public void onClickEvent(){
        create.setText("Create an account");
        back.setOnClickListener(this);
        create.setOnClickListener(this);
        txt_resend.setOnClickListener(this);
        txt_sendotp.setOnClickListener(this);
        ed_username.addTextChangedListener(new MyTextWatcher(ed_username));
        ed_password.addTextChangedListener(new MyTextWatcher(ed_password));
        ed_repassword.addTextChangedListener(new MyTextWatcher(ed_repassword));
        otp_edittext_validation();

    }


    public void otp_edittext_validation(){
        if(ed_otp != null){
            ed_otp.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {


                    Log.d("timer",txt_time.getText().toString());
                    String time_left=txt_time.getText().toString();
                    if(time_left.equals("00:00 seconds")){
                        Toast.makeText(getApplicationContext(),"Time ups! Please resend the otp ",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Log.d("value_of_otp",str.toString());
                        //start of if
                        //str_otp="123456";
                        if(str.length()==6 && str.toString().equals(str_otp) ){
                            Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();
                              waitTimer.cancel();

                        }
                        else{
                            ed_otp.setError(true);
                            Toast.makeText(getApplicationContext(),"Invalid Otp",Toast.LENGTH_SHORT).show();

                            ed_otp.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ed_otp.setText(null);
                                }
                            },1000);
                        }

                        //end of else
                    }

                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        if(id==R.id.back){
            startActivity(new Intent(this,Login.class));
        }
        if(id==R.id.account){

            createUser();
        }
        if(id==R.id.send_otp){
            otp_btn_click="1";
            str_username=ed_username.getText().toString();
            if(!str_username.equals("")){
                if(str_username.matches("[0-9]+")){
                    validateMobile();
                }
                else{
                    validateEmail();
                }
            }
            else{
                Toast.makeText(getApplicationContext(),"Please enter the username",Toast.LENGTH_SHORT).show();
            }

        }
        if(id==R.id.resend){
            waitTimer.cancel();

            String url=AllUrl.OTP+ed_username.getText().toString();
            Log.d("resend_otp",url);
            boolean flag=hasConnection();
            if(flag){
               // userOtp(url);
                userOtpVolley(url);

            }
            else{
                Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
            }


        }

    }


    public void createUser(){
        String username=ed_username.getText().toString();
        String password1=ed_password.getText().toString();
        String password2=ed_repassword.getText().toString();
        String otp=ed_otp.getText().toString();

        if(!username.equals("")){
            if(!password1.equals("")){
                if(!password2.equals("")){
                    if(password1.equals(password2)) {
                      //  if (!otp_btn_click.equals("1")) {

                        if (!otp.equals("")) {


                            boolean flag = hasConnection();
                            if (flag) {
                                // postRegisterUser(username,password1,"Normal","","","","");
                                postRegisterUserVolley(username, password1, "Normal", "", "", "", "");
                            } else {
                                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                            }
                        }



                        else{
                            Toast.makeText(getApplicationContext(),"Please enter otp",Toast.LENGTH_SHORT).show();
                        }
                       /* }
                        else{
                            Toast.makeText(getApplicationContext(),"Please Send otp for validation",Toast.LENGTH_SHORT).show();
                        }*/

                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Password doesnot match",Toast.LENGTH_SHORT).show();
                    }

                }
                else{
                    Toast.makeText(getApplicationContext(),"Please re enter the password",Toast.LENGTH_SHORT).show();
                }

            }
            else{
                Toast.makeText(getApplicationContext(),"Please enter password",Toast.LENGTH_SHORT).show();
            }

        }
        else{
            Toast.makeText(getApplicationContext(),"Please enter username",Toast.LENGTH_SHORT).show();
        }




    }



    private class MyTextWatcher implements TextWatcher {
        private View view;


        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Log.d("click_on_edit_text","beforeTextChanged");

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Log.d("click_on_edit_text","onTextChanged");

        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()){
                case R.id.username:
                    txt_user.setVisibility(View.VISIBLE);
                    txt_user.setTextColor(getResources().getColor(R.color.light_green));

                    break;
                case R.id.password:
                    txt_password.setVisibility(View.VISIBLE);
                    txt_password.setTextColor(getResources().getColor(R.color.light_green));
                    txt_user.setTextColor(getResources().getColor(R.color.light_blue));

                    break;
                case R.id.repassword:
                    txt_repassword.setVisibility(View.VISIBLE);
                    txt_repassword.setTextColor(getResources().getColor(R.color.light_green));
                    txt_password.setTextColor(getResources().getColor(R.color.light_blue));

                    break;




            }

        }
    }

    public static boolean isValidPhone(String phone)
    {
        String expression = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
        CharSequence inputString = phone;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        return matcher.matches();
    }

    private boolean validateEmail(){
        String email_id=ed_username.getText().toString().trim();

        if(isValidEmail(email_id)==false){
            Toast.makeText(getApplicationContext(),"please enter a valid email id",Toast.LENGTH_SHORT).show();

            return false;
        }
        else{
            String url=AllUrl.OTP+email_id;
            Log.d("email_otp",url);
            boolean flag=hasConnection();
            if(flag){
              //  userOtp(url);
                userOtpVolley(url);

            }
            else{
                Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
            }

        }
        return true;
    }

    private boolean validateMobile(){
        String phone_number=ed_username.getText().toString().trim();
        Log.d("length", String.valueOf(phone_number.length()));
        isValidPhone(phone_number);
        if(phone_number.length()>=10){
            if(isValidPhone(phone_number)==false){
                Toast.makeText(getApplicationContext(),"Please enter valid phone number",Toast.LENGTH_SHORT).show();
                return false;
            }
            else{
                String url=AllUrl.OTP+phone_number;
                Log.d("mobile_otp",url);
                boolean flag=hasConnection();
                if(flag){
                 //userOtp(url);
                    userOtpVolley(url);

                }
                else{
                    Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        }else{
            Toast.makeText(getApplicationContext(),"Please enter valid phone number",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private static boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email)&& Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    private void userOtpVolley(String url){

        loading= new ProgressDialog(CreateAccount.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("otp_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        str_otp=msg;
                        startCountDown();
                        otpRel.setVisibility(View.VISIBLE);
                        txt_sendotp.setVisibility(View.GONE);
                        MyApplication.getInstance().trackEvent("Create Account", "Send OTP", "Success");

                    }else{
                        Toast.makeText(CreateAccount.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);
                    // Print Error!
                    Log.d("otp_error",jsonError);
                }

                // hide the progress dialog
                loading.dismiss();

            }
        });

        jsonObjReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        //jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }







    //otp old
    //change the password
/*
    private void userOtpVolley(String url){
        loading= new ProgressDialog(CreateAccount.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("otp_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        str_otp=msg;
                        startCountDown();
                        otpRel.setVisibility(View.VISIBLE);
                        txt_sendotp.setVisibility(View.GONE);
                        MyApplication.getInstance().trackEvent("Create Account", "Send OTP", "Success");
                    }else{
                        Toast.makeText(CreateAccount.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }
*/

    private void postRegisterUserVolley(final String username, final String password, final String mode, final String provider, final String photo, final String name, final String gender){
        StringRequest postRequest = new StringRequest(Request.Method.POST,AllUrl.REGISTRATION,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("register_response", response);
                        loading.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String res= jsonObject.getString("response");
                            String message = jsonObject.getString("message");
                            if (res.equals("200")) {
                                String user_id=message;
                                Log.d("user_id_of_registartion",user_id);
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(CreateAccount.this);
                                builder1.setMessage("You have successfully registered!");
                                builder1.setCancelable(false);
                                builder1.setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                startActivity(new Intent(CreateAccount.this,Login.class));
                                                SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                                                editor.remove("newuser");
                                                editor.commit();
                                                editor.putString("newuser", username);
                                                editor.commit();

                                                MyApplication.getInstance().trackEvent("Create Account", "Create Account", "Success");
                                            }
                                        });
                                alert = builder1.create();
                                alert.show();
                                Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                                positive.setTextColor(getResources().getColor(R.color.female));

                            }
                            else{
                                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        loading.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {

                Map<String, String>  data = new HashMap<>();
                // the POST parameters:

                data.put("username",username);
                data.put("password",password);
                data.put("mode",mode);
                data.put("provider",provider);
                data.put("photo",photo);
                data.put("name",name);
                data.put("gender",gender);

                return data;
            }

        };

        Volley.newRequestQueue(this).getCache().clear();
        Volley.newRequestQueue(this).add(postRequest);

        loading= new ProgressDialog(CreateAccount.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();

    }


    public void startCountDown(){
        waitTimer= new CountDownTimer(millisInFuture,countDownInterval){
            public void onTick(long millisUntilFinished){
                //do something in every tick
                //Display the remaining seconds to app interface
                //1 second = 1000 milliseconds
                txt_time.setText("Seconds remaining : " + millisUntilFinished / 1000);
            }
            public void onFinish(){
                //Do something when count down finished
                txt_time.setText("00:00 seconds");
            }
        }.start();
    }

    public void setFont(){
        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        txt_title.setTypeface(bold_face);
        txt_detail.setTypeface(reg_face);
        txt_user.setTypeface(lato_bold);
        ed_username.setTypeface(semi_bold_face);
        txt_sendotp.setTypeface(semi_bold_italic);
        ed_otp.setTypeface(semi_bold_face);
        txt_resend.setTypeface(semi_bold_italic);
        txt_timer.setTypeface(semi_bold_face);
        txt_time.setTypeface(semi_bold_face);
        txt_password.setTypeface(lato_bold);
        ed_password.setTypeface(semi_bold_face);
        txt_repassword.setTypeface(lato_bold);
        ed_repassword.setTypeface(semi_bold_face);
        create.setTypeface(bold_face);

    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }



}

