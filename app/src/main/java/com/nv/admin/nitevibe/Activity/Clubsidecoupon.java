package com.nv.admin.nitevibe.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.nv.admin.nitevibe.Adapter.FragmentAdapter;
import com.nv.admin.nitevibe.R;

public class Clubsidecoupon extends AppCompatActivity {
    TabLayout tabLayout ;
    ViewPager viewPager ;
    FragmentAdapter fragmentAdapter;
    public static int selected_tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clubsidecoupon);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        tabLayout = findViewById(R.id.tab_layout1);
        viewPager = findViewById(R.id.pager1);

        tabLayout.addTab(tabLayout.newTab().setText("Coupon Entry"));
        tabLayout.addTab(tabLayout.newTab().setText("Without Coupon Entry"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        fragmentAdapter = new FragmentAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(fragmentAdapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab LayoutTab) {

                viewPager.setCurrentItem(LayoutTab.getPosition());
                selected_tab=LayoutTab.getPosition();

                switch (LayoutTab.getPosition()){
                    case 0:


                        break;
                    case 1:

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab LayoutTab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab LayoutTab) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Clubsidecoupon.this,ClubSideHome.class));
    }
}
