package com.nv.admin.nitevibe.Database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "UserChats")
public class ChatTable {
    @PrimaryKey(autoGenerate = true)
    private int uid;
    @ColumnInfo(name = "id")
    private String id;
    @ColumnInfo(name = "from")
    private String from;
    @ColumnInfo(name = "to")
    private String to;
    @ColumnInfo(name = "text")
    private String text;
    @ColumnInfo(name = "date")
    private String date;
    @ColumnInfo(name = "delivery_status")
    private String delivery_status;
    @ColumnInfo(name = "msg_type")
    private String msg_type;
    @ColumnInfo(name = "type")
    private String type="";
    @ColumnInfo(name = "img")
    private String image;
    public ChatTable(String id, String from, String to, String text, String date, String msg_type,String delivery_status,String image) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.text = text;
        this.date = date;
        this.msg_type = msg_type;
        this.delivery_status= delivery_status;
        this.image= image;
    }


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDelivery_status() {
        return delivery_status;
    }

    public void setDelivery_status(String delivery_status) {
        this.delivery_status = delivery_status;
    }

    public String getMsg_type() {
        return msg_type;
    }

    public void setMsg_type(String msg_type) {
        this.msg_type = msg_type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
