package com.nv.admin.nitevibe.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Adapter.BannerAdapter;
import com.nv.admin.nitevibe.Adapter.BannerModel;
import com.nv.admin.nitevibe.Adapter.ClubModel;
import com.nv.admin.nitevibe.Adapter.ClubRecyclerAdapter;
import com.nv.admin.nitevibe.BackgroundService.SensorService;
import com.nv.admin.nitevibe.Database.AppDatabse;
import com.nv.admin.nitevibe.Database.ChatTable;
import com.nv.admin.nitevibe.Database.DatabaseHandler;
import com.nv.admin.nitevibe.Database.SettingModel;
import com.nv.admin.nitevibe.OverlayTutorial.TutoShowcase;
import com.nv.admin.nitevibe.R;
import com.nv.admin.nitevibe.Retrofit.ApiClient;
import com.nv.admin.nitevibe.Retrofit.ApiInterface;
import com.nv.admin.nitevibe.custom.SessionManager;
import com.nv.admin.nitevibe.notification.Config;
import com.nv.admin.nitevibe.notification.NotificationUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.nv.admin.nitevibe.Activity.AllUrl.GET_NOTIFICATION_STATUS;

public class Club extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    //public static TextView  txt_location;
    public static RecyclerView clubRecycler;
    public ClubRecyclerAdapter clubAdapter;
    public static Spinner spin_location;
    public static CircleIndicator indicator;
    AlertDialog alert;
    List<ClubModel> data;
    boolean doubleBackToExitPressedOnce = false;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    //drop-down
    public static String locationUrl, clubListUrl;
    ArrayList locarrayList;
    public static String selectedLocationName;

    //for banner slider

    private TextView[] dots;
    int page_position = 0;
    public ViewPager banner_slider;
    private LinearLayout lin_dots;
    BannerAdapter bannerAdapter;
    ArrayList<BannerModel> bannerList;
    //    ArrayList<BannerModel> bannerData;
    ArrayList<String> bannerData;
    ArrayList<String> bannerEventData;
    ArrayList<String> bannerClubData;
    public static RelativeLayout bannerRel;
    public static String loginUserId, loginUserMode, loginUserType;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    ProgressDialog loading;
    public static ImageView userTab, homeTab, chatTab;
    public static TextView search_txt;
    public static Typeface semi_bold_face;
    //private  ServiceNoDelay mSensorService;
    private SensorService mSensorService;
    Intent mServiceIntent;
    Context ctx;
    private String TAG = getClass().getSimpleName();
    private final int LOCATION_REQUEST = 122;
    DatabaseHandler handler;
    SessionManager sessionManager;
    ApiInterface apiService;
    AppDatabse db;

    public Context getCtx() {
        return ctx;
    }

    //
    private boolean gps_enabled = false;
    private boolean network_enabled = false;
    Location location;
    public GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        setContentView(R.layout.activity_club_details);
        handler = new DatabaseHandler(this);
        db = Room.databaseBuilder(Club.this, AppDatabse.class, "UserChats").allowMainThreadQueries().build();
        sessionManager = new SessionManager(this);
        /* if (!sessionManager.isStatusSaved())*/
        {
            getStatus();
        }
        apiService = ApiClient.getClient().create(ApiInterface.class);

        sharedpreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");

        if (db.taksInterface().getTasksList().size() < 1) {
            Map map = new HashMap<String, String>();
            map.put("id", loginUserId);
            Call<ApiInterface.ChatListResponse> call = apiService.getAllMessage(map);
            call.enqueue(new Callback<ApiInterface.ChatListResponse>() {
                @Override
                public void onResponse(Call<ApiInterface.ChatListResponse> call, retrofit2.Response<ApiInterface.ChatListResponse> response) {
                    Log.e(TAG, "onResponse: " + response.body());
                    if (response.body().getResponse() == 200) {
                        List<ChatTable> chatTables = new ArrayList<>();
                        for (ApiInterface.ChatListResponse.Chat chat : response.body().getChats()) {
                            ChatTable chatTable = new ChatTable(chat.getChat_id(), chat.getMe(), chat.getYou(), chat.getMessage(), chat.getEntry(), "person", chat.getSent(), chat.getSticker());
                            chatTables.add(chatTable);
                        }
                        db.taksInterface().insertAll(chatTables);
                    }
                }

                @Override
                public void onFailure(Call<ApiInterface.ChatListResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure" + t.getMessage());
                }
            });
        }

        String clubOverlay = sharedpreferences.getString("cluboverlay", "");
        if (clubOverlay.equals("")) {
            displayTuto();
        }

        semi_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBold.ttf");

        Log.d("login_user_details", loginUserId + " " + loginUserMode);


        final LocationManager service = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        final boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage(this, 34992, this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationChecker(mGoogleApiClient, Club.this);

        if (enabled) {
            Log.d("gpsconnection", "high accuracy");
            mSensorService = new SensorService(getCtx());
            mServiceIntent = new Intent(getCtx(), mSensorService.getClass());
            if (!isMyServiceRunning(mSensorService.getClass())) {
                startService(mServiceIntent);
            }
        }



      /* final boolean gpsCheck=isLocationEnabled(Club.this);
        if (!enabled) {
        //if (!gpsCheck) {

            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("GPS is not enable. Please enable it.");
            builder1.setCancelable(false);
            builder1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int id) {
                           // startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),0);
                            //createLocationRequest();
                            //turnGPSOff();
                           // turnGPSOn();


                        }
                    });
            builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                   // finish();
                    dialog.dismiss();
                }
            });
            alert = builder1.create();
            alert.show();
            Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
            positive.setTextColor(getResources().getColor(R.color.female));
            Button negative=alert.getButton(AlertDialog.BUTTON_NEGATIVE);
            negative.setTextColor(getResources().getColor(R.color.female));
        } else {
            *//*mSensorService = new ServiceNoDelay(getApplicationContext());
             mServiceIntent = new Intent(getApplicationContext(), mSensorService.getClass());
            if (!isMyServiceRunning(mSensorService.getClass())) {
                startService(mServiceIntent);
            }*//*
            mSensorService = new SensorService(getCtx());
            mServiceIntent = new Intent(getCtx(), mSensorService.getClass());
            if (!isMyServiceRunning(mSensorService.getClass())) {
                startService(mServiceIntent);
            }
        }


*/


        bannerRel = findViewById(R.id.banner_rel);
        spin_location = findViewById(R.id.location);
        clubRecycler = findViewById(R.id.clubrecycler);
        indicator = findViewById(R.id.indicator);

        userTab = findViewById(R.id.usertab);
        homeTab = findViewById(R.id.hometab);
        chatTab = findViewById(R.id.chattab);

        search_txt = findViewById(R.id.location_txt);
        search_txt.setTypeface(semi_bold_face);


        homeTab.setImageDrawable(getResources().getDrawable(R.drawable.select_home));
        clickEvent();

        clubRecycler.setHasFixedSize(true);
        clubRecycler.setLayoutManager(new LinearLayoutManager(this));//Linear Items
        ScrollView scroll = findViewById(R.id.scrol);

        locationUrl = AllUrl.LOCATION;
        Log.d("location_list_url", locationUrl);

        boolean flag = hasConnection();
        if (flag) {

            getLocationVolley(locationUrl);
        } else {
            Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "onCreate: all permission granted.");
            } else {
                requestPermissions(new String[]{ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
            }
        }

    }

    private void getStatus() {
        final Map<String, String> map = new HashMap<>();
        map.put("id", getSharedPreferences(PREFS_NAME, MODE_PRIVATE).getString("user_id", ""));
        Log.e(TAG, "getStatus: " + getSharedPreferences(PREFS_NAME, MODE_PRIVATE).getString("user_id", ""));
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, GET_NOTIFICATION_STATUS, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("get_location_response", response.toString());
                try {
                    if (response.getString("status").equalsIgnoreCase("active")) {
                        handler.updateUser(new SettingModel("mute", "true"));
                    } else {
                        handler.updateUser(new SettingModel("mute", "false"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                Log.e(TAG, "onErrorResponse: " + error.getMessage());

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        RequestQueue queue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
        queue.getCache().clear();
        jsonObjReq.setShouldCache(false);

        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();

            }
        });

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    protected void createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void displayTuto() {
        TutoShowcase.from(this)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        // Toast.makeText(Club.this, "Tutorial dismissed", Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("cluboverlay", "1");
                        editor.commit();

                      /*  String profileOverlay=sharedpreferences.getString("profileoverlay","");
                        if(profileOverlay.equals("")){
                            displayTutoProfile();
                        }*/

                    }
                })
                .setContentView(R.layout.feed_overlay)
                .setFitsSystemWindows(true)
                .on(R.id.location_txt)
                .displaySwipableLeft()
                .delayed(399)
                .animated(true)
                .show();
    }

    protected void displayTutoProfile() {
        TutoShowcase.from(this)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        // Toast.makeText(Club.this, "Tutorial dismissed", Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("profileoverlay", "1");
                        editor.commit();
                    }
                })
                .setContentView(R.layout.profile_overlay)
                .setFitsSystemWindows(true)
                .on(R.id.usertab)
                .addCircle()

                .onClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("profileoverlay", "1");
                        editor.commit();
                    }
                })
                .on(R.id.usertab)
                .displaySwipableLeft()
                .delayed(399)
                .animated(true)
                .show();
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    public void clickEvent() {
        userTab.setOnClickListener(this);
        homeTab.setOnClickListener(this);
        chatTab.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.usertab) {
            displayFirebaseRegId();
            startActivity(new Intent(this, UserProfile.class));
            finish();
            MyApplication.getInstance().trackEvent("Profile", "User Profile", "Success");
        }
        if (id == R.id.hometab) {
            startActivity(new Intent(this, Club.class));
            finish();
            MyApplication.getInstance().trackEvent("Club", "Club", "Success");
        }
        if (id == R.id.chattab) {

            startActivity(new Intent(this, Chat.class));
            finish();
            MyApplication.getInstance().trackEvent("Chat", "Chat", "Success");
        }

    }


    private void getLocationVolley(String url) {

        loading = new ProgressDialog(this, R.style.full_screen_dialog) {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.feed_loader);
                getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT,
                        LinearLayout.LayoutParams.FILL_PARENT);
            }
        };

        loading.setCancelable(false);
        loading.show();

        MyApplication.getInstance().getRequestQueue().getCache().remove(url);
        //  MyApplication.getInstance().getRequestQueue().getCache().invalidate(url,false);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("get_location_response", response.toString());
                // loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {

                        JSONArray array = object.getJSONArray("message");
                        if (array.length() == 0) {
                            loading.dismiss();
                        } else {
                            loading.dismiss();

                            locarrayList = new ArrayList(array.length());
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);


                                String location_name = object1.getString("loc");
                                Log.d("name_loca", location_name);
                                locarrayList.add(location_name);

                            }
                            Log.d("location_array", locarrayList.toString());
                            String location = sharedpreferences.getString("location", "");
                            if (!location.equals("")) {
                                if (locarrayList.contains(location)) {
                                    int index = locarrayList.indexOf(location);
                                    Log.d("index_of", String.valueOf(index));
                                    locarrayList.remove(index);
                                    locarrayList.add(0, location);
                                } else {
                                    SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.remove("location");
                                    editor.commit();
                                }


                            }


                            String[] arr1 = (String[]) locarrayList.toArray(new String[locarrayList.size()]);


                            final ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(Club.this, R.layout.location_spin_item, R.id.loc_txt, arr1);

                            dataAdapter1.setDropDownViewResource(R.layout.location_spin_item);
                            spin_location.setAdapter(dataAdapter1);


                            spin_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                    selectedLocationName = spin_location.getSelectedItem().toString();

                                    SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.remove("location");
                                    editor.commit();
                                    editor.putString("location", selectedLocationName);
                                    editor.commit();

                                    Log.d("selected_location", selectedLocationName);

                                    boolean flag = hasConnection();
                                    if (flag) {

                                        postClubListVolley(loginUserId, selectedLocationName);
                                    } else {
                                        Toast.makeText(Club.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });


                        }
                    } else {
                        Toast.makeText(Club.this, object.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    /*Toast.makeText(Club.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
                /*Toast.makeText(Club.this,error.getMessage(), Toast.LENGTH_SHORT).show();*/
                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        RequestQueue queue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
        queue.getCache().clear();
        jsonObjReq.setShouldCache(false);

        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();

            }
        });

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    //post
    //post method
    private void postClubListVolley(final String user_id, final String loc) {
        StringRequest postRequest = new StringRequest(Request.Method.POST, AllUrl.CLUB_LIST,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("club_list_response", response);
                        // loading.dismiss();
                        try {
                            displayFirebaseRegId();
                            //   loading.dismiss();
                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("value_of_club_response", response);
                            bannerData = new ArrayList<>();
                            bannerEventData = new ArrayList<>();
                            bannerClubData = new ArrayList<>();
                            String res = jsonObject.getString("response");
                            JSONArray array = jsonObject.getJSONArray("message");
                            if (array.length() == 0) {
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(Club.this);
                                builder1.setMessage("No data found for your search.");
                                builder1.setCancelable(false);
                                builder1.setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();


                                                // finish();
                                            }
                                        });
                                alert = builder1.create();
                                alert.show();
                            } else {
                                data = new ArrayList<ClubModel>();
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);

                                    JSONArray club_array = object.getJSONArray("club");
                                    JSONArray banner_array = object.getJSONArray("banner");
                                    if (club_array.length() == 0) {

                                    } else {
                                        for (int j = 0; j < club_array.length(); j++) {
                                            JSONObject object1 = club_array.getJSONObject(j);

                                            ClubModel model = new ClubModel();

                                            model.club_id = object1.getString("club_id");
                                            model.club_name = object1.getString("club_name");
                                            model.club_date = object1.getString("date");
                                            model.club_img = object1.getString("picture");
                                            model.club_total = object1.getString("total");
                                            model.club_fav = object1.getString("favourite");
                                            model.club_male = object1.getString("male");
                                            model.club_total_ratio = object1.getString("total_ratio");
                                            model.club_female = object1.getString("female");
                                            Log.d("club_data", object1.getString("club_name"));


                                            data.add(model);
                                        }

                                        for (int h = 0; h < data.size(); h++) {
                                            String abc = String.valueOf(data.get(h));
                                            Log.d("value_of_data", abc);
                                        }

                                        clubAdapter = new ClubRecyclerAdapter(data, Club.this);
                                        clubRecycler.setAdapter(clubAdapter);


                                    }

                                    //banner
                                    if (banner_array.length() == 0) {

                                        bannerRel.setVisibility(View.GONE);
                                    } else {
                                        bannerRel.setVisibility(View.VISIBLE);
                                        for (int k = 0; k < banner_array.length(); k++) {
                                            JSONObject object2 = banner_array.getJSONObject(k);
                                            BannerModel bannerModel = new BannerModel();

                                            BannerModel.banner_img = object2.getString("banner_img");
                                            String ban_name = object2.getString("banner_img");
                                            BannerModel.banner_event_id = object2.getString("banner_event");
                                            String event = object2.getString("banner_event");

                                            String club = object2.getString("banner_club");

                                            bannerData.add(ban_name);
                                            bannerEventData.add(event);
                                            bannerClubData.add(club);

                                        }
                                        Log.d("banner_images", bannerData.toString());
                                        tutorialSlider();


                                    }
                                }


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        loading.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Log.d("post_volley", "entered in getParams");
                Map<String, String> data = new HashMap<>();
                // the POST parameters:

                data.put("user_id", user_id);
                data.put("loc", loc);


                return data;
            }

        };

        Volley.newRequestQueue(Club.this).getCache().clear();
        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();
            }
        });

        Volley.newRequestQueue(Club.this).add(postRequest);


    }


    //end of post

    public void tutorialSlider() {
        // method for initialisation

        init();

        // method for adding indicators
        addBottomDots(0);

        final Handler handler = new Handler();

        final Runnable update = new Runnable() {
            public void run() {
                if (page_position == bannerData.size()) {
                    page_position = 0;
                } else {
                    page_position = page_position + 1;
                }
                banner_slider.setCurrentItem(page_position, true);
            }
        };
        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 6000, 6000);


    }


    private void init() {

        banner_slider = Club.this.findViewById(R.id.banner_slider);
        lin_dots = Club.this.findViewById(R.id.dots);

        bannerAdapter = new BannerAdapter(Club.this, bannerData, bannerEventData, bannerClubData);
        banner_slider.setAdapter(bannerAdapter);
        indicator.setViewPager(banner_slider);

        banner_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[bannerData.size()];

        lin_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(Club.this);
            //dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setText(Html.fromHtml(" &#8226&nbsp;"));
            dots[i].setTextSize(30);
            // dots[i].setTextColor(Color.parseColor("#000000"));
            dots[i].setTextColor(getResources().getColor(R.color.transparent));
            lin_dots.addView(dots[i]);
        }

        if (dots.length > 0)

            dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
        //dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
        // dots[currentPage].setTextColor(getResources().getColor(R.color.light_green));
    }


    //notification display fcm id
    private void displayFirebaseRegId() {
        SharedPreferences pref = Club.this.getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e("firebase_id", "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId)) {
            //  txtRegId.setText("Firebase Reg Id: " + regId);
            //  txtRegId.setVisibility(View.GONE);
            String deviceurl = AllUrl.DEVICEID + loginUserId + "&device_id=" + regId + "&platform=Android";
            Log.d("deviceurl", deviceurl);
            userDeviceId(deviceurl);
        } else {
            //  Toast.makeText(Club.this,"Firebase Reg Id is not received yet!",Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(Club.this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(Club.this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(Club.this);
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(Club.this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private void userDeviceId(String url) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("user_device_id_response", response.toString());
                if (loading != null)
                    loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object

                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {
                        Log.d("device", "sucessfully");
                        MyApplication.getInstance().trackEvent("User Device id ", "Device ID Registration", "Success");

                    } else {
                        Log.d("device", "Failed");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                if (loading!=null)
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    private void userDeviceId1(String url) {
        class UserDeviceIdClass extends AsyncTask<String, Void, String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = new ProgressDialog(Club.this);
                loading.setIndeterminate(true);
                // loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.select_home));
                loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
                loading.setCancelable(false);
                loading.setMessage("Please wait...!");
                loading.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String uri = params[0];

                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while ((json = bufferedReader.readLine()) != null) {
                        sb.append(json + "\n");
                    }

                    return sb.toString().trim();

                } catch (Exception e) {
                    return null;
                }

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                try {
                    Log.d("user_device_id_response", s);

                    JSONObject object = new JSONObject(s);
                    String res = object.getString("response");
                    String msg = object.getString("message");

                    if (res.equals("200")) {

                        Log.d("device", "sucessfully");
                        MyApplication.getInstance().trackEvent("User Device id ", "Device ID Registration", "Success");
                    } else {
                        Toast.makeText(Club.this, msg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        UserDeviceIdClass gj = new UserDeviceIdClass();
        gj.execute(url);
    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) Club.this.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                finishAffinity();
            }
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    @Override
    protected void onDestroy() {
        //  stopService(mServiceIntent);
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.remove("chatuser");
        editor.commit();
        editor.apply();
        // Log.d("comitted", String.valueOf(abc));

        Log.i("MAINACT", "onDestroy!");
//        startService(mServiceIntent);
        super.onDestroy();
        /*if (!isMyServiceRunning(mSensorService.getClass())) {
            startService(mServiceIntent);
        }
        else{
            startService(mServiceIntent);
        }*/


    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE, 3);

            } catch (Exception e) {  //Settings.SettingNotFoundException e
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }


    ////new

    /**
     * Prompt user to enable GPS and Location Services
     *
     * @param mGoogleApiClient
     * @param activity
     */
    public static void locationChecker(GoogleApiClient mGoogleApiClient, final Activity activity) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    activity, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case LOCATION_REQUEST:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(Club.this, "Permission Granted.", Toast.LENGTH_SHORT).show();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                            showMessageOKCancel("You need to allow access to both the permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{ACCESS_FINE_LOCATION},
                                                        LOCATION_REQUEST);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(Club.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}
