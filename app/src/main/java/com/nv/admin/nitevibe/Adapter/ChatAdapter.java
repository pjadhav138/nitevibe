package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nv.admin.nitevibe.R;

import java.util.List;

/**
 * Created by Admin on 4/17/2018.
 */
public class ChatAdapter  extends RecyclerView.Adapter<ChatAdapter.MyView> {
    private List<ChattingModel> arrayList;
    private Context context;
    ChattingModel current;
    private boolean isChecked;
    public String favClubUrl;
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;

    public ChatAdapter(List<ChattingModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public ChatAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_list_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChatAdapter.MyView holder, int position) {
        sharedpreferences=context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        current=arrayList.get(position);
        Log.d("chatting_adapter","entered");
        Log.d("message_chat_adapter",current.message);
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public MyView(View itemView) {
            super(itemView);
        }
    }
}
