package com.nv.admin.nitevibe.Database;

/**
 * Created by Admin on 4/4/2018.
 */
public class SettingModel {
    String label,data;

    public SettingModel(String label, String data) {
        this.label = label;
        this.data = data;
    }

    public SettingModel() {
    }

    public String getLabel() {
        return label;
    }



    public void setLabel(String label) {
        this.label = label;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SettingModel{" +
                "label='" + label + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
