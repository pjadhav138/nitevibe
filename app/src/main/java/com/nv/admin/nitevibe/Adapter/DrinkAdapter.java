package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nv.admin.nitevibe.Activity.Drinks;
import com.nv.admin.nitevibe.R;
import com.nv.admin.nitevibe.custom.FlowLayout;
import com.nv.admin.nitevibe.custom.TagAdapter;
import com.nv.admin.nitevibe.custom.TagFlowLayout;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Admin on 3/20/2018.
 */
public class DrinkAdapter extends RecyclerView.Adapter<DrinkAdapter.MyView> {
    private List<DrinkModel> arrayList;
    public List<subDrinkModel> value111;
    private Context context;
    DrinkModel current;
    public static List<subDrinkModel> subDrinkList;

    public SubDrinkAdapter subAdapter;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold;
    public static List<String> indexlist = new ArrayList<>();

    public List<String> indexlistid = new ArrayList<>();
    public Set<String> savedindexlist=new HashSet<>(); //new
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    public static final String PREFS_NAME = "LoginPrefs";
    public static HashSet<Integer> integers;

    public static  List list;
	    public static Set<String> treesetList1;
	    public static Set<String> treesetList2;

    public DrinkAdapter(List<DrinkModel> drinkList, List<subDrinkModel> subDrinkList, Drinks context) {
        this.arrayList = drinkList;
        DrinkAdapter.subDrinkList = subDrinkList;
        this.context = context;
    }

    @Override
    public DrinkAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drink_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DrinkAdapter.MyView holder, int position) {
        sharedpreferences=context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = context.getSharedPreferences(PREFS_NAME, Context. MODE_PRIVATE).edit();
    //    savedindexlist = sharedpreferences.getStringSet("key",null);
        integers = new HashSet<>();
       // if(!savedindexlist.isEmpty()) {
            list = new ArrayList(savedindexlist);
      //  }

        final int select_pos=position;
        semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Calibri Bold.ttf");
        holder.label.setTypeface(calibri_bold);
        current=arrayList.get(position);
        Log.d("drink_name",current.drinkName);
        final  String drinkid= current.drinkId;
        holder.drink_name.setTypeface(semi_bold_face);
        holder.drink_name.setText(current.drinkName);
        savedindexlist=new TreeSet<String>(Drinks.predrinks);
       	        treesetList1 = new TreeSet<String>(savedindexlist);
       	        list=new ArrayList(treesetList1);

        //in some cases, it will prevent unwanted situations
        holder.chk.setOnCheckedChangeListener(null);
        //if true, your checkbox will be selected, else unselected





        //holder.chk.setChecked(current.isSelected());
        holder.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(holder.chk.isChecked()==true) {
                    holder.subRel.setVisibility(View.VISIBLE);
                    final LayoutInflater mInflater = LayoutInflater.from(context);

                    holder.drinkLayout.setAdapter(new TagAdapter<subDrinkModel>(context, subDrinkList) {


                        @Override
                        public void setSelected(int position, String s) {
                               /*if (savedindexlist == null) {
                                   Log.d("size", "sixe 0");
                               } else {
                                   List<Integer> integers = new ArrayList<>();
                                   List list = new ArrayList(savedindexlist);
                                   for (int i = 0; i < list.size(); i++) {
                                       subDrinkModel model = subDrinkList.get(position);
                                       if (model.subDrinkName.equals(list.get(i))) {
                                           int index = Integer.parseInt(model.subDrinkId);
                                           integers.add(index);
                                           setSelectedList(integers);
                                       }

                                   }
                               }*/
                        }

                        @Override
                        public View getView(FlowLayout parent, int position, subDrinkModel subDrinkModel) {
                            subDrinkModel = subDrinkList.get(position);
                            final TextView tv = (TextView) mInflater.inflate(R.layout.tv, holder.drinkLayout, false);
                                /*if(drinkid.equals("1")){
                                    holder.chk.setChecked(true);
                                    if(savedindexlist==null){
                                        Log.d("size","sixe 0");
                                    }else{
                                        List<Integer> integers = new ArrayList<>();
                                        List list = new ArrayList(savedindexlist);
                                        for(int i = 0;i<list.size();i++ ) {
                                            subDrinkModel model = subDrinkList.get(position);
                                            if (model.subDrinkName.equals(list.get(i))) {
                                                int index = Integer.parseInt(model.subDrinkId);
                                                integers.add(index);
                                                TagFlowLayout.mSelectedView.addAll(integers);
                                            }

                                        }
                                        Log.d("size","sixe"+savedindexlist.size());
                                    }
                                }*/

                            for (int i = 0; i < list.size(); i++) {
                                int index;
                                subDrinkModel model = subDrinkList.get(position);
                                if (model.subDrinkName.equals(list.get(i))) {
                                    index = Integer.parseInt(model.subDrinkId);
                                    integers.add(index);
                                }
                                Log.d("integrers","int"+integers.toString());
                                TagFlowLayout.preCheckedList.addAll(integers);
                                Log.d("TAg","tagsize"+TagFlowLayout.preCheckedList.size());
                                Log.d("integer_size", String.valueOf(integers.size()));
                            }

                            if (subDrinkModel.maindrinkid.equals(drinkid)) {
                                tv.setText(subDrinkModel.subDrinkName);
                                   /* if (savedindexlist == null) {
                                        Log.d("size", "sixe 0");
                                    } else {
                                        Set<Integer> integers = new HashSet<>();
                                        List list = new ArrayList(savedindexlist);
                                        for (int i = 0; i < list.size(); i++) {
                                            subDrinkModel model = subDrinkList.get(position);
                                            if (model.subDrinkName.equals(list.get(i))) {
                                                int index = Integer.parseInt(model.subDrinkId);
                                                integers.add(index);
                                                setSelectedList(integers);
                                            }

                                        }
                                    }*/
                            }else {
                                tv.setVisibility(View.GONE);
                            }

                            return tv;
                        }

                    });
                    holder.drinkLayout.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
                        @Override
                        public boolean onTagClick(View view, int position, FlowLayout parent) {
                            String clicked_id=subDrinkList.get(position).subDrinkId;
                            if(treesetList1.contains(clicked_id)) {
                                for (String integer : new ArrayList<>(treesetList1)) {
                                    if (integer.equals(clicked_id)) {
                                        treesetList1.remove(integer);

                                        editor.remove("sub_drink_id");
                                        editor.putStringSet("sub_drink_id", treesetList1);
                                        Log.d("indesremoved", treesetList1.toString());
                                    }

                                }
                            }else{
                                treesetList1.add(clicked_id);
                                indexlist.addAll(treesetList1);
                                editor.remove("sub_drink_id");
                                editor.putStringSet("sub_drink_id",treesetList1);
                                Log.d("indesadded",treesetList1.toString());
                            }


                            return true;
                        }
                    });

                   /* holder.drinkLayout.setOnSelectListener(new TagFlowLayout.OnSelectListener()
                    {
                        @Override
                        public void onSelected(Set<Integer> selectPosSet)
                        {
                            Log.d("selected_position_value",selectPosSet.toString());

                        }
                    });*/

                    holder.subDrinkRecycler.setHasFixedSize(true);
                    holder.subDrinkRecycler.setLayoutManager(new LinearLayoutManager(context));//Linear Items
                    value111 = new ArrayList<subDrinkModel>();
                    for (int i = 0; i < subDrinkList.size(); i++) {
                        subDrinkModel subModel = subDrinkList.get(i);
                        value111.add(subModel);
                    }

                      /*  if(savedindexlist.size()!=0) {
                            subAdapter = new SubDrinkAdapter(subDrinkList, context);
                            holder.subDrinkRecycler.setAdapter(subAdapter);
                            subAdapter.setSelected(savedindexlist);
                        }else {
                            subAdapter = new SubDrinkAdapter(subDrinkList, context);
                            holder.subDrinkRecycler.setAdapter(subAdapter);
                        }*/

                }
                else{
                    holder.subRel.setVisibility(View.GONE);
                }
            }
        });


        holder.drink_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.chk.isChecked()==true){
                    holder.chk.setChecked(false); //uncheck
                    holder.subRel.setVisibility(View.GONE);
                }
                else if(holder.chk.isChecked()==false){
                    holder.chk.setChecked(true); //check
                    holder.subRel.setVisibility(View.VISIBLE);
                    final LayoutInflater mInflater = LayoutInflater.from(context);

                    holder.drinkLayout.setAdapter(new TagAdapter<subDrinkModel>(context, subDrinkList) {


                        @Override
                        public void setSelected(int position, String s) {

                        }

                        @Override
                        public View getView(FlowLayout parent, int position, subDrinkModel subDrinkModel) {
                            subDrinkModel = subDrinkList.get(position);
                            final TextView tv = (TextView) mInflater.inflate(R.layout.tv, holder.drinkLayout, false);
                            tv.setTypeface(semi_bold_face);


                            for (int i = 0; i < list.size(); i++) {
                                int index;
                                subDrinkModel model = subDrinkList.get(position);
                                if (model.subDrinkName.equals(list.get(i))) {
                                    index = Integer.parseInt(model.subDrinkId);
                                    integers.add(index);
                                }
                                Log.d("integrers","int"+integers.toString());

                                TagFlowLayout.preCheckedList.addAll(integers);
                                Log.d("TAg","tagsize"+TagFlowLayout.preCheckedList.size());
                            }

                            if (subDrinkModel.maindrinkid.equals(drinkid)) {
                                tv.setText(subDrinkModel.subDrinkName);

                            }else {
                                tv.setVisibility(View.GONE);
                            }


                            return tv;
                        }

                    });
                    holder.drinkLayout.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
                        @Override
                        public boolean onTagClick(View view, int position, FlowLayout parent) {
                            String clicked_id=subDrinkList.get(position).subDrinkId;
                            if(treesetList1.contains(clicked_id)) {
                                for (String integer : new ArrayList<>(treesetList1)) {
                                    if (integer.equals(clicked_id)) {
                                        treesetList1.remove(integer);

                                        editor.remove("sub_drink_id");
                                        editor.putStringSet("sub_drink_id", treesetList1);
                                        Log.d("indesremoved", treesetList1.toString());
                                    }

                                }
                            }else{
                                treesetList1.add(clicked_id);
                                indexlist.addAll(treesetList1);
                                editor.remove("sub_drink_id");
                                editor.putStringSet("sub_drink_id",treesetList1);
                                Log.d("indesadded",treesetList1.toString());
                            }


                            return true;
                        }
                    });


                    holder.subDrinkRecycler.setHasFixedSize(true);
                    holder.subDrinkRecycler.setLayoutManager(new LinearLayoutManager(context));//Linear Items
                    value111 = new ArrayList<subDrinkModel>();
                    for (int i = 0; i < subDrinkList.size(); i++) {
                        subDrinkModel subModel = subDrinkList.get(i);
                        value111.add(subModel);
                    }


                } //end of else
            }
        });

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public CheckBox chk;
        public TextView drink_name,label;
        public RecyclerView subDrinkRecycler;
        public  RelativeLayout subRel;

        public TagFlowLayout drinkLayout;
        public MyView(View itemView) {
            super(itemView);
            chk= itemView.findViewById(R.id.chk);
            drink_name= itemView.findViewById(R.id.txt_drink);
            subDrinkRecycler= itemView.findViewById(R.id.subDrinkRecycler);
            subRel= itemView.findViewById(R.id.subRel);
            drinkLayout= itemView.findViewById(R.id.subDrinkFlow);
            label= itemView.findViewById(R.id.label);

        }
    }

}
