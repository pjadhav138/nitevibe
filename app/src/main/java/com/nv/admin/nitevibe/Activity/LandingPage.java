package com.nv.admin.nitevibe.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.nv.admin.nitevibe.Adapter.TutorialAdapter;
import com.nv.admin.nitevibe.Adapter.TutorialModel;
import com.nv.admin.nitevibe.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class LandingPage extends AppCompatActivity implements View.OnClickListener {
    public static Button fbLogin;
    public static TextView emailLogin;
    public static LoginButton loginBtn;
    //for fb login
    private CallbackManager callbackManager;
    private ProgressDialog progressDialog;
    User user;
    AlertDialog alert;
    //for tutorial slider
    private TextView[] dots;
    int page_position = 0;
    public ViewPager tutorial_slider;
    private LinearLayout lin_dots;
    TutorialAdapter tutorialAdapter;
    ArrayList<TutorialModel> tutorialList;
    boolean doubleBackToExitPressedOnce = false;
    public static Typeface bold_face, extra_bold_face, reg_face, semi_bold_face, calibri_bold, lato_bold, semi_bold_italic;
    //shared_pref
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId, loginUserMode, loginUserType;
    public static TextView txt_terms;

    ProgressDialog loading;
    public static LandingPage landing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //FacebookSdk.sdkInitialize(LandingPage.this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_landing_page);
        landing = this;

        fbLogin = findViewById(R.id.facebook);
        emailLogin = findViewById(R.id.email);
        txt_terms = findViewById(R.id.terms);
        // fbLogin.setOnClickListener(this);
        emailLogin.setOnClickListener(this);
        txt_terms.setOnClickListener(this);

        setFont();


        sharedpreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");

        Log.d("shared_pref", loginUserId + " " + loginUserType + " " + loginUserMode);


        if (loginUserId.equals("") && loginUserType.equals("") && loginUserMode.equals("")) {
            tutorialSlider();
        } else {
            //startActivity(new Intent(this,Home.class));
            if (loginUserType.equals("User")) {
                startActivity(new Intent(this, Club.class));
            } else if (loginUserType.equals("sub_admin")) {
                startActivity(new Intent(this, ClubSideHome.class));
            }

        }


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.facebook) {
            //Toast.makeText(getApplicationContext(),"facebook login",Toast.LENGTH_SHORT).show();

        }
        if (id == R.id.email) {
            Intent i = new Intent(LandingPage.this, Login.class);
            startActivity(i);

        }
        if (id == R.id.terms) {
            Intent i = new Intent(LandingPage.this, Terms.class);
            startActivity(i);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        Log.d("OnResume", "Entered On Resume");

        callbackManager = CallbackManager.Factory.create();

        loginBtn = findViewById(R.id.fb_login_button);

        loginBtn.setReadPermissions("public_profile", "email");
        // Set permissions
        MyApplication.getInstance().trackEvent("Login", "Facebook", "Success");


        // btnLogin= (Button) findViewById(R.id.btnLogin_facebook);
        fbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // progressDialog = new ProgressDialog(LandingPage.this);
                //  progressDialog.setMessage("Loading...");
                //  progressDialog.show();

                loginBtn.performClick();

                loginBtn.setPressed(true);

                loginBtn.invalidate();

                loginBtn.registerCallback(callbackManager, mCallBack);

                loginBtn.setPressed(false);

                loginBtn.invalidate();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivity", "entered on activity");
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            // progressDialog.dismiss();
            Log.d("Enteringcallback", "Entered call Back");

            // App code
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {

                            Log.e("response: ", response + "");
                            try {
                                user = new User();
                                user.facebookID = object.getString("id");
                                user.email = object.optString("email");
                                user.name = object.getString("name");
                                user.gender = object.getString("gender");


                                user.profile = "https://graph.facebook.com/" + user.facebookID + "/picture?type=large";

                                Log.d("profile_photo", user.profile);

                                JSONObject data = response.getJSONObject();
                                if (data.has("picture")) {
                                    String profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");
                                    // Bitmap profilePic= BitmapFactory.decodeStream(profilePicUrl .openConnection().getInputStream());
                                    // mImageView.setBitmap(profilePic);
                                    Log.d("user_profile_photo", profilePicUrl);
                                }

                                PrefUtils.setCurrentUser(user, LandingPage.this);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            String userEmail = user.email;
                            String userName = user.name;
                            String userGender = user.gender;
                            String userId = user.facebookID;

                            //  Toast.makeText(LandingPage.this,"welcome "+user.name,Toast.LENGTH_LONG).show();
                            String userProfile = "https://graph.facebook.com/" + user.facebookID + "/picture?type=large";
                            Log.d("facebook_login_details", userEmail + " " + userName + " " + userGender + " " + userId + " " + userProfile);

                            Bitmap bm = getBitmapFromURL(userProfile);
                            // Bitmap resized = Bitmap.createScaledBitmap(bm, 936,1200,true);
                            String base64 = encodeFromString(bm);
                            String gender = null;
                            //Todo updated Null point exception handle if gender not fetch from server
                            if ("male".equals(userGender)) {
                                gender = "Male";
                            } else if ("female".equals(userGender)) {
                                gender = "Female";
                            }else {
                                gender = "Male";
                            }

                            postRegisterUserVolley(userEmail, "", "Facebook", userId, base64, userName, gender);

                           /* if(!base64.equals("")){
                               // postRegisterUser(userEmail,"","Facebook",userId,base64,userName,gender);
                                postRegisterUserVolley(userEmail,"","Facebook",userId,base64,userName,gender);
                            }*/


                        }

                    });

            Bundle parameters = new Bundle();
            //parameters.putString("fields", "id,name,email,gender,birthday,link");

            parameters.putString("fields", "id,email,name,gender,cover,picture.type(large)");

            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            // progressDialog.dismiss();
        }

        @Override
        public void onError(FacebookException e) {
            // progressDialog.dismiss();
        }
    };


    //post method

    private void postRegisterUserVolley(final String username, final String password, final String mode, final String provider, final String photo, final String name, final String gender) {
        StringRequest postRequest = new StringRequest(Request.Method.POST, AllUrl.REGISTRATION,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("register_fb_response", response);
                        loading.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String res = jsonObject.getString("response");
                            String message = jsonObject.getString("message");
                            if (res.equals("200")) {
                                String user_id = message;
                                Log.d("user_id_of_registartion", user_id);

                                SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                                editor.remove("mode");
                                editor.remove("user_id");
                                editor.remove("user_type");
                                editor.commit();

                                editor.putString("mode", "Facebook");
                                editor.putString("user_id", user_id);
                                editor.putString("user_type", "User");
                                editor.commit();

                                // startActivity(new Intent(LandingPage.this,Home.class));
                                startActivity(new Intent(LandingPage.this, Club.class));
                            } else if (res.equals("205")) {
                                String user_id = message;
                                SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                                editor.remove("mode");
                                editor.remove("user_id");
                                editor.remove("user_type");
                                editor.commit();

                                editor.putString("mode", "Facebook");
                                editor.putString("user_id", user_id);
                                editor.putString("user_type", "User");
                                editor.commit();
                                startActivity(new Intent(LandingPage.this, Club.class));

                            } else {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        loading.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> data = new HashMap<>();
                // the POST parameters:

                data.put("username", username);
                data.put("password", password);
                data.put("mode", mode);
                data.put("provider", provider);
                data.put("photo", photo);
                data.put("name", name);
                data.put("gender", gender);

                return data;
            }

        };

        Volley.newRequestQueue(this).getCache().clear();
        Volley.newRequestQueue(this).add(postRequest);

        loading = new ProgressDialog(LandingPage.this, R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();

    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String encodeFromString(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();

        return Base64.encodeToString(b, Base64.DEFAULT);
    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    public void tutorialSlider() {
        // method for initialisation

        init();

        // method for adding indicators
        addBottomDots(0);

        final Handler handler = new Handler();

        final Runnable update = new Runnable() {
            public void run() {
                if (page_position == tutorialList.size()) {
                    page_position = 0;
                } else {
                    page_position = page_position + 1;
                }
                tutorial_slider.setCurrentItem(page_position, true);
            }
        };

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 6000, 6000);

    }

    private void init() {

        tutorial_slider = findViewById(R.id.tut_slider);
        lin_dots = findViewById(R.id.dots);

        tutorialList = new ArrayList<TutorialModel>();

//Add few items to slider_image_list ,this should contain url of images which should be displayed in slider
// here i am adding few sample image links, you can add your own

        tutorialList.add(new TutorialModel("", "", R.drawable.logo));
        tutorialList.add(new TutorialModel(getResources().getString(R.string.step1_title), getResources().getString(R.string.step1_txt), R.drawable.step1));
        tutorialList.add(new TutorialModel(getResources().getString(R.string.step2_title), getResources().getString(R.string.step2_txt), R.drawable.step2));
        tutorialList.add(new TutorialModel(getResources().getString(R.string.step3_title), getResources().getString(R.string.step3_txt), R.drawable.step3));


        tutorialAdapter = new TutorialAdapter(tutorialList, LandingPage.this);
        tutorial_slider.setAdapter(tutorialAdapter);

        tutorial_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[tutorialList.size()];

        lin_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml(" &#8226&nbsp;"));
            dots[i].setTextSize(30);
            dots[i].setTextColor(getResources().getColor(R.color.transparent));
            lin_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(getResources().getColor(R.color.light_green));
    }


    public void setFont() {
        bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold.ttf");
        extra_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        reg_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        semi_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBold.ttf");
        calibri_bold = Typeface.createFromAsset(getAssets(), "fonts/Calibri Bold.ttf");
        lato_bold = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");
        semi_bold_italic = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBoldItalic.ttf");

        emailLogin.setTypeface(semi_bold_face);
        fbLogin.setTypeface(bold_face);
        txt_terms.setTypeface(reg_face);


    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }


}
