package com.nv.admin.nitevibe.RangeSeekBar;

/**
 * Created by Admin on 3/8/2018.
 */
public class CrystalSeekbar  {
    //////////////////////////////////////////
    // PRIVATE CONSTANTS
    //////////////////////////////////////////

    private static final int INVALID_POINTER_ID = 255;
    //private static int DEFAULT_THUMB_WIDTH ;
    //private static int DEFAULT_THUMB_HEIGHT;

    private final float NO_STEP = -1f;

    //////////////////////////////////////////
    // PUBLIC CONSTANTS CLASS
    //////////////////////////////////////////

    public static final class DataType {
        public static final int LONG = 0;
        public static final int DOUBLE = 1;
        public static final int INTEGER = 2;
        public static final int FLOAT = 3;
        public static final int SHORT = 4;
        public static final int BYTE = 5;
    }

    public static final class Position {
        public static final int LEFT = 0;
        public static final int RIGHT = 1;
    }

    public static final class ColorMode {
        public static final int SOLID = 0;
        public static final int GRADIENT = 1;
    }



}
