package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nv.admin.nitevibe.Activity.Events;
import com.nv.admin.nitevibe.R;

import java.util.List;

/**
 * Created by Admin on 4/13/2018.
 */
public class ViewProfileEventAdapter  extends RecyclerView.Adapter<ViewProfileEventAdapter.MyView> {
    private List<ViewProfileEventModel> arrayList;
    private Context context;
    ViewProfileEventModel current;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;

    public ViewProfileEventAdapter(List<ViewProfileEventModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public ViewProfileEventAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_profile_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewProfileEventAdapter.MyView holder, int position) {

        semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        current=arrayList.get(position);
        holder.txt_name.setTypeface(semi_bold_face);

        String event_id=current.eventId;
        String event_name=current.eventName;
        String event_date=current.eventDate;

        String eventValue;

        if(event_date.equals("")){
            eventValue=event_name;

        }
        else{
            eventValue=event_name+", "+event_date;
        }

        holder.txt_name.setText(eventValue);
        holder.txt_eventid.setText(current.eventId);
        holder.txt_clubid.setText(current.eventClub);


        holder.txt_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(context,holder.txt_eventid.getText().toString(),Toast.LENGTH_SHORT).show();
                Intent i=new Intent(context, Events.class);
                i.putExtra("banner_club_id",holder.txt_clubid.getText().toString());
                i.putExtra("banner_event_id",holder.txt_eventid.getText().toString());
                i.putExtra("banner_from","viewprofile");
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        public TextView txt_name,txt_eventid,txt_clubid;
        public MyView(View itemView) {
            super(itemView);
            txt_name= itemView.findViewById(R.id.txt);
            txt_eventid= itemView.findViewById(R.id.eventid);
            txt_clubid= itemView.findViewById(R.id.clubid);
        }
    }
}
