package com.nv.admin.nitevibe.custom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

/**
 * Created by Admin on 4/11/2018.
 */
public class MatchRoundRect extends ImageView {
    int w,h;
    public MatchRoundRect(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        Drawable drawable = getDrawable();

        if (drawable == null) {
            return;
        }

        if (getWidth() == 0 || getHeight() == 0) {
            return;
        }
        Bitmap b = ((BitmapDrawable) drawable).getBitmap();
        Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);

        w = getWidth(); h = getHeight();
        Log.d("width", String.valueOf(w));



        Bitmap roundBitmap = getOvalCroppedBitmap(bitmap, w);
        canvas.drawBitmap(roundBitmap, 0, 0, null);

    }

    public static Bitmap getOvalCroppedBitmap(Bitmap bitmap, int radius) {
        Bitmap finalBitmap;
        if (bitmap.getWidth() != radius || bitmap.getHeight() != radius)
            finalBitmap = Bitmap.createScaledBitmap(bitmap, radius, radius,
                    false);
        else
            finalBitmap = bitmap;
        Bitmap output = Bitmap.createBitmap(finalBitmap.getWidth(),
                finalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        int offset = 50;
        Paint paint = new Paint();
        final Rect rect = new Rect(offset, offset, finalBitmap.getWidth(), finalBitmap.getHeight());

        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        //paint.setColor(Color.parseColor("#BAB399"));
        paint.setColor(Color.parseColor("#FF4081"));
        paint.setStrokeWidth(15);

        // paint.setColor(Color.parseColor("#000"));
        //RectF oval = new RectF(0, 0, 130, 150);
        //RectF oval = new RectF(0, 0, 250, 450);
        Log.d("width_value", String.valueOf(finalBitmap.getWidth()));
        Log.d("height_value", String.valueOf(finalBitmap.getHeight()));

        int cornersRadius = 100;


//        RectF oval=new RectF(offset, offset, 280, 400);
        RectF oval;
        if (android.os.Build.VERSION.SDK_INT > 22) {
            Log.d("Round_rect","greater");
            oval=new RectF(offset, offset, 200, 290);
        }else{
            Log.d("Round_rect","less");
            oval=new RectF(offset, offset, 280, 400);
        }

        canvas.drawRoundRect(
                oval, // rect
                cornersRadius, // rx
                cornersRadius, // ry
                paint // Paint
        );



        // canvas.drawOval(oval, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(finalBitmap, rect, oval, paint);

        return output;
    }

}
