package com.nv.admin.nitevibe.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Adapter.ProfileSliderAdapter;
import com.nv.admin.nitevibe.Adapter.ViewProfileDrinkAdapter;
import com.nv.admin.nitevibe.Adapter.ViewProfileEventAdapter;
import com.nv.admin.nitevibe.Adapter.ViewProfileEventModel;
import com.nv.admin.nitevibe.R;
import com.nv.admin.nitevibe.custom.StoriesProgressView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class View_Profile extends AppCompatActivity implements View.OnClickListener,StoriesProgressView.StoriesListener {
    public static ImageView img_back,img_chat,img_user,info;
    public static ViewPager profile_slider;
    public static TextView txt_name,txt_occupation,txt_city,txt_about,bio_label;
    TextView about_label,drink_label;

    public static RelativeLayout aboutRel;
    public ProfileSliderAdapter sliderAdapter;
    public static String str_userid,str_from;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    AlertDialog alert;
    public static ArrayList<String> imageList;
    public static ArrayList<String> drinkList;
    private StoriesProgressView storiesProgressView;
    private int counter = 0;
    long pressTime = 0L;
    long limit = 500L;
    public static View skip,reverse;
    private final long[] durations = new long[]{
            500L, 1000L, 1500L, 4000L, 5000L, 1000,
    };
    private static final int PROGRESS_COUNT = 2;
    public static RelativeLayout bioRel,drinkRel,eventRel,otherRel;
    public  RecyclerView drinkRecycler,eventRecycler;
    public static ViewProfileDrinkAdapter drinkAdapter;
    public static ViewProfileEventAdapter eventAdapter;
    public static ArrayList<ViewProfileEventModel> eventList;
    RecyclerView.LayoutManager mLayoutManager;
    boolean info_flag=false;
    public static String str_bio_value,str_come_from;
    ProgressDialog loading;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;



    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();

                        storiesProgressView.pause();


                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();

                        storiesProgressView.resume();


                    return limit < now - pressTime;
            }
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__profile);


        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        Bundle extra=getIntent().getExtras();
        if(extra!=null){
            //values coming from ChatMenu
            str_userid=extra.getString("view_user_id");
            Log.d("view_user_id",str_userid);
            str_from="Menu";
        }
        else{
            //value comming form Mainprofilestack or chatting or
            str_userid=sharedpreferences.getString("userid","");
            str_from=sharedpreferences.getString("from","");
        }



        img_back= findViewById(R.id.back);
        img_chat= findViewById(R.id.chat_img);
        img_user= findViewById(R.id.imageView1);

        storiesProgressView= findViewById(R.id.profile_slider);

        txt_name= findViewById(R.id.txt_name);
        txt_occupation= findViewById(R.id.txt_occupation);
        txt_city= findViewById(R.id.txt_city);
        txt_about= findViewById(R.id.txt_bio);
        bio_label= findViewById(R.id.bio_label);
////////
        about_label= findViewById(R.id.bio_label);
        drink_label= findViewById(R.id.drink_label);

        eventRecycler= findViewById(R.id.eventRecycler);
        drinkRecycler= findViewById(R.id.drinkRecycler);
        info= findViewById(R.id.info);


        drinkRecycler.setHasFixedSize(true);
        eventRecycler.setHasFixedSize(true);

        drinkRecycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        eventRecycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));



        aboutRel= findViewById(R.id.biorel);
        drinkRel= findViewById(R.id.drinkrel);

        bioRel= findViewById(R.id.biorel);
        drinkRel= findViewById(R.id.drinkrel);
        eventRel= findViewById(R.id.eventrel);
        otherRel= findViewById(R.id.otherrel);

       // otherRel.setVisibility(View.GONE);



        skip= findViewById(R.id.skip);
        reverse= findViewById(R.id.reverse);

        setFont();
        onClickEvent();
      /*  reverse.setOnTouchListener(onTouchListener);
        skip.setOnTouchListener(onTouchListener);*/

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storiesProgressView.skip();
                onNext();
            }
        });
        skip.setOnTouchListener(onTouchListener);

        reverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storiesProgressView.reverse();
                onPrev();
            }
        });
        reverse.setOnTouchListener(onTouchListener);

        boolean flag=hasConnection();
        if (flag) {
            String url=AllUrl.VIEW_PROFILE+str_userid;
           // getUserInfo(url);
            getUserInfoVolley(url);
        }
        else{
            Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
        }


        if(counter>0){
            storiesProgressView.setStoriesListener(this);
            storiesProgressView.startStories();
            storiesProgressView.setStoriesCount(PROGRESS_COUNT);
            storiesProgressView.setStoryDuration(1000L);
        }





    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        if(id==R.id.back){

            //Toast.makeText(getApplicationContext(),"back",Toast.LENGTH_SHORT).show();
            if(str_from.equals("Chatting")){  //chatting
                MyApplication.getInstance().trackEvent("Profile", "Chat", "Success");
                startActivity(new Intent(View_Profile.this,Chat.class));
            }
            else if (str_from.equals("Swipe")){ //mainprofilestack
                MyApplication.getInstance().trackEvent("Profile", "Swipe", "Success");
               // startActivity(new Intent(View_Profile.this,Swipe.class));
                super.onBackPressed();

            }
            else if(str_from.equals("Going")){  //goingswipeadapter
                MyApplication.getInstance().trackEvent("Profile", "Whos Going", "Success");
             //startActivity(new Intent(View_Profile.this,WhosGoing.class));
                super.onBackPressed();
            }
            else if(str_from.equals("whosgoing")){  //from whosnonprefadapter
                startActivity(new Intent(View_Profile.this,WhosGoing.class));
            }
            else if(str_from.equals("Menu")){
               // startActivity(new Intent(View_Profile.this,ChatMenu.class));
                super.onBackPressed();
            }

        }
        else if(id==R.id.chat_img){
            //home
            MyApplication.getInstance().trackEvent("Profile", "Club", "Success");
            startActivity(new Intent(View_Profile.this,Club.class));
        }
        else if(id==R.id.skip){
            Log.d("value_of_counter", String.valueOf(counter));
            storiesProgressView.setStoriesListener(this);
            storiesProgressView.startStories();
            storiesProgressView.skip();
        }
        else if(id==R.id.reverse){
            storiesProgressView.setStoriesListener(this);
            storiesProgressView.startStories();
            storiesProgressView.reverse();
        }
        else if(id==R.id.info){

          //  otherRel.setVisibility(View.VISIBLE);

        }

    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
    }*/

    public void onClickEvent(){
        img_back.setOnClickListener(this);
        img_chat.setOnClickListener(this);
        info.setOnClickListener(this);
       // skip.setOnClickListener(this);
      //  reverse.setOnClickListener(this);
    }

    private void getUserInfoVolley(String url){

        loading= new ProgressDialog(View_Profile.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("user_profile_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String res = jsonObject.getString("response");
                    JSONArray array = jsonObject.getJSONArray("message");
                    if (array.length() == 0) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(View_Profile.this);
                        builder1.setMessage("No data found for your search.");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        // finish();
                                    }
                                });
                        alert = builder1.create();
                        alert.show();
                        Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                        positive.setTextColor(getResources().getColor(R.color.female));
                    }
                        else{
                        for(int i=0;i<array.length();i++){
                            JSONObject object = array.getJSONObject(i);

                            String user_id=object.getString("user_id");
                            String user_name=object.getString("user_name");
                            String user_age=object.getString("age");
                            String location=object.getString("location");
                            String occupation=object.getString("occupation");
                            String bio=object.getString("bio");
                            str_bio_value=bio;
                            String name;
                            if(user_age.equals("")){
                                name=user_name;
                            }
                            else{
                                name=user_name+", "+user_age;
                            }
                            txt_name.setText(name);

                            if(occupation.equals("")){
                                txt_occupation.setVisibility(View.GONE);
                            }
                            else{
                                txt_occupation.setVisibility(View.VISIBLE);
                                txt_occupation.setText(occupation);
                            }

                            if(location.equals("")){
                                txt_city.setVisibility(View.GONE);
                            }
                            else{
                                txt_city.setVisibility(View.VISIBLE);
                                txt_city.setText(location);
                            }

                            if(bio.equals("")){
                                bioRel.setVisibility(View.GONE);
                            }
                            else{
                                bioRel.setVisibility(View.VISIBLE);
                                txt_about.setText(bio);
                            }




                            //photos array
                            JSONArray photoarr=object.getJSONArray("photos");
                            int photo_len=photoarr.length();

                          /*  storiesProgressView.setStoriesCount(photoarr.length());
                            storiesProgressView.setStoriesListener(View_Profile.this);
                            storiesProgressView.startStories();*/
                            if(photoarr.length()==0){
                                imageList=new ArrayList<>();
                                String default_img="http://rednwhite.co.in/images/user/user.png";
                                imageList.add(default_img);

                                Picasso.with(View_Profile.this)
                                        .load(default_img)
                                        .into(img_user);
                                storiesProgressView.setStoriesCount(imageList.size());
                                storiesProgressView.setStoriesListener(View_Profile.this);
                                storiesProgressView.startStories();

                                reverse.setOnTouchListener(onTouchListener);
                                skip.setOnTouchListener(onTouchListener);

                            }
                            else{
                                storiesProgressView.setStoriesCount(photoarr.length());
                                storiesProgressView.setStoriesListener(View_Profile.this);
                                storiesProgressView.startStories();

                                reverse.setOnTouchListener(onTouchListener);
                                skip.setOnTouchListener(onTouchListener);

                                imageList=new ArrayList<>();
                                for(int j=0;j<photoarr.length();j++){
                                    JSONObject object1=photoarr.getJSONObject(j);

                                    String profile=object1.getString("img");
                                    imageList.add(profile);
                                }

                                Log.d("size_of_photoarr", String.valueOf(photoarr.length()));

                                Picasso.with(View_Profile.this)
                                        .load(imageList.get(counter))
                                        .into(img_user);
                            }

                            //drinks array
                            JSONArray drinkarr=object.getJSONArray("drinks");
                            if(drinkarr.length()==0){
                                drinkRel.setVisibility(View.GONE);
                            }
                            else{
                                drinkRel.setVisibility(View.VISIBLE);
                                drinkList=new ArrayList<>();
                                for (int k=0;k<drinkarr.length();k++){
                                    JSONObject object2=drinkarr.getJSONObject(k);

                                    String drink_name=object2.getString("drink");
                                    drinkList.add(drink_name);
                                }
                                drinkAdapter=new ViewProfileDrinkAdapter(drinkList,View_Profile.this);
                                drinkRecycler.setAdapter(drinkAdapter);
                            }

                            //event array
                            JSONArray eventarr=object.getJSONArray("event");
                            if(eventarr.length()==0){
                                eventRel.setVisibility(View.GONE);
                            }
                            else{
                                eventRel.setVisibility(View.VISIBLE);
                                eventList=new ArrayList<>();
                                for (int k=0;k<eventarr.length();k++){
                                    ViewProfileEventModel model=new ViewProfileEventModel();

                                    JSONObject object3=eventarr.getJSONObject(k);
                                    model.eventId=object3.getString("event_id");
                                    model.eventName=object3.getString("event_name");
                                    model.eventDate=object3.getString("event_date");
                                    model.eventClub=object3.getString("event_club");

                                    eventList.add(model);


                                }
                                eventAdapter=new ViewProfileEventAdapter(eventList,View_Profile.this);
                                eventRecycler.setAdapter(eventAdapter);

                            }




                        }
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }


    public void setFont(){

        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");
        txt_name.setTypeface(bold_face);
        txt_occupation.setTypeface(reg_face);
        txt_city.setTypeface(reg_face);
        txt_about.setTypeface(semi_bold_face);
        about_label.setTypeface(calibri_bold);
        drink_label.setTypeface(calibri_bold);

    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    @Override
    public void onNext() {
        if(imageList.size()>0){
            if(counter==imageList.size()-1) return;

            Picasso.with(this)
                    .load(imageList.get(++counter))
                    .into(img_user);

            Log.d("value_of_counter", String.valueOf(counter));

        }
    }

    @Override
    public void onPrev() {
        if(imageList.size()>0){
            if((counter-1<0)) return;
            Picasso.with(this)
                    .load(imageList.get(--counter))
                    .into(img_user);
        }

    }

    @Override
    public void onComplete() {

    }
    @Override
    protected void onDestroy() {
        // Very important !
        storiesProgressView.destroy();
        super.onDestroy();
    }
}
