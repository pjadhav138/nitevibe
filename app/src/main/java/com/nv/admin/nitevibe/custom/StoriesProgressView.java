package com.nv.admin.nitevibe.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 3/21/2018.
 */
public class StoriesProgressView extends LinearLayout {
    private final LayoutParams PROGRESS_BAR_LAYOUT_PARAM = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
    private final LayoutParams SPACE_LAYOUT_PARAM = new LayoutParams(5, LayoutParams.WRAP_CONTENT);

    private final List<PausableProgressBar> progressBars = new ArrayList<>();

    private int storiesCount = -1;

    /**
     * pointer of running animation
     */
    private int current = 0;
    private StoriesListener storiesListener;
    private
    boolean isReverse;
    boolean isComplete;

    //////////

    private TextView[] dots;
    private LinearLayout lin_dots;

  /*  public void setStoriesListener1(Context context) {
        this= context;
    }*/
    ////////

    public interface StoriesListener {
        void onNext();

        void onPrev();

        void onComplete();
    }

    public StoriesProgressView(Context context) {
        this(context, null);
    }

    public StoriesProgressView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public StoriesProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public StoriesProgressView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        setOrientation(LinearLayout.HORIZONTAL);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, jp.shts.android.storiesprogressview.R.styleable.StoriesProgressView);
        storiesCount = typedArray.getInt(jp.shts.android.storiesprogressview.R.styleable.StoriesProgressView_progressCount, 0);
        typedArray.recycle();
        bindViews();
    }

    private void bindViews() {
        progressBars.clear();
        removeAllViews();

      /*  //dots
        lin_dots = (LinearLayout) findViewById(R.id.dots);
        dots = new TextView[storiesCount];
        for(int j=0;j<storiesCount;j++){
            dots[j] = new TextView(this);
            //dots[i].setText(Html.fromHtml("&#8226;"));
            dots[j].setText(Html.fromHtml(" &#8226&nbsp;"));
            dots[j].setTextSize(30);
             dots[j].setTextColor(Color.parseColor("#000000"));
          //  dots[j].setTextColor(getResources().getColor(R.color.light_blue));
            lin_dots.addView(dots[j]);

        }*/



////original
        for (int i = 0; i < storiesCount; i++) {
            final PausableProgressBar p = createProgressBar();
            progressBars.add(p);
            addView(p);
            if ((i + 1) < storiesCount) {
                addView(createSpace());
            }
        }
    }

    private PausableProgressBar createProgressBar() {
        PausableProgressBar p = new PausableProgressBar(getContext());
        p.setLayoutParams(PROGRESS_BAR_LAYOUT_PARAM);
        return p;
    }

    private View createSpace() {
        View v = new View(getContext());
        v.setLayoutParams(SPACE_LAYOUT_PARAM);
        return v;
    }

    /**
     * Set story count and create views
     *
     * @param storiesCount story count
     */
    public void setStoriesCount(int storiesCount) {
        this.storiesCount = storiesCount;

        bindViews();
    }

    /**
     * Set storiesListener
     *
     * @param storiesListener StoriesListener
     */
    public void setStoriesListener(StoriesListener storiesListener) {
        this.storiesListener = storiesListener;
    }

    /**
     * Skip current story
     */
    public void skip() {
        if (isComplete) return;
        PausableProgressBar p = progressBars.get(current);
        p.setMax();

    }

    /**
     * Reverse current story
     */
    public void reverse() {
        if (isComplete) return;
        isReverse = true;
        PausableProgressBar p = progressBars.get(current);
        p.setMin();

    }

    /**
     * Set a story's duration
     *
     * @param duration millisecond
     */
    public void setStoryDuration(long duration) {
        for (int i = 0; i < progressBars.size(); i++) {
            progressBars.get(i).setDuration(duration);
            progressBars.get(i).setCallback(callback(i));
        }
    }

    /**
     * Set stories count and each story duration
     *
     * @param durations milli
     */
    public void setStoriesCountWithDurations(@NonNull long[] durations) {
        storiesCount = durations.length;
        bindViews();
        for (int i = 0; i < progressBars.size(); i++) {
            progressBars.get(i).setDuration(durations[i]);
            progressBars.get(i).setCallback(callback(i));
        }
    }

    private PausableProgressBar.Callback callback(final int index) {
        return new PausableProgressBar.Callback() {
            @Override
            public void onStartProgress() {
                current = index;
                Log.d("current_value", String.valueOf(current));
            }

            @Override
            public void onFinishProgress() {
                if (isReverse) {
                    isReverse = false;
                    if (storiesListener != null) storiesListener.onPrev();

                    if (0 <= (current - 1)) {
                        PausableProgressBar p = progressBars.get(current - 1);
                        p.setMinWithoutCallback();
                        progressBars.get(--current).startProgress();
                    } else {
                        progressBars.get(current).startProgress();
                    }
                    return;
                }
                int next = current + 1;
                if (next <= (progressBars.size() - 1)) {
                    if (storiesListener != null) storiesListener.onNext();
                    progressBars.get(next).startProgress();
                } else {
                   // isComplete = true;
                  //  if (storiesListener != null) storiesListener.onComplete();


                    Log.d("stopping","last image view");


                }
            }
        };
    }

    /**
     * Start progress animation
     */
    public void startStories() {
        progressBars.get(0).startProgress();
    }

    /**
     * Need to call when Activity or Fragment destroy
     */
    public void destroy() {
        for (PausableProgressBar p : progressBars) {
            p.clear();
        }
    }

    /**
     * Pause story
     */
    public void pause() {
        progressBars.get(current).pauseProgress();
    }

    /**
     * Resume story
     */
    public void resume() {
        progressBars.get(current).resumeProgress();
    }
}
