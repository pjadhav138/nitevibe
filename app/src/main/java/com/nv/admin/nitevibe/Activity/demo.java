package com.nv.admin.nitevibe.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Adapter.ClubEventAdapter;
import com.nv.admin.nitevibe.Adapter.ClubEventModel;
import com.nv.admin.nitevibe.Adapter.ClubSliderAdapter;
import com.nv.admin.nitevibe.Adapter.FacilityModel;
import com.nv.admin.nitevibe.Adapter.FacilityRecyclerAdapter;
import com.nv.admin.nitevibe.Adapter.UserGoingAdapter;
import com.nv.admin.nitevibe.Adapter.UserGoingModel;
import com.nv.admin.nitevibe.R;
import com.github.lzyzsd.circleprogress.ArcProgress;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.hoang8f.android.segmented.SegmentedGroup;
import me.relex.circleindicator.CircleIndicator;

public class demo extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener, LocationListener,RadioGroup.OnCheckedChangeListener  {

    private ViewPager club_slider;
    private LinearLayout lin_dots;
    ClubSliderAdapter sliderPagerAdapter;
    ArrayList<String> clubImageList;
    private TextView[] dots;
    int page_position = 0;
    public static ImageView img_back, img_share, img_loc, img_fav;
    public static TextView txt_clubName, txt_clubRate, txt_time, txt_star, time_label, cost_label, fac_label, happy_label, happy_time, time_close, txt_going;
    public static Button btn_checkIn;
    public static RatingBar txt_clubStar;

    public static RecyclerView facilityRecycler, eventRecycler, goingRecycler;
    FacilityRecyclerAdapter facilityAdapter;
    ClubEventAdapter eventAdapter;
    UserGoingAdapter goingAdapter;
    List<FacilityModel> facilityList;
    List<ClubEventModel> eventList;
    List<UserGoingModel> usergoingList;
    public static String clubUrl, todayEventId="";
    public static String clubId;
    AlertDialog alert;
    public static Typeface bold_face, extra_bold_face, reg_face, semi_bold_face, calibri_bold, lato_bold, semi_bold_italic;
    RecyclerView.LayoutManager mLayoutManager;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId, loginUserMode, loginUserType;
    public static RelativeLayout facilityRel, eventRel, goingRel;
    public static String str_lat = "", str_long = "", str_current_event, str_club_name, str_checkin_value="0", str_radius;
    PopupWindow popupWindow;
    ///
    private GoogleMap mMap;
    boolean doubleBackToExitPressedOnce = false;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Circle mCircle;
    String age_pref, gender_pref;
    CircleIndicator indicator;
    ProgressDialog loading;
    //bottom sheet
    // BottomSheetBehavior variable
    private BottomSheetBehavior bottomSheetBehavior, bottomSheetBehavior11;
    // TextView variable

    public static String stagEntry, coupleEntry, leftCoupon;
    public static RelativeLayout couponRel, stagRel, coupleRel;
    public static TextView coupon_title, txt_stag, txt_couple;
    public static TextView congrat11, points11, congrat, points;

    public static ArcProgress couponProgress;
    public static ImageView userTab, homeTab, chatTab;
    public static TextView txt_event_name, txt_add_title, txt_address, txt_date_title, txt_date, txt_time_title, txt_time11, txt_entry_title, txt_entry_fee;
    public static Button btn_skip, btn_usecoupon;
    public static ImageView coupon_img;
    public static String ceventName, cdate, ctime, cstag, ccouple, cimg, caddress, club_mobileno;
    public static TextView txt_event_name111, txt_add_title111, txt_address111, txt_date_title111, txt_date111, txt_time_title111,
            txt_time111, txt_entry_title111, txt_entry_fee111;
    public static Button btn_skip111, btn_usecoupon111;
    public static ImageView coupon_img111;  //, contact_img;
    public static RoundedImageView contact_img;

    private boolean isChecked;
    public Handler handler;
    public static SegmentedGroup segment;
    public static RadioButton rb_stag,rb_couple;

    public static String str_coupon_final="",str_avail_coupon,str_coupon_start,str_coupon_end,str_user_pref_array,str_current_event_array;
 public static boolean isWithin1km;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_demo);
        setContentView(R.layout.demo2);

        sharedpreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);


        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");

        indicator = findViewById(R.id.indicator);

        img_back = findViewById(R.id.back);
        img_share = findViewById(R.id.share);
        img_loc = findViewById(R.id.imgLoc);
        img_fav = findViewById(R.id.fav);

        txt_clubName = findViewById(R.id.txt_name);
        txt_star = findViewById(R.id.txt_star_label);
        time_label = findViewById(R.id.txt_time_label);
        txt_clubRate = findViewById(R.id.txt_cost);
        txt_time = findViewById(R.id.txt_time);
        cost_label = findViewById(R.id.txt_cost_label);
        time_close = findViewById(R.id.txt_time_close);
        txt_going = findViewById(R.id.going_title);

        happy_label = findViewById(R.id.txt_happy_label);
        happy_time = findViewById(R.id.txt_happy);

        fac_label = findViewById(R.id.fac_title);

        txt_clubStar = findViewById(R.id.rating);


        btn_checkIn = findViewById(R.id.checkIn);

        facilityRel = findViewById(R.id.facilityrel);
        eventRel = findViewById(R.id.eventdetailrel);
        goingRel = findViewById(R.id.goingrel);


        couponRel = findViewById(R.id.couponrel);
        stagRel = findViewById(R.id.stagRel);
        coupleRel = findViewById(R.id.coupleRel);

        coupon_title = findViewById(R.id.coupon_title);
        txt_stag = findViewById(R.id.stag_txt);
        txt_couple = findViewById(R.id.couple_txt);

        couponProgress = findViewById(R.id.arc_progress);

        userTab = findViewById(R.id.usertab);
        homeTab = findViewById(R.id.hometab);
        chatTab = findViewById(R.id.chattab);

        contact_img= findViewById(R.id.imgcontact);
        segment= findViewById(R.id.segmented2);
        rb_stag= findViewById(R.id.buttonstag);
        rb_couple= findViewById(R.id.buttoncouple);


        setFont();

        //getting the club value from clubRecyclerAdapter
        clubId = sharedpreferences.getString("club_id", "");
        Log.d("club_id", clubId);


        onClickEvent();


        facilityRecycler = findViewById(R.id.facilityRecycler);
        facilityRecycler.setHasFixedSize(true);
        facilityRecycler.setLayoutManager(new LinearLayoutManager(this));//Linear Items

        eventRecycler = findViewById(R.id.eventRecycler);
        eventRecycler.setNestedScrollingEnabled(false);
        eventRecycler.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        eventRecycler.setLayoutManager(mLayoutManager);

        goingRecycler = findViewById(R.id.goingRecycler);
        goingRecycler.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        goingRecycler.setLayoutManager(mLayoutManager);

        boolean flag = hasConnection();
        if (flag) {
            String url = AllUrl.CLUB + clubId + "&user_id=" + loginUserId;
            Log.d("club_url",url);
            clubUrl = url.replace(" ", "%20");
            // clubDetail(clubUrl);
            clubSpecificDetail(clubUrl);
        } else {

            Toast.makeText(demo.this, "Please Check Your Internet Connection!!", Toast.LENGTH_LONG).show();
        }

        ////




        setUpMapIfNeeded();

        buildGoogleApiClient();
        mGoogleApiClient.connect();

        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottom_sheet));
        bottomSheetBehavior11 = BottomSheetBehavior.from(findViewById(R.id.bottom_sheet11));

        congrat= findViewById(R.id.congrats);
        points= findViewById(R.id.points);
        congrat11= findViewById(R.id.congrats11);
        points11= findViewById(R.id.points11);
        txt_event_name = findViewById(R.id.event_title);
        txt_add_title = findViewById(R.id.club_add_title);
        txt_address = findViewById(R.id.club_add);
        txt_date_title = findViewById(R.id.date_title);
        txt_date = findViewById(R.id.date);
        txt_time_title = findViewById(R.id.time_title);
        txt_time11 = findViewById(R.id.time);
        txt_entry_title = findViewById(R.id.entry_title);
        txt_entry_fee = findViewById(R.id.fees);
        btn_skip = findViewById(R.id.skip);
        btn_usecoupon = findViewById(R.id.usecoupon);
        coupon_img = findViewById(R.id.club_img);

        txt_event_name111 = findViewById(R.id.event_title11);
        txt_add_title111 = findViewById(R.id.club_add_title11);
        txt_address111 = findViewById(R.id.club_add11);
        txt_date_title111 = findViewById(R.id.date_title11);
        txt_date111 = findViewById(R.id.date11);
        txt_time_title111 = findViewById(R.id.time_title11);
        txt_time111 = findViewById(R.id.time11);
        txt_entry_title111 = findViewById(R.id.entry_title11);
        txt_entry_fee111 = findViewById(R.id.fees11);
        btn_skip111 = findViewById(R.id.skip11);
        btn_usecoupon111 = findViewById(R.id.usecoupon11);
        coupon_img111 = findViewById(R.id.club_img11);

        congrat.setTypeface(calibri_bold);
        points.setTypeface(calibri_bold);
        txt_event_name.setTypeface(bold_face);
        txt_add_title.setTypeface(semi_bold_face);
        txt_address.setTypeface(calibri_bold);
        txt_date_title.setTypeface(semi_bold_face);
        txt_date.setTypeface(calibri_bold);
        txt_time_title.setTypeface(semi_bold_face);
        txt_time11.setTypeface(calibri_bold);
        txt_entry_title.setTypeface(semi_bold_face);
        txt_entry_fee.setTypeface(calibri_bold);
        btn_skip.setTypeface(calibri_bold);
        btn_usecoupon.setTypeface(calibri_bold);

        congrat11.setTypeface(calibri_bold);
        points11.setTypeface(calibri_bold);
        txt_event_name111.setTypeface(bold_face);
        txt_add_title111.setTypeface(semi_bold_face);
        txt_address111.setTypeface(calibri_bold);
        txt_date_title111.setTypeface(semi_bold_face);
        txt_date111.setTypeface(calibri_bold);
        txt_time_title111.setTypeface(semi_bold_face);
        txt_time111.setTypeface(calibri_bold);
        txt_entry_title111.setTypeface(semi_bold_face);
        txt_entry_fee111.setTypeface(calibri_bold);
        btn_skip111.setTypeface(calibri_bold);
        btn_usecoupon111.setTypeface(calibri_bold);

        congrat.setTextColor(Color.BLACK);
        congrat11.setTextColor(Color.BLACK);
        points.setTextColor(Color.BLACK);
        points11.setTextColor(Color.BLACK);

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        btn_skip111.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetBehavior11.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        btn_usecoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String checkin_url = AllUrl.CHECK_IN + loginUserId + "&club_id=" + clubId + "&entry_type=Stag" + "&coupon=1";
                Log.d("check-in_url", checkin_url);

                userCheckInVolley(checkin_url);
            }
        });


        btn_usecoupon111.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String checkin_url = AllUrl.CHECK_IN + loginUserId + "&club_id=" + clubId + "&entry_type=Couple" + "&coupon=2";
                Log.d("check-in_url", checkin_url);
                userCheckInVolley(checkin_url);
            }
        });




    }


    public void onClickEvent() {
        img_back.setOnClickListener(this);
        btn_checkIn.setOnClickListener(this);
        img_loc.setOnClickListener(this);
        img_share.setOnClickListener(this);
        stagRel.setOnClickListener(this);
        coupleRel.setOnClickListener(this);
        userTab.setOnClickListener(this);
        homeTab.setOnClickListener(this);
        chatTab.setOnClickListener(this);
        contact_img.setOnClickListener(this);
        segment.setOnCheckedChangeListener(this);
        img_fav.setOnClickListener(this);
        img_fav.setOnClickListener(this);




    }
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        //str_coupon_final
        switch (i){
            case R.id.buttonstag:
                str_coupon_final="Stag";
                Log.d("coupon_val",str_avail_coupon);
                if(!str_avail_coupon.equals("") && str_checkin_value.equals("0")){

                    showStagCoupon();

                }
                else{
                    Log.d("coupon_val11","bdcbi");
                }


                break;
            case R.id.buttoncouple:
                str_coupon_final="Couple";
                if(!str_avail_coupon.equals("") && str_checkin_value.equals("0")){
                    showCoupleCoupon();
                }
                else{

                }
                break;

        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.back) {
            // super.onBackPressed();
            startActivity(new Intent(demo.this, Club.class));

        }
        if (id == R.id.checkIn) {
            Log.d("entry_type_value",str_coupon_final);
            if(str_checkin_value.equals("0")){
                ///new
                if(str_user_pref_array.equals("0")){
                    Toast.makeText(getBaseContext(), "Please fill your profile", Toast.LENGTH_LONG).show();

                /*    btn_usecoupon.setVisibility(View.GONE);
                    btn_usecoupon111.setVisibility(View.GONE);*/
                }
                else if(str_user_pref_array.equals("1")){
                    if(!todayEventId.equals("")){
                        if(isWithin1km){
                            Log.d("user_radius","Inside");
                            //btn_checkIn.setVisibility(View.VISIBLE);
                          /*  if(str_checkin_value.equals("0")){
                                btn_usecoupon.setVisibility(View.VISIBLE);
                                btn_usecoupon111.setVisibility(View.VISIBLE);
                            }
                            else{
                                btn_usecoupon.setVisibility(View.GONE);
                                btn_usecoupon111.setVisibility(View.GONE);
                            }*/



                            /////
                            if(str_coupon_final.equals("")){
                                Toast.makeText(getApplicationContext(),"Please select the entry type as stag or couple",Toast.LENGTH_SHORT).show();
                            }
                            else{
                                boolean flag=hasConnection();
                                if(flag){
                                    String checkin_url = AllUrl.CHECK_IN + loginUserId + "&club_id=" + clubId + "&entry_type=" +str_coupon_final+ "&coupon=0";
                                    Log.d("check-in_url", checkin_url);
                                    userCheckInVolley(checkin_url);
                                }
                                else{
                                    Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                        else{
                            Log.d("user_radius","Outside");
                             Toast.makeText(getBaseContext(), "You are not within the radius", Toast.LENGTH_LONG).show();
                             /*
                            btn_usecoupon.setVisibility(View.GONE);
                            btn_usecoupon111.setVisibility(View.GONE);*/

                        }


                    }
                    else{
                        Toast.makeText(getBaseContext(), "Their is no event today", Toast.LENGTH_LONG).show();
                    }


                }


            }
            else if(str_checkin_value.equals("1")){
                if(isWithin1km){
                    startActivity(new Intent(demo.this,Swipe.class));
                }
                else{
                    Toast.makeText(getBaseContext(), "You are not within the radius", Toast.LENGTH_LONG).show();
                }

            }



        }

        if (id == R.id.imgLoc) {
            Intent i = new Intent(this, MapLocation.class);
            i.putExtra("club_lat", str_lat);
            i.putExtra("club_lng", str_long);
            i.putExtra("club_name", str_club_name);
            startActivity(i);

        }
        if (id == R.id.share) {
            String clubid = clubId;
            byte[] data;

            data = clubid.getBytes(StandardCharsets.UTF_8);

            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            Log.d("base_64_conversion", base64);
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            // Add data to the intent, the receiving app will decide
            // what to do with it.

            String club_link=AllUrl.CLUB_SHARE+base64;
            share.putExtra(Intent.EXTRA_TEXT, club_link);

            this.startActivity(Intent.createChooser(share, "Share link!"));

        }
        if (id == R.id.stagRel) {
            showStagCoupon();

        }
        if (id == R.id.coupleRel) {
            showCoupleCoupon();
        }

        if (id == R.id.usertab) {
            startActivity(new Intent(this, UserProfile.class));
        }
        if (id == R.id.hometab) {
            startActivity(new Intent(this, Club.class));
        }
        if (id == R.id.chattab) {
            startActivity(new Intent(this, Chat.class));
        }
        if (id == R.id.imgcontact) {
            if (isPermissionGranted()) {
                call_action();

            }
        }


        if(id==R.id.fav){
            isChecked = !isChecked;
            img_fav.setImageResource(isChecked ? R.drawable.star_on_2 : R.drawable.star_off);

            Log.d("value of isCheck", String.valueOf(isChecked));

            String status = null;
            if(isChecked==true){
                status="Active";
            }
            else if(isChecked==false){
                status="Inactive";
            }

            boolean flag=hasConnection();
            if(flag){
                postfavclubvolley(loginUserId,clubId,"0",status);
            }
            else{
                Toast.makeText(getApplicationContext(),"Please check your internet connection!",Toast.LENGTH_LONG).show();
            }
        }


    }

    private boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    private void call_action() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + club_mobileno));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    call_action();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void showStagCoupon(){

        txt_event_name.setText(ceventName);
        txt_entry_fee.setText(stagEntry);
        txt_address.setText(caddress);
        txt_date.setText(cdate);
        txt_time11.setText(ctime);


        Picasso.with(getApplicationContext())
                .load(cimg)
                .into(coupon_img);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        if(isWithin1km){
            /////////////time comparison of coupon with current time
//
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");

            Date EndTime = null;
            Date StartTime=null;
            try {
                //  EndTime = dateFormat.parse("10:00");
                EndTime = dateFormat.parse(str_coupon_end);
                StartTime=dateFormat.parse(str_coupon_start);
                Date CurrentTime = dateFormat.parse(dateFormat.format(new Date()));
                Log.d("my_cutrrent_timeeee", String.valueOf(CurrentTime));


                if(CurrentTime.equals(StartTime) || CurrentTime.after(StartTime)){
                    //coupon time start
                    System.out.println("timeeee started ");
                    btn_usecoupon.setVisibility(View.VISIBLE);
                    btn_usecoupon111.setVisibility(View.VISIBLE);

                    if( CurrentTime.after(EndTime)){
                        //coupon time has end
                        System.out.println("timeeee has end ");
                        btn_usecoupon.setVisibility(View.GONE);
                        btn_usecoupon111.setVisibility(View.GONE);
                    }
                    else if(CurrentTime.before(EndTime)){
                        //coupon time id still left
                        System.out.println("timeeee is still remaing ");
                        btn_usecoupon.setVisibility(View.VISIBLE);
                        btn_usecoupon111.setVisibility(View.VISIBLE);
                    }
                }
                else if(CurrentTime.before(StartTime)){
                    //coupon time not yet started
                    System.out.println("timeeee not yet started ");
                    btn_usecoupon.setVisibility(View.GONE);
                    btn_usecoupon111.setVisibility(View.GONE);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else{
            btn_usecoupon.setVisibility(View.GONE);
            btn_usecoupon111.setVisibility(View.GONE);
        }
    }

    public void showCoupleCoupon(){
        txt_event_name111.setText(ceventName);
        txt_entry_fee111.setText(stagEntry);
        txt_address111.setText(caddress);
        txt_date111.setText(cdate);
        txt_time111.setText(ctime);


        Picasso.with(getApplicationContext())
                .load(cimg)
                .into(coupon_img111);
        bottomSheetBehavior11.setState(BottomSheetBehavior.STATE_EXPANDED);
        if(isWithin1km){
            /////////////time comparison of coupon with current time
//
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");

            Date EndTime = null;
            Date StartTime=null;
            try {
                //  EndTime = dateFormat.parse("10:00");
                EndTime = dateFormat.parse(str_coupon_end);
                StartTime=dateFormat.parse(str_coupon_start);
                Date CurrentTime = dateFormat.parse(dateFormat.format(new Date()));
                Log.d("my_cutrrent_timeeee", String.valueOf(CurrentTime));


                if(CurrentTime.equals(StartTime) || CurrentTime.after(StartTime)){
                    //coupon time start
                    System.out.println("timeeee started ");
                    btn_usecoupon.setVisibility(View.VISIBLE);
                    btn_usecoupon111.setVisibility(View.VISIBLE);

                    if( CurrentTime.after(EndTime)){
                        //coupon time has end
                        System.out.println("timeeee has end ");
                        btn_usecoupon.setVisibility(View.GONE);
                        btn_usecoupon111.setVisibility(View.GONE);
                    }
                    else if(CurrentTime.before(EndTime)){
                        //coupon time id still left
                        System.out.println("timeeee is still remaing ");
                        btn_usecoupon.setVisibility(View.VISIBLE);
                        btn_usecoupon111.setVisibility(View.VISIBLE);
                    }
                }
                else if(CurrentTime.before(StartTime)){
                    //coupon time not yet started
                    System.out.println("timeeee not yet started ");
                    btn_usecoupon.setVisibility(View.GONE);
                    btn_usecoupon111.setVisibility(View.GONE);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else{
            btn_usecoupon.setVisibility(View.GONE);
            btn_usecoupon111.setVisibility(View.GONE);
        }

    }



    //volley
    public void clubSpecificDetail(String url){
        loading= new ProgressDialog(demo.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("Main", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){
                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(demo.this);
                            builder1.setMessage("No data found for your search!");
                            builder1.setCancelable(false);
                            builder1.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();

                                        }
                                    });
                            alert = builder1.create();
                            alert.show();
                            Button positive =alert.getButton(AlertDialog.BUTTON_POSITIVE);
                            positive.setTextColor(getResources().getColor(R.color.female));
                        }
                        else{
                            for(int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);


                                String club_name = object1.getString("club_name");
                                str_club_name = club_name;
                                String club_rate = object1.getString("club_rate");
                                String club_time = object1.getString("club_time");
                                String club_happy_time = object1.getString("happy_time");
                                String club_cost_two = object1.getString("cost_two");
                                String club_lat = object1.getString("lat");
                                String club_lon = object1.getString("lon");
                                String club_fav = object1.getString("fav_club");
                                str_radius=object1.getString("radius");
                                club_mobileno=object1.getString("club_contact");

                                str_lat = club_lat;
                                str_long = club_lon;

                                if (club_fav.equals("1")) {
                                    img_fav.setImageResource(R.drawable.star_on_2);
                                    isChecked=true;


                                } else if (club_fav.equals("0")) {
                                    img_fav.setImageResource(R.drawable.star_off);
                                    isChecked=false;
                                }


                                txt_clubName.setText(club_name);
                                txt_clubRate.setText(club_cost_two);
                                txt_time.setText(club_time);
                                happy_time.setText(club_happy_time);
                                txt_clubStar.setRating(Float.parseFloat(club_rate));
                                //time_close

                                String[] time = club_time.split("-");

                                if (time.length == 0) {
                                    txt_time.setText("");
                                } else if (time.length == 1) {
                                    txt_time.setTextColor(getResources().getColor(R.color.light_green));
                                    txt_time.setText(club_time);
                                } else if (time.length == 2) {
                                    String start = time[0];
                                    String end = time[1];

                                    if (start.equals("Open")) {
                                        txt_time.setTextColor(getResources().getColor(R.color.light_green));
                                        txt_time.setText(start);
                                    } else {
                                        txt_time.setText(start);
                                    }

                                    if (end.equals("Closed")) {
                                        time_close.setTextColor(getResources().getColor(R.color.light_green));
                                        time_close.setText("-" + end);
                                    } else {
                                        time_close.setText("- CLOSES " + end);
                                    }


                                }

                                //user_pref
                                JSONArray prefarr = object1.getJSONArray("user_pref");
                                if (prefarr.length() == 0) {
                                    str_user_pref_array="0";

                                } else {
                                    str_user_pref_array="1";

                                    for (int p = 0; p < prefarr.length(); p++) {
                                        JSONObject object4 = prefarr.getJSONObject(p);
                                        age_pref = object4.getString("age");
                                        gender_pref = object4.getString("gender");


                                    }
                                }

                                //end user pref



                                //pica array
                                JSONArray picarray = object1.getJSONArray("club_pic");
                                clubImageList = new ArrayList<>();
                                if (picarray.length() == 0) {

                                } else {
                                    for (int j = 0; j < picarray.length(); j++) {
                                        String abc = String.valueOf(picarray.get(j));

                                        Log.d("pic_array", abc);
                                        clubImageList.add(abc);

                                    }
                                    clubImageSlider();

                                }
                                //end of pic array

                                //facility array
                                JSONArray facarray = object1.getJSONArray("facility");
                                facilityList = new ArrayList<>();
                                if (facarray.length() == 0) {
                                    facilityRel.setVisibility(View.GONE);
                                } else {
                                    facilityRel.setVisibility(View.VISIBLE);

                                    for (int k = 0; k < facarray.length(); k++) {
                                        JSONObject object3 = facarray.getJSONObject(k);
                                        FacilityModel model = new FacilityModel();

                                        String fac_name = object3.getString("fac_name");
                                        model.facilityName = fac_name;
                                        String fac_icon = object3.getString("fac_icon");
                                        model.facilityImage = fac_icon;
                                        facilityList.add(model);

                                    }
                                    facilityAdapter = new FacilityRecyclerAdapter(facilityList, demo.this);
                                    facilityRecycler.setAdapter(facilityAdapter);

                                }
                                //end of facility



                                //event array
                                JSONArray eventarr = object1.getJSONArray("event");
                                if (eventarr.length() == 0) {
                                    eventRel.setVisibility(View.GONE);

                                } else {
                                    eventRel.setVisibility(View.VISIBLE);
                                    eventList = new ArrayList<ClubEventModel>();
                                    for (int q = 0; q < eventarr.length(); q++) {

                                        JSONObject object5 = eventarr.getJSONObject(q);
                                        ClubEventModel model = new ClubEventModel();


                                        model.eventId = object5.getString("event_id");
                                        model.eventName = object5.getString("event_name");
                                        model.eventImg = object5.getString("event_img");
                                        model.eventDate = object5.getString("event_date");
                                        model.userGoing = object5.getString("user_going");
                                        model.male = object5.getString("male_count");
                                        model.female = object5.getString("female_count");
                                        model.fav = object5.getString("favourite");
                                        model.eventStartDate = object5.getString("event_start");
                                        model.eventEndDate = object5.getString("event_end");
                                        eventList.add(model);


                                    }
                                    eventAdapter = new ClubEventAdapter(eventList, demo.this);
                                    eventRecycler.setAdapter(eventAdapter);

                                }
                                //end of event

                                //going arra
                                JSONArray goingarr = object1.getJSONArray("going_arr");
                                if (goingarr.length() == 0) {
                                    goingRel.setVisibility(View.GONE);
                                } else {
                                    usergoingList = new ArrayList<>();
                                    goingRel.setVisibility(View.VISIBLE);
                                    for (int r = 0; r < goingarr.length(); r++) {
                                        JSONObject object6 = goingarr.getJSONObject(r);
                                        UserGoingModel model = new UserGoingModel();

                                        String user_id = object6.getString("user_id");
                                        String user_age = object6.getString("user_age");
                                        String user_gender = object6.getString("user_gender");
                                        String user_photo = object6.getString("user_photo");
                                        model.userId = object6.getString("user_id");
                                        model.userAge = object6.getString("user_age");
                                        model.userGender = object6.getString("user_gender");
                                        model.userPhoto = object6.getString("user_photo");

                                        usergoingList.add(model);

                                    }
                                    goingAdapter = new UserGoingAdapter(usergoingList, demo.this);
                                    goingRecycler.setAdapter(goingAdapter);
                                }

                                //end goig arra
                              /*  //today event
                                JSONArray todayevent = object1.getJSONArray("today_event");
                                if (todayevent.length() == 0) {
                                    btn_checkIn.setVisibility(View.GONE);

                                } else {
                                    btn_checkIn.setVisibility(View.VISIBLE);
                                    couponRel.setVisibility(View.VISIBLE);
                                    for (int t = 0; t < todayevent.length(); t++) {
                                        JSONObject object7 = todayevent.getJSONObject(t);

                                        String today_event_id = object7.getString("today_event_id");
                                        todayEventId=today_event_id;
                                        String today_event_start = object7.getString("today_event_start");
                                        String today_event_end = object7.getString("today_event_end");
                                        //public static String stagEntry,coupleEntry,leftCoupon;
                                        stagEntry=object7.getString("stag");
                                        coupleEntry=object7.getString("couple");
                                        leftCoupon=object7.getString("left_coupon");
                                        String total_coupon=object7.getString("coupon");
                                        str_avail_coupon=total_coupon;
                                        String checkin=object7.getString("checkin");
                                        //str_coupon_start,str_coupon_end;
                                        str_coupon_start=object7.getString("coupon_start");
                                        str_coupon_end=object7.getString("coupon_end");
                                        str_checkin_value=checkin;

                                        if(str_checkin_value.equals("0")){
                                            // btn_checkIn.setVisibility(View.VISIBLE);
                                            //  btn_usecoupon.setVisibility(View.VISIBLE);
                                            // btn_usecoupon111.setVisibility(View.VISIBLE);

                                            /////////////time comparison of coupon with current time
//
                                            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");

                                            Date EndTime = null;
                                            Date StartTime=null;
                                            try {
                                                //  EndTime = dateFormat.parse("10:00");
                                                EndTime = dateFormat.parse(str_coupon_end);
                                                StartTime=dateFormat.parse(str_coupon_start);
                                                Date CurrentTime = dateFormat.parse(dateFormat.format(new Date()));
                                                Log.d("my_cutrrent_timeeee", String.valueOf(CurrentTime));


                                                if(CurrentTime.equals(StartTime) || CurrentTime.after(StartTime)){
                                                    //coupon time start
                                                    System.out.println("timeeee started ");
                                                    btn_usecoupon.setVisibility(View.VISIBLE);
                                                    btn_usecoupon111.setVisibility(View.VISIBLE);

                                                    if( CurrentTime.after(EndTime)){
                                                        //coupon time has end
                                                        System.out.println("timeeee has end ");
                                                        btn_usecoupon.setVisibility(View.GONE);
                                                        btn_usecoupon111.setVisibility(View.GONE);
                                                    }
                                                    else if(CurrentTime.before(EndTime)){
                                                        //coupon time id still left
                                                        System.out.println("timeeee is still remaing ");
                                                        btn_usecoupon.setVisibility(View.VISIBLE);
                                                        btn_usecoupon111.setVisibility(View.VISIBLE);
                                                    }
                                                }
                                                else if(CurrentTime.before(StartTime)){
                                                    //coupon time not yet started
                                                    System.out.println("timeeee not yet started ");
                                                    btn_usecoupon.setVisibility(View.GONE);
                                                    btn_usecoupon111.setVisibility(View.GONE);
                                                }

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                        else if(str_checkin_value.equals("1")){
                                            //  btn_checkIn.setVisibility(View.GONE);
                                            btn_usecoupon.setVisibility(View.GONE);
                                            btn_usecoupon111.setVisibility(View.GONE);
                                        }

*//*
                                        str_current_event = today_event_id;
     //RelativeLayout couponRel,stagRel,coupleRel ,TextView coupon_title,txt_stag,txt_couple;ArcProgress couponProgress;
                                        if(total_coupon.equals("")){
                                            couponRel.setVisibility(View.GONE);
                                        }
                                        else{
                                            if(stagEntry.equals("NA") && !coupleEntry.equals("NA")){
                                                stagRel.setVisibility(View.GONE);
                                            }
                                            else if(coupleEntry.equals("NA") && !stagEntry.equals("NA")){
                                                coupleRel.setVisibility(View.GONE);
                                            }
                                            else if(stagEntry.equals("NA") && coupleEntry.equals("NA") && total_coupon.equals("")){
                                                couponRel.setVisibility(View.GONE);
                                            }
                                            couponProgress.setMax(Integer.parseInt(total_coupon));

                                            couponProgress.setProgress(Integer.parseInt(leftCoupon));

                                        }*//*

                                        String event_name=object7.getString("today_event_name");
                                        String club_address=object7.getString("club_address");
                                        String date=object7.getString("event_date");
                                        String club_img=object7.getString("club_img");
                                        ceventName=event_name;
                                        cdate=date;
                                        ctime=today_event_start+" - "+today_event_end;
                                        cstag=stagEntry;
                                        ccouple=coupleEntry;
                                        cimg=club_img;
                                        caddress=club_address;







                                    }
                                }


                                //end today event*/

                            }

                        }

                        String url = AllUrl.CLUB_COUPON + clubId + "&user_id=" + loginUserId;
                        String xyz = url.replace(" ", "%20");
                        clubCouponDetails(xyz);




                    }else{
                        Toast.makeText(demo.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    //current event for coupon reload
    public void clubCouponDetails(String url){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("club_coupon_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){
                        JSONArray array = object.getJSONArray("message");
                        if(array.length()==0){

                            str_current_event_array="0";
                        }
                        else{
                            str_current_event_array="1";
                            couponRel.setVisibility(View.VISIBLE);

                            for (int t = 0; t < array.length(); t++) {
                                JSONObject object7 = array.getJSONObject(t);

                                String today_event_id = object7.getString("today_event_id");
                                todayEventId=today_event_id;
                                String today_event_start = object7.getString("today_event_start");
                                String today_event_end = object7.getString("today_event_end");
                                //public static String stagEntry,coupleEntry,leftCoupon;
                                stagEntry=object7.getString("stag");
                                coupleEntry=object7.getString("couple");
                                leftCoupon=object7.getString("left_coupon");
                                String total_coupon=object7.getString("coupon");
                                str_avail_coupon=total_coupon;
                                String checkin=object7.getString("checkin");
                                //str_coupon_start,str_coupon_end;
                                str_coupon_start=object7.getString("coupon_start");
                                str_coupon_end=object7.getString("coupon_end");
                                str_checkin_value=checkin;



                                str_current_event = today_event_id;
                                Log.d("total_coupon",total_coupon);

                                if(total_coupon.equals("")){
                                    couponRel.setVisibility(View.GONE);
                                }
                                else{
                                    if(stagEntry.equals("NA") && !coupleEntry.equals("NA")){
                                        stagRel.setVisibility(View.GONE);
                                    }
                                    else if(coupleEntry.equals("NA") && !stagEntry.equals("NA")){
                                        coupleRel.setVisibility(View.GONE);
                                    }
                                    else if(stagEntry.equals("NA") && coupleEntry.equals("NA") && total_coupon.equals("")){
                                        couponRel.setVisibility(View.GONE);
                                    }
                                    couponProgress.setMax(Integer.parseInt(total_coupon));

                                    couponProgress.setProgress(Integer.parseInt(leftCoupon));

                                }

                                        String event_name=object7.getString("today_event_name");
                                        String club_address=object7.getString("club_address");
                                        String date=object7.getString("event_date");
                                        String club_img=object7.getString("club_img");
                                        ceventName=event_name;
                                        cdate=date;
                                        ctime=today_event_start+" - "+today_event_end;
                                        cstag=stagEntry;
                                        ccouple=coupleEntry;
                                        cimg=club_img;
                                        caddress=club_address;


                                handler = new Handler();
                                final Runnable r = new Runnable()
                                {
                                    public void run()
                                    {
                                        boolean flag = hasConnection();
                                        if (flag) {
                                            String url = AllUrl.CLUB_COUPON + clubId + "&user_id=" + loginUserId;
                                            Log.d("club_coupon_url",url);

                                            String xyz = url.replace(" ", "%20");
                                            // clubDetail(clubUrl);
                                            handler.postDelayed(this, 20000);
                                            clubCouponDetails(xyz);


                                        } else {
                                            handler.postDelayed(this, 1000);
                                            Toast.makeText(demo.this, "Please Check Your Internet Connection!!", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                };
                                handler.postDelayed(r, 1000);







                            }

                        }
                    }else{
                        Toast.makeText(demo.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());


            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    public void clubImageSlider() {

        // method for initialisation
        init();

        // method for adding indicators
        addBottomDots(0);

    }

    private void init() {

        club_slider = findViewById(R.id.slider);
        lin_dots = findViewById(R.id.dots);

        sliderPagerAdapter = new ClubSliderAdapter(demo.this, clubImageList);
        club_slider.setAdapter(sliderPagerAdapter);
        indicator.setViewPager(club_slider);

        club_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void addBottomDots(int currentPage) {


        dots = new TextView[clubImageList.size()];


        lin_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);

            dots[i].setText(Html.fromHtml(" &#8226&nbsp;"));
            dots[i].setTextSize(30);
            // dots[i].setTextColor(Color.parseColor("#000000"));
            dots[i].setTextColor(getResources().getColor(R.color.light_blue));
            lin_dots.addView(dots[i]);
        }

        if (dots.length > 0)

            //dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
            dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
    }





    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    private void userCheckInVolley(String url){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("check_in_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if(res.equals("200")){
                        SharedPreferences.Editor editor=getSharedPreferences(PREFS_NAME,MODE_PRIVATE).edit();
                        editor.remove("event_id");
                        editor.commit();
                        editor.putString("event_id", todayEventId);
                        editor.commit();
                        startActivity(new Intent(demo.this,Swipe.class));
                    }
                    else if (res.equals("203")) {
                        SharedPreferences.Editor editor=getSharedPreferences(PREFS_NAME,MODE_PRIVATE).edit();
                        editor.remove("event_id");
                        editor.commit();
                        editor.putString("event_id",todayEventId);
                        editor.commit();
                        startActivity(new Intent(demo.this,Swipe.class));

                    }
                    else{
                        Toast.makeText(demo.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(demo.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }

    public void setFont() {
        bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold.ttf");
        extra_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        reg_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        semi_bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBold.ttf");
        calibri_bold = Typeface.createFromAsset(getAssets(), "fonts/Calibri Bold.ttf");
        lato_bold = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");
        semi_bold_italic = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBoldItalic.ttf");

        txt_clubName.setTypeface(bold_face);
        txt_star.setTypeface(calibri_bold);
        time_label.setTypeface(calibri_bold);
        time_close.setTypeface(calibri_bold);
        txt_clubRate.setTypeface(calibri_bold);
        cost_label.setTypeface(calibri_bold);
        btn_checkIn.setTypeface(bold_face);
        txt_time.setTypeface(calibri_bold);
        fac_label.setTypeface(calibri_bold);
        happy_label.setTypeface(calibri_bold);
        happy_time.setTypeface(calibri_bold);
        coupon_title.setTypeface(calibri_bold);
        coupon_title.setTypeface(calibri_bold);
        coupon_title.setTypeface(calibri_bold);
        txt_going.setTypeface(calibri_bold);






    }


    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    protected synchronized void buildGoogleApiClient() {
        //  Toast.makeText(this, "buildGoogleApiClient", Toast.LENGTH_SHORT).show();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        // Toast.makeText(this, "onConnected", Toast.LENGTH_SHORT).show();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10);
        mLocationRequest.setFastestInterval(10);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(0.1F);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }




    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.

            // mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.clubmap);
            mapFragment.getMapAsync(this);
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        circle();

    }

    public void circle()
    {
        double radiusInMeters = 50.0;
        int strokeColor = 0xffff0000; //red outline
        int shadeColor = 0x44ff0000; //opaque red fill


        if(!str_lat.equals("") && !str_long.equals("")){
            double lat= Double.parseDouble(str_lat);
            double lng= Double.parseDouble(str_long);


            mCircle = mMap.addCircle (new CircleOptions()
                    //.center(new LatLng(19.1987507,72.9752881)) //green guru
                    // .center(new LatLng(19.197906,72.9754203))   //appmonks
                    .center(new LatLng(lat,lng))
                    .radius(radiusInMeters)
                    .fillColor(shadeColor)
                    .strokeColor(strokeColor)
                    .strokeWidth(1));
        }


    }

    @Override
    public void onConnectionSuspended(int i) {
        // Toast.makeText(this,"onConnectionSuspended",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        Log.d("locationtesting",  "lat: " + location.getLatitude() + " lon: " + location.getLongitude());

        circle();
        float[] distance = new float[2];
        if(!str_lat.equals("") && !str_long.equals("")){

            Log.d("distance_value", String.valueOf(distance[0]));
            Log.d("radius of circle", String.valueOf(mCircle.getRadius()));


            float[] results = new float[1];
            //green guru
            //  Location.distanceBetween(19.197906, 72.9754203, 19.1987507, 72.9752881, results);
            //vivianan
            //   Location.distanceBetween(19.197906, 72.9754203, 19.2085718, 72.9695301, results);
            //

            double lat= Double.parseDouble(str_lat);
            double lng= Double.parseDouble(str_long);

            Location.distanceBetween(lat,lng,location.getLatitude(),location.getLongitude(), results);
            float distanceInMeters = results[0];
            boolean isWithin10km = distanceInMeters < 10000;

            // boolean isWithin1km = distanceInMeters < 1000;
            int radius= Integer.parseInt(str_radius);
             isWithin1km = distanceInMeters < radius;
            Log.d("iswithinradius", String.valueOf(isWithin10km));
            Log.d("currenteventid",todayEventId);





        }


    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        setUpMapIfNeeded();
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    //post method
    private void postfavclubvolley(final String user_id, final String fav_id, final String mode, final String status){
        StringRequest postRequest = new StringRequest(Request.Method.POST,AllUrl.FAV_CLUB,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("fav_club_response", response);
                        // loading.dismiss();
                        try {
                            JSONObject object = new JSONObject(response);
                            String res = object.getString("response");
                            String message = object.getString("message");
                            if (res.equals("200")) {

                            }
                            else{
                                Toast.makeText(getApplicationContext(),"Failed to make club favourite.Please try again!",Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        loading.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> data = new HashMap<>();
                // the POST parameters:

                data.put("user_id",user_id);
                data.put("fav_id",fav_id);
                data.put("mode",mode);
                data.put("status",status);

                return data;
            }

        };

        Volley.newRequestQueue(getApplicationContext()).getCache().clear();

        Volley.newRequestQueue(getApplicationContext()).add(postRequest);


    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }


}
