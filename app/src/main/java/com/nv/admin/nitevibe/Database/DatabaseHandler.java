package com.nv.admin.nitevibe.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 4/4/2018.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "nitevibe_setting";
    private static final String TABLE_SETTING = "setting";
    private static final String LABEL = "label";
    private static final String DATA = "data";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_USER_TABLE = " CREATE TABLE "+ TABLE_SETTING + "(" + LABEL +
                " TEXT, " + DATA +" TEXT "+ ")";
        sqLiteDatabase.execSQL(CREATE_USER_TABLE);

        sqLiteDatabase.execSQL("INSERT INTO "+TABLE_SETTING+" VALUES ('mute','false');");
        sqLiteDatabase.execSQL("INSERT INTO "+TABLE_SETTING+" VALUES('show','true');");
        sqLiteDatabase.execSQL("INSERT INTO "+TABLE_SETTING+" VALUES('message','true');");
        sqLiteDatabase.execSQL("INSERT INTO "+TABLE_SETTING+" VALUES('match','true');");
        sqLiteDatabase.execSQL("INSERT INTO "+TABLE_SETTING+" VALUES('event','true');");
        sqLiteDatabase.execSQL("INSERT INTO "+TABLE_SETTING+" VALUES('location','true');");
        sqLiteDatabase.execSQL("INSERT INTO "+TABLE_SETTING+" VALUES('checkin','true');");


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " +" "+ TABLE_SETTING);
        onCreate(sqLiteDatabase);

    }


    //ADDING THE NEW USER
    public void addUser(SettingModel user){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(LABEL,user.getLabel());
        contentValues.put(DATA,user.getData());
        db.insert(TABLE_SETTING,null,contentValues);
        //closing the db connection
        db.close();

    }

    //GET ALL USERS
    public List<SettingModel> getAllData(){
        List<SettingModel> userList=new ArrayList<SettingModel>();
        String selectQuery=" SELECT * FROM "+" "+" "+ TABLE_SETTING;
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(selectQuery,null);

        //looping through all the loop and adding it to list
        if(cursor.moveToFirst()){
            do{
                SettingModel user=new SettingModel();

                user.setLabel(cursor.getString(0));
                user.setData(cursor.getString(1));
                userList.add(user);
            }while (cursor.moveToNext());
        }
        return userList;
    }

    //update value
    public int updateUser(SettingModel user){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();


        contentValues.put(LABEL,user.getLabel());
        contentValues.put(DATA,user.getData());


        // updating row
        return db.update(TABLE_SETTING, contentValues, LABEL + " = ?", new String[]{String.valueOf(user.getLabel())});
    }

    //get particular value

}
