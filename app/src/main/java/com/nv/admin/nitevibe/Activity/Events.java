package com.nv.admin.nitevibe.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nv.admin.nitevibe.Adapter.EventAdapter;
import com.nv.admin.nitevibe.Adapter.EventModel;
import com.nv.admin.nitevibe.Adapter.EventSliderAdapter;
import com.nv.admin.nitevibe.Adapter.WhosGoingModel;
import com.nv.admin.nitevibe.OverlayTutorial.TutoShowcase;
import com.nv.admin.nitevibe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Events extends AppCompatActivity {

    AlertDialog alert;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    public List<String> imgList;
  //  public List<EventModel> eventList;
    public ArrayList<EventModel> eventList;
    EventAdapter adapter;
    public static String club_id,event_id;
    public List<WhosGoingModel> goingList;
    public List<WhosGoingModel> fewGoingList;
    public RecyclerView eventRecycler;
    RecyclerView.LayoutManager mLayoutManager;
    public static String eventUrl;
    ProgressDialog loading;
    public ViewPager event_slider;
    EventSliderAdapter eventAdapter;
    public static String userCameFrom;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);

        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");
        //value comming from profileEventAdapter/
        club_id=sharedpreferences.getString("club_id","");
        //value comming from clubEventAdapter/profileEventAdapter
        event_id=sharedpreferences.getString("event_id","");

        Log.d("club_value",event_id+" c:"+club_id);
        String eventOverlay=sharedpreferences.getString("eventoverlay","");

        if(eventOverlay.equals("")){
            displayTutogoing();
        }

        event_slider= findViewById(R.id.event_slider);

        eventRecycler= findViewById(R.id.eventRecycler);
        eventRecycler.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        eventRecycler.setLayoutManager(mLayoutManager);


          Bundle extra=getIntent().getExtras();

        if(extra!=null){
            //value comming from BannerAdapter and ViewProfileEventAdapter
           String banner_club=extra.getString("banner_club_id");
            String banner_event=extra.getString("banner_event_id");
            String banner_from=extra.getString("banner_from");
            Log.d("intent_values",banner_club+" "+banner_event);

            if(banner_from.equals("banner")){
                eventUrl=AllUrl.BANNER_EVENTS+loginUserId+"&selected_id="+banner_event;
            }
            else if(banner_from.equals("viewprofile")){
                eventUrl=AllUrl.EVENTS+loginUserId+"&club_id="+banner_club+"&selected_id="+banner_event;
            }
          /*  else{
                eventUrl=AllUrl.EVENTS+loginUserId+"&club_id="+club_id+"&selected_id="+event_id;
            }*/


            boolean flag=hasConnection();
            if(flag){
                //  String url=AllUrl.EVENTS+loginUserId+"&club_id="+club_id+"&selected_id="+event_id;
                // Log.d("event_list_url",url);
                //eventList(url);
                Log.d("event_url",eventUrl);
              //  eventList(eventUrl,"banner");
                eventListVolley(eventUrl,banner_from);
                userCameFrom=banner_from;

            }
            else{
                Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
            }
        }
        else{
            eventUrl=AllUrl.EVENTS+loginUserId+"&club_id="+club_id+"&selected_id="+event_id;

            boolean flag=hasConnection();
            if(flag){
                //  String url=AllUrl.EVENTS+loginUserId+"&club_id="+club_id+"&selected_id="+event_id;
                // Log.d("event_list_url",url);
                //eventList(url);
                Log.d("event_url",eventUrl);
              //  eventList(eventUrl,"event");
                eventListVolley(eventUrl,"event");
                userCameFrom="event";

            }
            else{
                Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
            }
        }


        Log.d("Event_url",eventUrl);

    }

    protected void displayTuto() {
        TutoShowcase.from(this)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        // Toast.makeText(Club.this, "Tutorial dismissed", Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("ratiooverlay","1");
                        editor.commit();

                    }
                })
                .setContentView(R.layout.eventoverlay)
                .setFitsSystemWindows(true)
                .on(R.id.abc)
                .addRoundRect()

                .on(R.id.abc)
                .displaySwipableLeft()
                .delayed(399)
                .animated(true)
                .show();
    }

    protected void displayTutogoing() {
        TutoShowcase.from(this)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        // Toast.makeText(Club.this, "Tutorial dismissed", Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("eventoverlay","1");
                        editor.commit();
                    }
                })
                .setContentView(R.layout.goingoverlay)
                .setFitsSystemWindows(true)
                .on(R.id.abc)
                .on(R.id.btn)
                .addRoundRect()

                .on(R.id.abc)
                .displaySwipableLeft()
                .delayed(399)
                .animated(true)
                .show();
    }



    ///displaying event information

    private void eventListVolley(String url,final String type){

        loading= new ProgressDialog(Events.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("event_list_response", response.toString());
                loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    if(res.equals("200")){
                        JSONArray array = object.getJSONArray("message");
                        eventList=new ArrayList<EventModel>();

                        if(array.length()==0){


                        }
                        else{
                            for (int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);
                                EventModel model1=new EventModel();

                                String event_id=object1.getString("event_id");
                                String event_name=object1.getString("event_name");
                                String event_date=object1.getString("event_date");
                                String event_des=object1.getString("event_des");
                                String event_total=object1.getString("total");
                                String event_stag=object1.getString("stag_entry");
                                String event_couple=object1.getString("couple_entry");
                                String event_male_count=object1.getString("male_count");
                                String event_female_count=object1.getString("female_count");
                                club_id=object1.getString("club_id");

                                model1.clubId=object1.getString("club_id");
                                model1.eventId=object1.getString("event_id");
                                model1.eventName=object1.getString("event_name");
                                model1.eventDate=object1.getString("event_date");
                                model1.eventDesc=object1.getString("event_des");
                                model1.eventTotal=object1.getString("total");
                                String total123=object1.getString("total");
                                int total_people= Integer.parseInt(total123);

                                if(total_people>=1){
                                    String ratio_overlay=sharedpreferences.getString("ratiooverlay","");
                                    String eventoverlay=sharedpreferences.getString("eventoverlay","");
                                    if(eventoverlay.equals("1")){
                                        if(ratio_overlay.equals("")){
                                            displayTuto();
                                        }
                                    }

                                }
                                model1.eventStag=object1.getString("stag_entry");
                                model1.eventCouple=object1.getString("couple_entry");
                                model1.eventMale=object1.getString("male_count");
                                model1.eventFemale=object1.getString("female_count");

                            //   public String clubAddress,coupon,date,time,img;
                                model1.clubAddress=object1.getString("club_address");
                                model1.coupon=object1.getString("coupon");
                                model1.date=object1.getString("date");
                                model1.time=object1.getString("event_time");
                                model1.img=object1.getString("club_pic");
                                model1.leftcoupon=object1.getString("left_coupon");
                                model1.coupontime=object1.getString("coupon_time");

                                model1.type=type;

                                JSONArray prefArray=object1.getJSONArray("pref_arr");
                                String user_age="";
                                String userPref="";
                                if(prefArray.length()==0){

                                    user_age="";
                                    userPref="";
                                }
                                else{
                                    for (int k=0;k<prefArray.length();k++){
                                        JSONObject object2 = prefArray.getJSONObject(k);

                                        user_age=object2.getString("age");
                                        userPref=object2.getString("gender");

                                    }
                                }

                                model1.userAge=user_age;
                                model1.userGenderPref=userPref;

                                //end of user pref

                                ///event_pic
                                JSONArray picArray=object1.getJSONArray("event_pic");
                                imgList=new ArrayList<>();
                                if(picArray.length()==0){

                                }
                                else{
                                    for (int j=0;j<picArray.length();j++){
                                        JSONObject object2 = picArray.getJSONObject(j);

                                        String img_url=object2.getString("img");
                                        imgList.add(img_url);

                                    }
                                    model1.eventImages=imgList;
                                    // model1.eventImages.add(String.valueOf(imgList));
                                }

                                //whos going array
                                JSONArray goingArray=object1.getJSONArray("going_arr");

                                goingList=new ArrayList<>();
                                fewGoingList=new ArrayList<>();

                                if(goingArray.length()==0){
                                    model1.goingList=goingList;
                                    model1.fewGoingList=fewGoingList;

                                }
                                else{
                                    for(int g=0;g<goingArray.length();g++){
                                        JSONObject object3=goingArray.getJSONObject(g);
                                        WhosGoingModel model=new WhosGoingModel();

                                        model.userGoingId=object3.getString("user_going_id");
                                        model.userGoingName=object3.getString("name");
                                        model.userGoingProfile=object3.getString("profile");
                                        model.userGoingAge=object3.getString("age");
                                        model.userGoingGender=object3.getString("gender");
                                        model.userGoingCategory=object3.getString("category");
                                        model.userGoingEntry=object3.getString("entry");
                                        goingList.add(model);

                                        if(g<=4){
                                            fewGoingList.add(model);
                                        }
                                    }

                                    model1.goingList=goingList;
                                    model1.fewGoingList=fewGoingList;

                                }


                                eventList.add(model1);

                            } //end of main for

                           // adapter=new EventAdapter(eventList,imgList,goingList,fewGoingList,Events.this);
                           // eventRecycler.setAdapter(adapter);

                         /*   this.activity = activity;
                            this.arrayList = arrayList;
                            this.goingList = goingList;
                            this.fewGoingList = fewGoingList;
                            this.eventImages = eventImages;*/

                            eventAdapter = new EventSliderAdapter(Events.this,eventList,goingList,fewGoingList,imgList);
                            event_slider.setAdapter(eventAdapter);
                            event_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                @Override
                                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                }

                                @Override
                                public void onPageSelected(int position) {
                                    //addBottomDots(position);
                                }

                                @Override
                                public void onPageScrollStateChanged(int state) {

                                }
                            });

                        }
                    }else{
                        Toast.makeText(Events.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    @Override
    public void onBackPressed() {
        if(userCameFrom.equals("event")){

            startActivity(new Intent(Events.this,ClubEventDetails.class));
        }
        else if(userCameFrom.equals("banner")){

            startActivity(new Intent(Events.this,Club.class));
        }
        else if(userCameFrom.equals("viewprofile")){
            super.onBackPressed();

        }

       // startActivity(new Intent(Events.this,ClubEventDetails.class));
    }
}
