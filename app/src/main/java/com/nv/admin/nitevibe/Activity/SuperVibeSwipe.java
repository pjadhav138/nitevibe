package com.nv.admin.nitevibe.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nv.admin.nitevibe.R;
import com.nv.admin.nitevibe.custom.StoriesProgressView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SuperVibeSwipe extends AppCompatActivity implements View.OnClickListener, StoriesProgressView.StoriesListener {

    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId, loginUserMode, loginUserType, notifyEventId, notifyUserId;
    public static Typeface bold_face, extra_bold_face, reg_face, semi_bold_face, calibri_bold, lato_bold, semi_bold_italic;
    public PopupWindow popupWindow;
    AlertDialog alert;
    ProgressDialog loading;
    public static String userId, userSupervibe;
    public static ArrayList<String> imageList;
    private StoriesProgressView storiesProgressView;
    private int counter = 0;
    long pressTime = 0L;
    long limit = 500L;
    public static View skip, reverse;
    private final long[] durations = new long[]{
            500L, 1000L, 1500L, 4000L, 5000L, 1000,
    };
    private static final int PROGRESS_COUNT = 2;
    public RelativeLayout mRelativeLayout, rel_overlay;
    public static ImageView img_main_back;


    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();
                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
                    storiesProgressView.resume();
                    return limit < now - pressTime;
            }
            return false;
        }
    };
    public static ImageView img_back, img_info;
    public static RoundedImageView img_user, img_overlaybg;
    public static ImageView img_like, img_dislike, img_supervibe, img_stamp;
    public static TextView txt_name, txt_occupation, txt_city, txt_find;
    public static RelativeLayout rel_supervibe, rel_empty, rel_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_vibe_swipe);

        sharedpreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");
        notifyEventId = sharedpreferences.getString("notify_event", "");
        notifyUserId = sharedpreferences.getString("notify_user", "");

        storiesProgressView = findViewById(R.id.profile_slider);
        skip = findViewById(R.id.skip);
        reverse = findViewById(R.id.reverse);
        img_back = findViewById(R.id.back);
        img_user = findViewById(R.id.card_image);
        img_main_back = findViewById(R.id.back_img);
        img_info = findViewById(R.id.info);
        txt_name = findViewById(R.id.card_name);
        txt_occupation = findViewById(R.id.card_occupation);
        txt_city = findViewById(R.id.card_city);
        img_like = findViewById(R.id.like);
        img_dislike = findViewById(R.id.dislike);
        img_supervibe = findViewById(R.id.superlike);
        rel_supervibe = findViewById(R.id.supervibe_anim);
        rel_empty = findViewById(R.id.emptyRel);
        txt_find = findViewById(R.id.find_txt);
        mRelativeLayout = findViewById(R.id.rel0);
        rel_overlay = findViewById(R.id.overlay_rel);
        img_overlaybg = findViewById(R.id.img_bg);
        img_stamp = findViewById(R.id.img_stamp);
        rel_card = findViewById(R.id.cardLayout);


        bold_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold.ttf");
        reg_face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");

        txt_name.setTypeface(bold_face);
        txt_occupation.setTypeface(bold_face);
        txt_city.setTypeface(reg_face);

        onClickEvent();
        reverse.setOnTouchListener(onTouchListener);
        skip.setOnTouchListener(onTouchListener);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storiesProgressView.skip();
                onNext();
            }
        });
        skip.setOnTouchListener(onTouchListener);

        reverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storiesProgressView.reverse();
                onPrev();
            }
        });
        reverse.setOnTouchListener(onTouchListener);


        boolean flag = hasConnection();

        if (flag) {

            String url = AllUrl.WHO_SEND_SUPERVIBE + loginUserId + "&user_id=" + notifyUserId + "&event_id=" + notifyEventId;
            String url1 = url.replaceAll(" ", "%20");
            Log.d("supervibe_url", url1);
            getUserInfoVolley(url1);

        } else {
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
        }

        if (counter > 0) {
            storiesProgressView.setStoriesListener(this);
            storiesProgressView.startStories();
            storiesProgressView.setStoriesCount(PROGRESS_COUNT);
            storiesProgressView.setStoryDuration(1000L);
        }


    }

    public void onClickEvent() {
        img_like.setOnClickListener(this);
        img_dislike.setOnClickListener(this);
        img_supervibe.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_info.setOnClickListener(this);
        rel_supervibe.setOnClickListener(this);
        txt_find.setOnClickListener(this);
        img_main_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.back_img) {
            Intent i = new Intent(SuperVibeSwipe.this, Club.class);
            startActivity(i);
        }
        if (id == R.id.back) {
            //startActivity(new Intent(SuperVibeSwipe.this,Club.class));
            Intent i = new Intent(SuperVibeSwipe.this, Club.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
        if (id == R.id.info) {
            SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
            editor.remove("userid");
            editor.remove("from");
            editor.commit();
            editor.putString("userid", userId);
            editor.putString("from", "SuperVibeSwipe");
            editor.commit();
            startActivity(new Intent(SuperVibeSwipe.this, View_Profile.class));
        }
        if (id == R.id.dislike) {

            boolean flag = hasConnection();
            if (flag) {
                //initiateBg("dislike");  rel_overlay,img_overlaybg,img_stamp
                rel_overlay.setVisibility(View.VISIBLE);
                /*img_overlaybg.setBackground(getResources().getDrawable(R.drawable.dislike_bg));
                img_stamp.setImageDrawable(getResources().getDrawable(R.drawable.dislike));*/
                if (android.os.Build.VERSION.SDK_INT >= 21) {

                    img_overlaybg.setBackgroundDrawable(getResources().getDrawable(R.drawable.dislike_bg, getTheme()));
                    img_stamp.setBackgroundDrawable(getResources().getDrawable(R.drawable.dislike, getTheme()));

                } else {

                    img_overlaybg.setBackgroundDrawable(getResources().getDrawable(R.drawable.dislike_bg));
                    img_stamp.setBackgroundDrawable(getResources().getDrawable(R.drawable.dislike));

                }
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        rel_overlay.setVisibility(View.GONE);
                        String url = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + notifyUserId + "&category=2&event_id=" + notifyEventId;
                        Log.d("swipe_url", url);
                        insertSwipeData(url, "");
                    }
                }, 1000);

            } else {
                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
            }

        }
        if (id == R.id.superlike) {
            boolean flag = hasConnection();
            if (flag) {
                if (userSupervibe.equals("3")) {
                    Toast.makeText(getApplicationContext(), "No supervibes left", Toast.LENGTH_SHORT).show();
                } else {
                    //  StartSmartAnimation.startAnimation(view, AnimationType.BounceInDown, 1000,0,false,1500);
                    rel_overlay.setVisibility(View.VISIBLE);
                    /*img_overlaybg.setBackground(getResources().getDrawable(R.drawable.superlike_bg));
                    img_stamp.setImageDrawable(getResources().getDrawable(R.drawable.superlike_stamp));*/

                    if (android.os.Build.VERSION.SDK_INT >= 21) {

                        img_overlaybg.setBackgroundDrawable(getResources().getDrawable(R.drawable.superlike_bg, getTheme()));
                        img_stamp.setBackgroundDrawable(getResources().getDrawable(R.drawable.superlike_stamp, getTheme()));

                    } else {

                        img_overlaybg.setBackgroundDrawable(getResources().getDrawable(R.drawable.superlike_bg));
                        img_stamp.setBackgroundDrawable(getResources().getDrawable(R.drawable.superlike_stamp));

                    }

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            rel_overlay.setVisibility(View.GONE);
                            String url = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + notifyUserId + "&category=3&event_id=" + notifyEventId;
                            Log.d("swipe_url", url);
                            insertSwipeData(url, "vibes");

                        }
                    }, 1000);
                }

            } else {
                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
            }
        }
        if (id == R.id.like) {
            boolean flag = hasConnection();
            if (flag) {
                //  initiateBg("like");
                rel_overlay.setVisibility(View.VISIBLE);
               /* img_overlaybg.setBackground(getResources().getDrawable(R.drawable.like_bg));
                img_stamp.setImageDrawable(getResources().getDrawable(R.drawable.like));*/
                if (android.os.Build.VERSION.SDK_INT >= 21) {

                    img_overlaybg.setBackgroundDrawable(getResources().getDrawable(R.drawable.like_bg, getTheme()));
                    img_stamp.setBackgroundDrawable(getResources().getDrawable(R.drawable.like, getTheme()));

                } else {

                    img_overlaybg.setBackgroundDrawable(getResources().getDrawable(R.drawable.like_bg));
                    img_stamp.setBackgroundDrawable(getResources().getDrawable(R.drawable.like));

                }
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        rel_overlay.setVisibility(View.GONE);
                        String url = AllUrl.SWIPE + loginUserId + "&swipe_user_id=" + notifyUserId + "&category=1&event_id=" + notifyEventId;
                        Log.d("swipe_url", url);
                        insertSwipeData(url, "");

                    }
                }, 1000);


            } else {
                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
            }

        }
        if (id == R.id.supervibe_anim) {


        }
        if (id == R.id.find_txt) {

            Intent i = new Intent(SuperVibeSwipe.this, Club.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }


    }

    private void insertSwipeData(String url, final String vibes) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("swipe_userdataresponse", response.toString());
                //  loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {
                        JSONArray array = object.getJSONArray("message");
                        if (array.length() == 0) {
                            rel_card.setVisibility(View.GONE);
                            rel_empty.setVisibility(View.VISIBLE);
                        } else {
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                String user_id = object1.getString("user_id");
                                String user_photo = object1.getString("user_photo");
                                String match_id = object1.getString("match_id");
                                String match_photo = object1.getString("match_photo");

                                String url = AllUrl.MATCH_NOTIFICATION + loginUserId + "&match_id=" + match_id;
                                matchNotification(url);


                                initiatePopupWindow(user_id, user_photo, match_id, match_photo);
                            }
                        }


                    } else {
                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                // loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void matchNotification(String url) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("matchnotify_response", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg = object.getString("message");
                    if (res.equals("200")) {
                        Log.d("notify_msg", "sucess");

                    } else {
                        // Toast.makeText(ServiceNoDelay.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    //  Toast.makeText(ServiceNoDelay.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());
                // Toast.makeText(ServiceNoDelay.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog


            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    private void initiatePopupWindow(String userId, String userPhoto, String matchId, String matchPhoto) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        // Inflate the custom layout/view
        View customView = inflater.inflate(R.layout.profile_match_row, null);


        popupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        );

        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            popupWindow.setElevation(5.0f);
        }

        // Get a reference for the custom view close button

        CircleImageView userImg = customView.findViewById(R.id.girl_profile);
        CircleImageView matchImg = customView.findViewById(R.id.boy_profile);

        TextView chat = customView.findViewById(R.id.chat);
        TextView swipe = customView.findViewById(R.id.swipe);
        TextView txt = customView.findViewById(R.id.txt);
        RoundedImageView profileimg = customView.findViewById(R.id.profile);

        txt.setTypeface(semi_bold_italic);
        swipe.setTypeface(bold_face);
        chat.setTypeface(bold_face);

        Log.d("userPhoto", userPhoto);
        Log.d("match_photo", matchPhoto);


        if (userPhoto.equals("")) {

        } else {


            Glide.with(this).load(userPhoto).into(userImg);


        }

        if (matchPhoto.equals("")) {

        } else {


            Glide.with(this).load(matchPhoto).into(matchImg);


        }

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SuperVibeSwipe.this, Chat.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

                // startActivity(new Intent(SuperVibeSwipe.this,Chat.class));
            }
        });

        swipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();
                rel_card.setVisibility(View.GONE);
                rel_empty.setVisibility(View.VISIBLE);

            }
        });

        popupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);


    }


    @Override
    public void onNext() {
        if (counter == imageList.size() - 1) return;

        Picasso.with(this)
                .load(imageList.get(++counter))
                .into(img_user);

        Log.d("value_of_counter", String.valueOf(counter));
    }

    @Override
    public void onPrev() {
        if ((counter - 1 < 0)) return;
        Picasso.with(this)
                .load(imageList.get(--counter))
                .into(img_user);

    }

    @Override
    public void onComplete() {

    }

    @Override
    protected void onDestroy() {
        // Very important !
        storiesProgressView.destroy();
        super.onDestroy();
    }

    private void getUserInfoVolley(String url) {

        loading = new ProgressDialog(SuperVibeSwipe.this, R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("user_profile_response", response.toString());
                loading.dismiss();
                try {
                    //rel_empty.setVisibility(View.GONE);
                    rel_card.setVisibility(View.VISIBLE);

                    // Parsing json object response
                    // response will be a json object
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String res = jsonObject.getString("response");
                    JSONArray array = jsonObject.getJSONArray("message");
                    if (array.length() == 0) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(SuperVibeSwipe.this);
                        builder1.setMessage("No data found for your search.");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        // finish();
                                    }
                                });
                        alert = builder1.create();
                        alert.show();
                        Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                        positive.setTextColor(getResources().getColor(R.color.female));
                    } else {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            //public static String userId,userSupervibe;
                            userId = object.getString("user_id");
                            String userName = object.getString("user_name");
                            String userAge = object.getString("user_age");
                            String userLoc = object.getString("user_loc");
                            String userOccupation = object.getString("user_occupation");
                            String userBio = object.getString("user_bio");
                            String userProfilepic = object.getString("profile_url");
                            userSupervibe = object.getString("superlikes");

                            //  public static TextView txt_name,txt_occupation,txt_city,txt_find;

                            if (userAge.equals("")) {
                                txt_name.setText(userName);
                            } else {
                                txt_name.setText(userName + " ," + userAge);
                            }

                            txt_occupation.setText(userOccupation);
                            txt_city.setText(userLoc);

                            if (userSupervibe.equals("0")) {
                                if (android.os.Build.VERSION.SDK_INT >= 21) {

                                    img_supervibe.setBackgroundDrawable(getResources().getDrawable(R.drawable.supervibe3, getTheme()));

                                } else {
                                    img_supervibe.setBackgroundDrawable(getResources().getDrawable(R.drawable.supervibe3));

                                }

                            } else if (userSupervibe.equals("1")) {

                                if (android.os.Build.VERSION.SDK_INT >= 21) {

                                    img_supervibe.setBackgroundDrawable(getResources().getDrawable(R.drawable.supervibe2, getTheme()));
                                } else {
                                    img_supervibe.setBackgroundDrawable(getResources().getDrawable(R.drawable.supervibe2));

                                }
                            } else if (userSupervibe.equals("2")) {
                                if (android.os.Build.VERSION.SDK_INT >= 21) {
                                    img_supervibe.setBackgroundDrawable(getResources().getDrawable(R.drawable.supervibe1, getTheme()));
                                } else {
                                    img_supervibe.setBackgroundDrawable(getResources().getDrawable(R.drawable.supervibe1));

                                }
                            } else if (userSupervibe.equals("3")) {

                                if (android.os.Build.VERSION.SDK_INT >= 21) {
                                    img_supervibe.setBackgroundDrawable(getResources().getDrawable(R.drawable.supervibe0, getTheme()));
                                } else {
                                    img_supervibe.setBackgroundDrawable(getResources().getDrawable(R.drawable.supervibe0));

                                }


                            } else {
                                // holder.supervibe.setBackgroundDrawable(getContext().getDrawable(R.drawable.supervibe3));
                            }

                            //photos array
                            JSONArray photoarr = object.getJSONArray("user_photos");
                            storiesProgressView.setStoriesCount(photoarr.length());
                            storiesProgressView.setStoriesListener(SuperVibeSwipe.this);
                            storiesProgressView.startStories();

                            if (photoarr.length() == 0) {

                            } else {
                                imageList = new ArrayList<>();
                                for (int j = 0; j < photoarr.length(); j++) {
                                    JSONObject object1 = photoarr.getJSONObject(j);

                                    String profile = object1.getString("img");
                                    imageList.add(profile);
                                }

                                Log.d("size_of_photoarr", String.valueOf(photoarr.length()));

                                Picasso.with(SuperVibeSwipe.this)
                                        .load(imageList.get(counter))
                                        .into(img_user);
                            }


                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }


    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }
}
