package com.nv.admin.nitevibe.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;

public class EntryDetails extends AppCompatActivity {
    public static ImageView back;
    public static TextView txt_stag_rate,txt_couple_rate,txt_stag_label,txt_couple_label;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;
    public static String stag_rate,couple_rate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_details);

        txt_stag_rate= findViewById(R.id.stag_rate);
        txt_couple_rate= findViewById(R.id.couple_rate);

        txt_stag_label= findViewById(R.id.stag_txt);
        txt_couple_label= findViewById(R.id.couple_txt);

        back= findViewById(R.id.back);

        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");


        Bundle extra=getIntent().getExtras();
        if(extra!=null){
            //values coming from EventAdapter
            stag_rate=extra.getString("stag");
            couple_rate=extra.getString("couple");

            if(stag_rate.equals("NA")){
                txt_stag_rate.setTypeface(semi_bold_italic);
                txt_stag_rate.setTextColor(getResources().getColor(R.color.pink));
                txt_stag_rate.setText("Not allowed");
            }
            else if(stag_rate.equals("0")){
                txt_stag_rate.setTypeface(semi_bold_italic);
                txt_stag_rate.setTextColor(getResources().getColor(R.color.pink));
                txt_stag_rate.setText("Free");
            }
            else{
                txt_stag_rate.setTypeface(semi_bold_face);
                txt_stag_rate.setText(stag_rate);

            }
            if(couple_rate.equals("NA")){
                txt_couple_rate.setTypeface(semi_bold_italic);
                txt_couple_rate.setTextColor(getResources().getColor(R.color.pink));
                txt_couple_rate.setText("Not allowed");
            }
            else if(couple_rate.equals("0")){
                txt_couple_rate.setTypeface(semi_bold_italic);
                txt_couple_rate.setTextColor(getResources().getColor(R.color.pink));
                txt_couple_rate.setText("Free");
            }
            else{
                txt_couple_rate.setTypeface(semi_bold_face);
                txt_couple_rate.setText(couple_rate);

            }

        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EntryDetails.this,Events.class));

            }
        });
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(EntryDetails.this,Events.class));
        super.onBackPressed();
    }
}
