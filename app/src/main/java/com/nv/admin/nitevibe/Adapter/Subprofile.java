package com.nv.admin.nitevibe.Adapter;

/**
 * Created by Admin on 5/8/2018.
 */
public class Subprofile {
    public String img,img_id;

    public Subprofile(){}

    public Subprofile(String img, String img_id) {
        this.img = img;
        this.img_id = img_id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg_id() {
        return img_id;
    }

    public void setImg_id(String img_id) {
        this.img_id = img_id;
    }
}
