package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.StrictMode;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nv.admin.nitevibe.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Admin on 4/11/2018.
 */
public class MatchListAdapter  extends RecyclerView.Adapter<MatchListAdapter.MyView> {
    private List<MatchProfileModel> arrayList;
    private Context context;
    MatchProfileModel current;

    public MatchListAdapter(List<MatchProfileModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public MatchListAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.match_list_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MatchListAdapter.MyView holder, int position) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        current=arrayList.get(position);
        String user_profile=current.match_profile;

        String category_came=current.matchCategory;

        if(user_profile.equals("")){
         /*   Bitmap icon = BitmapFactory.decodeResource(context.getResources(),R.drawable.default_icon);

            RoundedBitmapDrawable roundedImageDrawable = createRoundedBitmapImageDrawableWithBorder(icon,1);
            holder.matchImg.setImageDrawable(roundedImageDrawable);*/
            holder.matchImg.setImageDrawable(context.getResources().getDrawable(R.drawable.circle_profile));

        }
        else{
            Bitmap img = getBitmapFromURL(user_profile);
           // holder.matchImg.setImageBitmap(img);
            RoundedBitmapDrawable roundedImageDrawable = createRoundedBitmapImageDrawableWithBorder(img,5);
            holder.matchImg.setImageDrawable(roundedImageDrawable);

        }

         if(category_came.equals("3")){
          holder.supervibe_img.setVisibility(View.VISIBLE);
         }
         else{
            holder.supervibe_img.setVisibility(View.GONE);
         }

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        //MatchRoundRect matchImg;
        //RoundedImageView matchImg;
        //CircleImageView matchImg;
        ImageView matchImg;
        ImageView supervibe_img;
        public MyView(View itemView) {
            super(itemView);
           // matchImg=(RoundedImageView)itemView.findViewById(R.id.match_img);
          //   matchImg=(CircleImageView)itemView.findViewById(R.id.match_img);
            matchImg= itemView.findViewById(R.id.match_img);
            supervibe_img= itemView.findViewById(R.id.supervibe);
        }
    }

    //convert image url to bitmap
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private RoundedBitmapDrawable createRoundedBitmapImageDrawableWithBorder(Bitmap bitmap,int widthValue){
        int bitmapWidthImage = bitmap.getWidth();
        int bitmapHeightImage = bitmap.getHeight();
        int borderWidthHalfImage = 4;

        int bitmapRadiusImage = Math.min(bitmapWidthImage,bitmapHeightImage)/2;
        int bitmapSquareWidthImage = Math.min(bitmapWidthImage,bitmapHeightImage);
        int newBitmapSquareWidthImage = bitmapSquareWidthImage+borderWidthHalfImage;

        Bitmap roundedImageBitmap = Bitmap.createBitmap(newBitmapSquareWidthImage,newBitmapSquareWidthImage,Bitmap.Config.ARGB_8888);
        Canvas mcanvas = new Canvas(roundedImageBitmap);
        // mcanvas.drawColor(Color.RED);
        // mcanvas.drawColor(getResources().getColor(R.color.transparent));
        int i = borderWidthHalfImage + bitmapSquareWidthImage - bitmapWidthImage;
        int j = borderWidthHalfImage + bitmapSquareWidthImage - bitmapHeightImage;

        mcanvas.drawBitmap(bitmap, i, j, null);

        Paint borderImagePaint = new Paint();


        borderImagePaint.setStyle(Paint.Style.STROKE);
        borderImagePaint.setAntiAlias(true);
        borderImagePaint.setFilterBitmap(true);
        borderImagePaint.setDither(true);


        // borderImagePaint.setStrokeWidth(borderWidthHalfImage*2);
        borderImagePaint.setStrokeWidth(widthValue);
        //  borderImagePaint.setColor(Color.GRAY);
        borderImagePaint.setColor(context.getResources().getColor(R.color.male));
        mcanvas.drawCircle(mcanvas.getWidth()/2, mcanvas.getWidth()/2, newBitmapSquareWidthImage/2, borderImagePaint);

        RoundedBitmapDrawable roundedImageBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(),roundedImageBitmap);
        roundedImageBitmapDrawable.setCornerRadius(bitmapRadiusImage);
        roundedImageBitmapDrawable.setAntiAlias(true);
        roundedImageBitmapDrawable.setFilterBitmap(true);
        roundedImageBitmapDrawable.setDither(true);
        return roundedImageBitmapDrawable;
    }

}
