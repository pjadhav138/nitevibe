package com.nv.admin.nitevibe;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.widget.VideoView;

import com.nv.admin.nitevibe.Activity.LandingPage;

public class MainActivity extends AppCompatActivity {
    public static VideoView splashScreen;
    //sharedpref
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
       requestWindowFeature(Window.FEATURE_NO_TITLE);
        Log.d("activity","This is main activity");

       setContentView(R.layout.activity_main);
        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        Log.d("shared_pref",loginUserId+" "+loginUserType+" "+loginUserMode);

        if(loginUserId.equals("") && loginUserType.equals("") && loginUserMode.equals("")){
            Log.d("landing_page","empty_shared_pref");
               splashScreen= findViewById(R.id.splash);


        Uri video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.nitevibe);
        splashScreen.setVideoURI(video);

        splashScreen.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                startNextActivity();
            }
        });

        splashScreen.start();
        }
        else{
            Log.d("landing_page","not_empty_pref");
            startActivity(new Intent(this, LandingPage.class));
            //startActivity(new Intent(this, CreateAccount.class));
        }






    }

    private void startNextActivity() {
        if (isFinishing())
            return;
        startActivity(new Intent(this, LandingPage.class));
        //startActivity(new Intent(this, demo.class));
        finish();

    }
}
