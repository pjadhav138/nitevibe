package com.nv.admin.nitevibe.custom;

public enum Quadrant {
    TopLeft, TopRight, BottomLeft, BottomRight
}
