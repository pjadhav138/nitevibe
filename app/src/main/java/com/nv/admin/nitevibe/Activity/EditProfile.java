package com.nv.admin.nitevibe.Activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nv.admin.nitevibe.Adapter.photoAdapter;
import com.nv.admin.nitevibe.R;
import com.nv.admin.nitevibe.RangeSeekBar.CustomRangeBar;
import com.nv.admin.nitevibe.RangeSeekBar.OnRangeSeekbarChangeListener;
import com.nv.admin.nitevibe.custom.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import info.hoang8f.android.segmented.SegmentedGroup;

public class EditProfile extends AppCompatActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener {
    public static ArrayList<String> userPhotoArrayList;
    public static ArrayList<String> userPhotoCount;
    RecyclerView photoRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    public static Button btn_male;
    boolean isPressed = false;
    public static RelativeLayout genderRel,malePrefRel,femalPrefRel;
    ///
    public static EditText ed_name,ed_age,ed_occupation,ed_city,ed_yourself;
    public static TextView txt_age,txt_city,txt_yourself,txt_minage,txt_maxage,txt_male,txt_female,txt_pref_male,txt_pref_female,txt_drink;
    public static CustomSwitchButton tog_age,city_tog,drink_tog;
    public static CustomRangeBar ageSeekBar;
    public static ImageView img_male,img_female,img_male_pref,img_female_pref,back,img_add_drink;
    private int PICK_IMAGE_REQUEST = 1;
    public static String str_userName,str_UserAge,str_userGender,str_userOccupation,str_userCity,str_userYourself,str_minAge,str_maxAge,str_genderPref="";
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold;
    public static TextView mainLabel,photoLabel,nameLabel,ageLabel,ageStatusLabel,genderLabel,occupationLabel,cityLabel,cityStatusLabel,
    yourselfLabel,yourselfLimit,prefLabel,agePrefLabel,genderPref,drinkLabel,drinkStatusLabel,addDrinkLabel;
    android.app.AlertDialog alert;
    public RelativeLayout drinkRel;
    //shared
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    public static String str_drinklist,str_drinklist_id;
    public static String str_name,str_age,str_age_status,str_gender="",str_occupation,str_city,str_city_status,str_bio,str_minage,str_maxage,str_genderpref,str_drinks,str_drink_status,
                          str_male_pref="",str_female_pref="",user_img_url,str_gender_pref="";

    private android.support.v4.app.Fragment fragment;
    private android.support.v4.app.FragmentManager fragmentManager;
    ProgressDialog loading;
    public static SegmentedGroup genderSegment,prefSegment;
    public static RadioButton rb_male,rb_female,rb_male_pref,rb_both,rb_female_pref;
    public static String imageFilePath;//Made it static as need to override the original image with compressed image.
    public static TextView drinkid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);


        sharedpreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");

        imageFilePath = CommonUtils.getFilename();

        mainLabel= findViewById(R.id.txt);
        photoLabel= findViewById(R.id.phototitle);
        nameLabel= findViewById(R.id.txt_name);
        ageLabel= findViewById(R.id.txt_age);
        ageStatusLabel= findViewById(R.id.txt_age_show);
        genderLabel= findViewById(R.id.txt_gender);
        occupationLabel= findViewById(R.id.txt_occupation);
        cityLabel= findViewById(R.id.txt_city);
        cityStatusLabel= findViewById(R.id.txt_city_show);
        yourselfLabel= findViewById(R.id.txt_yourself);
        yourselfLimit= findViewById(R.id.txt_limit);
        prefLabel= findViewById(R.id.txt_preference);
        agePrefLabel= findViewById(R.id.txt_age_pre);
        genderPref= findViewById(R.id.txt_gender_pref);
        drinkLabel= findViewById(R.id.txt_drink);
        drinkStatusLabel= findViewById(R.id.txt_drink_show);
        addDrinkLabel= findViewById(R.id.txt_drink_add);
        drinkid= findViewById(R.id.drinkids);


        genderSegment= findViewById(R.id.segmented2);
        prefSegment= findViewById(R.id.segmented3);


 ///public static RadioButton rb_male,rb_female,rb_male_pref,rb_both,rb_female_pref;
        rb_male= findViewById(R.id.button21);
        rb_female= findViewById(R.id.button22);
        rb_male_pref= findViewById(R.id.buttonmale);
        rb_both= findViewById(R.id.buttonboth);
        rb_female_pref= findViewById(R.id.buttonfemale);


        ////
        drinkRel= findViewById(R.id.drink_main_rel);
        txt_drink= findViewById(R.id.drink);

        ed_name= findViewById(R.id.name);
        ed_age= findViewById(R.id.age);
        ed_occupation= findViewById(R.id.occupation);
        ed_city= findViewById(R.id.city);
        ed_yourself= findViewById(R.id.yourself);

        txt_age= findViewById(R.id.txt_age_show);
        txt_city= findViewById(R.id.txt_city_show);
        txt_yourself= findViewById(R.id.txt_limit);
        txt_minage= findViewById(R.id.minage);
        txt_maxage= findViewById(R.id.maxage);

        txt_male= findViewById(R.id.maletxt);
        txt_female= findViewById(R.id.femaletxt);
        txt_pref_male= findViewById(R.id.maleprefxt);
        txt_pref_female= findViewById(R.id.femalepreftxt);

        tog_age= findViewById(R.id.age_tog);
        city_tog= findViewById(R.id.city_tog);
        drink_tog= findViewById(R.id.drink_tog);




        ageSeekBar= findViewById(R.id.age_range);


       // img_male=(ImageView)findViewById(R.id.male_img);
       // img_female=(ImageView)findViewById(R.id.female_img);
        img_male_pref= findViewById(R.id.malepref_img);
        img_female_pref= findViewById(R.id.femalepref_img);
        back= findViewById(R.id.back);
        img_add_drink= findViewById(R.id.drink_add);


       // maleRel=(RelativeLayout)findViewById(R.id.malerel);
       // femaleRel=(RelativeLayout)findViewById(R.id.femalerel);
        malePrefRel= findViewById(R.id.maleprefrel);
        femalPrefRel= findViewById(R.id.femaleprefrel);


        /*on click event called*/
        onClickEvent();
        //setting font
        setFont();

        tog_age.setOnCheckedChangeListener(new CustomSwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomSwitchButton view, boolean isChecked) {
                Log.d("tog_age_value", String.valueOf(isChecked));
                String value= String.valueOf(isChecked);
                if(value.equals("true")){
                    str_age_status="Show";
                    txt_age.setText(str_age_status);
                }
                else{
                    str_age_status="Hide";
                    txt_age.setText(str_age_status);
                }


            }
        });

        city_tog.setOnCheckedChangeListener(new CustomSwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomSwitchButton view, boolean isChecked) {
                Log.d("tog_city_value", String.valueOf(isChecked));
                String value= String.valueOf(isChecked);
                if(value.equals("true")){
                    str_city_status="Show";
                    txt_city.setText(str_city_status);
                }
                else{
                    str_city_status="Hide";
                    txt_city.setText(str_city_status);
                }

            }
        });

        drink_tog.setOnCheckedChangeListener(new CustomSwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomSwitchButton view, boolean isChecked) {
                Log.d("tog_drink_value", String.valueOf(isChecked));
                String value= String.valueOf(isChecked);
                if(value.equals("true")){
                    str_drink_status="Show";
                    drinkStatusLabel.setText(str_drink_status);
                }
                else{
                    str_drink_status="Hide";
                    drinkStatusLabel.setText(str_drink_status);
                }

            }
        });


        boolean flag=hasConnection();
        if(flag){
            String url=AllUrl.DISPLAY_PROFILE+loginUserId;
          //  UserProfile(url);
            UserProfileVolley(url);
        }
        else{
            Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();

        }



        ed_yourself.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int wordLength=ed_yourself.length();
                String wordLimit= String.valueOf(wordLength);
                txt_yourself.setText(wordLimit+"/500");


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        photoRecyclerView= findViewById(R.id.photorecycler);
        photoRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        photoRecyclerView.setLayoutManager(mLayoutManager);



    }

    public void onClickEvent(){
        back.setOnClickListener(this);
        img_add_drink.setOnClickListener(this);
        ageSeekBar.setOnClickListener(this);

        prefSegment.setOnCheckedChangeListener(this);


        if(loginUserMode.equals("Facebook")){
            ed_name.setFocusable(false);
            rb_male.setEnabled(false);
            rb_female.setEnabled(false);
        }
        else{
            genderSegment.setOnCheckedChangeListener(this);
        }


       /* if(!loginUserMode.equals("Facebook")){
            genderSegment.setOnCheckedChangeListener(this);
        }*/


    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.button21: //male gender
                str_gender="Male";
                break;
            case R.id.button22:  //female gender
                str_gender="Female";
                break;
            case R.id.buttonmale:
                str_gender_pref="Male";
                break;
            case R.id.buttonboth:
                str_gender_pref="Male,Female";
                break;
            case R.id.buttonfemale:
                str_gender_pref="Female";
                break;
            default:
                // Nothing to do
        }
    }




    @Override
    public void onClick(View view) {
        int id=view.getId();

        if(id==R.id.back){
            getAllFieldValue();

        }

        if(id==R.id.drink_add){
            getAllFieldValueDrinks();

        }
        if(id==R.id.age_range){
            setRangeBar();
        }

    }


    private void setRangeBar(){
        ageSeekBar
               /* .setLeftThumbDrawable(R.drawable.range_button)
                .setLeftThumbHighlightDrawable(R.drawable.range_button)
                .setRightThumbDrawable(R.drawable.range_button)
                .setRightThumbHighlightDrawable(R.drawable.range_button)*/
                .setDataType(CustomRangeBar.DataType.INTEGER)
                .apply();

        // set listener
        ageSeekBar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                String p1= String.valueOf(minValue);
                int p2= Integer.parseInt(p1);

                String m1= String.valueOf(maxValue);
                int m2= Integer.parseInt(m1);

                Log.d("min_value",p1);
                Log.d("max_value",m1);

                //int xPos=((p2+m2));
                int xPos=((p2+m2)/2);

                Log.d("position", String.valueOf(xPos));

                int x2=xPos;
                Log.d("positionX2", String.valueOf(x2));
                //tvMin.setPadding(x2, 0, 0, 0);
               // txt_minage.setX(x2);


                txt_minage.setText(String.valueOf(minValue));
                //tvMin.setX(Float.parseFloat(rangeSeekbar.getX() + val));

                txt_maxage.setText(String.valueOf(maxValue));


            }
        });



    }


    public class ImageCompression extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            if (strings.length == 0 || strings[0] == null)
                return null;

            return CommonUtils.compressImage(strings[0]);
        }

        protected void onPostExecute(String imagePath) {
            // imagePath is path of new compressed image.
          //  mivImage.setImageBitmap(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()));
            Log.d("the_selectedimg",imagePath);
            String abc=convertToBase64(imagePath);
            Log.d("img_base64",abc);

        }
    }

    private String convertToBase64(String imagePath)

    {

        Bitmap bm = BitmapFactory.decodeFile(imagePath);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);


        byte[] byteArrayImage = baos.toByteArray();

        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

        boolean flag=hasConnection();
        if(flag){
            //  addSinglePhoto(loginUserId,selected_img);
            addSinglePhotoVolley(loginUserId,encodedImage);
        }

        return encodedImage;

    }



    private void UserProfileVolley(String url){
        loading= new ProgressDialog(EditProfile.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("displayprofileresponse", response.toString());
               // loading.dismiss();
                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject object = new JSONObject(response.toString());
                    String res = object.getString("response");
                    String msg=object.getString("message");
                    if(res.equals("200")){
                        JSONArray array = object.getJSONArray("message");

                        if(array.length()==0){


                        }
                        else{
                            for (int i=0;i<array.length();i++){
                                JSONObject object1 = array.getJSONObject(i);


                                String userName=object1.getString("name");
                                String userGender=object1.getString("gender");
                                String userAge=object1.getString("age");
                                String userAgeStatus=object1.getString("age_status");
                                String userLoc=object1.getString("loc");
                                String userLocStatus=object1.getString("loc_status");
                                String userOccupation=object1.getString("occupation");
                                String userPrefAge=object1.getString("pref_age");
                                String userPrefGender=object1.getString("pref_gender");
                                String userBio=object1.getString("bio");
                                String userDrink=object1.getString("drink");
                                String userDrinkStatus=object1.getString("drink_status");
                                String userProfilePic=object1.getString("profile");


                                if(userProfilePic.equals("")){
                                    user_img_url="null";
                                }
                                else{
                                    user_img_url=userProfilePic;
                                }

                                SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                                editor.remove("profile");
                                editor.commit();
                                editor.putString("profile", user_img_url);
                                editor.commit();

                                if(!userName.equals("")){
                                    ed_name.setText(userName);
                                }

                                if(!userGender.equals("")){
                                    if(userGender.equals("Male")){

                                        str_gender="Male";

                                        rb_male.setChecked(true);

                                    }
                                    else if(userGender.equals("Female")){

                                        str_gender="Female";
                                        rb_female.setChecked(true);
                                    }

                                }
                                //age
                                if(!userAge.equals("")){
                                    ed_age.setText(userAge);
                                }
                                //age status
                                if(!userAgeStatus.equals("")){
                                    ageStatusLabel.setText(userAgeStatus);

                                    if(userAgeStatus.equals("Show")){

                                        tog_age.setChecked(true);

                                    }
                                    else if(userAgeStatus.equals("Hide")){
                                        tog_age.setChecked(false);
                                    }
                                }
                                else{
                                    ageStatusLabel.setText("Hide");
                                    tog_age.setChecked(false);
                                }
                                //city
                                if(!userLoc.equals("")){
                                    ed_city.setText(userLoc);
                                }

                                //city status
                                if(!userLocStatus.equals("")){
                                    cityStatusLabel.setText(userLocStatus);

                                    if(userLocStatus.equals("Show")){
                                        city_tog.setChecked(true);

                                    }
                                    else if(userLocStatus.equals("Hide")){
                                        city_tog.setChecked(false);

                                    }
                                }
                                else{
                                    cityStatusLabel.setText("Hide");
                                    city_tog.setChecked(false);
                                }

                                //occupation
                                if(!userOccupation.equals("")){
                                    ed_occupation.setText(userOccupation);
                                }
                                //user age pref
                                if(!userPrefAge.equals("")){
                                    setRangeBar();

                                    String[] userarray = userPrefAge.split("-");
                                    String start_age=userarray[0];
                                    String end_age=userarray[1];

                                    txt_minage.setText(start_age);
                                    float min= Float.parseFloat(start_age);
                                    float max= Float.parseFloat(end_age);

                                    txt_maxage.setText(end_age);
                                    ageSeekBar
                                            /*.setLeftThumbDrawable(R.drawable.range_button)
                                            .setLeftThumbHighlightDrawable(R.drawable.range_button)
                                            .setRightThumbDrawable(R.drawable.range_button)
                                            .setRightThumbHighlightDrawable(R.drawable.range_button)*/
                                            .setDataType(CustomRangeBar.DataType.INTEGER)
                                            .setMaxStartValue(max)
                                            .setMinStartValue(min)


                                            .apply();
                                       // .setGap(10)



                                    //setRangeBar();

                                }
                                else{
                                    //  Actions over custom range seek bar
                                    setRangeBar();

                                }
                                //user gender prf
                                //public static RadioButton rb_male,rb_female,rb_male_pref,rb_both,rb_female_pref;
                                if(!userPrefGender.equals("")){
                                    if(userPrefGender.equals("Male")){

                                        str_male_pref="Male";
                                        rb_male_pref.setChecked(true);

                                    }
                                    else if(userPrefGender.equals("Female")){

                                        str_female_pref="Female";
                                        rb_female_pref.setChecked(true);
                                    }
                                    else {
                                        str_male_pref="Male";
                                        str_female_pref="Female";
                                        rb_both.setChecked(true);
                                    }

                                }
                                //bio
                                if(!userBio.equals("")){
                                    ed_yourself.setText(userBio);
                                }
                                //drinks
                                if(!userDrink.equals("")){
                                    drinkRel.setVisibility(View.VISIBLE);

                                    txt_drink.setText(userDrink);

                                }
                                else{
                                    drinkRel.setVisibility(View.GONE);
                                }
                                //drink status
                                if(!userDrinkStatus.equals("")){

                                    drinkStatusLabel.setText(userDrinkStatus);
                                    if(userDrinkStatus.equals("Show")){
                                        drink_tog.setChecked(true);

                                    }
                                    else if(userDrinkStatus.equals("Hide")){
                                        drink_tog.setChecked(false);

                                    }

                                }
                                else{
                                    drinkStatusLabel.setText("Hide");
                                    drink_tog.setChecked(false);

                                }


                                //values from drinks class (list of drink selected)
                                Bundle extra=getIntent().getExtras();
                                if(extra!=null){
                                    //values coming from Drinks
                                    str_drinklist=extra.getString("drink_name");
                                    Log.d("selected_drink_name",str_drinklist);
                                    str_drinklist_id=extra.getString("drink_id");
                                    Log.d("selected_drink_list",str_drinklist);
                                    Log.d("selected_drink_id",str_drinklist_id);

                                    Log.d("length", String.valueOf(str_drinklist.length()));

                                    if(str_drinklist.length()==0){
                                         Log.d("drink_list","empty_drink_list");
                                    }
                                    else if(str_drinklist.length()>0){
                                        drinkRel.setVisibility(View.VISIBLE);
                                        txt_drink.setText(str_drinklist);
                                        drinkid.setText(str_drinklist_id);

                                    }

                                }
                                else{
                                    //drinkRel.setVisibility(View.GONE);
                                }






                                JSONArray photoArray=object1.getJSONArray("photos");

                                if(photoArray.length()==0){
                                    Log.d("photos_array","empty photo array");
                                    userPhotoArrayList=new ArrayList<>();
                                    userPhotoCount=new ArrayList<>();
                                    userPhotoCount.add("1");
                                    userPhotoCount.add("2");
                                    userPhotoCount.add("3");
                                    userPhotoCount.add("4");
                                    userPhotoCount.add("5");

                                    userPhotoArrayList.add("");
                                    userPhotoArrayList.add("");
                                    userPhotoArrayList.add("");
                                    userPhotoArrayList.add("");
                                    userPhotoArrayList.add("");

                                    mAdapter=new photoAdapter(EditProfile.this,userPhotoArrayList,userPhotoCount);
                                    photoRecyclerView.setAdapter(mAdapter);

                                }
                                else{
                                    userPhotoArrayList=new ArrayList<>();
                                    userPhotoCount=new ArrayList<>();
                                    int j=0;
                                    for(j=0;j<photoArray.length();j++){
                                        // for(int j=0;j<=4;j++){
                                        //    while(j<=4) {
                                        JSONObject object2 = photoArray.getJSONObject(j);

                                        String pic = object2.getString("img");
                                        String count = String.valueOf(j+1);
                                        userPhotoCount.add(count);

                                        userPhotoArrayList.add(pic);
                                        //  }j++;

                                    }
                                    Log.d("value_of_j", String.valueOf(j));

                                    int len=5-photoArray.length();

                                    Log.d("remaining_len", String.valueOf(len));

                                    if(len!=0){
                                        for(int k=1;k<=len;k++){
                                            String val= String.valueOf(j+k);


                                            userPhotoCount.add(val);
                                            userPhotoArrayList.add("");


                                        }
                                    }

                                    mAdapter=new photoAdapter(EditProfile.this,userPhotoArrayList,userPhotoCount);
                                    photoRecyclerView.setAdapter(mAdapter);
                                }



                                loading.dismiss();
                            } //end of main for
                        }



                    }

                   /* else{
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                    }*/
                   // }
                else{
                        loading.dismiss();
                        Toast.makeText(EditProfile.this,object.getString("message"),Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("main", "Error: " + error.getMessage());

                // hide the progress dialog
                loading.dismiss();

            }
        });
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        jsonObjReq.setShouldCache(false);
        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();
            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    @Override
    public void onBackPressed() {
        getAllFieldValue();
       // super.onBackPressed();

    }


    public void getAllFieldValue(){

        Log.d("gender_value",str_gender);
        str_userName=ed_name.getText().toString();
        str_UserAge=ed_age.getText().toString();
        str_age_status=txt_age.getText().toString();
        str_userGender=str_gender;
        str_userOccupation=ed_occupation.getText().toString();
        str_userCity=ed_city.getText().toString();
        str_city_status=txt_city.getText().toString();
        str_userYourself=ed_yourself.getText().toString();
        str_minAge=txt_minage.getText().toString();
        str_maxAge=txt_maxage.getText().toString();

        Log.d("range_value",str_minAge+" "+str_maxAge);
        //str_genderPref =abc;
        str_drinks=txt_drink.getText().toString();
        str_drink_status=drinkStatusLabel.getText().toString();

        String abc1;
        if(userPhotoArrayList.isEmpty()){
             abc1="";
        }
        else{
             abc1= TextUtils.join(",",userPhotoArrayList);
        }


        ArrayList<String> photoarray11=new ArrayList<>();
        StringBuilder sb = new StringBuilder();

        if(str_userGender.equals(null) || str_userGender.equals("")){
            str_userGender="";
        }
        else{
            str_userGender=str_userGender;
        }

        if(str_genderPref.equals(null) || str_genderPref.equals("")){
            str_genderPref="";
        }




        Log.d("user_update_data",str_userName+" :"+str_UserAge+": "+str_age_status+": "+str_userGender+" :"+str_userOccupation+": "+
                str_userCity+": "+str_city_status+": "+str_userYourself+": "+str_minAge+": "+str_maxAge+": "+str_genderPref+": "+
                str_drinks+": "+ str_drink_status+": "+abc1+ " ::");
        Log.d("userName",str_userName);
        Log.d("userAge",str_UserAge);


        String agepref=str_minAge+"-"+str_maxAge;
        //str_gender_pref
        str_genderPref=str_gender_pref;


        if(!str_UserAge.equals("")){
            if(!str_userGender.equals("")){
                boolean flag=hasConnection();
                if(flag){

                    //  postUpdateProfile(loginUserId,str_userName,str_UserAge,str_age_status,str_userGender,str_userOccupation,str_userCity,str_city_status,str_userYourself,agepref,str_genderPref,str_drinks,str_drink_status);
                    postUpdateProfileVolley(loginUserId,str_userName,str_UserAge,str_age_status,str_userGender,str_userOccupation,str_userCity,str_city_status,str_userYourself,agepref,str_genderPref,str_drinks,str_drink_status);
                }
                else{
                    Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                }

            }
            else{
                Toast.makeText(getApplicationContext(),"Select your gender to proceed",Toast.LENGTH_SHORT).show();
            }

        }
        else{
            Toast.makeText(getApplicationContext(),"Fill your age to proceed",Toast.LENGTH_SHORT).show();
        }











    }

    public void getAllFieldValueDrinks(){
        String abc=null;
        if(str_male_pref.equals("") && str_female_pref.equals("")){
            abc="not_selected";
        }
        else if(!str_male_pref.equals("") && !str_female_pref.equals("")){
            abc="Male,Female";
        }
        else if(str_male_pref.equals("") && !str_female_pref.equals("")){
            abc="Female";
        }
        else if(!str_male_pref.equals("") && str_female_pref.equals("")){
            abc="Male";
        }
        //str_gender_pref



        str_userName=ed_name.getText().toString();
        str_UserAge=ed_age.getText().toString();
        str_age_status=txt_age.getText().toString();
        str_userGender=str_gender;
        str_userOccupation=ed_occupation.getText().toString();
        str_userCity=ed_city.getText().toString();
        str_city_status=txt_city.getText().toString();
        str_userYourself=ed_yourself.getText().toString();
        str_minAge=txt_minage.getText().toString();
        str_maxAge=txt_maxage.getText().toString();
        //str_genderPref =abc;
        str_drinks=txt_drink.getText().toString();
        str_drink_status=drinkStatusLabel.getText().toString();

        String abc1= TextUtils.join(",",userPhotoArrayList);
        ArrayList<String> photoarray11=new ArrayList<>();
        StringBuilder sb = new StringBuilder();


        Log.d("user_update_data",str_userName+" "+str_UserAge+" "+str_age_status+" "+str_userGender+" "+str_userOccupation+" "+
                str_userCity+" "+str_city_status+" "+str_userYourself+" "+str_minAge+" "+str_maxAge+" "+str_genderPref+" "+
                str_drinks+" "+ str_drink_status+" "+abc1+ " ");

        String agepref=str_minAge+"-"+str_maxAge;
        //str_gender_pref
        str_genderPref=str_gender_pref;




        boolean flag=hasConnection();
        if(flag){

            //  postUpdateProfile(loginUserId,str_userName,str_UserAge,str_age_status,str_userGender,str_userOccupation,str_userCity,str_city_status,str_userYourself,agepref,str_genderPref,str_drinks,str_drink_status);
            drinkUpdateProfileVolley(loginUserId,str_userName,str_UserAge,str_age_status,str_userGender,str_userOccupation,str_userCity,str_city_status,str_userYourself,agepref,str_genderPref,str_drinks,str_drink_status);
        }
        else{
            Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
        }


        //    super.onBackPressed();






    }


    //post method
    private void addSinglePhotoVolley(final String user_id,final String photo){
        StringRequest postRequest = new StringRequest(Request.Method.POST,AllUrl.ADD_PHOTO,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("add_photo_response", response);
                        loading.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String res = jsonObject.getString("response");
                            String message = jsonObject.getString("message");
                            if (res.equals("200")) {
                                boolean flag=hasConnection();
                                if(flag){
                                    String url=AllUrl.DISPLAY_PROFILE+loginUserId;
                                    UserProfileVolley(url);
                                }
                                else{
                                    Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();

                                }
                            }
                            else{
                                Toast.makeText(getApplicationContext(),"Failed to add photo.Please try again",Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        loading.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {

                Map<String, String>  data = new HashMap<>();
                // the POST parameters:


                data.put("user_id",user_id);
                data.put("photo",photo);

                return data;
            }

        };

        Volley.newRequestQueue(this).getCache().clear();

        Volley.newRequestQueue(this).add(postRequest);
        loading= new ProgressDialog(EditProfile.this,R.style.MyAlertDialogStyle);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();

    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public void setFont(){
        bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(getAssets(),"fonts/Calibri Bold.ttf");





        mainLabel.setTypeface(bold_face);
        photoLabel.setTypeface(calibri_bold);
        nameLabel.setTypeface(calibri_bold);
        ageLabel.setTypeface(calibri_bold);
        ageStatusLabel.setTypeface(calibri_bold);
        genderLabel.setTypeface(calibri_bold);
        occupationLabel.setTypeface(calibri_bold);
        cityLabel.setTypeface(calibri_bold);
        cityStatusLabel.setTypeface(calibri_bold);
        yourselfLabel.setTypeface(calibri_bold);
        yourselfLimit.setTypeface(calibri_bold);
        prefLabel.setTypeface(bold_face);
        agePrefLabel.setTypeface(calibri_bold);
        genderPref.setTypeface(calibri_bold);
        drinkLabel.setTypeface(calibri_bold);
        drinkStatusLabel.setTypeface(calibri_bold);
        addDrinkLabel.setTypeface(calibri_bold);

        ed_name.setTypeface(semi_bold_face);
        ed_age.setTypeface(semi_bold_face);
        ed_occupation.setTypeface(semi_bold_face);
        ed_city.setTypeface(semi_bold_face);
        ed_yourself.setTypeface(semi_bold_face);


        txt_minage.setTypeface(calibri_bold);
        txt_maxage.setTypeface(calibri_bold);
        txt_male.setTypeface(semi_bold_face);
        txt_female.setTypeface(semi_bold_face);
        txt_pref_male.setTypeface(semi_bold_face);
        txt_pref_female.setTypeface(semi_bold_face);

        txt_drink.setTypeface(semi_bold_face);





    }

    //post method
    private void postUpdateProfileVolley(final String user_id, final String name,final String userage, final String agestatus,
final String gender,final String occupation,final String city,final String citystatus,final String about,final String agepref,
 final String genderpref,final String drinkpref,final String drinkstatus){
        StringRequest postRequest = new StringRequest(Request.Method.POST,AllUrl.UPDATE_PROFILE,
                        new Response.Listener<String>() {

            @Override
                    public void onResponse(String response) {
                        Log.d("update_profile_response", response);
                        loading.dismiss();
                        try {
                            JSONObject object = new JSONObject(response);
                            String res = object.getString("response");
                            String message = object.getString("message");

                            if (res.equals("200")) {
                                boolean flag=hasConnection();

                               // startActivity(new Intent(EditProfile.this,Club.class));
                                startActivity(new Intent(EditProfile.this,UserProfile.class));
                                MyApplication.getInstance().trackEvent("User Profile", "Update", "User Profile Updated");

                        }
                            else{
                                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        loading.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Log.d("post_volley","entered in getParams");
                Map<String, String>  data = new HashMap<>();
                // the POST parameters:

                data.put("user_id",user_id);
                data.put("name",name);
                data.put("age",userage);
                data.put("age_status",agestatus);
                data.put("gender",gender);
                data.put("occupation",occupation);
                data.put("city",city);
                data.put("city_status",citystatus);
                data.put("about",about);
                data.put("age_pref",agepref);
                data.put("gender_pref",genderpref);
                data.put("drink_pref",drinkpref);
                data.put("drink_status",drinkstatus);

                return data;
            }

        };

      Volley.newRequestQueue(this).getCache().clear();
        Volley.newRequestQueue(getApplicationContext()).addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                MyApplication.getInstance().getRequestQueue().getCache().clear();
            }
        });
        Volley.newRequestQueue(this).add(postRequest);
        loading= new ProgressDialog(EditProfile.this,R.style.MyAlertDialogStyle);
        // loading= new ProgressDialog(EditProfile.this);
                            loading.setIndeterminate(true);
                            loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
                            loading.setCancelable(false);
                            loading.setMessage("Please wait...!");
                            loading.show();


    }


    //post method
    private void drinkUpdateProfileVolley(final String user_id, final String name,final String userage, final String agestatus,
                                         final String gender,final String occupation,final String city,final String citystatus,final String about,final String agepref,
                                         final String genderpref,final String drinkpref,final String drinkstatus){
        StringRequest postRequest = new StringRequest(Request.Method.POST,AllUrl.UPDATE_PROFILE,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("update_profile_response", response);
                        loading.dismiss();
                        try {
                            JSONObject object = new JSONObject(response);
                            String res = object.getString("response");
                            String message = object.getString("message");

                            if (res.equals("200")) {

                                boolean flag=hasConnection();
                                Log.d("str_drink_id",txt_drink.getText().toString());
                                Log.d("str_drink_l",drinkid.getText().toString());
                                Intent intent=new Intent(EditProfile.this,Drinks.class);
                                intent.putExtra("drinknames",txt_drink.getText().toString());
                                //    intent.putExtra("drinkids",str_drinklist_id);
                                startActivity(intent);

                            }
                            else{
                                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        loading.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Log.d("post_volley","entered in getParams");
                Map<String, String>  data = new HashMap<>();
                // the POST parameters:

                data.put("user_id",user_id);
                data.put("name",name);
                data.put("age",userage);
                data.put("age_status",agestatus);
                data.put("gender",gender);
                data.put("occupation",occupation);
                data.put("city",city);
                data.put("city_status",citystatus);
                data.put("about",about);
                data.put("age_pref",agepref);
                data.put("gender_pref",genderpref);
                data.put("drink_pref",drinkpref);
                data.put("drink_status",drinkstatus);

                return data;
            }

        };

        Volley.newRequestQueue(this).getCache().clear();

        Volley.newRequestQueue(this).add(postRequest);
        loading= new ProgressDialog(EditProfile.this);
        loading.setIndeterminate(true);
        loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_animation));
        loading.setCancelable(false);
        loading.setMessage("Please wait...!");
        loading.show();


    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    ///displaying user information
    private void UserProfile11(String url) {
        class UserProfileClass11 extends AsyncTask<String,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading= new ProgressDialog(EditProfile.this);
                loading.setIndeterminate(true);
                //loading.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animatiion));
                loading.setCancelable(false);
                loading.setMessage("Please wait...!");
                loading.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String uri = params[0];

                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while((json = bufferedReader.readLine())!= null){
                        sb.append(json+"\n");
                    }

                    return sb.toString().trim();

                }catch(Exception e){
                    return null;
                }

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                try {
                    Log.d("display_profile_url_11",s);

                    JSONObject object=new JSONObject(s);
                    String res=object.getString("response");
                    String msg=object.getString("message");

                    if(res.equals("200")) {
                        JSONArray array = object.getJSONArray("message");

                        if(array.length()==0){


                        }
                        else{



                        }



                    }

                    else{
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                    }
                }catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }
        UserProfileClass11 display = new UserProfileClass11();
        display.execute(url);
    }

    public static String encodeFromString(Bitmap bm){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();

        return Base64.encodeToString(b, Base64.DEFAULT);
    }


}

