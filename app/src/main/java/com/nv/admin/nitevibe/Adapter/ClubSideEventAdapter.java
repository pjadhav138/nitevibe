package com.nv.admin.nitevibe.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nv.admin.nitevibe.R;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Admin on 4/26/2018.
 */
public class ClubSideEventAdapter extends RecyclerView.Adapter<ClubSideEventAdapter.MyView> {
    private List<ClubEventModel> arrayList;
    private Context context;
    ClubEventModel current;
    private boolean isChecked;
    public String favClubUrl;
    SharedPreferences sharedpreferences;
    public static final String PREFS_NAME = "LoginPrefs";
    public static String loginUserId,loginUserMode,loginUserType;
    public static Typeface bold_face,extra_bold_face,reg_face,semi_bold_face,calibri_bold,lato_bold,semi_bold_italic;


    public ClubSideEventAdapter(List<ClubEventModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public ClubSideEventAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.club_side_event_row, parent, false);
        MyView viewHolder = new MyView(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ClubSideEventAdapter.MyView holder, int position) {
        sharedpreferences=context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        loginUserId=sharedpreferences.getString("user_id","");
        loginUserType=sharedpreferences.getString("user_type","");
        loginUserMode=sharedpreferences.getString("mode","");
        current=arrayList.get(position);
        bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Bold.ttf");
        extra_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-ExtraBold.ttf");
        reg_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Regular.ttf");
        semi_bold_face=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBold.ttf");
        calibri_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Calibri Bold.ttf");
        lato_bold=Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Bold.ttf");
        semi_bold_italic=Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-SemiBoldItalic.ttf");

        holder.date.setTypeface(extra_bold_face);
        holder.month.setTypeface(semi_bold_face);
        holder.name.setTypeface(extra_bold_face);
        holder.rate.setTypeface(semi_bold_face);
        holder.total.setTypeface(semi_bold_face);

        String event_date=current.eventDate;
        String[] abc=event_date.split(" ");
        String date=abc[0];
        String mnth=abc[1];

        holder.date.setText(date);
        holder.month.setText(mnth);
        String event_id=current.eventId;
        holder.name.setText(current.eventName);
        String user_going=current.userGoing;
        int going= Integer.parseInt(user_going);

        holder.event_id.setText(current.eventId);

        if(going>10){
            holder.total.setVisibility(View.VISIBLE);
            holder.total.setText(user_going+ "People are going");
            holder.progressRel.setVisibility(View.VISIBLE);
            holder.progressBar.setMax(Integer.parseInt(current.male));
            holder.progressBar.setSecondaryProgress(Integer.parseInt(current.female));


        }
        else{
            holder.total.setVisibility(View.GONE);
            holder.progressRel.setVisibility(View.GONE);
        }


        if(!current.eventImg.equals("")){
            Picasso.with(context.getApplicationContext())
                    .load(current.eventImg)
                    .into(holder.main_img);
        }


        String fav=current.fav;


    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class MyView extends RecyclerView.ViewHolder {
        //public ImageView main_img;
        public RoundedImageView main_img;
        public TextView date,month,name,rate,total,event_id;
        public RelativeLayout progressRel;
        public ProgressBar progressBar;
        public MyView(View itemView) {
            super(itemView);
           /* main_img=(ImageView)itemView.findViewById(R.id.club_img);*/
            main_img= itemView.findViewById(R.id.club_img);

            date= itemView.findViewById(R.id.date);
            month= itemView.findViewById(R.id.month);
            name= itemView.findViewById(R.id.club_name);
            rate= itemView.findViewById(R.id.club_rate);
            total= itemView.findViewById(R.id.club_total);
            progressRel= itemView.findViewById(R.id.progress_rel);
            progressBar= itemView.findViewById(R.id.progressBar);
            event_id= itemView.findViewById(R.id.event_id);


        }
    }
}
