package com.nv.admin.nitevibe.Activity;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nv.admin.nitevibe.Adapter.ChatModel;
import com.nv.admin.nitevibe.Adapter.RecyclerChatAdapter;
import com.nv.admin.nitevibe.Database.AppDatabse;
import com.nv.admin.nitevibe.Database.ChatTable;
import com.nv.admin.nitevibe.R;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.nv.admin.nitevibe.Chatting2.getBitmapFromURL;

public class Chatting extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    ImageView back, more, send, nv_icon, emoji_icon;
    CircleImageView user_img;
    TextView txt_name;
    RecyclerView messageRecycler;
    private Socket mSocket;
    AppDatabse db;
    public GridView sticker_gridview;
    Integer[] imageIDs = {
            R.drawable.stickers_01, R.drawable.stickers_02

    };
    private Boolean isConnected = true;
    SharedPreferences sharedpreferences;
    String loginUserId, loginUserMode, loginUserType;

    public static final String PREFS_NAME = "LoginPrefs";
    EmojIconActions emojIcon;
    EmojiconEditText text_keyboard;
    RecyclerChatAdapter adapter;
    private String TAG = getClass().getSimpleName();
    private List<ChatModel> chatMessages = new ArrayList<>();
    String str_select_userid, str_select_username, str_select_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        sharedpreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        loginUserId = sharedpreferences.getString("user_id", "");
        loginUserType = sharedpreferences.getString("user_type", "");
        loginUserMode = sharedpreferences.getString("mode", "");
        db = Room.databaseBuilder(Chatting.this, AppDatabse.class, "UserChats").allowMainThreadQueries().build();
        back = findViewById(R.id.back);
        more = findViewById(R.id.more);
        send = findViewById(R.id.send);
        nv_icon = findViewById(R.id.nv_icon);
        emoji_icon = findViewById(R.id.emoji_icon);
        txt_name = findViewById(R.id.txt_name);
        user_img = findViewById(R.id.user_img);
        messageRecycler = findViewById(R.id.messageRecycler);
        sticker_gridview = findViewById(R.id.stickers_gridview);
        chatMessages = getArrayModel();
        adapter = new RecyclerChatAdapter(Chatting.this, chatMessages, str_select_userid);
        messageRecycler.setLayoutManager(new LinearLayoutManager(Chatting.this));
        messageRecycler.setAdapter(adapter);
        emoji_icon.setOnClickListener(this);
        nv_icon.setOnClickListener(this);
        more.setOnClickListener(this);
        back.setOnClickListener(this);
        send.setOnClickListener(this);
        user_img.setOnClickListener(this);

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            //values from ChatListAdapter
            str_select_userid = extra.getString("user_id");
            str_select_username = extra.getString("user_name");
            str_select_profile = extra.getString("select_user_profile");
            Log.d("chatting_val", str_select_userid + " " + str_select_username);

            txt_name.setText(str_select_username);

            if (str_select_profile.equals("")) {
                user_img.setImageResource(R.drawable.default_icon);
            } else {
                Bitmap icon = getBitmapFromURL(str_select_profile);
                user_img.setImageBitmap(icon);
            }


            SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
            editor.remove("chatuser");
            editor.commit();
            editor.putString("chatuser", str_select_userid);
            editor.commit();
//        text_keyboard.addTextChangedListener(new MyTextWatcher(text_keyboard));
            MyApplication app = (MyApplication) getApplication();
            mSocket = app.getSocket();
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            adapter = new RecyclerChatAdapter(Chatting.this, chatMessages, str_select_userid);
            adapter.notifyDataSetChanged();
            sticker_gridview.setAdapter(new StickersGridView(Chatting.this));
            sticker_gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                }
            });
        }
    }


    @Override
    public void onRefresh() {

    }

    @Override
    public void onClick(View view) {

    }

    private class StickersGridView extends BaseAdapter {
        private Context mContext;

        public StickersGridView(Context c) {
            mContext = c;
        }

        public int getCount() {
            return imageIDs.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView mImageView;

            if (convertView == null) {
                mImageView = new ImageView(mContext);
                mImageView.setLayoutParams(new GridView.LayoutParams(250, 250));
                mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                mImageView.setPadding(20, 20, 20, 20);
            } else {
                mImageView = (ImageView) convertView;
            }
            Glide.with(mContext)
                    .load(imageIDs[position]).
                    /* asBitmap().*/
                            override(230, 230).into(mImageView);
            return mImageView;
        }

    }

    private class MyTextWatcher implements TextWatcher {
        public MyTextWatcher(EmojiconEditText text_keyboard) {

        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("new message", onNewMessage);
        mSocket.on("user joined", onUserJoined);
        mSocket.on("user left", onUserLeft);
        mSocket.on("delivery", onDelivery);
        mSocket.on("msg_read", onReadMessage);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("new message", onNewMessage);
        mSocket.off("user joined", onUserJoined);
        mSocket.off("user left", onUserLeft);
        mSocket.off("delivery", onDelivery);
        mSocket.off("msg_read", onReadMessage);
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isConnected) {
                        if (null != loginUserId) {
                            mSocket.emit("add user", loginUserId);
                        }
                        isConnected = true;
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "diconnected");
                    isConnected = false;
                    mSocket.disconnect();
//                    Toast.makeText(getActivity().getApplicationContext(), R.string.disconnect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");
//                    Toast.makeText(getActivity().getApplicationContext(), R.string.error_connect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "run: onNewMessage" + data);

                    try {
                        ChatModel model = new ChatModel();
                        model.setId(data.getString("id"));
                        model.setSender(data.getString("from"));
                        model.setMessage(data.getString("text"));
                        model.setReceiver(loginUserId);
                        model.setSent("sent");
                        model.setImage(data.getString("sticker"));
                        model.setUserimage("");
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String strdate = format.format(Long.parseLong(data.getString("date")));
                        model.setDate(strdate);
                        mSocket.emit("user_msg_read", data);
                        Date d = new Date(data.getLong("date"));
                        Calendar c = Calendar.getInstance();
                        c.setTime(d);
                        chatMessages.add(model);
                        adapter.notifyDataSetChanged();
                        if (db.taksInterface().isExist(data.getString("id")).size() < 1)
                            db.taksInterface().insertAll(new ChatTable(data.getString("id"), data.getString("from"), loginUserId, data.getString("text"), String.valueOf(c.getTimeInMillis()), "person", "received", data.getString("sticker")));

                        Log.e(TAG, "run: size - " + chatMessages.size());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "run: onUserJoined" + data);
//
                }
            });
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "run: onUserLeft" + data);

//                    addLog(getResources().getString(R.string.message_user_left, username));
//                    addParticipantsLog(numUsers);
//                    removeTyping(sessionManager.getUserDetails().get(sessionManager.KEY_EmployeeCode));
                }
            });
        }
    };

    private Emitter.Listener onDelivery = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "onDelivery" + data);


                }
            });
        }
    };

    private Emitter.Listener onReadMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    JSONObject data = (JSONObject) args[0];
                    JSONObject data = (JSONObject) args[0];
                    Log.e(TAG, "run: onReadMessage" + data);
                    /*adapter = new CustomChatAdapter(Chatting.this, chatMessages);
                    messageList.setAdapter(adapter);
                    messageList.setSelection(messageList.getCount());*/
                }
            });
        }
    };


    private Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            Log.e(TAG, "call: onLogin" + data);
        }
    };



    private List<ChatModel> getArrayModel() {
        List<ChatTable> array = db.taksInterface().getChatForUser(sharedpreferences.getString("user_id",""), str_select_userid);

        List<ChatModel> chatMessages = new ArrayList<>();

        //else part
        for (ChatTable chatTable : array) {


            ChatModel model = new ChatModel();

            model.setId(chatTable.getId());
            model.setSender(chatTable.getFrom());
            model.setMessage(chatTable.getText());

            model.setReceiver(chatTable.getTo());
            model.setSent(chatTable.getDelivery_status());
            model.setImage(chatTable.getImage());
//                model.setUserimage(chatTable.getId());
            model.setChat_id(chatTable.getId());
            model.setDate(chatTable.getDate());
            chatMessages.add(model);
        }

        return chatMessages;
    }

    private void scrollDown() {
        messageRecycler.scrollToPosition(chatMessages.size() - 1);
    }

}
